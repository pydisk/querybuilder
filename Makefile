format:
	black querybuilder/

lint:
	black querybuilder/ --check --verbose --diff --color
