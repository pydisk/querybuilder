import querybuilder as qb
from querybuilder.atoms.columns import (
    TableColumn,
    Placeholder,
    Value,
    Constant,
    One,
    Zero,
    Null,
    True_,
    False_,
)
from querybuilder.atoms.relations import Table, View
from querybuilder.atoms.constraints import *
from querybuilder.atoms.schemas import Schema, DB

# nodes
ndid = TableColumn(
    int,
    "id",
    schema_name="public",
    relation_name="node_data_ids",
    constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()),
)
node_data_ids = Table("node_data_ids", schema_name="public", columns=(ndid,))

ndidr = TableColumn(
    int,
    "data_set",
    schema_name="public",
    relation_name="node_data",
    constraints=(ColumnReferences(ndid), ColumnNotNull()),
)
ndkey = TableColumn(
    str,
    "key",
    schema_name="public",
    relation_name="node_data",
    constraints=(ColumnNotNull(),),
)
ndval = TableColumn(
    str,
    "value",
    schema_name="public",
    relation_name="node_data",
    constraints=(ColumnNotNull(),),
)
node_data = Table(
    "node_data",
    schema_name="public",
    columns=(ndidr, ndkey, ndval),
    constraints=(TableUnique(ndidr.tuple_with(ndkey)),),
)

nid = ndid.buildfrom(ndid, schema_name="public", relation_name="nodes")
nname = TableColumn(
    str,
    "name",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnUnique(), ColumnNotNull()),
)
ndata = TableColumn(
    int,
    "data",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnReferences(ndid),),
)
nodes = Table("nodes", schema_name="public", columns=(nid, nname, ndata))

nodestore = (
    nodes.left_join(node_data_ids, on=nid.eq(ndid))
    .left_join(node_data, on=ndid.eq(ndidr))
    .select((nname, ndkey, ndval))
    .alias("nodestore")
)

# edges
edid = ndid.buildfrom(ndid, schema_name="public", relation_name="edge_data_ids")
edge_data_ids = Table("edge_data_ids", schema_name="public", columns=(edid,))

edidr = TableColumn(
    int,
    "data_set",
    schema_name="public",
    relation_name="edge_data",
    constraints=(ColumnReferences(edid), ColumnNotNull()),
)
edkey = ndkey.buildfrom(ndkey, schema_name="public", relation_name="edge_data")
edval = ndval.buildfrom(ndval, schema_name="public", relation_name="edge_data")
edge_data = Table(
    "edge_data",
    schema_name="public",
    columns=(edidr, edkey, edval),
    constraints=(TableUnique(edidr.tuple_with(edkey)),),
)

eid = edid.buildfrom(edid, schema_name="public", relation_name="edges")
esrc = TableColumn(
    int,
    "source",
    schema_name="public",
    relation_name="edges",
    constraints=(ColumnReferences(nid), ColumnNotNull()),
)
etrgt = esrc.buildfrom(esrc, name="target")
edata = TableColumn(
    int,
    "data",
    schema_name="public",
    relation_name="edges",
    constraints=(ColumnReferences(edid),),
)
edges = Table(
    "edges",
    schema_name="public",
    columns=(eid, esrc, etrgt, edata),
    constraints=(TableUnique(esrc.tuple_with(etrgt)), TableCheck(esrc.gt(etrgt))),
)

edgestore = (
    edges.left_join(edge_data_ids, on=eid.eq(edid))
    .left_join(edge_data, on=edid.eq(edidr))
    .select((esrc, etrgt, edkey, edval))
    .alias("edgestore")
)

# graph
gdid = ndid.buildfrom(ndid, schema_name="public", relation_name="graph_data")
gdkey = TableColumn(
    str,
    "key",
    schema_name="public",
    relation_name="graph_data",
    constraints=(ColumnUnique(), ColumnNotNull()),
)
gdval = ndval.buildfrom(ndval, schema_name="public", relation_name="graph_data")
graph_data = Table("graph_data", schema_name="public", columns=(gdid, gdkey, gdval))

graphstore = graph_data.select((gdkey, gdval)).alias("graphstore")

# data
datastore = (
    node_data.select(
        columns=(Constant(str, "node"), *node_data.columns), aliases={0: "origin"}
    )
    .union_all(edge_data.select(columns=(Constant(str, "edge"), *edge_data.columns)))
    .union_all(graph_data.select(columns=(Constant(str, "graph"), *graph_data.columns)))
)

# schema
public = Schema("public", objects=(graph_data, nodes, node_data, edges, edge_data))

# database
db = DB(schemata=(public,))
