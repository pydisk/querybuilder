import querybuilder as qb
from querybuilder.atoms.columns import (
    TableColumn,
    Placeholder,
    Named,
    Value,
    Constant,
    One,
    Zero,
    Null,
    True_,
    False_,
)
from querybuilder.atoms.relations import Table, View
from querybuilder.atoms.constraints import *
from querybuilder.atoms.schemas import Schema, DB

# nodes
nid = TableColumn(
    int,
    "id",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()),
)
nname = TableColumn(
    str,
    "name",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnUnique(), ColumnNotNull()),
)
ncat = TableColumn(
    str,
    "category",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnCheck(Named(str, "category").inset(set("ABCD"))),),
)
ncolor = TableColumn(
    str,
    "color",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnDefault(Constant(str, "black")),),
)
nshape = TableColumn(
    str,
    "shape",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnDefault(Constant(str, "black")),),
)
ngen = TableColumn(int, "generation", schema_name="public", relation_name="nodes")
nodes = Table(
    "nodes", schema_name="public", columns=(nid, nname, ncat, ncolor, nshape, ngen)
)

nodestore = (
    nodes.select(
        columns=(nodes.columns.name, Constant(str, "category"), nodes.columns.category),
        aliases={1: "key", 2: "value"},
    )
    .union_all(
        nodes.select(
            columns=(nodes.columns.name, Constant(str, "color"), nodes.columns.color),
        )
    )
    .union_all(
        nodes.select(
            columns=(nodes.columns.name, Constant(str, "shape"), nodes.columns.shape),
        )
    )
    .union_all(
        nodes.select(
            columns=(
                nodes.columns.name,
                Constant(str, "generation"),
                nodes.columns.generation.cast(str),
            ),
        )
    )
    .alias("nodestore")
)

# edges
eid = nid.buildfrom(nid, schema_name="public", relation_name="edges")
esrc = TableColumn(
    int,
    "source",
    schema_name="public",
    relation_name="edges",
    constraints=(ColumnReferences(nid), ColumnNotNull()),
)
etrgt = esrc.buildfrom(esrc, name="target")
eweight = TableColumn(
    int,
    "weight",
    schema_name="public",
    relation_name="edges",
    constraints=(ColumnNotNull(), ColumnDefault(Constant(int, 1))),
)
ecat = ncat.buildfrom(ncat, schema_name="public", relation_name="edges")
ecolor = ncolor.buildfrom(ncolor, schema_name="public", relation_name="edges")
egen = ngen.buildfrom(ngen, schema_name="public", relation_name="edges")
edges = Table(
    "edges",
    schema_name="public",
    columns=(eid, esrc, etrgt, eweight, ecat, ecolor, egen),
    constraints=(TableUnique(esrc.tuple_with(etrgt)), TableCheck(esrc.gt(etrgt))),
)

edgestore = (
    edges.select(
        columns=(
            edges.columns.source,
            edges.columns.target,
            Constant(str, "weight"),
            edges.columns.weight.cast(str),
        ),
        aliases={2: "key", 3: "value"},
    )
    .union_all(
        edges.select(
            columns=(
                edges.columns.source,
                edges.columns.target,
                Constant(str, "category"),
                edges.columns.category,
            ),
        )
    )
    .union_all(
        edges.select(
            columns=(
                edges.columns.source,
                edges.columns.target,
                Constant(str, "color"),
                edges.columns.color,
            ),
        )
    )
    .union_all(
        edges.select(
            columns=(
                edges.columns.source,
                edges.columns.target,
                Constant(str, "generation"),
                edges.columns.generation.cast(str),
            ),
        )
    )
    .alias("edgestore")
)

# graph
gid = nid.buildfrom(nid, schema_name="public", relation_name="graphs")
gname = TableColumn(
    str,
    "name",
    schema_name="public",
    relation_name="graphs",
    constraints=(
        ColumnUnique(),
        ColumnNotNull(),
    ),
)
gdescr = TableColumn(str, "description", schema_name="public", relation_name="graphs")
gcolor = TableColumn(
    str,
    "color",
    schema_name="public",
    relation_name="graphs",
    constraints=(ColumnDefault(Constant(str, "white")),),
)
gversion = TableColumn(
    int,
    "version",
    schema_name="public",
    relation_name="graphs",
    constraints=(ColumnNotNull(),),
)
graphs = Table(
    "graphs", schema_name="public", columns=(gid, gname, gdescr, gcolor, gversion)
)

graphstore = (
    graphs.select(
        columns=(Constant(str, "name"), graphs.columns.name),
        where=gid.eq(0),
        aliases={0: "key", 1: "value"},
    )
    .union_all(
        graphs.select(
            columns=(Constant(str, "description"), graphs.columns.description),
        )
    )
    .union_all(
        graphs.select(
            columns=(Constant(str, "color"), graphs.columns.color),
        )
    )
    .union_all(
        graphs.select(
            columns=(Constant(str, "version"), graphs.columns.version.cast(str)),
        )
    )
    .alias("graphstore")
)

# schema
public = Schema("public", objects=(graphs, nodes, edges))

# database
db = DB(schemata=(public,))
