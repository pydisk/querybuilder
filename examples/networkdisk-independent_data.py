import querybuilder as qb
from querybuilder.atoms.columns import (
    TableColumn,
    Placeholder,
    Value,
    Constant,
    One,
    Zero,
    Null,
    True_,
    False_,
    Named,
)
from querybuilder.atoms.relations import Table, View
from querybuilder.atoms.constraints import *
from querybuilder.atoms.schemas import Schema, DB

# nodes
nid = TableColumn(
    int,
    "id",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()),
)
nname = TableColumn(
    str,
    "name",
    schema_name="public",
    relation_name="nodes",
    constraints=(ColumnUnique(), ColumnNotNull()),
)
ndid = nid.buildfrom(nid, schema_name="public", relation_name="node_data")
ndnod = TableColumn(
    int,
    "node",
    schema_name="public",
    relation_name="node_data",
    constraints=(ColumnReferences(nid), ColumnNotNull()),
)
ndkey = TableColumn(
    str,
    "key",
    schema_name="public",
    relation_name="node_data",
    constraints=(ColumnNotNull(),),
)
ndval = TableColumn(
    str,
    "value",
    schema_name="public",
    relation_name="node_data",
    constraints=(ColumnNotNull(),),
)
nodes = Table("nodes", schema_name="public", columns=(nid, nname))
node_data = Table(
    "node_data",
    schema_name="public",
    columns=(ndid, ndnod, ndkey, ndval),
    constraints=(
        TableUnique(
            Named(ndnod.sqltype, ndnod.name).tuple_with(
                Named(ndkey.sqltype, ndkey.name)
            )
        ),
    ),
)
nodestore = (
    nodes.left_join(node_data, on=nid.eq(ndnod))
    .select((nname, ndkey, ndval))
    .alias("nodestore")
)

# edges
eid = nid.buildfrom(nid, schema_name="public", relation_name="edges")
esrc = ndnod.buildfrom(
    ndnod, name="source", schema_name="public", relation_name="edges"
)
etrgt = esrc.buildfrom(esrc, name="target")
edid = nid.buildfrom(nid, schema_name="public", relation_name="edge_data")
ededg = TableColumn(
    int,
    "edge",
    schema_name="public",
    relation_name="edge_data",
    constraints=(ColumnReferences(eid), ColumnNotNull()),
)
edkey = ndkey.buildfrom(ndkey, schema_name="public", relation_name="edge_data")
edval = ndval.buildfrom(ndval, schema_name="public", relation_name="edge_data")
edges = Table(
    "edges",
    schema_name="public",
    columns=(eid, esrc, etrgt),
    constraints=(
        TableUnique(
            Named(esrc.sqltype, esrc.name).tuple_with(Named(etrgt.sqltype, etrgt.name))
        ),
        TableCheck(esrc.gt(etrgt)),
    ),
)
edge_data = Table(
    "edge_data",
    schema_name="public",
    columns=(edid, ededg, edkey, edval),
    constraints=(
        TableUnique(
            Named(ededg.sqltype, ededg.name).tuple_with(
                Named(edkey.sqltype, edkey.name)
            )
        ),
    ),
)
edgestore = (
    edges.left_join(edge_data, on=eid.eq(ededg))
    .select((esrc, etrgt, edkey, edval))
    .alias("edgestore")
)

# graph
gdid = nid.buildfrom(nid, schema_name="public", relation_name="graph_data")
gdkey = TableColumn(
    str,
    "key",
    schema_name="public",
    relation_name="graph_data",
    constraints=(ColumnUnique(), ColumnNotNull()),
)
gdval = ndval.buildfrom(ndval, schema_name="public", relation_name="graph_data")
graph_data = Table("graph_data", schema_name="public", columns=(gdid, gdkey, gdval))

graphstore = graph_data.select((gdkey, gdval)).alias("graphstore")

# schema
public = Schema("public", objects=(graph_data, nodes, node_data, edges, edge_data))

# database
db = DB(schemata=(public,))
