from querybuilder.helpers import table, schema, colspec, make_column


@schema
class networkdisk:
    class nodes:
        id: colspec(int, primary_key=True, generated_as_identity=True)
        name: colspec(
            str, not_null=True, unique=True, check=lambda col, rel: col.neq("")
        )

    class node_data:
        id: colspec(int, primary_key=True, generated_as_identity=True)
        node: colspec(int, references="nodes.id", not_null=True)
        key: colspec(str, not_null=True)
        value: colspec(str, not_null=True)

        @table.unique
        def _(table):
            return table.columns.node.tuple_with(table.columns.key)

    class edges:
        id: colspec(int, primary_key=True, generated_as_identity=True)
        source: colspec(int, references="nodes.id", not_null=True)
        target: colspec(int, references="nodes.id", not_null=True)
        weight: colspec(float, not_null=True, default=1)
        with table.collector() as cons:

            @cons.unique
            def _(table):
                return table.columns.source.tuple_with(table.columns.target)

            @cons.check
            def _(table):
                return table.columns.source.leq(table.columns.target)

    class edge_data:
        id: colspec(int, primary_key=True, generated_as_identity=True)
        edge: colspec(int, references="edges.id", not_null=True)
        key: colspec(str, not_null=True)
        value: colspec(str, not_null=True)

        @table.unique
        def _(table):
            return table.columns.edge.tuple_with(table.columns.key)

    class graph_data:
        key: colspec(str, primary_key=True)
        value: colspec(str, not_null=True)

    @schema.view
    def symedges(schema):
        edges = schema.relations.edges
        rel = edges.select(
            where=edges.columns.source.neq(edges.columns.target)
        ).union_all(
            edges.select(
                columns=(
                    edges.columns.id,
                    edges.columns.target,
                    edges.columns.source,
                    edges.columns.weight,
                )
            )
        )
        return rel

    @schema.view
    def nodestore(schema):
        rel = schema.relations.nodes.left_join(
            schema.relations.node_data,
            on=schema.relations.nodes.columns.id.eq(
                schema.relations.node_data.columns.node
            ),
        )
        return rel.select(columns=(rel.columns[1], *rel.columns[4:5]))

    @schema.view(order="A")
    def pre_edgestore(schema):
        edges = schema.relations.edges
        edge_data = schema.relations.edge_data
        rel0 = edges.left_join(
            edge_data, on=edges.columns.id.eq(edge_data.columns.edge)
        ).select((*edges.columns[:3], "key", "value"), where=lambda f: f.columns.key.neq("weight"))
        rel1 = edges.select(
            columns=(*edges.columns[:3], make_column("weight"), edges.columns.weight),
            aliases={3: "key", 4: "value"},
        )
        rel = rel0.union(rel1, all=True)
        return rel

    @schema.view(order="B")
    def edgestore(schema):
        nodes = schema.relations.nodes
        pedgs = schema.relations.pre_edgestore
        nodes_src = nodes.alias("source_nodes")
        nodes_trgt = nodes.alias("target_nodes")
        rel = pedgs.inner_join(
            nodes_src, on=pedgs.columns.source.eq(nodes_src.columns.id)
        ).inner_join(nodes_trgt, on=pedgs.columns.target.eq(nodes_trgt.columns.id))
        return rel.select(
            columns=[
                nodes_src.columns.name,
                nodes_trgt.columns.name,
                pedgs.columns.key,
                pedgs.columns.value,
            ],
            aliases={0: "source", 1: "target"},
        )

    @schema.view(order="C")
    def adjacency(schema):
        nodes = schema.relations.nodes
        pedgs = schema.relations.pre_edgestore
        nodes_trgt = nodes.alias("target_nodes")
        rel0 = pedgs.inner_join(
            nodes_trgt, on=pedgs.columns.target.eq(nodes_trgt.columns.id)
        )
        rel1 = nodes.left_join(rel0, on=nodes.columns.id.eq(rel0.columns.source))
        return rel1.select(
            columns=[
                nodes.columns.name,
                rel0.columns.name,
                rel0.columns.key,
                rel0.columns.value,
            ],
            aliases={0: "source", 1: "target"},
        )
