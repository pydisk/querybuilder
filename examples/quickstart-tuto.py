from querybuilder.helpers import table, schema
from querybuilder.atoms.columns import (
    Value,
    Constant,
    Placeholder,
    Zero,
    One,
    True_,
    False_,
    Tuple,
)
from querybuilder.drivers.sqlite.connector import Connector
import querybuilder as qb


@schema
class main:
    class nodes:
        id: int
        name: str

    class node_data:
        id: int
        node: int
        key: str
        value: str

    class edges:
        id: int
        source: int
        target: int

    class edge_data:
        id: int
        edge: int
        key: str
        value: str

    class graph_data:
        id: int
        key: str
        value: str

    @classmethod
    def nodestore(cls):
        nodestore = (
            cls.nodes.left_join(
                cls.node_data, on=cls.nodes.columns.id.eq(cls.node_data.columns.node)
            )
            .select(
                columns=(
                    cls.nodes.columns.name,
                    cls.node_data.columns.key,
                    cls.node_data.columns.value,
                )
            )
            .as_view("nodestore")
        )
        return nodestore

    @classmethod
    def edgestore(cls):
        nodes_src = cls.nodes.alias("nodes_src")
        nodes_trgt = cls.nodes.alias("nodes_trgt")
        edgestore = (
            (cls.edges)
            .inner_join(nodes_src, on=cls.edges.columns.source.eq(nodes_src.columns.id))
            .inner_join(
                nodes_trgt, on=cls.edges.columns.target.eq(nodes_trgt.columns.id)
            )
            .left_join(
                cls.edge_data, on=cls.edges.columns.id.eq(cls.edge_data.columns.edge)
            )
            .select(
                columns=(
                    nodes_src.columns.name,
                    nodes_trgt.columns.name,
                    cls.edge_data.columns.key,
                    cls.edge_data.columns.value,
                ),
                aliases={0: "source", 1: "target"},
            )
        )
        # No need for calling .as_view method: schema Helper converts Select queries into views
        return edgestore

    @classmethod
    def graphstore(cls):
        return cls.graph_data.select(columns=cls.graph_data.columns[1:])
