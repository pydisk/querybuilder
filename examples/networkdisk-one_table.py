import querybuilder as qb
from querybuilder.atoms.columns import (
    TableColumn,
    Placeholder,
    Value,
    Constant,
    One,
    Zero,
    Null,
    True_,
    False_,
    BooleanCombination,
)
from querybuilder.atoms.relations import Table, View
from querybuilder.atoms.constraints import *
from querybuilder.atoms.schemas import Schema, DB

gid = TableColumn(
    int,
    "id",
    schema_name="public",
    relation_name="graph",
    constraints=(ColumnPrimaryKey("pk"), ColumnGeneratedAsIdentity()),
)
gsrc = TableColumn(str, "node", schema_name="public", relation_name="graph")
gtrgt = TableColumn(str, "target", schema_name="public", relation_name="graph")
gkey = TableColumn(str, "key", schema_name="public", relation_name="graph")
gval = TableColumn(str, "value", schema_name="public", relation_name="graph")

check = gval.isnull().or_(gkey.isnotnull()).and_(gtrgt.isnull().or_(gsrc.isnotnull()))

graph = Table(
    "graph",
    schema_name="public",
    columns=(gid, gsrc, gtrgt, gkey, gval),
    constraints=(ColumnCheck(check),),
)

nodestore = graph.select(
    (gsrc, gkey, gval), where=gsrc.isnotnull().and_(gtrgt.isnull())
).alias("nodestore")
edgestore = graph.select((gsrc, gtrgt, gkey, gval), where=gtrgt.isnotnull()).alias(
    "edgestore"
)
graphstore = graph.select((gkey, gval), where=gsrc.isnull()).alias("graphstore")
