from __future__ import annotations
from typing import Optional, Self, Mapping
from querybuilder.queries.queries import Query
import querybuilder.atoms.clauses as qbclauses
import querybuilder.atoms.schemas as qbschemas
import querybuilder as qb


class DDLQuery(Query):
    __slots__ = ("target",)
    _is_readonly = False

    def __init__(self, target, **kwargs):
        self.target = target
        super().__init__(**kwargs)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        return self.target.accept(accumulator)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            super()._get_subtokenize_kwargs(tokenizer),
            name=self.target.subtokenize(tokenizer),
        )

    def _substitute(self, substitutions: Mapping) -> Self:
        target = self.target.substitute(substitutions)

        return self.buildfrom(self, target=target)


# TODO: CreateTable, CreateTableAs, CreateView as three distinct objects (CreateView
#       might subclass CreateTableAs).
class CreateTable(DDLQuery):
    __slots__ = ("if_not_exists", "as_query", "temporary")

    def __init__(
        self,
        table: qb.atoms.relations.Table,
        if_not_exists: bool = False,
        as_query: Optional[qb.queries.dql.DQLQuery] = None,
        temporary: bool = False,
        **kwargs,
    ):
        self.if_not_exists = if_not_exists
        self.as_query = as_query
        self.temporary = temporary
        super().__init__(target=table, **kwargs)

    @property
    def table(self):
        return self.target

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        add_kwargs = {}

        if self.temporary:
            add_kwargs["temporary"] = qbclauses.TemporaryWrapper().subtokenize(
                tokenizer
            )

        if self.if_not_exists:
            add_kwargs["if_not_exists"] = qbclauses.IfNotExistsWrapper().subtokenize(
                tokenizer
            )

        if self.as_query:
            add_kwargs["as_query"] = qbclauses.AsQueryWrapper(
                self.as_query
            ).subtokenize(tokenizer)
            # TODO: Does CREATE TABLE AS QUERY accept column aliasing?
        else:
            add_kwargs["columns"] = tuple(
                qbclauses.TableColumnWrapper(c).subtokenize(tokenizer)
                for c in self.target.columns
            )

        # TODO: Does CREATE TABLE AS QUERY accept constraints?
        add_kwargs["constraints"] = tuple(
            c.subtokenize(tokenizer) for c in self.target.constraints
        )

        return dict(**kwargs, **add_kwargs)

    def _substitute(self, substitutions: Mapping) -> Self:
        if self.as_query:
            as_query = self.as_query.substitute(substitutions)
            self = self.buildfrom(self, as_query=as_query)

        return super()._substitute(substitutions)


class CreateView(DDLQuery):
    __slots__ = ("if_not_exists", "temporary")

    @property
    def view(self):
        return self.target

    def __init__(
        self,
        view: qb.atoms.relations.View,
        if_not_exists: bool = False,
        temporary: bool = False,
        **kwargs,
    ):
        self.if_not_exists = if_not_exists
        self.temporary = temporary
        super().__init__(target=view, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        add_kwargs = {}

        if self.temporary:
            add_kwargs["temporary"] = qbclauses.TemporaryWrapper().subtokenize(
                tokenizer
            )

        if self.if_not_exists:
            add_kwargs["if_not_exists"] = qbclauses.IfNotExistsWrapper().subtokenize(
                tokenizer
            )

        if self.view.aliases:
            add_kwargs["columns"] = tuple(
                qbclauses.TableColumnWrapper(c).subtokenize(tokenizer)
                for c in self.target.columns
            )

        add_kwargs["as_query"] = qbclauses.AsQueryWrapper(
            self.view.defquery
        ).subtokenize(tokenizer)

        return dict(**kwargs, **add_kwargs)


class CreateSchema(DDLQuery):
    __slots__ = ("if_not_exists",)

    def __init__(self, schema: qbschemas.Schema, if_not_exists: bool = False, **kwargs):
        self.if_not_exists = if_not_exists
        super().__init__(target=schema, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = dict(
            **super()._get_subtokenize_kwargs(tokenizer),
        )
        if self.if_not_exists:
            kwargs["if_not_exists"] = (
                qbclauses.IfNotExistsWrapper().subtokenize(tokenizer),
            )
        return kwargs


class CreateIndex(DDLQuery):
    __slots__ = (
        "index",
        "if_not_exists",
    )

    def __init__(self, index: qbschemas.Index, if_not_exists: bool = False):
        self.index = index
        self.if_not_exists = if_not_exists
        super().__init__(target=index)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs.update(self.index._get_creation_kwargs(tokenizer))

        if self.if_not_exists:
            kwargs["if_not_exists"] = qbclauses.IfNotExistsWrapper().subtokenize(
                tokenizer
            )

        return kwargs


class Drop(DDLQuery):
    __slots__ = ("cascade", "if_exists")

    def __init__(
        self,
        target: qb.atoms.atoms.Atom,
        if_exists: bool = False,
        cascade: Optional[bool] = None,
        **kwargs,
    ):
        self.cascade = cascade
        self.if_exists = if_exists
        super().__init__(target, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = dict(
            **super()._get_subtokenize_kwargs(tokenizer),
        )

        if self.cascade is not None:  # display "RESTRICT" if cascade is False
            kwargs["cascade"] = qbclauses.DropCascadeClause(self.cascade).subtokenize(
                tokenizer
            )

        if self.if_exists:
            kwargs["if_exists"] = qbclauses.IfExistsWrapper().subtokenize(tokenizer)

        kwargs["target_type"] = qbclauses.DBObjectNameWrapper(self.target).subtokenize(
            tokenizer
        )

        return kwargs
