"""Python objects for SQL transaction control sublanguage

SQL standard transaction control queries, such as 'START TRANSACTION' or 'COMMIT WORK'
are implemented here.  Other example are queries for managing savepoints.

Dialectized classes and functions are:
    Initiate : class
        the type for generating 'SET TRANSACTION' and 'START TRANSACTION' queries.
    Set : class
        the type allowing to generate 'SET TRANSACTION …' queries.
    Start : class
        the type allowing to generate 'START TRANSACTION …' queries.
    Begin : alias for Start
    Conclude : class
        the type for generating 'COMMIT WORK' and 'ROLLBACK WORK' queries.
    Commit : class
        the type corresponding to 'COMMIT WORK' queries.
    End : alias for Commit
    Rollback : class
        the type corresponding to 'ROLLBACK WORK' queries.
    ManageSavepoint: class
        the type for generating queries managing (e.g., creating, releasing or
        back-rolling to) savepoints.
    RollbackToSavepoint : class
        the type for generating 'ROLLBACK TRANSACTION TO SAVEPOINT …' queries.
    CreateSavepoint : class
        the type for generating 'SAVEPOINT …' queries.
    ReleaseSavepoint : class
        the type for generating 'RELEASE SAVEPOINT …' queries.

"""

from querybuilder.queries.queries import Query


class TCLQuery(Query):
    __slots__ = ()
    _is_readonly = True

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(action=self._action, **super()._get_subtokenize_kwargs(tokenizer))


# START
class Initiate(TCLQuery):
    __slots__ = ("isolation_level", "read_only", "diagnostic_size")
    _isolation_level = (
        "SERIALIZABLE",
        "REPEATABLE READ",
        "READ COMMITTED",
        "READ UNCOMMITTED",
    )

    def __init__(
        self, isolation_level=None, read_only=None, diagnostic_size=None, **kwargs
    ):
        # TODO: what is diagnostic size?  Is it standard SQL?  Is it useful here?
        if diagnostic_size is not None:
            self.diagnostic_size = diagnostic_size
            raise NotImplementedError("tcl.Set do not implement diagnostic size")

        isolation_level = (
            self._isolation_level[isolation_level]
            if isinstance(isolation_level, int)
            else isolation_level
        )
        self.isolation_level = isolation_level
        self.read_only = read_only
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            isolation_level=self.isolation_level,
            read_only=self.read_only,
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def set_isolation_level(self, isolation_level):
        isolation_level = (
            self._isolation_level[isolation_level]
            if isinstance(isolation_level, int)
            else isolation_level
        )
        return self.buildfrom(self, isolation_level=isolation_level)

    def set_read_only(self, read_only=True):
        return self.buildfrom(self, read_only=read_only)

    def set_read_write(self, read_write=True):
        return self.set_read_only(
            read_only=None if read_write is None else not read_write
        )


class Set(Initiate):
    __slots__ = ()
    _action = "SET"


class Start(Set):
    __slots__ = ()
    _action = "START"


Begin = Start


# CONCLUDE (COMMIT/ROLLBACK)
class Conclude(TCLQuery):
    __slots__ = ("chain",)

    def __init__(self, chain=None, **kwargs):
        self.chain = chain
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(chain=self.chain, **super()._get_subtokenize_kwargs(tokenizer))


class Commit(Conclude):
    __slots__ = ()
    _action = "COMMIT"


End = Commit


class Rollback(Conclude):
    __slots__ = ()
    _action = "ROLLBACK"


# SAVEPOINTS MANAGEMENT
class ManageSavepoint(TCLQuery):
    __slots__ = ("savepoint",)

    def __init__(self, savepoint, **kwargs):
        self.savepoint = savepoint
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            savepoint=self.savepoint, **super()._get_subtokenize_kwargs(tokenizer)
        )


class RollbackToSavepoint(ManageSavepoint):
    __slots__ = ()
    _action = "ROLLBACK"


class CreateSavepoint(ManageSavepoint):
    __slots__ = ()
    _action = "CREATE"


class ReleaseSavepoint(ManageSavepoint):
    __slots__ = ()
    _action = "RELEASE"
