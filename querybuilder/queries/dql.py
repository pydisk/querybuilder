from __future__ import annotations
import functools
from typing import (
    Callable,
    Iterable,
    Mapping,
    Optional,
    Self,
    Sequence,
    Tuple,
    Union,
    overload,
)
import querybuilder.utils.stores as qbstores
import querybuilder.utils.constants as qbconstants
from querybuilder.utils.decorators import method_accepting_lambdas
from querybuilder.queries.queries import Query
import querybuilder as qb
import querybuilder.atoms.relations as qbrelations


class DQLQuery(Query, qbrelations.Relation):
    __slots__ = ()
    _is_readonly = True

    # TODO: add method here to get a "connected relation" (aka Iterable Query)?

    @method_accepting_lambdas
    def as_with(
        self, name: str, materialized: Optional[bool] = None
    ) -> qb.atoms.relations.With:
        return qb.atoms.relations.With(self, name=name, materialized=materialized)

    @overload
    def build_recursive_with(
        self: DQLQuery,
        rule: Callable[[qbrelations.Named], DQLQuery],
        /,
        name: Optional[str] = None,
        materialized: Optional[bool] = None,
        **kwargs,
    ) -> qbrelations.With: ...

    @overload
    def build_recursive_with(
        self: DQLQuery,
        rule: None = None,
        /,
        name: Optional[str] = None,
        materialized: Optional[bool] = None,
        **kwargs,
    ) -> Callable[[Callable[[qbrelations.Named], DQLQuery]], qbrelations.With]: ...

    def build_recursive_with(
        self: DQLQuery,
        rule: Optional[Callable[[qbrelations.Named], DQLQuery]] = None,
        /,
        name: Optional[str] = None,
        materialized: Optional[bool] = None,
        **kwargs,
    ) -> (
        Callable[[Callable[[qbrelations.Named], DQLQuery]], qbrelations.With]
        | qbrelations.With
    ):
        """Helper to produce recursive WITH relations

        Parameters
        ----------
        self: DQLQuery
            the base DQL query of the recursive relation.

        rule: Optional[Callable[[qbrelations.Named], DQLQuery]], default=None
            a function that, given a named relation which mimics the recursive relation to
            produce (i.e., with same name and same columns), returns a DQL query that
            recursively produces tuples for augmenting the recursive relation.  If None,
            the default, the method works as a pre-parametrized decorator, that is, it
            returns a decorator which expects a function as above-described.  See examples
            below.

        name: Optional[str], default=None
            the name of the recursive WITH relation to produce.  If None, the name of the
            `rule` function parameter is taken instead.

        materialized: Optional[bool], default=None
            see `qbrelations.With` documentation.

        **kwargs:
            see `qbrelations.With` documentation.

        Examples
        --------
        >>> Constant = qb.atoms.columns.Constant
        >>> columns = [ Constant(int, 3), Constant(str, "foo"), Constant(bool, True) ]
        >>> base = Select(columns, aliases=enumerate(["numbr", "txt", "flag"]))
        >>> str(base)
        "SELECT 3 AS numbr, 'foo' AS txt, True AS flag"
        >>> @base.build_recursive_with(name="bar", materialized=True)
        ... def foobar(relation):
        ...     columns = [relation.columns.numbr.add(1), relation.columns.txt, Constant(bool, False)]
        ...     return relation.select(columns)
        >>> str(foobar)
        'bar'
        >>> str(foobar.select().with_closure())
        "WITH RECURSIVE bar AS MATERIALIZED (SELECT 3 AS numbr, 'foo' AS txt, True AS flag UNION SELECT bar.numbr + 1, bar.txt, False FROM bar) SELECT bar.numbr, bar.txt, bar.flag FROM bar"

        Instead of using the @-decorator form of the method, one could have directly pass
        the function `foobar` as first (positional-only) argument of the function.  Also,
        not specifying `name=bar` would have result in a recursive WITH relation named
        `'foobar'` since the given function is named so.

        """

        def decorator(
            func: Callable[[qbrelations.Named], DQLQuery],
            /,
        ) -> qbrelations.With:
            with_relation_name = name or func.__name__
            recnamed = qbrelations.RecursiveNamed.from_query(self, with_relation_name)
            recrel = self.union(func(recnamed))
            return recrel.as_with(
                with_relation_name, materialized=materialized, **kwargs
            )

        if rule:
            return decorator(rule)
        return decorator

    @method_accepting_lambdas
    def union(self, other, all=None, **kwargs):
        return SetCombination(
            qbconstants.SetCombinator.UNION, self, other, all=all, **kwargs
        )

    union_all = functools.partialmethod(union, all=True)

    @method_accepting_lambdas
    def intersect(self, other, all=None, **kwargs):
        return SetCombination(
            qbconstants.SetCombinator.INTERSECT, self, other, all=all, **kwargs
        )

    intersect_all = functools.partialmethod(intersect, all=True)

    @method_accepting_lambdas
    def except_(self, other, all=None, **kwargs):
        return SetCombination(
            qbconstants.SetCombinator.EXCEPT, self, other, all=all, **kwargs
        )

    except_all = functools.partialmethod(except_, all=True)

    def create_view(
        self, name: str, if_not_exists: bool = False, temporary: bool = False
    ) -> qb.queries.ddl.CreateView:
        return qb.atoms.relations.View(name, self).create(
            if_not_exists=if_not_exists, temporary=temporary
        )

    def tokenize(self, tokenizer):
        wrs = self.get_free_with()

        if wrs:
            closure = self.with_closure(with_relations=wrs)
            return closure.tokenize(tokenizer)
        else:
            return super().tokenize(tokenizer)


class Values(DQLQuery):
    __slots__ = (
        "values",
        "orderby",
        "limit",
        "offset",
        "limit_with_ties",
        #'fetch'
    )

    def __init__(
        self,
        values: Iterable[
            tuple[qb.atoms.columns.Column | qb.atoms.pseudo_columns.Default, ...]
        ],
        orderby: Optional[qb.atoms.columns.Column] = None,
        limit: Optional[int] = None,
        limit_with_ties: bool = False,
        offset: Optional[int] = None,
        **kwargs,
    ):
        self.values = tuple(values)
        self.orderby = orderby
        self.limit = limit  # TODO: Postgre only feature for Values
        self.limit_with_ties = limit_with_ties
        self.offset = offset
        columns: list[qb.atoms.columns.Column | qb.atoms.pseudo_columns.Default] = []
        for i, c in enumerate(self.values[0], start=1):
            if isinstance(c, qb.atoms.pseudo_columns.Default):
                columns.append(c)
            else:
                columns.append(qb.helpers.columns.name_column(c, name=f"column{i}"))
        super().__init__(columns=columns, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        # kwargs.pop("columns") # we don't need the columns to tokenize the relation
        add_kwargs = {}

        add_kwargs["values"] = tuple(
            qb.atoms.columns.Tuple(*v).subtokenize(tokenizer) for v in self.values
        )

        if self.orderby:
            add_kwargs["orderby"] = qb.atoms.clauses.OrderColumn(
                self.orderby
            ).subtokenize(tokenizer)
        if self.offset is not None:
            add_kwargs["offset"] = qb.atoms.clauses.Offset(self.offset).subtokenize(
                tokenizer
            )
        if self.limit is not None:
            add_kwargs["limit"] = qb.atoms.clauses.Limit(
                self.limit, with_ties=self.limit_with_ties
            ).subtokenize(tokenizer)
        return dict(**kwargs, **add_kwargs)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for row in self.values:
            for col in row:
                accumulator = col.accept(accumulator)
        if self.orderby:
            accumulator = self.orderby.accept(accumulator)
        return accumulator

    def _substitute(self, substitutions: Mapping) -> Self:
        d = dict(
            values=tuple(
                tuple(c.substitute(substitutions) for c in t) for t in self.values
            )
        )

        if self.orderby:
            d["orderby"] = self.orderby.substitute(substitutions)

        return self.buildfrom(self, **d)


class Select(DQLQuery):
    __slots__: tuple[str, ...] = (
        "selected_columns",
        "aliases",
        "from_",
        "where",
        "groupby",
        "having",
        "orderby",
        "limit",
        "limit_with_ties",
        "offset",
        "distinct",
    )
    _scopable = True

    def __init__(
        self,
        selected_columns: Sequence[
            qb.atoms.columns.Column | qb.atoms.pseudo_columns.Star
        ],
        aliases: Mapping[int, str] | Iterable[tuple[int, str]] = (),
        distinct: Optional[qb.atoms.columns.Column | bool] = None,
        from_: Optional[qb.atoms.relations.Fromable] = None,
        where: Optional[qb.atoms.columns.Column] = None,
        groupby: Optional[qb.atoms.columns.Column] = None,
        having: Optional[qb.atoms.columns.Column] = None,
        orderby: Optional[qb.atoms.columns.Column] = None,
        limit: Optional[int] = None,
        limit_with_ties: bool = False,
        offset: Optional[int] = None,
        **kwargs,
    ):
        self.aliases: Mapping[int, str] = qbstores.Frozenmap(aliases)
        self.distinct = distinct
        self.where = where
        self.groupby = groupby
        self.having = having
        self.orderby = orderby
        self.limit = limit
        self.limit_with_ties = limit_with_ties
        self.offset = offset
        self.from_ = from_

        super().__init__(columns=selected_columns, **kwargs)

    @property
    def columns(self):
        columns = []
        for i, c in enumerate(self.selected_columns):
            if isinstance(c, qb.atoms.pseudo_columns.Star):
                if c.relation:
                    star_columns = c.relation.columns
                else:
                    star_columns = self.from_.columns
                columns.extend(star_columns)
            else:
                columns.append(
                    qb.helpers.columns.name_column(c, self.aliases[i])
                    if i in self.aliases
                    else c
                )

        return self._column_store_factory(columns)

    def _init_columns(self, selected_columns):
        for k in self.aliases.keys():
            if 0 <= k < len(selected_columns) and isinstance(
                selected_columns[k], qb.atoms.pseudo_columns.Star
            ):
                raise KeyError("Cannot alias a * column")
        for c in selected_columns:
            if isinstance(c, qb.atoms.pseudo_columns.Star):
                assert self.from_  # TODO: Maybe another exception would be better
                break

        self.selected_columns = self._column_store_factory(selected_columns)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for ocol in self.selected_columns:
            accumulator = ocol.accept(accumulator)

        for res in (self.from_, self.groupby, self.having, self.orderby, self.where):
            if res:
                accumulator = res.accept(accumulator)

        return accumulator

    def set_selected_columns(
        self,
        selected_columns: Iterable[
            str | int | qb.atoms.columns.Column | qb.atoms.pseudo_columns.Star
        ],
        /,
    ) -> Select:
        """Method for resetting selected columns of the query

        Aliases are not preserved and Star is allowed.

        Parameters
        ----------
        selected_columns: Iterable[str | int | Column | Star]
            the specifications of the columns to select.  Each specification might be an
            index (int), a name (str), an explicit column, or the special operator Star.
            In the two first cases, it is resolved within the `selected_columns` store,
            using the `resolve` method.  Please consider using `set_columns` for setting
            columns using similar specifications that are resolved within the `columns`
            store context instead.  A side effect of the method is that all aliases are
            dropped.

        Related methods
        ---------------
        set_columns

        Examples
        --------
        >>> from querybuilder.helpers import table, make_column
        >>> t = table(type("t", (), dict(__annotations__=dict(x=int, y=str))))
        >>> const2 = make_column(2)
        >>> sq = t.select([t.columns[0], const2, t.columns[1]], aliases={2: "bar"})
        >>> str(sq)
        'SELECT t.x, 2, t.y AS bar FROM t'
        >>> str(sq.set_selected_columns([t.columns[1], const2, t.columns[0]]))
        'SELECT t.y, 2, t.x FROM t'

        Notice that aliases have been dropped.  The just-generated query could have been
        obtained in the following way as well:
        >>> str(sq.set_selected_columns([2, const2, "x"]))
        'SELECT t.y, 2, t.x FROM t'

        Indeed, in the list parameter `[2, Constant(int, 2), "x"]`, the first element
        (`2`) stands for the selected column of index 2 (ie, `sq.selected_columns[2]`),
        and the last one (`"x"`) stands for the selected column that is unambiguously
        named "x" (i.e., `sq.selected_columns._["x"]`).

        It is also possible to use `querybuilder.atoms.pseudo_columns.Star()`, or the
        `"*"` shorthand, in the sequence of selected columns.
        >>> from querybuilder.atoms.pseudo_columns import Star
        >>> str(sq.set_selected_columns(["*", 2, Star(), "x"]))
        'SELECT *, t.y, *, t.x FROM t'

        """
        cols: list[qb.atoms.columns.Column | qb.atoms.pseudo_columns.Star] = []
        for c in selected_columns:
            if c == "*":
                assert self.from_
                cols.append(qb.atoms.pseudo_columns.Star())
            elif isinstance(c, qb.atoms.pseudo_columns.Star):
                assert self.from_
                cols.append(c)
            elif isinstance(c, qb.atoms.columns.Column):
                cols.append(c)
            else:
                # assert isinstance(c, (int, str))
                cols.append(self.selected_columns.resolve(c))
        return self.set_aliases(())._set_columns(cols)

    def set_columns(
        self, columns: Iterable[str | int | qb.atoms.columns.Column], /
    ) -> Select:
        """Method for resetting exposed columns of the query

        Aliases are kept; Star is forbidden.

        Parameters
        ----------
        columns: Iterable[str | int | qb.atoms.columns.Column]
            the specifications of the columns to select.  Each specification might be an
            index (int), a name (str), or an explicit column.  In the first two cases it
            is resolved within the `columns` store, using the `resolve` method.  If the
            corresponding column was aliased, then the alias is kept.  The special Star
            operator is not allowed here since the specifications are resolved according
            to the exposed columns, and not their selected origins.

        Related methods
        ---------------
        set_selected_columns

        Examples
        --------
        >>> from querybuilder.helpers import table, make_column
        >>> t = table(type("t", (), dict(__annotations__=dict(x=int, y=str))))
        >>> const2 = make_column(2)
        >>> sq = t.select(
        ...     [t.columns[0], const2, t.columns[1], t.columns[1]],
        ...     aliases={2: "bar"},
        ... )
        >>> str(sq)
        'SELECT t.x, 2, t.y AS bar, t.y FROM t'
        >>> str(sq.set_columns([sq.columns[2], const2, sq.columns[0], sq.columns[3]]))
        'SELECT t.y AS bar, 2, t.x, t.y FROM t'

        Notice that aliases have been kept.  The just-generated query could have been
        obtained in the following way as well:
        >>> str(sq.set_columns([2, const2, "x", "y"]))
        'SELECT t.y AS bar, 2, t.x, t.y FROM t'

        Indeed, in the list parameter `[2, Constant(int, 2), "x", "y"]`, `2` stands for
        the exposed column of index 2 in the query (ie, `sq.columns[2]`), and `"x"` and
        `"y"` stand for the exposed column that is unambiguously named "x" and "y",
        respectively, (ie, `sq.columns._["x"]` and `sq.columns._["y"]`).
        """
        cols = []
        aliases = {}
        index: Optional[int]
        for i, c in enumerate(columns):
            assert not isinstance(c, qb.atoms.pseudo_columns.Star)

            if isinstance(c, int):
                index = c
            elif isinstance(c, str):
                c = self.columns._[c]
                index = self.columns.index(c)
            elif c in self.columns:
                index = self.columns.index(c)
            else:
                orig_c = c
                index = None

            if index is not None:
                orig_c = self.selected_columns[index]
                alias = self.aliases.get(index)
                if alias:
                    aliases[i] = alias
            cols.append(orig_c)
        return self._set_columns(cols).set_aliases(aliases)

    def set_distinct(self, distinct):
        return self.buildfrom(self, distinct=distinct)

    @method_accepting_lambdas
    def set_aliases(self, aliases):
        return self.buildfrom(self, aliases=qbstores.Frozenmap(aliases))

    @method_accepting_lambdas
    def update_aliases(
        self: Self, aliases: Iterable[tuple[int, str]] | Mapping[int, str]
    ) -> Self:
        d = dict(self.aliases)
        d.update(aliases)
        return self.set_aliases(d)

    def reset_aliases(self):
        return self.buildfrom(self, aliases=())

    @method_accepting_lambdas
    def set_where(self, where):
        return self.buildfrom(self, where=where)

    @method_accepting_lambdas
    def set_groupby(self, groupby):
        return self.buildfrom(self, groupby=groupby)

    @method_accepting_lambdas
    def set_having(self, having):
        return self.buildfrom(self, having=having)

    @method_accepting_lambdas
    def set_order(self, orderby, /):
        return self.buildfrom(
            self,
            orderby=orderby,
        )

    def set_limit(
        self, limit: Optional[int] = None, limit_with_ties: Optional[bool] = None
    ) -> Select:
        return self.buildfrom(
            self,
            limit=limit,
            limit_with_ties=self.limit_with_ties
            if limit_with_ties is None
            else limit_with_ties,
        )

    def set_offset(self, offset):
        return self.buildfrom(self, offset=offset)

    @method_accepting_lambdas
    def add_where(self, where):
        if self.where:
            return self.set_where(self.where & where)
        return self.set_where(where)

    @method_accepting_lambdas
    def add_having(self, having):
        if self.having:
            return self.set_having(self.having & having)
        return self.set_having(having)

    def exists_column(self):
        return qb.atoms.columns.Exists(self)

    def exists_query(self):
        return Select((self.exists_column(),))

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        add_kwargs = {}
        add_kwargs["columns"] = tuple(
            qb.atoms.clauses.AliasedColumn(c, self.aliases.get(i, None)).subtokenize(
                tokenizer
            )
            if isinstance(c, qb.atoms.columns.Column)
            else c.subtokenize(tokenizer)
            for i, c in enumerate(self.selected_columns)
        )
        if self.distinct:
            add_kwargs["distinct"] = qb.atoms.clauses.DistinctColumn(
                self.distinct
            ).subtokenize(tokenizer)
        if self.from_:
            add_kwargs["from_"] = qb.atoms.clauses.RelationClauseWrapper(
                self.from_
            ).subtokenize(tokenizer)
        if self.where:
            add_kwargs["where"] = qb.atoms.clauses.WhereColumn(self.where).subtokenize(
                tokenizer
            )
        if self.groupby:
            add_kwargs["groupby"] = qb.atoms.clauses.GroupColumn(
                self.groupby
            ).subtokenize(tokenizer)
            if self.having:
                add_kwargs["having"] = qb.atoms.clauses.HavingColumn(
                    self.having
                ).subtokenize(tokenizer)
        if self.orderby:
            add_kwargs["orderby"] = qb.atoms.clauses.OrderColumn(
                self.orderby
            ).subtokenize(tokenizer)
        if self.offset is not None:
            add_kwargs["offset"] = qb.atoms.clauses.Offset(self.offset).subtokenize(
                tokenizer
            )
        if self.limit is not None:
            add_kwargs["limit"] = qb.atoms.clauses.Limit(
                self.limit, with_ties=self.limit_with_ties
            ).subtokenize(tokenizer)

        return dict(**kwargs, **add_kwargs)

    @method_accepting_lambdas
    def as_view(self, name, schema_name: Optional[str] = None, aliases=(), **kwargs):
        return qb.atoms.relations.View(
            name, self, schema_name=schema_name, aliases=aliases, **kwargs
        )

    def _set_columns(self, columns):
        return self.buildfrom(self, columns=columns)

    def _post_getstate(self, state):
        state["columns"] = state.pop("selected_columns")
        return state

    def _substitute(self, substitutions: Mapping):
        columns = [c.substitute(substitutions) for c in self.selected_columns]
        d = dict(columns=columns)

        for k in ("where", "distinct", "groupby", "orderby", "having", "from_"):
            res = getattr(self, k)
            if hasattr(res, "substitute"):
                res = res.substitute(substitutions)
            d[k] = res

        return self.buildfrom(self, **d)


class SetCombination(DQLQuery):
    """
    Parameters
    ----------
    combinator: SetCombinator
        the Boolean combinator to use: "UNION", "INTERSECT", or "EXCEPT".

    queries: relations.Executable
        the executable relations to combine.

    all: bool or None, default=None
        whether to select rows with multiplicity or distinct rows only.  If None (the
        default), the default SQL behavior (distinct rows only) is used.

    kwargs:
        Additional keyworded parameters for super initialization.
    """

    __slots__ = ("combinator", "all", "left", "right")
    _scopable = True

    def __init__(
        self,
        combinator: qbconstants.SetCombinator,
        left: DQLQuery,
        right: DQLQuery,
        all: Optional[bool] = None,
        **kwargs,
    ):
        assert left.arity == right.arity
        self.combinator = combinator
        self.all = all
        self.left = left
        self.right = right
        super().__init__(columns=left.columns, **kwargs)

    def descend(self, visitor):
        visitor = super().descend(visitor)
        visitor = self.left.accept(visitor)
        visitor = self.right.accept(visitor)
        return visitor

    @method_accepting_lambdas
    def set_aliases(self, aliases):
        return self.buildfrom(self, left=self.left.set_aliases(aliases))

    def _post_getstate(self, state):
        state.pop("columns")
        return state

    def __setstate__(self, state):
        state.setdefault("columns", state["left"].columns)
        super().__setstate__(state)

    def _get_subtokenize_kwargs(self, tokenizer):
        scope_left = isinstance(self.left, SetCombination)
        scope_right = isinstance(self.right, SetCombination)

        return dict(
            subrelations=(
                self.left.subtokenize(tokenizer, scoped=scope_left),
                self.right.subtokenize(tokenizer, scoped=scope_right),
            ),
            combinator=qb.atoms.clauses.SetCombinatorWrapper(
                self.combinator, all=self.all
            ).subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def _substitute(self, substitutions: Mapping) -> Self:
        left = self.left.substitute(substitutions)
        right = self.right.substitute(substitutions)
        return self.buildfrom(self, left=left, right=right)


class WithClosure(DQLQuery):
    """Represents the closure of a DQL query with some with relations.

    Parameters
    ----------

    query: DQLQuery
       the query that is closed

    with_relations: tuple[With] or With
        the With relation(s) that should be defined in this closure

    Examples
    --------

    >>> from querybuilder.atoms.columns import Constant
    >>> w1 = Select((Constant(int, 1),)).as_with(name="T", materialized=True)
    >>> w2 = Select((Constant(str, "a"),)).as_with(name="T2", materialized=False)
    >>> j = w1.full_join(w2).select()
    """ """
    >>> str(j.with_closure((w1, w2)))
    "WITH T AS MATERIALIZED (SELECT 1), T2 AS NOT MATERIALIZED (SELECT 'a') SELECT 1, 'a' FROM T FULL JOIN T2 ON True"
    """  # noqa: E501

    __slots__ = ("query", "with_relations")

    def __init__(
        self,
        query: DQLQuery,
        with_relations: Union[
            Tuple[qb.atoms.relations.With, ...], qb.atoms.relations.With
        ],
    ):
        self.query = query

        if not isinstance(with_relations, tuple):
            with_relations = (with_relations,)

        self.with_relations = with_relations

        super().__init__(columns=self.query.columns)

    @property
    def recursive(self):
        return any(wq.recursive for wq in self.with_relations)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)

        accumulator = self.query.accept(accumulator)

        for w in self.with_relations:
            accumulator = w.accept(accumulator)

        return accumulator

    def with_closure(self, with_relations=None):
        if with_relations is None:  # TODO: scrap this feature?
            with_relations = self.get_free_with()

        return self.buildfrom(self, with_relations=self.with_relations + with_relations)

    def _get_subtokenize_kwargs(self, tokenizer):
        d = super()._get_subtokenize_kwargs(tokenizer)
        return dict(
            **d,
            recursive=self.recursive,
            with_relations=tuple(
                wq.to_definition_clause().subtokenize(tokenizer)
                for wq in self.with_relations
            ),
            query=self.query.subtokenize(tokenizer),
        )

    @method_accepting_lambdas
    def alias(self, name):
        # TODO: protocol or inheritance?
        return qb.atoms.relations.Aliased(self, name)

    def _substitute(self, substitutions: Mapping) -> Self:
        query = self.query.substitute(substitutions)
        with_relations = tuple(w.substitute(substitutions) for w in self.with_relations)

        return self.buildfrom(self, query=query, with_relations=with_relations)
