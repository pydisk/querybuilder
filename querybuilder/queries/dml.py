from __future__ import annotations
from typing import Optional, Mapping, Self
from querybuilder.queries.queries import Query
import querybuilder as qb


class DMLQuery(Query):
    """Base abstract class for DML queries

    Parameters
    ----------
    schema_relation: schema.Relation
        the schema relation (TABLE or VIEW) in which to insert values.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.
    """

    __slots__ = ("schema_relation",)
    _is_readonly = False

    def __init__(self, schema_relation: qb.atoms.relations.Relation, **kwargs):
        self.schema_relation = schema_relation
        super().__init__(**kwargs)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        return self.schema_relation.accept(accumulator)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            schema_relation=self.schema_relation.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def _substitute(self, substitutions: Mapping) -> Self:
        schema_relation = self.schema_relation.substitute(substitutions)

        return self.buildfrom(self, schema_relation=schema_relation)


class Insert(DMLQuery):
    """INSERT query class

    Parameters
    ----------
    schema_relation:
        the schema relation (TABLE or VIEW) in which to insert values.

    in_columns: iterable of qb.atoms.columns.Column
        a subset of the schema_relation's columns, in which to insert values.

    query: Optional[qbqueries.dql.DQLQuery]
        the query from which values to insert are taken.
        If no query is provided then default values will be inserted.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.

    Example
    -------
    >>> from querybuilder.atoms.relations import Named as NamedRel
    >>> from querybuilder.atoms.columns import Constant, Named as NamedCol
    >>> from querybuilder.queries.dql import Values

    An Insert query can be instantiated as follow:
    >>> values = Values([(Constant(int, 42), Constant(str, "bar")),])
    >>> str(Insert(NamedRel("foo"), (NamedCol(int, "x"), NamedCol(str, "y")), values))
    "INSERT INTO foo (x, y) VALUES (42, 'bar')"

    The query can be omitted to insert default values:
    >>> str(Insert(NamedRel("foo"), (NamedCol(int, "x"), NamedCol(str, "y"))))
    'INSERT INTO foo (x, y) DEFAULT VALUES'

    """

    __slots__ = ("in_columns", "query")

    # TODO: implement alias and conflict

    def __init__(
        self,
        schema_relation: qb.atoms.relations.Named,
        in_columns: tuple[qb.atoms.columns.Named, ...] = (),
        query: Optional[qb.queries.dql.DQLQuery] = None,
        **kwargs,
    ):
        self.query = query
        self.in_columns = in_columns
        super().__init__(schema_relation, **kwargs)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for c in self.in_columns:
            accumulator = c.accept(accumulator)
        if self.query:
            accumulator = self.query.accept(accumulator)
        return accumulator

    def _get_subtokenize_kwargs(self, tokenizer):
        CW = qb.atoms.clauses.AliasedColumn
        in_columns = tuple(
            map(
                lambda c: CW(qb.helpers.columns.unqualify(c)).subtokenize(tokenizer),
                self.in_columns,
            )
        )

        if self.query:
            query = self.query.subtokenize(tokenizer)
        else:
            query = qb.atoms.clauses.DefaultValuesClause().subtokenize(tokenizer)

        return dict(
            query=query,
            in_columns=in_columns,
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def _substitute(self, substitutions: Mapping) -> Self:
        in_columns = tuple(c.substitute(substitutions) for c in self.in_columns)

        query = self.query and self.query.substitute(substitutions)

        self = self.buildfrom(self, in_columns=in_columns, query=query)

        return super()._substitute(substitutions)


class Delete(DMLQuery):
    """DELETE query class

    Parameters
    ----------
    schema_relation:
        the schema relation (TABLE or VIEW) in which to insert values.

    where: Optional qb.atoms.columns.Column
        condition on rows of schema_relation to delete.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.

    Examples
    -------

    >>> from querybuilder.atoms.relations import Named as NamedRel
    >>> from querybuilder.atoms.columns import Constant, Named as NamedCol

    >>> str(Delete(NamedRel("foo"), NamedCol(int, "x").eq(3)))
    'DELETE FROM foo WHERE x = 3'

    >>> str(Delete(NamedRel("foo")))
    'DELETE FROM foo'
    """

    __slots__ = ("where",)

    def __init__(
        self,
        schema_relation: qb.atoms.relations.Relation,
        where: Optional[qb.atoms.columns.Column] = None,
        **kwargs,
    ):
        self.where = where
        super().__init__(schema_relation, **kwargs)

    @property
    def context(self):
        ctxt = super().context
        if self.where:
            ctxt = ctxt.add_placeholders(*self.where.context.placeholders)
        return ctxt

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        if self.where:
            accumulator = self.where.accept(accumulator)
        return accumulator

    def _substitute(self, substitutions):
        where = self.where and self.where.substitute(substitutions)
        self = self.buildfrom(self, where=where)
        return super()._substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["schema_relation"] = self.schema_relation.subtokenize(tokenizer)
        WC = qb.atoms.clauses.WhereColumn
        if self.where:
            kwargs["where"] = WC(self.where).subtokenize(tokenizer)
        return kwargs


class Update(DMLQuery):
    """UPDATE query class

    Parameters
    ----------
    schema_relation: schema.Relation
        the schema relation (TABLE or VIEW) in which to insert values.

    set_columns: Mapping
        a nonempty Mapping, mapping columns of schema_relation to columns.
        Each key/value pair yields a `key = value` in the SET clause.

    where: columns.Object or None, default=None
        condition on rows of schema_relation to update.

    kwargs:    Mapping
        additional keyworded parameters for super initialization.

    Examples
    -------

    >>> from querybuilder.atoms.relations import Named as NamedRel
    >>> from querybuilder.atoms.columns import Constant, Named as NamedCol

    >>> set_cols = {
    ...     NamedCol(int, "x"): Constant(int, 3),
    ...     NamedCol(str, "y"): Constant(str, "bar"),
    ... }
    >>> str(Update(NamedRel("foo"), set_cols))
    "UPDATE foo SET x = 3, y = 'bar'"
    """

    __slots__ = ("where", "set_columns")

    def __init__(
        self,
        schema_relation: qb.atoms.relations.Relation,
        set_columns: Mapping[
            qb.atoms.columns.Named,
            qb.atoms.columns.Column | qb.atoms.pseudo_columns.Default,
        ],
        where: Optional[qb.atoms.columns.Column] = None,
        **kwargs,
    ):
        self.set_columns = set_columns
        self.where = where
        super().__init__(schema_relation=schema_relation, **kwargs)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for cold, cnew in self.set_columns.items():
            accumulator = cold.accept(accumulator)
            accumulator = cnew.accept(accumulator)
        if self.where:
            accumulator = self.where.accept(accumulator)

        return accumulator

    @classmethod
    def _state2hash(cls, state):
        set_columns = state.pop("set_columns")
        set_columns = tuple(set_columns.items())
        state["set_columns"] = set_columns
        return super()._state2hash(state)

    def __hash__(self):
        state = self.__getstate__()
        set_columns = state.pop("set_columns")
        set_columns = tuple(set_columns.items())

        state = tuple(state) + set_columns

        return hash(state)

    def _substitute(self, substitutions: Mapping) -> Self:
        where = self.where and self.where.substitute(substitutions)

        set_columns = {
            k.substitute(substitutions): v.substitute(substitutions)
            for k, v in self.set_columns.items()
        }
        self = self.buildfrom(self, where=where, set_columns=set_columns)

        return super()._substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["schema_relation"] = self.schema_relation.subtokenize(tokenizer)
        if self.where:
            WC = qb.atoms.clauses.WhereColumn
            kwargs["where"] = WC(self.where).subtokenize(tokenizer)
        SC = qb.atoms.clauses.SetColumnsClause
        kwargs["set_columns"] = SC(self.set_columns).subtokenize(tokenizer)

        return kwargs


class Truncate(DMLQuery):
    __slots__ = ()
