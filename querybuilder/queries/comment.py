from querybuilder.queries.queries import Query


class CommentQuery(Query):
    __slots__ = ()
    _is_readonly = True


class SingleComment(CommentQuery):
    """Inline comments

    Parameters
    ----------
    message: str
        the commented message

    Note
    ----
    If the message contains the newline char, then it will be splitted
    on this char, so that each line becomes a comment, prefixed by the
    inline comment delimiter.

    Examples
    --------
    >>> str(SingleComment("this is a comment"))
    '-- this is a comment\\n'
    >>> str(SingleComment("this are\\ntwo comments"))
    '-- this are\\n-- two comments\\n'
    """

    __slots__ = ("message",)

    def __init__(self, message, **kwargs):
        self.message = message
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(message=self.message, **super()._get_subtokenize_kwargs(tokenizer))


class MultilineComment(CommentQuery):
    """Multiline comments

    Parameters
    ----------
    messages: tuple of str
        the commented message lines

    Examples
    --------
    >>> str(MultilineComment("You could do", "that", "or that"))
    '/*\\n  You could do\\n  that\\n  or that\\n*/'
    """

    __slots__ = ("messages",)

    def __init__(self, *messages, **kwargs):
        # self.messages = '\n'.join(messages).split('\n')
        self.messages = messages
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            messages=self.messages, **super()._get_subtokenize_kwargs(tokenizer)
        )
