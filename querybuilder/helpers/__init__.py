from __future__ import annotations
import querybuilder.helpers.columns
import querybuilder.helpers.schema as schemas
from .columns import make_column, name_column, unqualify
from .schema import table, schema, ColumnSpec, Collector
import querybuilder as qb

colspec = ColumnSpec

Placeholder: type[qb.atoms.columns.Placeholder]
