from __future__ import annotations
from typing import Any, Callable, Optional, cast
from querybuilder.utils.constants import MISSING, _MISSING_TYPE
from querybuilder.atoms.columns import Column
import querybuilder as qb

Placeholder: type[qb.atoms.columns.Placeholder]
Star: type[qb.atoms.pseudo_columns.Star]


def make_column(
    value: Any | _MISSING_TYPE = MISSING,
    /,
    *,
    constant: Optional[bool] = None,
    sqltype: Optional[type] = None,
    sqltyper: Optional[Callable[[Any], Optional[type]]] = None,
    key: Optional[str] = None,
):  # -> qb.atoms.columns.Column | Callable[[Any], qb.atoms.columns.Column]
    """Converts a python object to a valid Value or Constant column

    Parameters
    ----------
    value: Any | _MISSING_TYPE, default=MISSING
        The value to convert.  If MISSING, then a preparametrized single-argument
        function decorator is returned.

    constant: Optional[bool], default=None
        Whether to produce a Constant (if True) or Value (if False) column.  If not
        given (None, by default), then the choice is made according to the type of
        `value` as follows: int, float, bool values as well as None are wrapped within
        a Constant column, while other kind of values are wrapped within a Value column
        in order to prevent SQL injections.

    sqltype: Optional[type], default=None
        Default sqltype for the column to generate in case `sqltyper` fails to return a
        type.

    sqltyper: Optional[Callable[[Any], Optional[type]]], default=None
        How to determine the sqltype of the column to produce from the given value.  If
        provided, the sqltype of the generated column will be obtained by the expression
        `sqltyper(value) or sqltype`, thus allowing a fallback to `sqltype`.  Otherwise,
        the expression `sqltype or type(value)` is used instead.  In each case, the
        expression should return a python `type`.

    key: Optional[str], default=None
        The key of the Value column that is produced, if any (ignored if a Constant is
        produced).

    """

    typer: Callable[[Any], type]
    if sqltyper is None and sqltype is None:
        typer = type
    elif sqltyper is None:

        def typer(_):
            return cast(type, sqltype)

    elif sqltype is not None:

        def typer(o):
            return cast(
                type, cast(Callable[[Any], type | None], sqltyper)(o) or sqltype
            )

    else:
        typer = cast(Callable[[Any], type], sqltyper)

    decorator: Callable[[Any], qb.atoms.columns.Constant | qb.atoms.columns.Value]
    if constant is None:

        def decorator(val: Any) -> qb.atoms.columns.Constant | qb.atoms.columns.Value:
            sqltype = typer(val)
            if val is True:
                return qb.atoms.columns.True_()
            elif val is False:
                return qb.atoms.columns.False_()
            elif val is None:
                return qb.atoms.columns.Null()
            constant = sqltype in {int, float, bool, type(None)}

            if constant:
                return qb.atoms.columns.Constant(sqltype, val)
            return qb.atoms.columns.Value(sqltype, val, key=key)

    elif constant:

        def decorator(val: Any) -> qb.atoms.columns.Constant:
            sqltype = typer(val)
            return qb.atoms.columns.Constant(sqltype, val)

    else:

        def decorator(val: Any) -> qb.atoms.columns.Value:
            sqltype = typer(val)
            return qb.atoms.columns.Value(sqltype, val, key=key)

    if value is MISSING:
        return decorator
    return decorator(value)


def name_column(
    column_or_type: qb.atoms.columns.Column | type,
    name: Optional[str] = None,
    relation_name: Optional[str] = None,
    schema_name: Optional[str] = None,
    preserve_table_column: bool = False,
) -> qb.atoms.columns.Column:
    if isinstance(column_or_type, type):
        column = Column(column_or_type)
    else:
        column = column_or_type
    if not name:
        name = getattr(column_or_type, "name", None)

    if preserve_table_column and isinstance(column, qb.atoms.columns.TableColumn):
        return column.buildfrom(
            column,
            name=name,
            relation_name=relation_name,
            schema_name=schema_name,
        )

    if name:
        return qb.atoms.columns.Named(
            sqltype=column.sqltype,
            name=name,
            relation_name=relation_name,
            schema_name=schema_name,
        )
    return column


def unqualify(column: qb.atoms.columns.Named):
    """Unqualifies a Named column."""
    # equivalent to: name_column(column), isn't it?
    return qb.atoms.columns.Named(column.sqltype, column.name)
