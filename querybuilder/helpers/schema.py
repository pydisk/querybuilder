from __future__ import annotations
from functools import partial, singledispatchmethod, wraps
from typing import (
    Any,
    Callable,
    ClassVar,
    Collection,
    ContextManager,
    Iterable,
    Iterator,
    Optional,
    NamedTuple,
    TypeVar,
    Union,
    cast,
    overload,
    TYPE_CHECKING,
)

from querybuilder.utils.constants import MISSING, _MISSING_TYPE
from querybuilder.helpers.columns import make_column, name_column
from querybuilder.atoms.columns import (
    Column,
    Constant as ConstantColumn,
    Named as NamedColumn,
    TableColumn,
    Value as ValueColumn,
)
from querybuilder.atoms.relations import (
    Named as NamedRelation,
    Prenamed as PrenamedRelation,
    Table,
    View,
)

from querybuilder.atoms.schemas import Schema, Index
import querybuilder as qb

if TYPE_CHECKING:
    T = TypeVar("T")


# Collectors
class _CollectedNT(NamedTuple):
    kind: str
    func: Callable
    name: Optional[str]
    order: Optional[int | float | str]


class Collected(_CollectedNT):
    kind_types: ClassVar[dict[str, type]] = {}
    order_type_levels: ClassVar[dict[type, int]] = {
        type(None): 0,
        int: 1,
        float: 1,
        str: 2,
    }
    order_kind_levels: ClassVar[dict[str, int]] = dict(
        primary_key=0, foreign_key=1, unique=2, check=3, exclude=4, view=0, index=1
    )

    @classmethod
    def _order_key(
        cls, ie: tuple[int, Collected]
    ) -> tuple[int, None | int | float | str, int, int, str]:
        i, e = ie
        otl = cls.order_type_levels
        okl = cls.order_kind_levels
        return (
            otl.get(e.order.__class__, 7),
            e.order,
            i,
            okl.get(e.kind, 7),
            e.name or "",
        )

    @staticmethod
    def __new__(
        cls,
        kind: str,
        func: Callable,
        name: Optional[str] = None,
        order: Optional[int | float | str] = None,
    ) -> Collected:
        name = name or dict(_=None).get(func.__name__, func.__name__)
        return _CollectedNT.__new__(cls, kind=kind, func=func, name=name, order=order)

    @classmethod
    def new_of_kind(cls, kind):
        @overload
        def decored(
            func: None = None,
            name: Optional[str] = None,
            order: Optional[int | float | str] = None,
        ) -> Callable[[Callable], Collected]: ...

        @overload
        def decored(
            func: Callable,
            name: Optional[str] = None,
            order: Optional[int | float | str] = None,
        ) -> Collected: ...

        def decored(
            func: Optional[Callable] = None,
            name: Optional[str] = None,
            order: Optional[int | float | str] = None,
        ) -> Callable[[Callable], Collected] | Collected:
            if func is None:
                return lambda func: decored(func, name, order)
            return cls(kind, func, name, order)

        return decored

    def call(self, *args) -> qb.atoms.constraints.TableConstraint | View | Index:
        typ = self.kind_types[self.kind]

        if issubclass(typ, (View, Index)):
            assert len(args) == 1
            res = self.func(*args)
        elif issubclass(typ, qb.atoms.constraints.TableForeignKey):
            assert len(args) == 2
            res = self.func(*args)
        else:
            res = self.func(args[0])

        return self._post_call(res, typ)

    @singledispatchmethod
    def _post_call(self, res, typ):
        raise TypeError(res.__class__)


class Collector(Collection[Collected], ContextManager):
    __slots__: tuple[str, ...] = ("elements", "names", "kinds")

    def __init__(
        self, elements: Iterable[Collected] = (), /, *, kinds: Iterable[str] = ()
    ):
        elements = [
            ie[1] for ie in sorted(enumerate(elements), key=Collected._order_key)
        ]
        names = set()
        for e in elements:
            if e.name in names:
                raise ValueError(f"Got several elements named {e.name}")
            elif e.name:
                names.add(e.name)
        self.elements = elements
        self.names = names
        self.kinds = set(kinds)

    def __iter__(self) -> Iterator[Collected]:
        return iter(self.elements)

    def __len__(self) -> int:
        return len(self.elements)

    def __contains__(self, elt) -> bool:
        if not isinstance(elt, Collected):
            return False
        _, r = self._dichotomic_search(elt)
        return r is not None

    def __repr__(self) -> str:
        return f"{self.__class__}({self.elements!r}, kinds={self.kinds!r})"

    def __dir__(self) -> list[str]:
        d = set(super().__dir__())
        d.update(self.kinds)
        return list(d)

    def __getattr__(self, attr):
        if attr in self.kinds:
            return partial(self.register, attr)
        raise AttributeError(attr)

    def __enter__(self):
        return self

    def __exit__(self, exc, exct, tb):
        pass

    def _dichotomic_search(self, elt: Collected) -> tuple[int, Optional[Collected]]:
        b, e = 0, len(self)
        while b < e:
            n = (b + e) // 2
            r = self.elements[n]
            if r == elt:
                return n, r
            if Collected._order_key((n, r)) <= Collected._order_key((n, elt)):
                b, e = n + 1, e
            else:
                b, e = b, n
        return b, None

    def add(self, collected: Collected):
        i, r = self._dichotomic_search(collected)
        if r is not None:
            raise ValueError(f"Got several times the collected element {collected}")
        if collected.name:
            if collected.name in self.names:
                raise ValueError(f"Got several elements named {collected.name}")
            self.names.add(collected.name)
        self.elements.insert(i, collected)

    @overload
    def register(
        self,
        kind: str,
        func: None = None,
        /,
        *,
        name: Optional[str] = None,
        order: Optional[int | float | str] = None,
    ) -> Callable[[Callable], Collected]: ...

    @overload
    def register(
        self,
        kind: str,
        func: Callable,
        /,
        *,
        name: Optional[str] = None,
        order: Optional[int | float | str] = None,
    ) -> Collected: ...

    def register(
        self,
        kind: str,
        func: Optional[Callable] = None,
        /,
        *,
        name: Optional[str] = None,
        order: Optional[int | float | str] = None,
    ) -> Callable[[Callable], Collected] | Collected:
        def subdecorator(func: Callable) -> Collected:
            f = Collected(kind, func, name=name, order=order)
            self.add(f)
            return f

        if func:
            return subdecorator(func)
        return subdecorator


# Specifications
class ColumnSpec:
    __slots__ = (
        "sqltype",
        "name",
        "relation_name",
        "schema_name",
        "primary_key",
        "references",
        "unique",
        "not_null",
        "check",
        "default",
        "generated_as_identity",
        "generated_always_as",
    )

    def __init__(
        self,
        sqltype_or_references: Optional[type | str | NamedColumn] = None,
        /,
        name: Optional[str] = None,
        relation_name: Optional[str] = None,
        schema_name: Optional[str] = None,
        *,
        sqltype: Optional[type] = None,
        references: Optional[
            str
            | NamedColumn
            | Callable[
                [PrenamedRelation, Optional[Schema]], NamedColumn | tuple | dict | str
            ]
        ] = None,
        primary_key: bool | str = False,
        unique: bool | str = False,
        not_null: bool | str = False,
        check: Optional[
            str | Column | Callable[[NamedColumn, Optional[PrenamedRelation]], Column]
        ] = None,
        default: Any | _MISSING_TYPE = MISSING,
        generated_as_identity: bool | str = False,
        generated_always_as: Optional[
            str | Column | Callable[[NamedRelation], Column]
        ] = None,
    ):
        if sqltype_or_references is None:
            assert sqltype or references
        elif isinstance(sqltype_or_references, str):
            assert not references
            references = sqltype_or_references
        elif isinstance(sqltype_or_references, Column):
            assert not references
            references = sqltype_or_references
            if sqltype:
                assert sqltype == sqltype_or_references.sqltype
            else:
                sqltype = references.sqltype
        else:
            sqltype = sqltype_or_references
        self.sqltype = sqltype
        self.name = name
        self.relation_name = relation_name
        self.schema_name = schema_name
        self.primary_key = primary_key
        self.references = references
        self.unique = unique
        self.not_null = not_null
        self.check = check
        self.default = default
        self.generated_as_identity = generated_as_identity
        self.generated_always_as = generated_always_as

    def to_named_column(
        self: NamedColumn | ColumnSpec,
        relation_name: str | None | _MISSING_TYPE = MISSING,
        schema_name: str | None | _MISSING_TYPE = MISSING,
    ) -> NamedColumn:
        assert self.name
        assert self.sqltype
        if relation_name is MISSING:
            relation_name = self.relation_name
        elif relation_name is None:
            schema_name = None
        relation_name = cast(Optional[str], relation_name)
        if schema_name is MISSING:
            schema_name = self.schema_name
        schema_name = cast(Optional[str], schema_name)
        if relation_name:
            assert not self.relation_name or self.relation_name == relation_name
        if schema_name:
            assert relation_name
            assert not self.schema_name or self.schema_name == schema_name

        return NamedColumn(self.sqltype, self.name, relation_name, schema_name)

    @classmethod
    def _resolve_str_spec(
        cls,
        column: str,
        relation: Optional[PrenamedRelation] = None,
        schema: Optional[Schema] = None,
        sqltype: Optional[type] = None,
    ) -> NamedColumn:
        col = column.split(".")
        found = cls._get_column(*col, relation=relation, schema=schema, default=None)
        if found:
            if sqltype:
                assert found.sqltype == sqltype
            sqltype = found.sqltype
            col = [e for e in (found.schema_name, found.relation_name, found.name) if e]
        elif not sqltype:
            raise ValueError(f"No column {column} found in given relation/schema")
        return NamedColumn(sqltype, *reversed(col))

    @classmethod
    def _get_column(
        cls,
        *col: str,
        relation: Optional[PrenamedRelation],
        schema: Optional[Schema] = None,
        default: Optional[T] = None,
    ) -> NamedColumn | Optional[T]:
        if len(col) == 1:
            if not relation:
                return default
            return getattr(relation.columns, col[0], default)
        elif len(col) == 2:
            if schema:
                relation = getattr(schema.relations, col[0], None)
            if not relation or relation.name != col[0]:
                return default
            return cls._get_column(*col[1:], relation=relation, default=default)
        elif len(col) == 3:
            if schema and schema.name == col[0]:
                relation = getattr(schema.relations, col[1], relation)
            if not relation or relation.schema_name != col[0]:
                relation = None
            return cls._get_column(*col[1:], relation=relation, default=default)
        raise ValueError(col)

    def to_table_column(
        self: NamedColumn | ColumnSpec,
        relation: Optional[NamedRelation] = None,
        schema: Optional[Schema] = None,
    ) -> TableColumn:
        # Get correct names
        assert self.name

        if self.relation_name:
            relation_name = self.relation_name
            assert not relation or relation.name == relation_name
        else:
            assert relation
            relation_name = relation.name

        schema_name: Optional[str]
        if self.schema_name:
            schema_name = self.schema_name
            assert not relation or relation.schema_name == schema_name
            assert not schema or schema.name == schema_name
        elif relation:
            schema_name = relation.schema_name
            assert not schema or schema.name == schema_name
        elif schema:
            schema_name = schema.name
        else:
            schema_name = None

        # Hack for allowing call on NamedColumn (not ColumnSpec)
        if isinstance(self, NamedColumn):
            return TableColumn.buildfrom(
                self, relation_name=relation_name, schema_name=schema_name
            )

        # Build table column
        sqltype = self.sqltype
        constraints: list[qb.atoms.constraints.ColumnConstraint] = []
        if relation:
            # Make anonymous copy of relation
            rel = PrenamedRelation(
                columns=map(name_column, relation.columns),
                name=relation_name,
                schema_name=schema_name,
            )

        if isinstance(self.primary_key, str):
            constraints.append(
                qb.atoms.constraints.ColumnPrimaryKey(name=self.primary_key)
            )
        elif self.primary_key:
            constraints.append(qb.atoms.constraints.ColumnPrimaryKey())

        ref: Optional[
            str | NamedColumn | qb.atoms.constraints.ColumnReferences | tuple | dict
        ]
        if callable(self.references):
            assert relation
            ref = self.references(rel, schema)
        else:
            ref = self.references
        if ref is not None:
            cstr = self._resolve_references(
                ref, relation=rel, schema=schema, sqltype=self.sqltype
            )
            if sqltype:
                assert cstr.refcolumn.sqltype == sqltype
            else:
                sqltype = cstr.refcolumn.sqltype
            constraints.append(cstr)

        if isinstance(self.unique, str):
            constraints.append(qb.atoms.constraints.ColumnUnique(name=self.unique))
        elif self.unique:
            constraints.append(qb.atoms.constraints.ColumnUnique())

        if isinstance(self.not_null, str):
            constraints.append(qb.atoms.constraints.ColumnNotNull(name=self.not_null))
        elif self.not_null:
            constraints.append(qb.atoms.constraints.ColumnNotNull())

        check: Optional[str | Column | qb.atoms.constraints.ColumnCheck | tuple | dict]
        if callable(self.check):
            check = self.check(self.to_named_column(relation_name=None), rel)
        else:
            check = self.check
        if check is not None:
            if isinstance(check, str):
                constraints.append(
                    qb.atoms.constraints.ColumnCheck(
                        self._resolve_str_spec(check, rel, schema)
                    )
                )
            elif isinstance(check, tuple):
                constraints.append(qb.atoms.constraints.ColumnCheck(*check))
            elif isinstance(check, dict):
                constraints.append(qb.atoms.constraints.ColumnCheck(**check))
            elif isinstance(check, Column):
                constraints.append(qb.atoms.constraints.ColumnCheck(check))
            elif isinstance(check, qb.atoms.constraints.ColumnCheck):
                constraints.append(check)
            else:
                raise TypeError(check.__class__)

        default: Union[
            _MISSING_TYPE,
            ConstantColumn,
            ValueColumn,
            tuple,
            dict,
            qb.atoms.constraints.ColumnDefault,
        ]
        if callable(self.default):
            # TODO: when is a callable default useful?
            default = self.default(self.to_named_column())
        else:
            default = self.default
        if default is not MISSING:
            if isinstance(default, (ValueColumn, ConstantColumn)):
                constraints.append(qb.atoms.constraints.ColumnDefault(default))
            elif isinstance(default, tuple):
                constraints.append(qb.atoms.constraints.ColumnDefault(*default))
            elif isinstance(default, dict):
                constraints.append(qb.atoms.constraints.ColumnDefault(**default))
            elif isinstance(default, qb.atoms.constraints.ColumnDefault):
                constraints.append(default)
            elif default is not MISSING:
                constraints.append(
                    qb.atoms.constraints.ColumnDefault(
                        make_column(default, constant=True, sqltype=self.sqltype)
                    )
                )
            else:
                raise TypeError(default.__class__)

        if isinstance(self.generated_as_identity, str):
            constraints.append(
                qb.atoms.constraints.ColumnGeneratedAsIdentity(
                    name=self.generated_as_identity
                )
            )
        elif self.generated_as_identity:
            constraints.append(qb.atoms.constraints.ColumnGeneratedAsIdentity())

        if self.generated_always_as is not None:
            # TODO
            raise NotImplementedError()

        assert self.sqltype
        return TableColumn(
            self.sqltype,
            name=self.name,
            relation_name=relation_name,
            schema_name=schema_name,
            constraints=constraints,
        )

    @classmethod
    def _resolve_references(
        cls,
        ref: str | NamedColumn | qb.atoms.constraints.ColumnReferences | tuple | dict,
        relation: Optional[PrenamedRelation] = None,
        schema: Optional[Schema] = None,
        sqltype: Optional[type] = None,
    ) -> qb.atoms.constraints.ColumnReferences:
        if isinstance(ref, qb.atoms.constraints.ColumnReferences):
            return ref

        cstr: qb.atoms.constraints.ColumnReferences
        referenced: str | NamedColumn
        args: tuple = ()
        kwargs: dict = {}
        if isinstance(ref, (str, NamedColumn)):
            referenced = ref
        elif isinstance(ref, tuple):
            assert ref and isinstance(ref[0], (str, NamedColumn))
            referenced = ref[0]
            args = ref[1:]
        elif isinstance(ref, dict):
            assert isinstance(ref.get("refcolumn"), (str, NamedColumn))
            kwargs = dict(ref)
            referenced = kwargs.pop("refcolumn")
        else:
            raise TypeError(ref.__class__)

        refcol: NamedColumn
        if isinstance(referenced, str):
            refcol = cls._resolve_str_spec(
                referenced, relation, schema, sqltype=sqltype
            )
        else:
            refcol = referenced

        cstr = qb.atoms.constraints.ColumnReferences(refcol, *args, **kwargs)

        return cstr


class TableSpec:
    __slots__ = ("columns", "_name", "_schema_name", "collector")
    collectables = {
        "primary_key",
        "foreign_key",
        "check",
        "unique",
    }  # TODO: add "exclude"

    @property
    def name(self) -> Optional[str]:
        return self._name

    @name.setter
    def name(self, value: Optional[str]):
        self._rename(name=value)

    @property
    def schema_name(self) -> Optional[str]:
        return self._schema_name

    @schema_name.setter
    def schema_name(self, value: Optional[str]):
        self._rename(schema_name=value)

    def _rename(
        self,
        *,
        name: str | None | _MISSING_TYPE = MISSING,
        schema_name: str | None | _MISSING_TYPE = MISSING,
    ):
        if name is MISSING:
            name = getattr(self, "name", None)
        if schema_name is MISSING:
            schema_name = getattr(self, "schema_name", None)
        if TYPE_CHECKING:
            name = cast(Optional[str], name)
            schema_name = cast(Optional[str], schema_name)
        self._name = name
        self._schema_name = schema_name
        for c in self.columns:
            if isinstance(c, ColumnSpec):
                c.relation_name = c.relation_name or name
                c.schema_name = c.schema_name or schema_name
            assert c.relation_name == name
            assert c.schema_name == schema_name

    def __init__(
        self,
        name: Optional[str] = None,
        columns: Iterable[NamedColumn | ColumnSpec] = (),
        schema_name: Optional[str] = None,
        collecteds: Iterable[Collected] = (),
    ):
        self.columns = list(columns)
        self._name = name
        self.schema_name = schema_name
        self.collector = Collector(collecteds)

    def to_prenamed_relation(self, schema_name: Optional[str] = None):
        assert self.name
        return PrenamedRelation(
            columns=(name_column(ColumnSpec.to_named_column(c)) for c in self.columns),
            name=self.name,
            schema_name=self.schema_name or schema_name,
        )

    def to_named_relation(
        self: TableSpec | Table, schema_name: Optional[str] = None
    ) -> NamedRelation:
        assert self.name
        columns = (ColumnSpec.to_named_column(c) for c in self.columns)
        assert (
            not self.schema_name or not schema_name or self.schema_name == schema_name
        )
        return NamedRelation(
            columns=columns, name=self.name, schema_name=self.schema_name or schema_name
        )

    def to_table(self: TableSpec | Table, schema: Optional[Schema] = None) -> Table:
        if self.schema_name:
            schema_name = self.schema_name
            assert not schema or schema_name == schema.name
        elif schema:
            schema_name = schema.name
        else:
            schema_name = None
        if isinstance(self, Table):
            if self.schema_name != schema_name:
                return self.buildfrom(self, schema_name=schema_name)
            return self
        relation = self.to_prenamed_relation(schema_name=schema_name)
        columns: list[TableColumn] = [
            ColumnSpec.to_table_column(c, relation=relation, schema=schema)
            for c in self.columns
        ]
        constraints: list[qb.atoms.constraints.TableConstraint] = []
        for cons in self.collector:
            assert cons.kind in self.collectables
            res = cons.call(relation, schema)
            assert isinstance(res, qb.atoms.constraints.TableConstraint)
            constraints.append(res)
        return Table(
            columns=columns,
            name=self.name,
            schema_name=schema_name,
            constraints=constraints,
        )

    @overload
    @classmethod
    def fromclass(
        cls,
        klass: None = None,
        /,
        *,
        name: Optional[str] = None,
        schema_name: Optional[str] = None,
        schema: Optional[Schema] = None,
    ) -> Callable[[type], TableSpec]: ...

    @overload
    @classmethod
    def fromclass(
        cls,
        klass: type,
        /,
        *,
        name: Optional[str] = None,
        schema_name: Optional[str] = None,
        schema: Optional[Schema] = None,
    ) -> TableSpec: ...

    @classmethod
    def fromclass(
        cls,
        klass: Optional[type] = None,
        /,
        *,
        name: Optional[str] = None,
        schema_name: Optional[str] = None,
        schema: Optional[Schema] = None,
    ) -> TableSpec | Callable[[type], TableSpec]:
        if klass is None:
            return lambda klass: cls.fromclass(
                klass, name=name, schema_name=schema_name, schema=schema
            )

        if schema:
            if schema_name:
                assert schema_name == schema.name
            else:
                schema_name = schema.name

        name = name or klass.__name__

        columns: list[TableColumn | ColumnSpec] = []
        for k, v in getattr(klass, "__annotations__", {}).items():
            if (
                isinstance(v, TableColumn)
                and v.relation_name == name
                and v.schema_name == schema_name
                and not any(c.name == v.name for c in columns)
            ):
                columns.append(v)
                continue

            if isinstance(v, (type, str, NamedColumn)):
                v = ColumnSpec(v, name=k)

            if not isinstance(v, ColumnSpec):
                raise ValueError((k, v))

            v.name = v.name or k
            if v.sqltype is not None:
                columns.append(v)
                continue

            assert v.references
            if isinstance(v.references, Column):
                c = v.references
            elif isinstance(v.references, qb.atoms.constraints.ColumnReferences):
                c = v.references.refcolumn
            else:
                relation = cls(
                    name=name, schema_name=schema_name, columns=columns
                ).to_prenamed_relation(schema_name=schema_name)
                ref: Union[
                    str,
                    tuple,
                    dict,
                    NamedColumn,
                    qb.atoms.constraints.ColumnReferences,
                ]
                if callable(v.references):
                    ref = v.references(relation, schema)
                else:
                    ref = v.references
                c = ColumnSpec._resolve_references(
                    ref, relation=relation, schema=schema
                ).refcolumn
            v.sqltype = c.sqltype
            assert v.sqltype
            columns.append(v)

        collectors: list[Collector] = []
        collecteds: list[Collected] = []
        for k, v in klass.__dict__.items():
            if k.startswith("__") and k.endswith("__"):
                continue
            if isinstance(v, staticmethod):
                v = v.__func__
            if isinstance(v, Collector):
                collectors.append(v)
                collecteds.extend(v)
            elif isinstance(v, Collected):
                if any(v in coll for coll in collectors):
                    continue
                collecteds.append(v)
            # ignore other type of k/v, so that they can be used as auxiliary function
            # or constants.

        spec = cls(
            name=name, schema_name=schema_name, columns=columns, collecteds=collecteds
        )

        return spec


class SchemaSpec:
    __slots__ = ("tables", "_name", "collector")
    collectables = {"view", "index"}

    @property
    def name(self) -> Optional[str]:
        return self._name

    @name.setter
    def name(self, value: Optional[str]):
        self._name = value
        for t in self.tables:
            if isinstance(t, TableSpec):
                t.schema_name = value
            assert t.schema_name == value

    def __init__(
        self,
        name: Optional[str],
        tables: Iterable[Table | TableSpec] = (),
        collecteds: Iterable[Collected] = (),
    ):
        self.tables = list(tables)
        self.name = name
        self.collector = Collector(collecteds)

    def to_preschema(self) -> Schema:
        assert self.name
        objects: list[NamedRelation] = [
            TableSpec.to_named_relation(t, schema_name=self.name) for t in self.tables
        ]
        return Schema(self.name, objects=objects)

    def to_schema(self) -> Schema:
        assert self.name
        preschema = self.to_preschema()
        objects: list[NamedRelation | View | Index] = [
            TableSpec.to_table(t, preschema) for t in self.tables
        ]
        preschema_outdated = False
        for f in self.collector:
            if f.order is not None and preschema_outdated:
                preschema = Schema(self.name, objects=objects)
            assert f.kind in self.collectables
            res = f.call(preschema)
            res = res.buildfrom(res, schema_name=self.name)
            objects.append(cast(Union[NamedRelation, View, Index], res))
            preschema_outdated = True
        if not self.collector or preschema_outdated:
            preschema = Schema(self.name, objects=objects)
        return preschema

    @overload
    @classmethod
    def fromclass(
        cls, klass: None = None, /, *, name: Optional[str] = None
    ) -> Callable[[type], SchemaSpec]: ...

    @overload
    @classmethod
    def fromclass(cls, klass: type, /, *, name: Optional[str] = None) -> SchemaSpec: ...

    @classmethod
    def fromclass(
        cls, klass: Optional[type] = None, /, *, name: Optional[str] = None
    ) -> SchemaSpec | Callable[[type], SchemaSpec]:
        if klass is None:
            return lambda klass: cls.fromclass(klass, name=name)

        name = name or klass.__name__
        tables: list[Table | TableSpec] = []
        collecteds: list[Collected] = []
        for k, v in klass.__dict__.items():
            if k.startswith("__") and k.endswith("__"):
                continue
            elif isinstance(v, Collector):
                collecteds.extend(v)
            elif isinstance(v, Collected):
                collecteds.append(v)
            elif isinstance(v, Table):
                if not v.schema_name:
                    v = v.buildfrom(v, schema_name=name)
                else:
                    assert v.schema_name == name
                tables.append(v)
            elif isinstance(v, TableSpec):
                v.name = v.name or k
                v.schema_name = v.schema_name or name
                assert v.schema_name == name
                tables.append(v)
            elif isinstance(v, type):
                schema = cls(
                    name=name, tables=tables, collecteds=collecteds
                ).to_preschema()
                v = TableSpec.fromclass(v, name=k, schema_name=name, schema=schema)
                tables.append(v)
            # ignore other type of k/v, so that they can be used as auxiliary function
            # or constants.

        spec = cls(name=name, tables=tables, collecteds=collecteds)

        return spec


# table decorator
@overload
def table(
    cls: None = None,
    /,
    *,
    name: Optional[str] = None,
    schema_name: Optional[str] = None,
) -> Callable[[type], Table]: ...


@overload
def table(
    cls: type, /, *, name: Optional[str] = None, schema_name: Optional[str] = None
) -> Table: ...


@wraps(TableSpec.fromclass)
def table(
    cls: Optional[type] = None,
    /,
    *,
    name: Optional[str] = None,
    schema_name: Optional[str] = None,
) -> Table | Callable[[type], Table]:
    if cls is None:
        return lambda klass: table(klass, name=name, schema_name=schema_name)
    tspec = TableSpec.fromclass(cls, name=name, schema_name=schema_name)
    return tspec.to_table()


for kind in TableSpec.collectables:
    setattr(table, kind, Collected.new_of_kind(kind))
setattr(table, "collector", partial(Collector, kinds=TableSpec.collectables))


# schema decorator
@overload
def schema(
    cls: None = None, /, *, name: Optional[str] = None
) -> Callable[[type], Schema]: ...


@overload
def schema(cls: type, /, *, name: Optional[str] = None) -> Schema: ...


@wraps(SchemaSpec.fromclass)
def schema(
    cls: Optional[type] = None, /, *, name: Optional[str] = None
) -> Schema | Callable[[type], Schema]:
    if cls is None:
        return lambda klass: schema(klass, name=name)

    sspec = SchemaSpec.fromclass(cls, name=name)
    return sspec.to_schema()


for kind in SchemaSpec.collectables:
    setattr(schema, kind, Collected.new_of_kind(kind))
setattr(schema, "collector", partial(Collector, kinds=SchemaSpec.collectables))
setattr(schema, "table", table)
