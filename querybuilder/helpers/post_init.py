from functools import singledispatchmethod
from typing import cast
from querybuilder.helpers.schema import Collected
import querybuilder as qb

Collected.kind_types.update(
    primary_key=qb.atoms.constraints.TablePrimaryKey,
    foreign_key=qb.atoms.constraints.TableForeignKey,
    unique=qb.atoms.constraints.TableUnique,
    check=qb.atoms.constraints.TableCheck,
    view=qb.atoms.relations.View,
    index=qb.atoms.schemas.Index,
)

register = cast(singledispatchmethod, Collected._post_call).register


@register(qb.atoms.constraints.TableConstraint)
@register(qb.atoms.relations.View)
@register(qb.atoms.schemas.Index)
def _(self, res, typ):
    return res


@register(tuple)
@register(list)
def _(self, res, typ):
    assert res
    kwargs = {}
    if issubclass(typ, qb.atoms.relations.View):
        if not isinstance(res[0], str):
            res = (self.name, *res)
    elif issubclass(typ, qb.atoms.schemas.Index):
        if len(res) < 3:
            res = (*res, self.name)
    elif issubclass(typ, qb.atoms.constraints.TableConstraint):
        kwargs["name"] = self.name
    return typ(*res, **kwargs)


@register(dict)
def _(self, res, typ):
    res.setdefault("name", self.name)
    return typ(**res)


@register(qb.atoms.columns.Column)
def _(self, res, typ):
    assert issubclass(typ, qb.atoms.constraints.TableConstraint)
    assert not issubclass(type, qb.atoms.constraints.TableForeignKey)
    return self._post_call((res,), typ)


@register(qb.queries.dql.DQLQuery)
def _(self, res, typ):
    # TODO: allow TableAs here
    assert issubclass(typ, qb.atoms.relations.View)
    return self._post_call((res,), typ)


qb.helpers.columns.Placeholder = qb.atoms.columns.Placeholder
qb.helpers.columns.Star = qb.atoms.pseudo_columns.Star
qb.helpers.Placeholder = qb.atoms.columns.Placeholder

del _, register
