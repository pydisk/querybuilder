from __future__ import annotations
from datetime import date, datetime, time, timedelta
from functools import singledispatchmethod, update_wrapper
from typing import (
    Any,
    Callable,
    Iterable,
    List,
    Optional,
    TypeVar,
    Union,
    cast,
    Tuple,
)
from typing_extensions import Concatenate, ParamSpec
from uuid import UUID

from querybuilder.utils.decorators import TypeDispatch
from querybuilder.formatting.tokentree import TkStr, TkSeq, TkTree, TkInd
from querybuilder.formatting.preformatter import transform, flatten, append_to_tree
import querybuilder.utils.constants as qbconstants
from querybuilder.formatting import token as qbtoken
import querybuilder.formatting.paramstyle as qbparamstyle

import querybuilder.atoms.atoms as atoms
import querybuilder.atoms.relations as relations
import querybuilder.atoms.pseudo_columns as qbpseudo_columns
import querybuilder.atoms.columns as qbcolumns
import querybuilder.atoms.clauses as clauses
import querybuilder.atoms.schemas as qbschemas
import querybuilder.atoms.constraints as qbconstraints
import querybuilder.queries.dql as dql
import querybuilder.queries.dml as dml
import querybuilder.queries.ddl as ddl
import querybuilder.queries.tcl as tcl
import querybuilder.queries.comment as comment
import querybuilder.visitors as qbvisitors

P = ParamSpec("P")
Tknzr = TypeVar("Tknzr", bound="Tokenizer")


def name_and_defer(
    func: Callable[Concatenate[Tknzr, P], TkTree],
) -> Callable[Concatenate[Tknzr, P], TkTree]:
    def f(
        tokenizer: Tknzr,
        *args: P.args,
        name: Optional[str] = None,
        defer: Optional[str] = None,
        **kwargs: P.kwargs,
    ) -> TkTree:
        newtree: List[Any] = []
        if name:
            naming = tokenizer.tokenize_keyword("CONSTRAINT", finalspace=True)
            naming += tokenizer.tokenize_name(name=name)
            newtree.append(naming)
            newtree.append(TkInd())

        newtree.append(func(tokenizer, *args, **kwargs))
        if defer:
            newtree.append(tokenizer.tokenize_keyword(defer))
        return tuple(newtree)

    update_wrapper(f, func)
    return f


class Tokenizer:
    __slots__ = ()
    paramstyle: qbparamstyle.KeyedParamStyle = qbparamstyle.NamedParamStyle()
    anonymous_paramstyle: qbparamstyle.UnkeyedParamStyle = (
        qbparamstyle.QmarkParamStyle()
    )

    def tokenize_name(
        self,
        name: Optional[str] = None,
        relation_name: Optional[str] = None,
        schema_name: Optional[str] = None,
    ) -> TkSeq:
        rawnames: Iterable[str] = filter(None, (schema_name, relation_name, name))
        tknames: Iterable[TkSeq] = (TkStr(qbtoken.Name, n).to_seq() for n in rawnames)
        sep = TkStr(qbtoken.Punctuation, ".").to_seq()
        return flatten(self.tokenize_list(sep, tknames))

    def tokenize_keyword(
        self, keyword, initialspace: bool = False, finalspace: bool = False
    ) -> TkSeq:
        rawkeywords = keyword.split(" ")
        tkkeywords: Iterable[TkSeq] = (
            TkStr(qbtoken.Keyword, k).to_seq() for k in rawkeywords
        )
        spaced_keywords = flatten(tuple(tkkeywords))
        tspace = TkStr(qbtoken.Whitespace, " ").to_seq()
        if initialspace:
            spaced_keywords = tspace + spaced_keywords
        if finalspace:
            spaced_keywords = spaced_keywords + tspace
        return spaced_keywords

    def tokenize_placeholder_key(
        self, key: Optional[str], anonymous_index: Optional[int] = None
    ) -> TkSeq:
        if key is None and anonymous_index is not None:
            key = f"auto{anonymous_index}"
        if key is None:
            fmt = self.anonymous_paramstyle()
        else:
            fmt = self.paramstyle(key)
        return TkStr(qbtoken.Token.Name.Variable, fmt).to_seq()

    def tokenize_jointype(self, jointype, natural) -> TkSeq:
        kw = f"{jointype.value} JOIN"
        if natural:
            kw = f"NATURAL {kw}"
        return self.tokenize_keyword(kw)

    def tokenize_operator(self, operator) -> TkSeq:
        return TkStr(qbtoken.Token.Operator, operator).to_seq()

    def tokenize_combinator(self, combinator) -> TkSeq:
        return TkStr(qbtoken.Token.Keyword, combinator).to_seq()

    def tokenize_transaction_characteristics(
        self, isolation_level: Optional[str], read_only: Optional[bool]
    ) -> TkTree:
        newtree = []
        if isolation_level:
            newtree.append(self.tokenize_keyword(f"ISOLATION LEVEL {isolation_level}"))
        if read_only is not None:
            newtree.append(
                self.tokenize_keyword(f"READ {read_only and 'ONLY' or 'WRITE'}")
            )
        if not newtree:
            return ()
        return (TkInd(), tuple(newtree))

    def tokenize_function(self, function) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Function, function).to_seq()

    def tokenize_aggregator(self, aggregator) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Function.Aggregator, aggregator).to_seq()

    def tokenize_comment(self, string="", /) -> TkSeq:
        return TkStr(qbtoken.Comment, f"-- {string}").to_seq()

    def tokenize_value(self, value: Any, with_comment_marker: bool = False) -> TkSeq:
        seq: List[TkStr] = []
        if with_comment_marker:
            seq.extend(flatten(self.tokenize_comment()))
        seq.append(TkStr(qbtoken.Comment.Args.Arrow, "←"))
        if value is qbconstants.MISSING:
            seq.append(TkStr(qbtoken.Comment.Args.Value.MISSING, "ø"))
        else:
            seq.append(TkStr(qbtoken.Comment.Args.Value, repr(value)))
        return TkSeq(seq)

    def tokenize_values(self, values: qbvisitors.stores.PlaceholderStore) -> TkTree:
        if values:
            keyed_map = values.to_keyed_map(autokey=lambda i, j: str(j))
            valued_map = values.to_valued_map(keyed_map)
            valued_map_repr = ", ".join(
                f"{k}: {'ø' if v is qbconstants.MISSING else repr(v)}"
                for k, v in valued_map.items()
            )
            return self.tokenize_comment(f"↖{{{valued_map_repr}}}")
        return ()

    def tokenize_list(
        self, separator: TkSeq, values: Iterable[TkTree], accolate: bool = False
    ) -> tuple[TkTree, ...]:
        if not values:
            return tuple(values)

        values_list = list(values)
        out = []
        for e in values_list[:-1]:
            if accolate:
                out.append(append_to_tree(e, separator))
            else:
                out.append(e)
                out.append(separator)
        out.append(values_list[-1])
        return tuple(out)

    #########
    # Scope #
    #########

    # TODO: rename function
    def scope_it(
        self,
        tktree: TkTree,
        left: TkTree = TkStr(qbtoken.Punctuation, "(").to_seq(),
        right: TkTree = TkStr(qbtoken.Punctuation, ")").to_seq(),
        noindent: bool = False,
    ) -> tuple[TkTree, ...]:
        if noindent:
            return (left, tktree, right)
        return (left, (TkInd(), tktree), right)

    def unscope_it(
        self,
        tktree: TkTree,
    ) -> TkTree:
        """Remove outermost parentheses.

        Useful when drivers require to not scope in SQL-scopable places.
        For instance, "ORDER BY column" with SQLite must not be scopped while
        the standard ask for it to be scopped.
        """
        if not len(tktree) == 3:
            return tktree
        try:
            left, tktree_center, right = tktree
        except ValueError:
            return tktree
        if not len(left) == 1 or not len(right) == 1:
            return tktree
        if left[0] != TkStr(qbtoken.Punctuation, "("):
            return tktree
        if right[0] != TkStr(qbtoken.Punctuation, ")"):
            return tktree
        return cast(TkTree, tktree_center)

    ###################################

    @singledispatchmethod
    def __call__(self, obj, /, **kwargs) -> TkTree:
        raise TypeError(obj)

    ###############
    # DQL QUERIES #
    ###############

    @__call__.register(dql.Values)
    def _(
        self,
        obj: dql.Values,
        /,
        *,
        values: Iterable[TkTree] = (),
        orderby: TkTree = (),
        offset: TkTree = (),
        limit: TkTree = (),
    ) -> TkTree:
        tvalues = self.tokenize_keyword("VALUES", finalspace=True)

        tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
        newtree = self.tokenize_list(tcomma, values, accolate=True)

        return (tvalues, newtree, (orderby, (offset, limit)))

    @__call__.register(dql.Select)
    def _(
        self,
        obj: dql.Select,
        /,
        *,
        columns: tuple[TkTree, ...],
        distinct: TkTree = (),
        from_: TkTree = (),
        where: TkTree = (),
        groupby: TkTree = (),
        having: TkTree = (),
        orderby: TkTree = (),
        offset: TkTree = (),
        limit: TkTree = (),
    ) -> TkTree:
        select = (self.tokenize_keyword("SELECT"), TkInd(), distinct)

        tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
        columns = (TkInd(),) + self.tokenize_list(tcomma, columns, accolate=True)

        return (
            (select, TkInd(), columns),
            TkInd(),
            from_,
            where,
            (groupby, TkInd(), having),
            orderby,
            (offset, limit),
        )

    @__call__.register(dql.SetCombination)
    def _(
        self,
        obj: dql.SetCombination,
        /,
        *,
        subrelations: tuple[TkTree, TkTree],
        combinator: TkTree,
    ) -> TkTree:
        return (subrelations[0], combinator, subrelations[1])

    @__call__.register(clauses.DistinctColumn)
    def _(
        self, obj: clauses.DistinctColumn, /, *, column: Optional[TkTree] = None
    ) -> TkTree:
        if not column:
            return self.tokenize_keyword("DISTINCT")
        distinct = self.tokenize_keyword("DISTINCT ON")
        column = self.scope_it(column)
        return (distinct, column)

    @__call__.register(clauses.WithClause)
    def _(
        self,
        obj: clauses.WithClause,
        /,
        *,
        query: TkTree = (),
        name: str,
        materialized: bool,
    ) -> TkTree:
        tname = self.tokenize_name(name)

        if materialized:
            tas = self.tokenize_keyword("AS MATERIALIZED")
        elif materialized is None:
            tas = self.tokenize_keyword("AS")
        else:
            tas = self.tokenize_keyword("AS NOT MATERIALIZED")

        return ((tname, tas), TkInd(), *self.scope_it(query))

    @__call__.register(dql.WithClosure)
    def _(
        self,
        obj: dql.WithClosure,
        /,
        *,
        recursive: bool = False,
        with_relations: Iterable[TkTree] = (),
        query: TkTree,
    ) -> TkTree:
        if with_relations == ():
            return (query,)

        keyword = ["WITH", "WITH RECURSIVE"][recursive]
        newtree: List[TkTree] = [self.tokenize_keyword(keyword)]
        tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
        newtree.extend(self.tokenize_list(tcomma, with_relations, accolate=True))
        newtree.append(query)
        return tuple(newtree)

    @__call__.register(clauses.AliasedColumn)
    def _(
        self,
        obj: clauses.AliasedColumn,
        /,
        *,
        column: TkTree,
        alias: Optional[str] = None,
    ) -> TkTree:
        if alias is None:
            return (column,)

        as_ = self.tokenize_keyword("AS", initialspace=True, finalspace=True)
        alias_tok = self.tokenize_name(alias)
        return (column, TkInd(), (as_, alias_tok))

    @__call__.register(clauses.RelationClauseWrapper)
    def _(self, obj: clauses.RelationClauseWrapper, /, *, relation: TkTree) -> TkTree:
        from_ = self.tokenize_keyword("FROM")
        return (from_, TkInd(), relation)

    @__call__.register(clauses.WhereColumn)
    def _(self, obj: clauses.WhereColumn, /, *, column: TkTree) -> TkTree:
        where = self.tokenize_keyword("WHERE")
        return (where, TkInd(), column)

    @__call__.register(clauses.GroupColumn)
    def _(self, obj: clauses.GroupColumn, /, *, column: TkTree) -> TkTree:
        groupby = self.tokenize_keyword("GROUP BY")
        return (groupby, TkInd(), column)

    @__call__.register(clauses.HavingColumn)
    def _(self, obj: clauses.HavingColumn, /, *, column: TkTree) -> TkTree:
        having = self.tokenize_keyword("HAVING")
        return (having, TkInd(), column)

    @__call__.register(clauses.OrderColumn)
    def _(
        self,
        obj: clauses.OrderColumn,
        /,
        *,
        column: TkTree,
        how: Optional[bool] = None,
    ) -> TkTree:
        newtree: List[TkTree] = []
        newtree.append(self.tokenize_keyword("ORDER BY", finalspace=True))
        newtree.append(TkInd())
        newtree.append(column)
        if how:
            newtree.append(self.tokenize_keyword("ASC"))
        elif how is False:
            newtree.append(self.tokenize_keyword("DESC"))
        return tuple(newtree)

    @__call__.register(clauses.Offset)
    def _(self, obj: clauses.Offset, /, *, offset: int) -> TkTree:
        offset_tok = self.tokenize_keyword("OFFSET")
        constant = self.tokenize_constant(int, offset)
        return (offset_tok, constant)

    @__call__.register(clauses.Limit)
    def _(self, obj: clauses.Limit, /, *, limit: int, with_ties: bool) -> TkTree:
        seq = self.tokenize_keyword("FETCH FIRST", finalspace=True)
        seq += self.tokenize_constant(int, limit)
        seq += self.tokenize_keyword(
            f"ROW{'S' if  limit > 1 else ''}", initialspace=True
        )
        if with_ties:
            seq += self.tokenize_keyword("WITH TIES", initialspace=True)
        else:
            seq += self.tokenize_keyword("ONLY", initialspace=True)
        return (seq,)

    @__call__.register(clauses.SetCombinatorWrapper)
    def _(
        self,
        obj: clauses.SetCombinatorWrapper,
        /,
        *,
        combinator: qbconstants.SetCombinator,
        all: Optional[bool] = None,
    ) -> TkTree:
        comb = f"{combinator.value}"
        if all:
            comb += " ALL"
        elif all is False:
            comb += " DISTINCT"

        return (self.tokenize_keyword(comb),)

    @__call__.register(clauses.TemporaryWrapper)
    def _(self, obj: clauses.TemporaryWrapper, /) -> TkTree:
        return self.tokenize_keyword("TEMPORARY")

    @__call__.register(clauses.IfNotExistsWrapper)
    def _(self, obj: clauses.IfNotExistsWrapper, /) -> TkTree:
        return self.tokenize_keyword("IF NOT EXISTS")

    @__call__.register(clauses.IfExistsWrapper)
    def _(self, obj: clauses.IfExistsWrapper, /) -> TkTree:
        return self.tokenize_keyword("IF EXISTS")

    @__call__.register(clauses.AsQueryWrapper)
    def _(self, obj: clauses.AsQueryWrapper, /, *, subquery: TkTree) -> TkTree:
        as_ = self.tokenize_keyword("AS")
        return (as_, subquery)

    @__call__.register(clauses.DefaultValuesClause)
    def _(self, obj: clauses.DefaultValuesClause, /) -> TkTree:
        return self.tokenize_keyword("DEFAULT VALUES")

    @__call__.register(clauses.UniqueClause)
    def _(self, obj: clauses.UniqueClause, /) -> TkTree:
        return self.tokenize_keyword("UNIQUE")

    @__call__.register(clauses.SetColumnsClause)
    def _(
        self, obj: clauses.SetColumnsClause, /, set_columns: Iterable[TkTree]
    ) -> TkTree:
        tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
        return (
            self.tokenize_keyword("SET"),
            TkInd(),
            self.tokenize_list(tcomma, set_columns, accolate=True),
        )

    @__call__.register(clauses.DropCascadeClause)
    def _(self, obj: clauses.DefaultValuesClause, /, cascade: bool) -> TkTree:
        if cascade:
            return (self.tokenize_keyword("CASCADE"),)
        return (self.tokenize_keyword("RESTRICT"),)

    @__call__.register(clauses.CollationWrapper)
    def _(self, obj: clauses.CollationWrapper, /, string: str) -> TkTree:
        return (
            self.tokenize_keyword("COLLATE"),
            TkStr(qbtoken.Token.Literal.String, f'"{string}"').to_seq(),
        )

    @__call__.register(clauses.AscWrapper)
    def _(self, obj: clauses.AscWrapper, /, asc: bool) -> TkTree:
        if asc:
            return (self.tokenize_keyword("ASC"),)
        return (self.tokenize_keyword("DESC"),)

    ###############
    # DML QUERIES #
    ###############

    @__call__.register(dml.Insert)
    def _(
        self,
        obj: dml.Insert,
        /,
        *,
        schema_relation: TkTree,
        query: TkTree,
        in_columns: tuple[TkTree, ...],
    ) -> TkTree:
        insert_into = self.tokenize_keyword("INSERT INTO")

        subtree: List[TkTree] = []
        subtree.append((insert_into, TkInd(), schema_relation))

        if in_columns != ():
            tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
            cols_with_commas = self.tokenize_list(tcomma, in_columns, accolate=True)
            cols_tree = self.scope_it(cols_with_commas)
            subtree.append(TkInd())
            subtree.extend(cols_tree)

        return (tuple(subtree), query)

    @__call__.register(dml.Delete)
    def _(
        self,
        obj: dml.Delete,
        /,
        *,
        schema_relation: TkTree,
        where: Optional[TkTree] = None,
    ) -> TkTree:
        delete_from = self.tokenize_keyword("DELETE FROM")
        tree: List[TkTree] = [(delete_from, TkInd(), schema_relation)]
        if where:
            tree.append(where)

        return tuple(tree)

    @__call__.register(dml.Update)
    def _(
        self,
        obj: dml.Update,
        /,
        *,
        schema_relation: TkTree,
        set_columns: TkTree,
        where: Optional[TkTree] = None,
    ) -> TkTree:
        newtree: List[TkTree] = []
        update = self.tokenize_keyword("UPDATE")
        newtree.append((update, schema_relation))

        newtree.append(set_columns)

        if where:
            newtree.append(where)

        return tuple(newtree)

    ###############
    # DDL QUERIES #
    ###############

    @__call__.register(ddl.CreateTable)
    def _(
        self,
        obj: ddl.CreateTable,
        /,
        *,
        name: TkTree,
        as_query: TkTree = (),
        columns: tuple[TkTree, ...] = (),
        constraints: tuple[TkTree, ...] = (),
        if_not_exists: TkTree = (),
        temporary: TkTree = (),
    ) -> TkTree:
        create = (
            self.tokenize_keyword("CREATE"),
            temporary,
            self.tokenize_keyword("TABLE"),
        )

        newtree = ((create, if_not_exists), name)

        tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
        constraints = self.tokenize_list(tcomma, constraints, accolate=True)
        if as_query:
            assert not columns
            constraints = self.scope_it(constraints)
            return (newtree, as_query, constraints)

        columns = self.tokenize_list(tcomma, columns, accolate=True)
        if constraints:
            columns_and_constraints = self.tokenize_list(
                tcomma, (columns, constraints), accolate=True
            )
        else:
            columns_and_constraints = columns

        columns_and_constraints = self.scope_it(columns_and_constraints)
        return (newtree, columns_and_constraints)

    @__call__.register(ddl.CreateView)
    def _(
        self,
        obj: ddl.CreateTable,
        /,
        *,
        name: TkTree,
        as_query: TkTree = (),
        columns: tuple[TkTree, ...] = (),
        if_not_exists: TkTree = (),
        temporary: TkTree = (),
    ) -> TkTree:
        create = (
            self.tokenize_keyword("CREATE"),
            temporary,
            self.tokenize_keyword("VIEW"),
        )

        newtree = ((create, if_not_exists), name)

        if columns:
            tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
            columns = self.scope_it(self.tokenize_list(tcomma, columns, accolate=True))
            return ((newtree, columns), as_query)
        return (newtree, as_query)

    @__call__.register(ddl.CreateIndex)
    def _(
        self,
        obj: ddl.CreateIndex,
        /,
        *,
        name: TkTree,
        schema_relation: TkTree,
        columns: Tuple[TkTree, ...],
        if_not_exists: TkTree = (),
        unique: TkTree = (),
        where: TkTree = (),
    ) -> TkTree:
        create = (
            self.tokenize_keyword("CREATE"),
            unique,
            self.tokenize_keyword("INDEX"),
        )

        columns = self.tokenize_list(
            TkStr(qbtoken.Punctuation, ",").to_seq(), columns, accolate=True
        )
        columns = self.scope_it(columns)
        on = (self.tokenize_keyword("ON"), schema_relation, columns)

        return ((create, if_not_exists), name, on, where)

    @__call__.register(ddl.CreateSchema)
    def _(
        self,
        obj: ddl.CreateSchema,
        /,
        *,
        name: TkTree,
        if_not_exists: TkTree = (),
    ) -> TkTree:
        create = self.tokenize_keyword("CREATE SCHEMA")
        return ((create, if_not_exists), name)

    @__call__.register(ddl.Drop)
    def _(
        self,
        obj: ddl.Drop,
        /,
        *,
        target_type: TkTree,
        name: TkTree,
        if_exists: TkTree = (),
        cascade: TkTree = (),
    ):
        drop = flatten((self.tokenize_keyword("DROP"), target_type))

        return ((drop, if_exists), name, cascade)

    @__call__.register(clauses.TableColumnWrapper)
    def _(
        self,
        obj: clauses.TableColumnWrapper,
        /,
        *,
        name: TkTree,
        sqltype: type,
        constraints: tuple[TkTree],
    ) -> TkTree:
        type = self.tokenize_sqltype(sqltype)
        return ((name, type), constraints)

    @__call__.register(clauses.DBObjectNameWrapper)
    def _(self, obj: clauses.DBObjectNameWrapper, /, *, dbobj: atoms.Atom) -> TkTree:
        if isinstance(dbobj, qbschemas.Schema):
            string = "SCHEMA"
        elif isinstance(dbobj, qbschemas.Index):
            string = "INDEX"
        elif isinstance(dbobj, relations.View):
            string = "VIEW"
        elif isinstance(dbobj, relations.Named):
            string = "TABLE"
        else:
            raise TypeError(dbobj)

        return self.tokenize_keyword(string)

    ###############
    # TCL QUERIES #
    ###############

    @__call__.register(tcl.Initiate)
    def _(
        self,
        obj: tcl.Initiate,
        /,
        *,
        action: str,
        isolation_level: Optional[str] = None,
        read_only: Optional[bool] = None,
    ) -> TkTree:
        action_tok = self.tokenize_keyword(f"{action} TRANSACTION")
        charact = self.tokenize_transaction_characteristics(isolation_level, read_only)
        return (action_tok, *charact)

    @__call__.register(tcl.Conclude)
    def _(
        self, obj: tcl.Conclude, /, *, action: str, chain: Optional[bool] = None
    ) -> TkTree:
        newtree = []
        newtree.append(self.tokenize_keyword(f"{action} WORK"))
        # TODO: make chain be a wrapped clause
        if chain is not None:
            newtree.append(TkInd())
            newtree.append(self.tokenize_keyword(f"AND{chain and ' ' or ' NO '}CHAIN"))
        return tuple(newtree)

    @__call__.register(tcl.ManageSavepoint)
    def _(self, obj: tcl.ManageSavepoint, /, *, action: str, savepoint: str) -> TkTree:
        newtree = []
        if action == "CREATE":
            newtree.append(self.tokenize_keyword("SAVEPOINT", finalspace=True))
        elif action == "ROLLBACK":
            newtree.append(
                self.tokenize_keyword(f"{action} TO SAVEPOINT", finalspace=True)
            )
        else:
            newtree.append(
                self.tokenize_keyword(f"{action} SAVEPOINT", finalspace=True)
            )
        newtree.append(self.tokenize_name(savepoint))
        return tuple(newtree)

    ############
    # COMMENTS #
    ############

    @__call__.register(comment.SingleComment)
    def _(self, obj: comment.SingleComment, /, *, message: str) -> TkTree:
        # TODO: use tokenize_comment instead?
        seq = TkSeq()
        for msg in message.split("\n"):
            seq += TkStr(qbtoken.Comment.Single, "-- ").to_seq()
            seq += TkStr(qbtoken.Comment.Single, msg).to_seq()
            seq += TkStr(qbtoken.Punctuation.Linebreak, "\n").to_seq()
        return seq

    @__call__.register(comment.MultilineComment)
    def _(
        self, obj: comment.MultilineComment, /, *, messages: tuple[str, ...]
    ) -> TkTree:
        seq = TkStr(qbtoken.Comment.Multiline, "/*").to_seq()
        seq += TkStr(qbtoken.Punctuation.Linebreak, "\n").to_seq()
        for message in messages:
            seq += TkStr(qbtoken.Whitespace.Indentation, "  ").to_seq()
            seq += TkStr(qbtoken.Comment.Multiline, message).to_seq()
            seq += TkStr(qbtoken.Punctuation.Linebreak, "\n").to_seq()
        seq += TkStr(qbtoken.Comment.Multiline, "*/").to_seq()
        return seq

    ###############
    # CONSTRAINTS #
    ###############

    @__call__.register(qbconstraints.Constraint)
    @name_and_defer
    def _(self, obj: qbconstraints.Constraint, /, **kwargs) -> TkTree:
        # TODO: temporary rule during dev
        return self.tokenize_keyword(str(type(obj)))

    @__call__.register(qbconstraints.ColumnPrimaryKey)
    @name_and_defer
    def _(self, obj: qbconstraints.ColumnPrimaryKey, /) -> TkTree:
        return self.tokenize_keyword("PRIMARY KEY")

    @__call__.register(qbconstraints.ColumnNotNull)
    @name_and_defer
    def _(self, obj: qbconstraints.ColumnNotNull, /) -> TkTree:
        return self.tokenize_keyword("NOT NULL")

    @__call__.register(qbconstraints.ColumnUnique)
    @name_and_defer
    def _(self, obj: qbconstraints.ColumnUnique, /) -> TkTree:
        return self.tokenize_keyword("UNIQUE")

    @__call__.register(qbconstraints.ColumnGeneratedAsIdentity)
    @name_and_defer
    def _(
        self, obj: qbconstraints.ColumnGeneratedAsIdentity, /, *, always: bool
    ) -> TkTree:
        return self.tokenize_keyword(
            f"GENERATED {always and 'ALWAYS' or 'BY DEFAULT'} AS IDENTITY",
        )

    @__call__.register(qbconstraints.ColumnGeneratedAlwaysAs)
    @name_and_defer
    def _(
        self,
        obj: qbconstraints.ColumnGeneratedAlwaysAs,
        /,
        *,
        expression: TkTree,
    ) -> TkTree:
        newtree: List[TkTree] = []
        newtree.append(self.tokenize_keyword("GENERATED ALWAYS AS"))
        newtree.append(expression)
        newtree.append(self.tokenize_keyword("STORED"))
        return tuple(newtree)

    @__call__.register(qbconstraints.ColumnReferences)
    @name_and_defer
    def _(
        self,
        obj: qbconstraints.ColumnReferences,
        /,
        *,
        schema_name: Optional[str],
        relation_name: str,
        column_name: str,
        ondelete: TkTree = (),
        onupdate: TkTree = (),
    ) -> TkTree:
        newtree: List[TkTree] = []
        newtree.append(self.tokenize_keyword("REFERENCES"))

        subtree: TkTree = self.tokenize_name(name=column_name)
        if relation_name:
            left = self.tokenize_name(
                schema_name=schema_name, relation_name=relation_name
            )
            left += TkStr(qbtoken.Punctuation, "(").to_seq()
            subtree = self.scope_it(subtree, left=left)
        newtree.append(subtree)

        return (tuple(newtree), TkInd(), ondelete, onupdate)

    @__call__.register(qbconstraints.ColumnDefault)
    @name_and_defer
    def _(self, obj: qbconstraints.ColumnDefault, /, *, expression: TkTree) -> TkTree:
        # TODO: fix double initial space when constraint isn't named
        kw = self.tokenize_keyword("DEFAULT")
        return (kw, expression)

    @__call__.register(qbconstraints.ColumnCheck)
    @__call__.register(qbconstraints.TableCheck)
    @name_and_defer
    def _(
        self,
        obj: Union[qbconstraints.ColumnCheck, qbconstraints.TableCheck],
        /,
        *,
        expression: TkTree,
    ) -> TkTree:
        newtree: List[TkTree] = []
        newtree.append(self.tokenize_keyword("CHECK"))
        newtree.append(TkStr(qbtoken.Punctuation, "(").to_seq())
        newtree.append(expression)
        newtree.append(TkStr(qbtoken.Punctuation, ")").to_seq())
        return tuple(newtree)

    @__call__.register(qbconstraints.TableUnique)
    @name_and_defer
    def _(self, obj: qbconstraints.TableUnique, /, *, expression: TkTree) -> TkTree:
        kw = self.tokenize_keyword("UNIQUE")
        return (kw, expression)

    @__call__.register(qbconstraints.TablePrimaryKey)
    @name_and_defer
    def _(self, obj: qbconstraints.TablePrimaryKey, /, *, expression: TkTree) -> TkTree:
        kw = self.tokenize_keyword("PRIMARY KEY")
        return (kw, expression)

    @__call__.register(qbconstraints.TableForeignKey)
    @name_and_defer
    def _(
        self,
        obj: qbconstraints.TableForeignKey,
        /,
        *,
        expression: TkTree,
        schema_name: str,
        relation_name: str,
        column_name: str,
        ondelete: TkTree = (),
        onupdate: TkTree = (),
    ) -> TkTree:
        newtree: List[TkTree] = []
        newtree.append(self.tokenize_keyword("FOREIGN KEY"))
        newtree.append(expression)
        newtree.append(self.tokenize_keyword("REFERENCES"))

        seq = self.tokenize_name(relation_name=relation_name, schema_name=schema_name)
        seq += TkStr(qbtoken.Punctuation, "(").to_seq()
        seq += self.tokenize_name(name=column_name)
        seq += TkStr(qbtoken.Punctuation, ")").to_seq()
        newtree.append(seq)

        newtree.extend((TkInd(), ondelete, onupdate))
        return tuple(newtree)

    @__call__.register(clauses.OnChangeWrapper)
    def _(
        self,
        obj: clauses.OnChangeWrapper,
        /,
        *,
        change: str,
        policy: qbconstraints.OnChangePolicy,
    ) -> TkTree:
        return self.tokenize_keyword(f"ON {change} {policy.value}")

    ###########
    # SCHEMAS #
    ###########

    @__call__.register(qbschemas.IndexedColumn)
    def _(
        self,
        obj: qbschemas.IndexedColumn,
        /,
        *,
        column: TkTree,
        collation: TkTree = (),
        asc: TkTree = (),
    ) -> TkTree:
        column = self.scope_it(column, noindent=True)

        return (column, collation, asc)

    @__call__.register(qbschemas.Schema)
    def _(self, obj: qbschemas.Schema, /, *, name: TkTree) -> TkTree:
        return name

    @__call__.register(qbschemas.Index)
    def _(self, obj: qbschemas.Index, /, *, name: TkTree) -> TkTree:
        return (name,)

    #############
    # RELATIONS #
    #############

    @__call__.register(relations.Named)
    def _(self, obj: relations.Named, /, *, name: TkTree) -> TkTree:
        return name

    @__call__.register(relations.Aliased)
    def _(
        self,
        obj: relations.Aliased,
        /,
        *,
        subrelation: TkTree,
        name: TkSeq,
        columns: Tuple[TkTree],
    ) -> TkTree:
        if columns:
            tcomma = TkStr(qbtoken.Punctuation, ",").to_seq()
            cols_tree = self.tokenize_list(tcomma, columns, accolate=True)
            name_tree = name + TkStr(qbtoken.Punctuation, "(").to_seq()
            name_tree = self.scope_it(cols_tree, noindent=True, left=name_tree)
        else:
            name_tree = name

        as_ = self.tokenize_keyword("AS")
        if isinstance(subrelation, TkSeq):
            return (subrelation, as_, name_tree)
        subrelation = cast(
            tuple[TkTree], subrelation
        )  # TODO: why does mypy fail to infer type without this cast?
        return (*subrelation[:-1], (subrelation[-1], (as_, name_tree)))

    @__call__.register(relations.CartesianProduct)
    def _(
        self, obj: relations.CartesianProduct, /, *, subrelations: tuple[TkTree, ...]
    ) -> TkTree:
        sep = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        res = self.tokenize_list(sep, subrelations, accolate=True)
        return res

    @__call__.register(relations.Join)
    def _(
        self,
        obj: relations.Join,
        /,
        *,
        subrelations: tuple[TkTree, TkTree],
        jointype: str,
        natural: bool,
        on: Optional[TkTree] = None,
        using: tuple[str, ...] = (),
    ) -> TkTree:
        assert not on or not using
        assert not natural or not on
        assert not natural or not using

        jt = self.tokenize_jointype(jointype, natural)
        newtree: list[TkTree] = [(subrelations[0], jt, subrelations[1]), TkInd()]

        if on:
            kw = self.tokenize_keyword("ON")
            newtree.append((kw, on))
        elif using:
            kw = self.tokenize_keyword("USING")
            newtree.append(kw)
            subtree: List[TkTree] = []
            for n in using:
                if subtree:
                    subtree.append(TkStr(qbtoken.Punctuation, ",").to_seq())
                subtree.append(self.tokenize_name(n))
            newtree.extend(self.scope_it(tuple(subtree)))

        return tuple(newtree)

    ###########
    # COLUMNS #
    ###########

    @__call__.register(qbpseudo_columns.Star)
    def _(self, obj: qbpseudo_columns.Star, /, *, relation: Optional[TkSeq]) -> TkTree:
        # * tokenized as an Operator to mirror pygments
        star = self.tokenize_operator("*")

        if not relation:
            return star

        return relation + TkStr(qbtoken.Punctuation, ".").to_seq() + star

    @__call__.register(qbcolumns.Named)
    def _(self, obj: qbcolumns.Named, /, *, name: TkTree) -> TkTree:
        return name

    @__call__.register(qbcolumns.Pretuple)
    def _(self, obj: qbcolumns.Pretuple, /, *, columns: tuple[TkTree, ...]) -> TkTree:
        sep = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        col = self.tokenize_list(sep, columns, accolate=True)
        return col

    @__call__.register(qbcolumns.Tuple)
    def _(self, obj: qbcolumns.Tuple, /, *, columns: tuple[TkTree, ...]) -> TkTree:
        sep = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        col = self.tokenize_list(sep, columns, accolate=True)
        # TODO: factorize using Pretuple
        return self.scope_it(col)

    @__call__.register(qbcolumns.BooleanCombination)
    def _(
        self,
        obj: qbcolumns.BooleanCombination,
        /,
        *,
        combinator: str,
        columns: tuple[TkTree, ...],
    ) -> TkTree:
        comb = self.tokenize_combinator(combinator)
        return self.tokenize_list(comb, columns)

    @__call__.register(qbcolumns.Not)
    def _(self, obj: qbcolumns.Not, /, *, combinator: str, column: TkTree) -> TkTree:
        comb = self.tokenize_combinator(combinator)
        return (comb, column)

    @__call__.register(qbcolumns.Comparison)
    def _(
        self,
        obj: qbcolumns.Comparison,
        /,
        *,
        operator: str,
        columns: tuple[TkTree, TkTree],
    ) -> TkTree:
        op = self.tokenize_operator(operator)
        return (columns[0], op, columns[1])

    @__call__.register(qbcolumns.InRelation)
    def _(
        self,
        obj: qbcolumns.InRelation,
        /,
        *,
        operator: str,
        columns: tuple[TkTree],
        relation: TkTree,
    ) -> TkTree:
        op = self.tokenize_operator(operator)
        return (columns, op, relation)

    @__call__.register(qbcolumns.ArithmeticOperation)
    def _(
        self,
        obj: qbcolumns.ArithmeticOperation,
        /,
        *,
        operator: str,
        columns: tuple[TkTree, ...],
    ) -> TkTree:
        op = self.tokenize_operator(operator)
        return self.tokenize_list(op, columns)

    @__call__.register(qbcolumns.Negate)
    def _(self, obj: qbcolumns.Negate, /, *, operator: str, column: TkTree) -> TkTree:
        op = self.tokenize_operator(operator)

        return flatten(op + flatten(column))

    @__call__.register(qbcolumns.Transform)
    def _(
        self, obj: qbcolumns.Transform, /, *, transformator: str, columns: tuple[TkTree]
    ) -> TkTree:
        func = self.tokenize_function(transformator)
        func += TkStr(qbtoken.Punctuation, "(").to_seq()
        return self.scope_it(columns, left=func)

    @__call__.register(qbcolumns.Cast)
    def _(
        self,
        obj: qbcolumns.Cast,
        /,
        *,
        sqltype: type,
        column: TkTree,
    ) -> TkTree:
        func = self.tokenize_function("CAST")
        func += TkStr(qbtoken.Punctuation, "(").to_seq()
        as_ = self.tokenize_keyword("AS")
        type_ = self.tokenize_sqltype(sqltype)
        return self.scope_it((column, (as_, type_)), left=func)

    @__call__.register(qbcolumns.Aggregate)
    def _aggre(
        self,
        obj: qbcolumns.Aggregate,
        /,
        *,
        aggregator: str,
        column: TkTree,
        distinct: bool = False,
    ) -> TkTree:
        agg = self.tokenize_aggregator(aggregator)
        subtree: List[TkTree] = []
        if distinct:
            subtree.append(self.tokenize_keyword("DISTINCT"))
        subtree.append(column)
        return (agg, self.scope_it(tuple(subtree)))

    @__call__.register(qbcolumns.Exists)
    def _(self, obj: qbcolumns.Exists, /, *, query: TkTree) -> TkTree:
        kw = self.tokenize_keyword("EXISTS")
        result: TkTree = (kw, self.scope_it(query))
        return result

    @__call__.register(qbcolumns.Case)
    def _(
        self,
        obj: qbcolumns.Case,
        /,
        *,
        when: tuple[tuple[TkTree, TkTree], ...],
        else_: Optional[TkTree] = None,
    ) -> TkTree:
        kwcase = self.tokenize_keyword("CASE")
        kwwhen = self.tokenize_keyword("WHEN")
        kwthen = self.tokenize_keyword("THEN")
        kwelse = self.tokenize_keyword("ELSE")
        kwend = self.tokenize_keyword("END")
        tree: list[TkTree] = []
        for wcond, wval in when:
            tree.append((kwwhen, wcond, kwthen, wval))
        if else_:
            tree.append((kwelse, else_))
        return (kwcase, tuple(tree), kwend)

    @__call__.register(qbcolumns.Placeholder)
    def _(self, obj: qbcolumns.Placeholder, /, *, key: Optional[str] = None) -> TkTree:
        return self.tokenize_placeholder_key(key)

    @__call__.register(qbcolumns.Constant)
    def _(self, obj: qbcolumns.Constant, /, *, sqltype: type, constant: Any) -> TkTree:
        return self.tokenize_constant(sqltype, constant)

    @__call__.register(qbpseudo_columns.Default)
    def _(self, obj: qbpseudo_columns.Default, /) -> TkTree:
        return self.tokenize_keyword("DEFAULT")

    ##################
    # Tokenize types #
    ##################

    # We define these at the end
    # so that mypy infers the right type for the _ function

    @TypeDispatch
    def tokenize_sqltype(self, type: type) -> TkSeq:
        if hasattr(type, "_sqltype"):
            return TkStr(qbtoken.Token.Name.Builtin, type._sqltype).to_seq()
        raise ValueError(f"Unsupported type {type}")

    @tokenize_sqltype.register(int)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "INTEGER").to_seq()

    @tokenize_sqltype.register(float)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "REAL").to_seq()

    @tokenize_sqltype.register(bool)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "BOOLEAN").to_seq()

    @tokenize_sqltype.register(str)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "TEXT").to_seq()

    # TODO: following PostgreSQL following doc, check standard SQL
    #       https://www.postgresql.org/docs/current/datatype-datetime.html#DATATYPE-DATETIME-TABLE
    @tokenize_sqltype.register(datetime)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "TIMESTAMP").to_seq()

    @tokenize_sqltype.register(date)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "DATE").to_seq()

    @tokenize_sqltype.register(time)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "TIME").to_seq()

    @tokenize_sqltype.register(timedelta)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "INTERVAL").to_seq()

    @tokenize_sqltype.register(UUID)
    def _(self, type: type) -> TkSeq:
        return TkStr(qbtoken.Token.Name.Builtin, "UUID").to_seq()

    ######################
    # Tokenize constants #
    ######################

    @TypeDispatch
    def tokenize_constant(self, typ: type, constant: Any) -> TkSeq:
        if hasattr(typ, "_hard_encode"):
            return TkStr(qbtoken.Token.Literal, typ._hard_encode(constant)).to_seq()
        raise TypeError(type)

    @tokenize_constant.register(type(None))
    def _(self, typ, constant) -> TkSeq:
        return TkStr(qbtoken.Token.Keyword, "NULL").to_seq()

    @tokenize_constant.register(int)
    @tokenize_constant.register(float)
    def _(self, typ, constant) -> TkSeq:
        return TkStr(qbtoken.Token.Literal.Number, str(typ(constant))).to_seq()

    @tokenize_constant.register(str)
    def _(self, typ, constant) -> TkSeq:
        return TkStr(qbtoken.Token.Literal.String, repr(typ(constant))).to_seq()

    @tokenize_constant.register(bool)
    def _(self, typ, constant) -> TkSeq:
        return TkStr(qbtoken.Token.Keyword, str(typ(constant)).capitalize()).to_seq()

    @tokenize_constant.register(datetime)
    @tokenize_constant.register(date)
    @tokenize_constant.register(time)
    @tokenize_constant.register(timedelta)
    def _(self, typ, constant) -> TkSeq:
        return TkStr(qbtoken.Token.Literal.Date, str(constant)).to_seq()

    @tokenize_constant.register(UUID)
    def _(
        self,
        typ: type,
        constant: UUID | bytes | tuple[int, int, int, int, int, int] | int | str,
    ) -> TkSeq:
        if isinstance(constant, UUID):
            c = constant
        elif isinstance(constant, bytes):
            c = UUID(bytes=constant)
        elif isinstance(constant, tuple):
            c = UUID(fields=constant)
        elif isinstance(constant, int):
            c = UUID(int=constant)
        else:
            c = UUID(constant)
        return TkStr(qbtoken.Token.Literal, repr(str(c))).to_seq()

    ############

    def _post(self, tokens: TkTree) -> TkTree:
        nokeytok = self.tokenize_placeholder_key(None)[0]
        i = 0

        def key_placeholder(e: TkTree) -> TkTree:
            nonlocal i
            if not isinstance(e, TkSeq):
                return e
            newe: TkSeq = e
            for j, tkstr in enumerate(newe):
                if tkstr == nokeytok:
                    newe = (
                        newe[:j]
                        + self.tokenize_placeholder_key(None, anonymous_index=i)
                        + newe[j + 1 :]
                    )
                    i += 1
            return newe

        return transform(key_placeholder, tokens)

    #######################
    # ATOM TRANSFORMATION #
    #######################

    @singledispatchmethod
    def transform(self, obj: atoms.Atom) -> atoms.Atom:
        return obj

    del _
