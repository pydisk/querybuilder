from __future__ import annotations
from typing import Any, Callable, ClassVar, Iterable, Mapping, Optional
from querybuilder.utils.logger import Logger
from querybuilder.utils.transaction_manager import TransactionManager
from querybuilder.utils.typing import DBAPI_ConnectProto, DBAPI_CursorProto
from querybuilder.drivers.sql.tokenizer import Tokenizer
from querybuilder.formatting.formatter import Formatter
from querybuilder.atoms.atoms import Atom
import querybuilder as qb


class Connector:
    """Base abstract class for connectors

    These connectors are wrappers of any connector implementing
    the Python Database API (PEP 249)¹.  They provide a way of
    executing queries given by mean of querybuilder queries
    object rather than strings.  Additionally, they will own
    an internal transaction manager and, possibly, a logger.

    Parameters
    ----------
    connect_info : any
        A mandatory parameter expected by the backend connector,
        to establish the connection.

    logger : bool or Mapping or Logger, default=False
        A logger specification.  A Boolean value will result in
        the default logger, initially activated or not according
        to this Boolean value.  A Mapping value will be passed
        as set of keyworded parameters to the default logger
        constructor.  A Logger will be kept unchanged.
        Alternatively, the logger might be set
        after initialization, using the set_logger method which
        accepts as unique argument any of these specifications.
        Logger may include an history manager (this is the case
        for the default logger).

    kwargs : Mapping
        Further keyworded parameters for super initialization.

    Abstract methods
    ----------------
    connect
        This parameterless method should establish the backend
        connection, and refers it as 'session' attribute.

    References
    ----------
    ¹ https://peps.python.org/pep-0249/)
    """

    __slots__ = (
        "_connect_info",
        "_session",
        "_logger",
        "transaction",
        "tokenizer",
        "_raw_formatter",
        "_pretty_formatter",
    )

    OperationalError: ClassVar[type[Exception]]
    tokenizer_factory: ClassVar[type[Tokenizer]]

    @property
    def logger(self):
        return self._logger

    @property
    def session(self) -> DBAPI_ConnectProto:
        if hasattr(self, "_session"):
            return self._session
        raise AttributeError("session")

    @session.deleter
    def session(self):
        del self._session

    def __init__(
        self,
        connect_info: Any,
        logger: bool | Mapping | Logger | type[Logger] = False,
        *,
        tokenizer: Optional[Mapping | Tokenizer | Callable[[], Tokenizer]] = None,
        raw_formatter: Optional[Formatter] = None,
        pretty_formatter: Optional[Formatter] = None,
        transaction_manager: Optional[
            int | Mapping | TransactionManager | type[TransactionManager]
        ] = None,
        initially_connected: bool = True,
    ):
        self._set_connect_info(connect_info)
        self._set_tokenizer(tokenizer)
        self._set_formatters(raw_formatter, pretty_formatter)
        self.set_logger(logger)
        self._set_transaction_manager(transaction_manager)
        if initially_connected:
            self.connect()

    def _set_tokenizer(
        self,
        tokenizer: Optional[Mapping | Callable[[], Tokenizer] | Tokenizer],
    ):
        Tokenizer = self.tokenizer_factory
        if tokenizer is None:
            self.tokenizer = Tokenizer()
        elif isinstance(tokenizer, Mapping):
            self.tokenizer = Tokenizer(**tokenizer)
        elif hasattr(tokenizer, "args") and hasattr(tokenizer, "kwargs"):
            self.tokenizer = Tokenizer(*tokenizer.args, **tokenizer.kwargs)
        elif callable(tokenizer):
            self.tokenizer = tokenizer()
        else:
            self.tokenizer = tokenizer

    def _set_formatters(
        self,
        raw_formatter: Optional[Formatter],
        pretty_formatter: Optional[Formatter],
    ):
        self._raw_formatter = raw_formatter or qb.settings["raw_formatter"]
        self._pretty_formatter = pretty_formatter or qb.settings["pretty_formatter"]

    def set_logger(
        self,
        logger: bool | Mapping | type[Logger] | Logger,
    ) -> None:
        if isinstance(logger, bool):
            self._logger = Logger(
                active=logger,
                tokenizer=self.tokenizer,
                formatter=self._pretty_formatter,
            )
        elif isinstance(logger, Mapping):
            logger_kwargs = dict(logger)
            logger_kwargs.setdefault("formatter", self._pretty_formatter)
            self._logger = Logger(**logger_kwargs)
        elif isinstance(logger, type):
            self._logger = logger(
                tokenizer=self.tokenizer, formatter=self._pretty_formatter
            )
        else:
            self._logger = logger

    def _set_transaction_manager(
        self,
        arg: Optional[
            int | Mapping | type[TransactionManager] | TransactionManager
        ] = None,
        /,
    ):
        if arg is None:
            self.transaction = TransactionManager(self)
        elif isinstance(arg, int):
            self.transaction = TransactionManager(self, on_all_contexts=arg)
        elif isinstance(arg, Mapping):
            self.transaction = TransactionManager(self, **arg)
        elif isinstance(arg, type):
            self.transaction = arg(self)
        else:
            self.transaction = arg

    def _set_connect_info(self, connect_info: Any):
        self._connect_info = connect_info

    def connect(self):
        raise NotImplementedError()

    def close(self):
        if hasattr(self, "session"):
            self.session.close()
            del self.session

    def __del__(self):
        self.close()

    def execute(
        self,
        query: qb.queries.queries.Query,
        parameters: Optional[Mapping[str, Any]] = None,
    ):
        with self.transaction(on_next_context=not query.is_readonly()) as t:  # type: ignore
            return t.execute(query, parameters=parameters)

    def executemany(
        self,
        query: qb.queries.queries.Query,
        parameters: Iterable[Mapping[str, Any]],
        size_hint=None,
    ):
        with self.transaction as t:
            return t.executemany(query, parameters, size_hint=size_hint)

    def cursor(self) -> DBAPI_CursorProto:
        return self.session.cursor()

    def __hash__(self):
        raise TypeError(f"unhashable type: {type(self)}")

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc, exct, tb):
        self.close()

    def stringify(
        self,
        query: Atom,
        /,
        *,
        formatter=None,
        pretty: bool = False,
    ):
        tokens = query.tokenize(self.tokenizer)
        formatter = formatter or (self._raw_formatter, self._pretty_formatter)[pretty]
        frmt = formatter.get_formatted(tokens)
        return frmt

    def pretty_print(
        self,
        query: Atom,
        /,
        *,
        formatter=None,
        pretty: bool = True,
        **print_kwargs,
    ):
        frmt = self.stringify(query, formatter=formatter, pretty=pretty)
        print(frmt, **print_kwargs)


class NullConnector(Connector):
    """A basic implementation of Connector, without backend

    This class is mostly aimed to debugging and testing.  It allows
    to visualize queries through the logger (activated by default),
    without executing anything on backend (in particular, SELECT
    queries will always return an empty relation).

    """

    class OperationalError(Exception):
        pass

    class NullDB:
        class NullCursor:
            def execute(self, query, args):
                pass

            def executemany(self, query, parameters):
                pass

            def __iter__(self):
                return iter(())

        def close(self):
            pass

        def cursor(self):
            return self.NullCursor()

        def commit(self):
            pass

        def rollback(self):
            pass

    def __init__(self, connect_info=None, logger=True, tokenizer=None, **kwargs):
        if tokenizer is None:
            tokenizer = qb.settings["tokenizer"]
        super().__init__(connect_info, logger=logger, tokenizer=tokenizer, **kwargs)

    def connect(self):
        if not hasattr(self, "session"):
            self.session = self.NullDB()
            self.execute(
                self.queries.comment.SingleComment(
                    "Connected to Null (fake connector: nothing is executed)."
                )
            )
        return self.session
