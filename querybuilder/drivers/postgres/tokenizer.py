from __future__ import annotations
from uuid import UUID
import warnings
from functools import singledispatchmethod
from typing import Any
from querybuilder.utils.decorators import TypeDispatch
from querybuilder.formatting.tokentree import TkTree, TkSeq, TkStr
from querybuilder.drivers.sql.tokenizer import Tokenizer as sqlTokenizer
import querybuilder.formatting.paramstyle as qbparamstyle
import querybuilder.queries.ddl as ddl


class Tokenizer(sqlTokenizer):
    paramstyle: qbparamstyle.KeyedParamStyle = qbparamstyle.PyformatParamStyle()
    anonymous_paramstyle: qbparamstyle.UnkeyedParamStyle = (
        qbparamstyle.FormatParamStyle()
    )

    @TypeDispatch
    def tokenize_constant(self, typ: type, constant: Any) -> TkSeq:  # type: ignore
        return super().tokenize_constant(typ, constant)

    @tokenize_constant.register(UUID)
    def _(
        self,
        typ: type,
        constant: UUID | bytes | tuple[int, int, int, int, int, int] | int | str,
    ) -> TkSeq:
        res = super().tokenize_constant(typ, constant)
        tkstr = res[0]
        return TkStr(tkstr[0], f"{tkstr[1]}::UUID").to_seq()

    @singledispatchmethod
    def __call__(self, *args, **kwargs) -> TkTree:
        return super().__call__(*args, **kwargs)

    @__call__.register(ddl.CreateView)
    def _(
        self,
        obj: ddl.CreateTable,
        /,
        *,
        name: TkTree,
        as_query: TkTree = (),
        columns: tuple[TkTree, ...] = (),
        if_not_exists: TkTree = (),
        temporary: TkTree = (),
    ) -> TkTree:
        if if_not_exists:
            warnings.warn(
                "IF NOT EXISTS clause not supported by PostgreSQL for CREATE VIEW"
                "queries, automatically ignoring it"
            )

        return super().__call__(
            obj,
            name=name,
            as_query=as_query,
            columns=columns,
            if_not_exists=(),
            temporary=temporary,
        )
