import psycopg
import querybuilder.drivers.sql.connector as qbconn
import querybuilder.drivers.postgres.tokenizer as qbpostgrestokenizer


class Connector(qbconn.Connector):
    __slots__ = ()
    OperationalError = psycopg.OperationalError
    tokenizer_factory = qbpostgrestokenizer.Tokenizer

    def __init__(
        self, connect_info="", logger=None, tokenizer=None, initially_connected=True
    ):
        super().__init__(
            connect_info,
            logger=logger,
            tokenizer=tokenizer,
            initially_connected=initially_connected,
        )

    def connect(self):
        if not hasattr(self, "session"):
            self._session = psycopg.connect(self._connect_info)
        return self._session
