import sqlite3
import querybuilder.drivers.sql.connector as qbconn
import querybuilder.drivers.sqlite.tokenizer as qbsqlitetokenizer
from uuid import UUID
from typing import Any


class Connector(qbconn.Connector):
    __slots__ = ()
    OperationalError = sqlite3.OperationalError
    tokenizer_factory = qbsqlitetokenizer.Tokenizer

    def connect(self):
        if not hasattr(self, "session"):
            self._session = sqlite3.connect(self._connect_info)
            # TODO: create a pseudo information_schema (script sql) -- see https://www.sqlite.org/pragma.html
        return self.session

    def cast_values(self, args: dict[str, Any]) -> dict[str, Any]:
        for k, v in args.items():
            if isinstance(v, UUID):
                args[k] = str(v)
        return args
