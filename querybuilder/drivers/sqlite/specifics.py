import querybuilder as qb


class ScopedDQL(qb.atoms.relations.Fromable):
    """A DQL query nested within parenthesis

    Be aware that, since ScopedDQL are sqlite specifics, they cannot be tokenized using
    the standard-SQL tokenizer (namely, qb.drivers.sql.tokenizer.Tokenizer).  This will
    make the pretty printing as well as the stringification (`__str__`) fail, by
    default.  Consider setting the default tokenizer to the sqlite one (namely,
    `qb.drivers.sqlite.tokenizer.Tokenizer`) so that both pretty printing and
    stringification would work (see examples below).


    Parameters
    ----------
    query: qb.queries.dql.DQLQuery
        the wrapped DQL query.

    kwargs: dict[str, Any]
        additional keyworded parameters for super initialization.

    Examples
    --------
    For pretty printing and stringification (`__str__`) of atoms to work properly, we
    need to set the default tokenizer of querybuilder to the sqlite one:
    >>> qb.settings["tokenizer"] = qb.drivers.sqlite.tokenizer.Tokenizer()

    Now, we can use our sqlite-specific ScopedDQL class:
    >>> from querybuilder.helpers import make_column
    >>> q01 = qb.queries.dql.Select([make_column(0), make_column(1)])
    >>> r01 = ScopedDQL(q01)
    >>> str(r01)
    '(SELECT 0, 1)'
    >>> q12 = qb.queries.dql.Select([make_column(1), make_column(2)])
    >>> r12 = ScopedDQL(q12)
    >>> str(r12)
    '(SELECT 1, 2)'
    >>> str(q12.union_all(ScopedDQL(q01.except_(q01))))
    'SELECT 1, 2 UNION ALL (SELECT 0, 1 EXCEPT SELECT 0, 1)'
    >>> r = qb.atoms.relations.CartesianProduct([r01, r12])
    >>> str(r.select("*"))
    'SELECT * FROM (SELECT 0, 1), (SELECT 1, 2)'
    """

    __slots__ = ("query",)

    def __init__(self, query: qb.queries.dql.DQLQuery, **kwargs):
        self.query = query
        super().__init__(columns=query.columns, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["query"] = self.query.subtokenize(tokenizer, scoped=True)
        return kwargs
