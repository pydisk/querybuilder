from __future__ import annotations
from uuid import UUID
import warnings
from functools import singledispatchmethod
from typing import Callable, List, Optional, cast
from querybuilder.utils.decorators import TypeDispatch
from querybuilder.formatting.tokentree import TkInd, TkTree, TkSeq, TkStr
from querybuilder.drivers.sql.tokenizer import Tokenizer as sqlTokenizer, name_and_defer
from querybuilder.drivers.sqlite.specifics import ScopedDQL
from querybuilder.atoms.clauses import Limit, OrderColumn, SetCombinatorWrapper
from querybuilder.atoms.atoms import Atom
import querybuilder.atoms.relations as qbrelations
import querybuilder.atoms.pseudo_columns as qbpseudo_columns
import querybuilder.atoms.columns as qbcolumns
import querybuilder.formatting as qbformatting
import querybuilder as qb


class Tokenizer(sqlTokenizer):
    __slots__ = ("get_target_database_for_attach",)

    def __init__(
        self,
        schema_strategy: type[Exception] | str | Callable[[str], str] | None = "",
        **kwargs,
    ):
        get_target_database_for_attach: Callable[[str], str]
        if (
            schema_strategy is None
            or isinstance(schema_strategy, type)
            and issubclass(schema_strategy, Exception)
        ):
            exc: type[Exception] = schema_strategy or ValueError

            def get_target_database_for_attach(name: str) -> str:
                raise exc(name)

        elif isinstance(schema_strategy, str):
            dbname: str = schema_strategy

            def get_target_database_for_attach(name: str) -> str:
                return dbname.format(name=name)

        else:
            get_target_database_for_attach = schema_strategy
        self.get_target_database_for_attach = get_target_database_for_attach
        super().__init__(**kwargs)

    ###########################

    @singledispatchmethod
    def __call__(self, *args, **kwargs) -> TkTree:
        return super().__call__(*args, **kwargs)

    # DQL
    @__call__.register(ScopedDQL)
    def _(self, obj: qb.queries.dql.Select, /, *, query: TkTree):
        return query

    @__call__.register(Limit)
    def _(
        self,
        obj: Limit,
        /,
        *,
        limit: int,
        with_ties: Optional[bool] = None,
    ) -> TkTree:
        limit_kw = self.tokenize_keyword("LIMIT")
        cst = self.tokenize_constant(int, limit)
        if with_ties:
            # TODO: is this the correct exception to raise here?
            raise NotImplementedError()

        return qbformatting.preformatter.flatten((limit_kw, cst))

    @__call__.register(OrderColumn)
    def _(
        self,
        obj: OrderColumn,
        /,
        *,
        column: TkTree,
        how: Optional[bool] = None,
    ) -> TkTree:
        newtree: List[TkTree] = []
        newtree.append(self.tokenize_keyword("ORDER BY", finalspace=True))
        newtree.append(TkInd())
        column = self.unscope_it(column)
        newtree.append(column)
        if how:
            newtree.append(self.tokenize_keyword("ASC"))
        elif how is False:
            newtree.append(self.tokenize_keyword("DESC"))
        return tuple(newtree)

    @__call__.register(SetCombinatorWrapper)
    def _(
        self,
        obj: qb.atoms.clauses.SetCombinatorWrapper,
        /,
        *,
        combinator: qb.utils.constants.SetCombinator,
        all: Optional[bool] = None,
    ) -> TkTree:
        # sqlite does not support INTERSECT ALL / EXCEPT ALL
        if all and combinator != qb.utils.constants.SetCombinator.UNION:
            raise ValueError(f"sqlite does not support {combinator} ALL")
        # sqlite does not support INTERSECT DISTINCT / EXCEPT DISTINCT / UNION DISTINCT
        # ⟶  fallback to INTERSECT / EXCEPT / UNION
        return super().__call__(obj, combinator=combinator, all=all or None)

    # TCL
    @__call__.register(qb.queries.tcl.Start)
    def _(
        self,
        obj: qb.queries.tcl.Start,
        /,
        *,
        action: str,
        isolation_level: Optional[str] = None,
        read_only: Optional[bool] = None,
    ) -> TkTree:
        act = self.tokenize_keyword("BEGIN TRANSACTION")
        # TODO: ISOLATION LEVEL/READ ONLY are not available in sqlite.  Where to raise?
        charact = self.tokenize_transaction_characteristics(isolation_level, read_only)
        return (act, TkInd(), charact)

    @__call__.register(qb.queries.tcl.Conclude)
    def _(
        self,
        obj: qb.queries.tcl.Conclude,
        /,
        *,
        action: str,
        chain: Optional[bool] = None,
    ) -> TkTree:
        tree = []
        tree.append(self.tokenize_keyword(f"{action} TRANSACTION"))
        if chain is not None:
            # TODO: CHAIN are not available in sqlite.  Where to raise?
            tree.append(TkInd())
            tree.append(
                self.tokenize_keyword(
                    f"AND{chain and ' ' or ' NO '}CHAIN", initialspace=True
                )
            )
        return tuple(tree)

    # DDL
    @__call__.register(qb.atoms.constraints.ColumnGeneratedAsIdentity)
    @name_and_defer
    def _(
        self, obj: qb.atoms.constraints.ColumnGeneratedAsIdentity, /, *, always: bool
    ) -> TkTree:
        if always:
            warnings.warn(
                "sqlite does not support GENERATED ALWAYS AS IDENTITY constraints"
            )
        # TODO: works only in case of integer column
        return self.tokenize_keyword("AUTOINCREMENT")

    @__call__.register(qb.atoms.constraints.ColumnReferences)
    @name_and_defer
    def _(
        self,
        obj: qb.atoms.constraints.ColumnReferences,
        /,
        *,
        schema_name: Optional[str],
        relation_name: str,
        column_name: str,
        ondelete: TkTree = (),
        onupdate: TkTree = (),
    ) -> TkTree:
        return super().__call__(
            obj,
            schema_name=None,
            relation_name=relation_name,
            column_name=column_name,
            ondelete=ondelete,
            onupdate=onupdate,
        )

    @__call__.register(qb.queries.ddl.CreateSchema)
    def _(
        self,
        obj: qb.queries.ddl.CreateSchema,
        /,
        *,
        name: TkTree,
        if_not_exists: TkTree = (),
    ) -> TkTree:
        target = self.get_target_database_for_attach(obj.target.name)
        if target == ":memory:":
            warnings.warn(
                "Sqlite does not support schema creation,"
                "instead in-memory (thus volatile) database is attached"
            )
        elif not target:
            warnings.warn(
                "Sqlite does not support schema creation,"
                "instead temporary (thus volatile) database is attached"
            )
        else:
            warnings.warn(
                "Sqlite does not support schema creation,"
                f"instead database {target!r} is attached"
            )
        tree = (
            self.tokenize_keyword("ATTACH DATABASE"),
            self.tokenize_constant(str, target),
            self.tokenize_keyword("AS"),
            (name,),
        )
        return tree

    @TypeDispatch
    def tokenize_sqltype(self, type: type) -> TkSeq:  # type: ignore
        return super().tokenize_sqltype(type)

    @tokenize_sqltype.register(UUID)
    def _(self, type: type) -> TkSeq:
        warnings.warn("sqlite does not support UUID, using TEXT type instead")
        return TkStr(qbformatting.token.Token.Name.Builtin, "TEXT").to_seq()

    del _

    #######################
    # ATOM TRANSFORMATION #
    #######################

    @singledispatchmethod
    def transform(self, obj: Atom) -> Atom:
        return super().transform(obj)

    @transform.register(qbrelations.Aliased)
    def _t(self, obj: qbrelations.Aliased) -> qbrelations.Relation:
        if not obj.column_aliases:
            return obj

        left_aliases = {i: name for i, name in enumerate(obj._get_column_names())}
        left_columns = tuple(qbcolumns.Null() for i in obj.subrelation.columns)
        naming_relation = qb.queries.dql.Select(
            left_columns, where=qbcolumns.False_(), aliases=left_aliases
        )

        subrel = obj.subrelation
        if not isinstance(subrel, qbrelations.Fromable):
            subrel = subrel.alias(obj.name)

        subrel = cast(qbrelations.Fromable, subrel)

        rel = naming_relation.union_all(subrel.select((qbpseudo_columns.Star(),)))

        return rel.alias(obj.name)

    @transform.register(qb.queries.dql.SetCombination)
    def _t2(self, obj: qb.queries.dql.SetCombination) -> qb.queries.dql.SetCombination:
        left = right = None
        if isinstance(obj.left, qb.queries.dql.SetCombination):
            left = self.transform(obj.left)
            if isinstance(left, qb.queries.dql.SetCombination):
                left = ScopedDQL(query=left).select([qb.atoms.pseudo_columns.Star()])
        if isinstance(obj.right, qb.queries.dql.SetCombination):
            right = self.transform(obj.right)
            if isinstance(right, qb.queries.dql.SetCombination):
                right = ScopedDQL(query=right).select([qb.atoms.pseudo_columns.Star()])
        if left or right:
            obj = obj.buildfrom(obj, left=left or obj.left, right=right or obj.right)
        return obj

    del _t, _t2
