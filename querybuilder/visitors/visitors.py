from typing import Self


class Visitor:
    __slots__ = ()

    def enter(self: Self, obj, /, *args, **kwargs) -> Self:
        return self

    def exit(self: Self, obj, /, *args, **kwargs) -> Self:
        return self


class Accumulator(Visitor):
    __slots__ = ()

    def __or__(self, other):
        raise NotImplementedError()

    def __ror__(self, other):
        return self | other

    def __sub__(self, other):
        raise NotImplementedError()

    def __add__(self, other):
        raise NotImplementedError()
