from functools import singledispatchmethod
from typing import Optional, Self
from querybuilder.visitors.visitors import Visitor


class RecursiveRelationDetector(Visitor):
    __slots__ = ("_found", "_deactivated_by")

    def __init__(self, deactivated_by: Optional[object] = None, found: bool = False):
        self._found = found
        self._deactivated_by = deactivated_by

    @property
    def active(self):
        return self._deactivated_by is None

    @property
    def found(self) -> bool:
        return self._found

    def enter(self, obj):
        if not self.active:
            return self
        elif self.found:
            return self
        return self.visit(obj)

    @singledispatchmethod
    def visit(self, obj):
        raise TypeError(f"Unexpected type {obj.__class__}")

    def exit(self, obj):
        if obj is self._deactivated_by:
            return self.reactivate()
        return self

    def deactivate(self: Self, obj: object) -> Self:
        return self.__class__(deactivated_by=obj, found=self.found)

    def reactivate(self: Self) -> Self:
        return self.__class__(deactivated_by=None, found=self.found)

    def set_found(self: Self, found: bool) -> Self:
        if not self.active:
            raise ValueError(
                f"Cannot set attribute 'found' of inactive {self.__class__.__name__!r} object"
            )
        return self.__class__(found=found)
