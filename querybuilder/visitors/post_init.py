from functools import singledispatchmethod
from typing import cast
from querybuilder.visitors.visitors import Visitor, Accumulator
from querybuilder.visitors.stores import WithQueryStore, PlaceholderStore
from querybuilder.visitors.detectors import RecursiveRelationDetector
import querybuilder.formatting as qbformatting
import querybuilder as qb


@cast(singledispatchmethod, WithQueryStore.enter).register(qb.atoms.atoms.Atom)
@cast(singledispatchmethod, PlaceholderStore.enter).register(qb.atoms.atoms.Atom)
@cast(singledispatchmethod, RecursiveRelationDetector.visit).register(
    qb.atoms.atoms.Atom
)
def _(self: Visitor, obj: qb.atoms.atoms.Atom) -> Visitor:
    return self


@cast(singledispatchmethod, WithQueryStore.enter).register(qb.atoms.relations.With)
@cast(singledispatchmethod, PlaceholderStore.enter).register(
    qb.atoms.columns.Placeholder
)
def _(self: Accumulator, obj: qb.atoms.columns.Placeholder) -> Accumulator:
    return self | (obj,)


@cast(singledispatchmethod, RecursiveRelationDetector.visit).register(
    qb.atoms.relations.With
)
def _(
    self: RecursiveRelationDetector, obj: qb.atoms.relations.With
) -> RecursiveRelationDetector:
    return self.deactivate(obj)


@cast(singledispatchmethod, RecursiveRelationDetector.visit).register(
    qb.atoms.relations.RecursiveNamed
)
def _(
    self: RecursiveRelationDetector, obj: qb.atoms.relations.With
) -> RecursiveRelationDetector:
    return self.set_found(True)


@cast(singledispatchmethod, WithQueryStore.enter).register(qb.queries.dql.WithClosure)
def _(self: Accumulator, obj: qb.queries.dql.WithClosure) -> Accumulator:
    return self - obj.with_relations


qb.drivers.sql.connector.Connector.tokenizer_factory = (
    qb.drivers.sql.tokenizer.Tokenizer
)

del _
