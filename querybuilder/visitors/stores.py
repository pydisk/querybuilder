from __future__ import annotations
from functools import singledispatchmethod
from typing import Any, Callable, Iterable, Optional
from querybuilder.atoms.columns import Placeholder
from querybuilder.utils.typing import KeyedProto, NamedProto
from querybuilder.utils.constants import MISSING
from querybuilder.utils.stores import UNamedStore, KeyedTuple
from querybuilder.visitors.visitors import Accumulator


class WithQueryStore(UNamedStore, Accumulator):
    __slots__ = ()

    @classmethod
    def _prepare(cls, iterable: Iterable[NamedProto] = (), /) -> Iterable[NamedProto]:
        seen: dict[str, NamedProto] = {}

        def fltr(e: NamedProto) -> bool:
            k = cls._get_key(e)
            if k is None:
                return True
            prev = seen.get(k)
            if prev is None:
                seen[k] = e
                return True
            elif prev is e:
                return False
            raise ValueError(f"Ambiguous {cls._key_attr} {k!r}, got {prev!r} and {e!r}")

        return filter(fltr, iterable)

    @singledispatchmethod
    def enter(self, obj):
        raise TypeError(f"Unexpected type {obj.__class__}")


class PlaceholderStore(KeyedTuple[Placeholder], Accumulator):
    """KeyedTuple gathering placeholders in order

    Anonymous (i.e., keyless) elements are allowed, as well as key repetitions as far as
    they are associated to equal elements (e.g., two placeholders of distinct sqltype
    should have distinct keys).

    """

    __slots__ = ()
    convert = None

    @staticmethod
    def _get_key(e: KeyedProto, /) -> Optional[str]:
        return e.key

    @singledispatchmethod
    def enter(self, obj):
        raise TypeError(f"Unexpected type {obj.__class__}")

    def to_keyed_map(
        self, autokey: Callable[[int, int], str] = lambda i, j: f"auto{i}"
    ) -> dict[str, Placeholder]:
        """Returns a key-to-Placeholder map

        Unkeyed Placeholder of the store are assigned a key using the autokey parameter
        which should generate a `str` key when called with two integers as parameter:
        the first one being the rank of the unkeyed Placeholder among all the unkeyed
        Placehoders, the second one being its index in the store.
        """
        d = {}
        i = 0
        for j, p in enumerate(self):
            key = self._get_key(p)
            if not key:
                key = autokey(i, j)
                i += 1
            d[key] = p
        return d

    @classmethod
    def to_valued_map(
        cls,
        keyed_map: dict[str, Placeholder],
        *posvals,
        **keyvals,
    ) -> dict[str, Any]:
        """Convert a key-to-Placeholder map into a key-to-Value map"""

        posvals_list = list(posvals)
        d = {}
        for key, p in keyed_map.items():
            if hasattr(p, "value"):
                d[key] = p.value
            elif key in keyvals:
                d[key] = keyvals[key]
            elif posvals:
                d[key] = posvals_list.pop(0)
            else:
                d[key] = MISSING
        return d

    def to_value_tuple(self, *posvals: Any, **keyvals: Any) -> tuple[Any, ...]:
        n = len(posvals)
        posvals_list = list(posvals)
        values = []
        for e in self:
            if e.key in keyvals:
                values.append(keyvals[e.key])
            elif posvals:
                values.append(posvals_list.pop(0))
            else:
                raise ValueError(
                    f"More than {n} positional placeholder values expected, got {n}"
                )
        if posvals_list:
            raise ValueError(
                f"{n-len(posvals)} positional placeholder values expected, got {n}"
            )
        return tuple(values)
