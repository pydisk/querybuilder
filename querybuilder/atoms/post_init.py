from querybuilder.atoms.schemas import Schema
import querybuilder as qb

Schema._object_types = (
    qb.atoms.relations.Table,
    qb.atoms.relations.View,
    qb.atoms.relations.TableAs,
    qb.atoms.schemas.Index,
)
