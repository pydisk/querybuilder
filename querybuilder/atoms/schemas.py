from __future__ import annotations
from typing import Iterable, Union, ClassVar, Optional, Tuple, Self, Mapping
import querybuilder.utils.stores as qbstores
import querybuilder.helpers.columns as qbcolhelpers
import querybuilder.atoms.atoms as qbatoms
import querybuilder.atoms.clauses as qbclauses
import querybuilder.atoms.columns as qbcolumns
import querybuilder as qb


class IndexedColumn(qbatoms.Atom):
    __slots__ = ("column", "asc", "collation")

    def __init__(
        self,
        column: qbcolumns.Column,
        asc: Optional[bool] = None,
        collation: Optional[str] = None,
    ):
        self.column = column
        self.asc = asc
        self.collation = collation

    def index(
        self, collation: Optional[str] = None, asc: Optional[bool] = None
    ) -> IndexedColumn:
        if not collation:
            collation = self.collation
        if asc is None:
            asc = self.asc
        return IndexedColumn(self.column, collation=collation, asc=asc)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)

        column = qbcolhelpers.unqualify(self.column)
        kwargs["column"] = qbclauses.AliasedColumn(column).subtokenize(tokenizer)

        if self.collation:
            kwargs["collation"] = qbclauses.CollationWrapper(
                self.collation
            ).subtokenize(tokenizer)

        if self.asc is not None:
            kwargs["asc"] = qbclauses.AscWrapper(self.asc).subtokenize(tokenizer)

        return kwargs

    def _substitute(self, substitutions: Mapping) -> Self:
        column = self.column.substitute(substitutions)

        return self.buildfrom(self, column=column)


class Index(qbatoms.Atom):
    __slots__ = ("schema_relation", "columns", "unique", "name", "where", "schema_name")

    def __init__(
        self,
        name: str,
        schema_relation: qb.atoms.relations.Named,
        columns: Tuple[qb.atoms.columns.Column | IndexedColumn, ...],
        unique: bool = False,
        where: Optional[qb.atoms.columns.Expression] = None,
        schema_name: Optional[str] = None,
    ):
        self.schema_relation = schema_relation
        self.columns = columns
        self.unique = unique
        self.name = name
        self.where = where
        self.schema_name = schema_name

    def create(self, if_not_exists: bool = False) -> qb.queries.ddl.CreateIndex:
        return qb.queries.ddl.CreateIndex(self, if_not_exists)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            name=tokenizer.tokenize_name(name=self.name),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def _get_creation_kwargs(self, tokenizer):
        kwargs = {}

        kwargs["schema_relation"] = self.schema_relation.subtokenize(tokenizer)

        kwargs["columns"] = (c.index().subtokenize(tokenizer) for c in self.columns)

        if self.unique:
            kwargs["unique"] = qbclauses.UniqueClause().subtokenize(tokenizer)

        if self.where:
            kwargs["where"] = qbclauses.WhereColumn(self.where).subtokenize(tokenizer)

        return kwargs

    def _substitute(self, substitutions: Mapping) -> Self:
        schema_relation = self.schema_relation.substitute(substitutions)
        where = self.where and self.where.substitute(substitutions)
        columns = tuple(c.substitute(substitutions) for c in self.columns)

        return self.buildfrom(
            self, schema_relation=schema_relation, columns=columns, where=where
        )


class Schema(qbatoms.Atom):
    name: str
    objects: qbstores.UNamedStore
    _object_types: ClassVar[tuple[type[qb.atoms.atoms.Atom], ...]]
    __slots__ = ("name", "objects")
    _store_factory = qbstores.UNamedStore

    @property
    def relations(self):
        def fltr(e):
            return isinstance(e, qb.atoms.relations.Named)

        return qbstores.StoreFilter(fltr, self.objects)

    @property
    def indexes(self):
        def fltr(e):
            return isinstance(e, Index)

        return qbstores.StoreFilter(fltr, self.objects)

    @property
    def r(self):
        """An alias of relations"""
        return self.relations

    def __init__(
        self,
        name: str,
        objects: Iterable[Union[qb.atoms.relations.Named, Index]] = (),
    ):
        self.name = name
        self.objects = self._store_factory(objects)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            name=tokenizer.tokenize_name(name=self.name),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def __setstate__(self, state):
        name = state["name"]
        state["objects"] = self._store_factory(
            o.buildfrom(o, schema_name=name) for o in state["objects"]
        )
        super().__setstate__(state)

    def create(self, if_not_exists: bool = False) -> qb.queries.ddl.CreateSchema:
        return qb.queries.ddl.CreateSchema(self, if_not_exists=if_not_exists)

    def drop(self, if_exists: bool = False, cascade: Optional[bool] = None):
        return qb.queries.ddl.Drop(self, if_exists=if_exists, cascade=cascade)

    def creation_script(self) -> list[qb.queries.ddl.DDLQuery]:
        return [
            self.create(),
            *(o.create() for o in self.objects if hasattr(o, "create")),
        ]

    def _substitute(self, substitutions: Mapping) -> Self:
        objects = (
            o.substitute(substitutions) if hasattr(o, "substitute") else o
            for o in self.objects
        )

        return self.buildfrom(self, objects=objects)


class DB:
    schemata: qbstores.UNamedStore
    __slots__ = ("schemata",)

    _store_factory = qbstores.UNamedStore

    def __init__(self, schemata: Iterable[Schema]):
        self.schemata = self._store_factory(schemata)
