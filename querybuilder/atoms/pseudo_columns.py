from __future__ import annotations
from typing import Optional
from querybuilder.atoms.atoms import Atom
import querybuilder as qb


class Star(Atom):
    """SQL star for selection

    The `*` symbol that can be used in `SELECT * FROM ...` or `... count(*) ...`
    statements for example.  This is NOT a `Column`.
    """

    __slots__ = ("relation",)

    def __init__(self, relation: Optional[qb.atoms.relations.Named] = None):
        self.relation = relation

    def _get_subtokenize_kwargs(
        self, tokenizer: qb.drivers.sql.tokenizer.Tokenizer
    ) -> dict[str, qb.formatting.tokentree.TkTree | None]:
        relation = None
        if self.relation:
            relation = self.relation.subtokenize(tokenizer)

        return {"relation": relation}


class Default(Atom):
    """A pseudo-column, whose value is provided by the DBMS at execution time

    Examples
    --------

    >>> from querybuilder.atoms.pseudo_columns import Default
    >>> str(Default())
    'DEFAULT'
    """

    __slots__ = ()
