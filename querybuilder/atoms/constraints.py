from __future__ import annotations
from typing import Optional, Self, Mapping
from enum import Enum

from querybuilder.atoms.atoms import Atom
from querybuilder.atoms.clauses import OnChangeWrapper
import querybuilder.atoms.columns as qbcolumns


class Constraint(Atom):
    __slots__ = ("_name", "deferrable", "initially_deferred")

    def __init__(
        self, name: Optional[str] = None, deferrable=None, initially_deferred=None
    ):
        self._name = name
        self.deferrable = deferrable = (
            deferrable if deferrable is not None or initially_deferred is None else True
        )
        self.initially_deferred = None if deferrable is False else initially_deferred

    def _get_subtokenize_kwargs(self, tokenizer):
        sup = super()._get_subtokenize_kwargs(tokenizer)

        if self.deferrable:
            if self.initially_deferred:
                sup["defer"] = "DEFERRABLE INITIALLY DEFERRED"
            elif self.initially_deferred is False:
                sup["defer"] = "DEFERRABLE INITIALLY IMMEDIATE"
        elif self.deferrable is False:
            sup["defer"] = "NOT DEFERRABLE"

        if self.name:
            sup["name"] = self.name

        return sup

    @property
    def name(self) -> Optional[str]:
        if self._name:
            return self._name
        return None

    @classmethod
    def buildfrom(cls, *instances, **overstate):
        if "name" in overstate:
            assert "_name" not in overstate
            overstate["_name"] = overstate.pop("name")
        return super().buildfrom(*instances, **overstate)


class ColumnConstraint(Constraint):
    __slots__ = ()


class SubcolumnConstraint(Constraint):
    __slots__ = ("subcolumn",)

    def __init__(self, subcolumn: qbcolumns.Column, **kwargs):
        self.subcolumn = subcolumn
        super().__init__(**kwargs)

    def _substitute(self, substitutions: Mapping) -> Self:
        subcolumn = self.subcolumn.substitute(substitutions)

        return self.buildfrom(self, subcolumn=subcolumn)


class ColumnNotNull(ColumnConstraint):
    __slots__ = ()


class ColumnDefault(SubcolumnConstraint, ColumnConstraint):
    __slots__ = ()

    def __init__(self, expression: qbcolumns.Column, **kwargs):
        super().__init__(expression, **kwargs)

    @property
    def expression(self):
        return self.subcolumn

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class ColumnCheck(SubcolumnConstraint, ColumnConstraint):
    __slots__ = ()

    # overloading for renaming parameter
    def __init__(self, expression: qbcolumns.Column, **kwargs):
        super().__init__(expression, **kwargs)

    @property
    def expression(self):
        return self.subcolumn

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class ColumnGeneratedAlwaysAs(SubcolumnConstraint, ColumnConstraint):
    __slots__ = ()

    def __init__(self, expression: qbcolumns.Expression, **kwargs):
        super().__init__(expression, **kwargs)

    @property
    def expression(self):
        return self.subcolumn

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class ColumnGeneratedAsIdentity(ColumnConstraint):
    __slots__ = ("always",)

    def __init__(self, always=True, **kwargs):
        self.always = always
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(always=self.always, **super()._get_subtokenize_kwargs(tokenizer))


class ColumnUnique(ColumnConstraint):
    __slots__ = ()


class ColumnPrimaryKey(ColumnUnique):
    __slots__ = ()


class OnChangePolicy(Enum):
    SET_NULL = "SET NULL"
    SET_DEFAULT = "SET DEFAULT"
    CASCADE = "CASCADE"
    RESTRICT = "RESTRICT"
    NO_ACTION = "NO ACTION"


class ColumnReferences(SubcolumnConstraint, ColumnConstraint):
    __slots__ = ("ondelete", "onupdate")
    # TODO: match / deferrable?

    def __init__(
        self,
        refcolumn: qbcolumns.Named,
        ondelete: Optional[OnChangePolicy] = None,
        onupdate: Optional[OnChangePolicy] = None,
        **kwargs,
    ):
        self.ondelete = ondelete
        self.onupdate = onupdate
        super().__init__(refcolumn, **kwargs)

    @property
    def refcolumn(self):
        return self.subcolumn

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = dict(
            schema_name=self.refcolumn.schema_name,
            relation_name=self.refcolumn.relation_name,
            column_name=self.refcolumn.name,
        )

        if self.ondelete:
            kwargs["ondelete"] = OnChangeWrapper("DELETE", self.ondelete).subtokenize(
                tokenizer
            )
        if self.onupdate:
            kwargs["onupdate"] = OnChangeWrapper("UPDATE", self.onupdate).subtokenize(
                tokenizer
            )

        return dict(super()._get_subtokenize_kwargs(tokenizer), **kwargs)


class TableConstraint(SubcolumnConstraint, Constraint):
    __slots__ = ()

    def __init__(self, expression: qbcolumns.Column, **kwargs):
        super().__init__(expression, **kwargs)

    @property
    def expression(self):
        return self.subcolumn

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            expression=self.expression.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class TableUnique(TableConstraint):
    __slots__ = ()

    def __init__(self, column: qbcolumns.Named | qbcolumns.Tuple, **kwargs):
        super().__init__(expression=column, **kwargs)

    # TODO: distinct_nulls ?

    def _get_subtokenize_kwargs(self, tokenizer):
        # TODO: why?
        return super()._get_subtokenize_kwargs(tokenizer)


class TableCheck(TableConstraint):
    __slots__ = ()


class TablePrimaryKey(TableUnique):
    __slots__ = ()


class TableForeignKey(TableConstraint):
    __slots__ = ("refcolumn", "ondelete", "onupdate")

    def __init__(
        self,
        column: qbcolumns.Column,
        refcolumn: qbcolumns.Column | str,
        ondelete: Optional[OnChangePolicy] = None,
        onupdate: Optional[OnChangePolicy] = None,
        **kwargs,
    ):
        if isinstance(refcolumn, str):
            refcolumn = qbcolumns.Named(column.sqltype, *reversed(refcolumn.split(".")))

        self.refcolumn = refcolumn
        self.ondelete = ondelete
        self.onupdate = onupdate
        super().__init__(expression=column, **kwargs)

    @property
    def column(self):
        return self.subcolumn

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs.update(
            schema_name=self.refcolumn.schema_name,
            relation_name=self.refcolumn.relation_name,
            column_name=self.refcolumn.name,
        )

        if self.ondelete:
            kwargs["ondelete"] = OnChangeWrapper("DELETE", self.ondelete).subtokenize(
                tokenizer
            )
        if self.onupdate:
            kwargs["onupdate"] = OnChangeWrapper("UPDATE", self.onupdate).subtokenize(
                tokenizer
            )

        return dict(super()._get_subtokenize_kwargs(tokenizer), **kwargs)

    def _substitute(self, substitutions: Mapping) -> Self:
        refcolumn = self.refcolumn.substitute(substitutions)

        self = self.buildfrom(self, refcolumn=refcolumn)

        return super()._substitute(substitutions)
