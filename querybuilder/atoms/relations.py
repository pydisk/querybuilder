from __future__ import annotations
from typing import (
    Tuple,
    Iterable,
    Mapping,
    Optional,
    cast,
    Any,
    Sequence,
    Self,
)
from typing_extensions import ClassVar
import functools
import itertools

from querybuilder.utils.constants import JoinType
from querybuilder.utils.decorators import method_accepting_lambdas
from querybuilder.utils.stores import Frozenmap, NamedStore, UNamedStore
from querybuilder.utils.typing import NamedProto
import querybuilder.helpers.columns as qbcolhelpers
from querybuilder.atoms.atoms import Atom
import querybuilder.atoms.columns as qbcolumns
import querybuilder.atoms.clauses as qbclauses
import querybuilder as qb


# Base classes
class Relation(Atom):
    """Base class for relations"""

    __slots__ = ("columns",)
    _column_store_factory: ClassVar[type] = NamedStore
    _scopable = False

    @property
    def c(self) -> NamedStore:
        return self.columns

    def __init__(self, columns=(), **kwargs):
        self._init_columns(columns)
        super().__init__(**kwargs)

    def _init_columns(self, columns):
        self.columns = self._column_store_factory(columns)

    def __setstate__(self, state):
        columns = state.pop("columns")
        super().__setstate__(state)
        self._init_columns(columns)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for c in self.columns:
            accumulator = c.accept(accumulator)
        return accumulator

    # RELATION SPECIFICS
    @property
    def arity(self):
        return len(self.columns)

    def alias(
        self,
        name: str,
        column_aliases: Mapping[int, str] | Iterable[tuple[int, str]] = (),
    ):
        return Aliased(self, name, column_aliases)

    def _substitute(self, substitutions: Mapping) -> Self:
        columns = tuple(c.substitute(substitutions) for c in self.columns)

        return self.buildfrom(self, columns=columns)


class Fromable(Relation):
    # An example of non-joinable Fromable is "a INNER JOIN b ON a.id=b.fid" see, e.g.,
    #       "c LEFT JOIN a INNER JOIN b ON a.id=b.fid ON c.id=a.fid"
    # Actually, no.  Above example works on both postgresql and sqlite3 (parenthesis
    #       required around INNER JOIN in sqlite3).
    __slots__ = ()

    @method_accepting_lambdas
    def select(
        self,
        columns: Optional[
            Sequence[str | int | qbcolumns.Column | qb.atoms.pseudo_columns.Star]
        ] = None,
        **kwargs,
    ) -> qb.queries.dql.Select:
        """

        Parameters
        ----------
        columns: tuple or None, default=None
            Either a tuple of column specifications or None.  In the latter
            case, all the columns of the relation `self` are used.  Column
            specifications are either indices (int), or unambiguous names
            (str), or columns.  If an index `i`, then the i-th column of
            the subrelation `self` is selected (`self.columns.byindex[i]`).
            If a name `n`, then the corresponding column is selected, if
            unambiguous (`self.columns.byname[n]`).

        Raises
        ------
        IndexError:
            if a column specified by index does not exist.

        KeyError:
            if a column specified by name does not exist.
        """
        columns_to_select: list[qbcolumns.Column | qb.atoms.pseudo_columns.Star]
        if columns is None:
            columns_to_select = self.columns
        else:
            columns_to_select = []
            for c in columns:
                if c == "*":
                    c = qb.atoms.pseudo_columns.Star()
                    columns_to_select.append(qb.atoms.pseudo_columns.Star())
                elif isinstance(c, (qbcolumns.Column, qb.atoms.pseudo_columns.Star)):
                    columns_to_select.append(c)
                else:
                    # assert isinstance(c, (int, str))
                    columns_to_select.append(self.columns.resolve(c))
        return qb.queries.dql.Select(
            from_=self, selected_columns=columns_to_select, **kwargs
        )

    @method_accepting_lambdas
    def join(self, jointype, other, on=None):
        """Returned a Join relation

        Parameters
        ----------
        jointype: str or qb.utils.constants.JoinType
            a value for qb.utils.constants.JoinType enumeration
        other: Fromable
            the relation with which to join
        on: column or None
            the column conditioning the join

        Notes
        -----
        Fromable's join_to method is acts as a shorthand to the
        present join method, in which self and other argument
        are exchanged.  E.g., `self.join(jointype, other, col)`
        returns the result of `other.join(jointype, self, col)`.
        """
        jointype = JoinType(jointype)
        return Join(jointype, self, other, on=on)

    cross_join = functools.partialmethod(join, JoinType.CROSS)
    left_join = functools.partialmethod(join, JoinType.LEFT)
    right_join = functools.partialmethod(join, JoinType.RIGHT)
    full_join = functools.partialmethod(join, JoinType.FULL)
    inner_join = functools.partialmethod(join, JoinType.INNER)

    @functools.wraps(join)
    @method_accepting_lambdas
    def join_to(self, jointype, other, on=None):
        return other.join(jointype, self, on=on)

    cross_join_to = functools.partialmethod(join_to, JoinType.CROSS)
    left_join_to = functools.partialmethod(join_to, JoinType.LEFT)
    right_join_to = functools.partialmethod(join_to, JoinType.RIGHT)
    full_join_to = functools.partialmethod(join_to, JoinType.FULL)
    inner_join_to = functools.partialmethod(join_to, JoinType.INNER)

    @method_accepting_lambdas
    def product(self, other: Fromable, **kwargs) -> CartesianProduct:
        return CartesianProduct([self, other], **kwargs)

    cartesian_product = product


class Prenamed(Relation):
    """Relation with a name, and possibly a schema_name

    Unlike Named relations, the columns of prenamed column do not inherit their relation
    name and schema names from the relation name and schema name.  They are aimed for
    internal use, when building schemas.

    Parameters
    ----------
    name: str
        the name of the relation.

    schema_name: Optional[str], default=None
        the schema name of the relation.

    **kwargs
        additional keyworded parameters (e.g., columns) for super initialization.
    """

    __slots__ = ("name", "schema_name")

    def __init__(self, name: str, schema_name: Optional[str] = None, **kwargs):
        self.name = name
        self.schema_name = schema_name
        super().__init__(**kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            name=tokenizer.tokenize_name(
                schema_name=self.schema_name, relation_name=self.name
            ),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class Named(Prenamed, Fromable):
    """Named relations, which are _fromable_

    Parameters
    ----------
    name: str
        the name of the relation.

    schema_name: Optional[str], default=None
        the schema name of the relation.

    **kwargs
        additional keyworded parameters (e.g., columns) for super initialization.
    """

    __slots__ = ()
    _scopable = False
    _preserve_table_column = False

    def _init_columns(self, columns: Iterable[qbcolumns.Column]):
        aliases = getattr(self, "aliases", {})
        columns = (
            qbcolhelpers.name_column(
                c,
                aliases.get(i),
                relation_name=self.name,
                schema_name=self.schema_name,
                preserve_table_column=self._preserve_table_column,
            )
            for i, c in enumerate(columns)
        )
        super()._init_columns(columns)

    def star(self):
        return qb.atoms.pseudo_columns.Star(relation=self)

    @classmethod
    def from_query(cls: type[Self], query: qb.queries.dql.DQLQuery, name: str) -> Self:
        return cls(name, columns=query.columns)


class RecursiveNamed(Named):
    __slots__ = ()


# Actual classes
class Aliased(Named):
    __slots__: tuple[str, ...] = ("subrelation", "column_aliases")

    def __init__(
        self,
        subrelation: Relation,
        name: str,
        column_aliases: Mapping[int, str] | Iterable[tuple[int, str]] = (),
        **kwargs,
    ):
        self.subrelation = subrelation
        self.column_aliases: Mapping[int, str] = Frozenmap(column_aliases)

        if self.column_aliases:
            for i, c in enumerate(self.subrelation.columns):
                if i not in self.column_aliases and not isinstance(c, qbcolumns.Named):
                    raise ValueError("Missing alias for an unnamed column")

        super().__init__(name, **kwargs)

    def _get_column_names(self):
        return tuple(
            self.column_aliases[i] if i in self.column_aliases else c.name
            for i, c in enumerate(self.subrelation.columns)
        )

    @property
    def columns(self):
        return self._column_store_factory(
            (
                qbcolhelpers.name_column(c, self.column_aliases[i])
                if i in self.column_aliases
                else qbcolhelpers.name_column(c, relation_name=self.name)
                for i, c in enumerate(self.subrelation.columns)
            )
        )

    def _init_columns(self, columns):
        assert not columns

    def alias(self, name):
        return self.buildfrom(self, name=name)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["subrelation"] = self.subrelation.subtokenize(tokenizer, scoped=True)
        if self.column_aliases:
            CW = qb.atoms.clauses.AliasedColumn
            kwargs["columns"] = tuple(
                map(
                    lambda c: CW(qbcolhelpers.unqualify(c)).subtokenize(tokenizer),
                    self.columns,
                )
            )
        else:
            kwargs["columns"] = ()

        return kwargs

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        accumulator = self.subrelation.accept(accumulator)
        return accumulator

    def _substitute(self, substitutions: Mapping):
        subrel = self.subrelation.substitute(substitutions)

        return self.buildfrom(self, subrelation=subrel)

    def __getstate__(self):
        state = super().__getstate__()
        state["subrelation"] = self.subrelation
        state["column_aliases"] = self.column_aliases
        state["columns"] = None  # maybe a little ugly?

        return state


class With(Named):
    __slots__ = ("subrelation", "materialized")

    @property
    def recursive(self) -> bool:
        recdetector = qb.visitors.detectors.RecursiveRelationDetector()
        recdetector = self.descend(recdetector)
        return recdetector.found

    def __init__(
        self,
        subrelation: qb.queries.dql.DQLQuery,
        name: str,
        materialized=None,
        **kwargs,
    ):
        columns = subrelation.columns
        self.subrelation = subrelation
        self.materialized = materialized
        super().__init__(name, columns=columns)

    def to_definition_clause(self):
        return qbclauses.WithClause(
            name=self.name, query=self.subrelation, materialized=self.materialized
        )

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        accumulator = self.subrelation.accept(accumulator)
        return accumulator

    def _substitute(self, substitutions: Mapping):
        subrel = self.subrelation.substitute(substitutions)

        return self.buildfrom(self, subrelation=subrel)


class CartesianProduct(Fromable):
    __slots__ = ("subrelations",)
    _scopable = False

    def __init__(self, subrelations, **kwargs):
        self.subrelations = subrelations
        super().__init__(columns=(), **kwargs)

    def _init_columns(self, columns: Iterable[qbcolumns.Column]):
        columns = itertools.chain.from_iterable((r.columns for r in self.subrelations))
        super()._init_columns(columns)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            subrelations=tuple(
                r.subtokenize(tokenizer, scoped=True) for r in self.subrelations
            ),
            **super()._get_subtokenize_kwargs(tokenizer),
        )

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        for r in self.subrelations:
            accumulator = r.accept(accumulator)
        return accumulator

    def _substitute(self, substitutions: Mapping):
        subrelations = tuple(r.substitute(substitutions) for r in self.subrelations)

        return self.buildfrom(self, subrelations=subrelations)


class Join(CartesianProduct):
    """Joined relations

    Parameters
    ----------
    jointype: qb.utils.constants.JoinType
        the type of join to perform.

    left: relations.Named
        the relation on the left hand side of the join.

    right: relations.Named
        the relation on the right hand side of the join.

    on: column or None, default=None
        a join condition.

    using: str or None, default=None
        column name on which to join.

    natural: bool, default=False
        whether the join is natural or not.

    **kwargs: mapping
        further keyworded parameters for super initialization.

    """

    __slots__ = ("jointype", "on", "using", "natural")
    _scopable = True

    def __init__(
        self,
        jointype,
        left: Fromable,
        right: Fromable,
        on: Optional[qbcolumns.Column] = None,
        using: Optional[tuple[str] | str] = None,
        natural=False,
        **kwargs,
    ):
        oun = sum(map(bool, (on, using, natural)))
        if not oun:
            # TODO: smart behavior there: is it acceptable?
            #       (sqlite3 does the same, not PostgreSQL)
            on = qbcolumns.True_()
        elif oun > 1:
            raise ValueError(
                "Parameters 'on', 'using', and 'natural' are mutually exclusive"
            )
        if using and not isinstance(using, tuple):
            using = (using,)

        self.jointype = jointype
        self.on = on
        self.using = using
        self.natural = natural
        super().__init__(subrelations=(left, right), **kwargs)

    def _init_columns(self, columns):
        if not self.using and not self.natural:
            return super()._init_columns(columns)
        # In SQL, the output of JOIN … USING and NATURAL JOIN suppresses redundant
        # columns.  In the resulting joint relation, these columns are somehow known
        # under several qualified names.
        # Example:
        #   SELECT a.id FROM a INNER JOIN b USING (id);
        #   SELECT b.id FROM a INNER JOIN b USING (id);
        # Here, we consider a.id to be a column of the joint relation, but not b.id.
        columns = []
        for col in columns:
            if self.using and col.name in self.using or self.natural:
                if col.name in columns._keys:
                    continue
            columns.append(col)
        super()._init_columns(columns)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        if self.on:
            accumulator = self.on.accept(accumulator)
        return accumulator

    @property
    def left(self):
        return self.subrelations[0]

    @property
    def right(self):
        return self.subrelations[1]

    @method_accepting_lambdas
    def set_on(self, on):
        assert not self.using
        assert not self.natural
        return self.buildfrom(self, on=on)

    @method_accepting_lambdas
    def add_on(self, on):
        if self.on:
            return self.set_on(self.on & on)
        assert not self.using
        assert not self.natural
        return self.set_on(on)

    def _substitute(self, substitutions: Mapping):
        on = self.on and self.on.substitute(substitutions)

        self = self.buildfrom(self, on=on)

        return super()._substitute(substitutions)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = dict(**super()._get_subtokenize_kwargs(tokenizer))
        kwargs.update(jointype=self.jointype, natural=self.natural)
        if self.on is not None:
            kwargs.update(on=self.on.subtokenize(tokenizer))
        elif self.using is not None:
            kwargs.update(using=self.using)
        return kwargs


class Table(Named):
    __slots__ = ("constraints",)
    _column_store_factory = UNamedStore
    _preserve_table_column = True

    def __init__(
        self,
        name,
        columns: Iterable[qbcolumns.TableColumn],
        schema_name: Optional[str] = None,
        constraints: Iterable[qb.atoms.constraints.TableConstraint] = (),
        **kwargs,
    ):
        cons: Iterable[NamedProto] = cast(Iterable[NamedProto], constraints)
        self.constraints = UNamedStore(cons)
        super().__init__(name, schema_name=schema_name, columns=columns, **kwargs)

    def create(
        self,
        if_not_exists: bool = False,
        as_query: Optional[qb.queries.dql.DQLQuery] = None,
        temporary: bool = False,
    ) -> qb.queries.ddl.CreateTable:
        return qb.queries.ddl.CreateTable(
            self, if_not_exists=if_not_exists, as_query=as_query, temporary=temporary
        )

    def drop(self, if_exists: bool = False, cascade: Optional[bool] = None):
        return qb.queries.ddl.Drop(self, if_exists=if_exists, cascade=cascade)

    @method_accepting_lambdas
    def alter(self, *args, **kwargs):
        raise NotImplementedError()

    @method_accepting_lambdas
    def insert(
        self,
        in_columns: tuple[qbcolumns.Named, ...] = (),
        # TODO: display following in doc:
        # `in_columns: tuple[Named, ...] | Callable[[Table], tuple[Named, ...]] = (),`
        query: Optional[qb.queries.dql.DQLQuery] = None,
        **kwargs,
    ) -> qb.queries.dml.Insert:
        return qb.queries.dml.Insert(self, in_columns, query, **kwargs)

    @method_accepting_lambdas
    def insert_values(
        self,
        in_columns: tuple[qbcolumns.Named, ...] = (),
        values: Iterable[
            Tuple[
                qbcolumns.Constant
                | qbcolumns.Placeholder
                | qb.atoms.pseudo_columns.Default
                | Any,
                ...,
            ]
        ] = (),
        **kwargs,
    ) -> qb.queries.dml.Insert:
        if not values:
            return self.insert(in_columns)

        prepared_values: list[
            tuple[qbcolumns.Column | qb.atoms.pseudo_columns.Default, ...]
        ] = []
        for t in values:
            prepared_t: list[qbcolumns.Column | qb.atoms.pseudo_columns.Default] = []
            for i, c in enumerate(t):
                if isinstance(c, (qbcolumns.Column, qb.atoms.pseudo_columns.Default)):
                    prepared_t.append(c)
                else:
                    prepared_t.append(
                        qb.helpers.columns.make_column(c, sqltype=in_columns[i].sqltype)
                    )
            prepared_values.append(tuple(prepared_t))
        q = qb.queries.dql.Values(prepared_values)

        return self.insert(in_columns, q)

    @method_accepting_lambdas
    def insert_many(
        self,
        in_columns: tuple[qbcolumns.Named, ...],
        keys: Optional[Mapping[int, str]] = None,
        **kwargs,
    ) -> qb.queries.dml.Insert:
        if not keys:
            keys = {}
        values = []
        for i, c in enumerate(in_columns):
            key = keys.get(i, c.name)
            values.append(c.to_placeholder(key))

        return self.insert_values(in_columns, [tuple(values)])

    @method_accepting_lambdas
    def delete(self, where: Optional[qbcolumns.Column] = None, **kwargs):
        return qb.queries.dml.Delete(self, where, **kwargs)

    def _resolve_set_columns_pair(
        self, k: str | qbcolumns.Column, v: Any
    ) -> Tuple[qbcolumns.Column, qbcolumns.Column]:
        if not isinstance(k, qbcolumns.Column):
            k = cast(qbcolumns.Column, self.columns.resolve(k))

        if not isinstance(v, qbcolumns.Column):
            v = cast(qbcolumns.Column, qbcolhelpers.make_column(v))

        return k, v

    def update(
        self,
        set_columns: Optional[
            Mapping[qbcolumns.Column | str | int, Any | qbcolumns.Column]
        ] = None,
        /,
        where: Optional[qbcolumns.Column] = None,
        **kwargs,
    ) -> qb.queries.dml.Update:
        def key_resolver(k):
            return k if isinstance(k, qbcolumns.Column) else self.columns.resolve(k)

        def val_resolver(v, sqltype):
            if isinstance(v, qbcolumns.Column):
                return v
            return qbcolhelpers.make_column(v, sqltype=sqltype)

        set_cols = {}
        if set_columns:
            for k, v in set_columns.items():
                k_col = key_resolver(k)
                v_col = val_resolver(v, k_col.sqltype)
                set_cols[k_col] = v_col

        for k, v in kwargs.items():
            k_col = key_resolver(k)
            if k_col in set_cols:
                raise KeyError(f"Duplicate key {k}")
            v_col = val_resolver(v, k_col.sqltype)
            set_cols[k_col] = v_col

        return qb.queries.dml.Update(self, set_cols, where)

    def truncate(self):
        return self.delete()

    def create_index(
        self,
        name: str,
        columns: tuple[qbcolumns.Column | qb.atoms.schemas.IndexedColumn, ...],
        unique: bool = False,
        where: Optional[qbcolumns.Expression] = None,
        if_not_exists: bool = False,
    ) -> qb.queries.ddl.CreateIndex:
        index = qb.atoms.schemas.Index(name, self, columns, unique, where)
        return index.create(if_not_exists=if_not_exists)

    def _substitute(self, substitutions: Mapping) -> Self:
        constraints = tuple(
            c.substitute(substitutions) if hasattr(c, "substitute") else c
            for c in self.constraints
        )

        self = self.buildfrom(self, constraints=constraints)

        return super()._substitute(substitutions)


class View(Named):
    __slots__ = ("aliases", "defquery")

    def __init__(
        self,
        name: str,
        defquery: qb.queries.dql.DQLQuery,
        schema_name: Optional[str] = None,
        aliases=(),
        **kwargs,
    ):
        self.defquery = defquery
        self.aliases = aliases = Frozenmap(aliases)
        columns = defquery.columns
        super().__init__(name, columns=columns, schema_name=schema_name, **kwargs)

    @property
    def column_origins(self):
        return self.defquery.columns

    def create(
        self, if_not_exists: bool = False, temporary: bool = False
    ) -> qb.queries.ddl.CreateView:
        return qb.queries.ddl.CreateView(
            self, if_not_exists=if_not_exists, temporary=temporary
        )

    def drop(self, if_exists: bool = False, cascade: Optional[bool] = None):
        return qb.queries.ddl.Drop(self, if_exists=if_exists, cascade=cascade)

    @method_accepting_lambdas
    def alter(self, *args, **kwargs):
        raise NotImplementedError()

    @method_accepting_lambdas
    def insert(self, *args, **kwargs):
        raise NotImplementedError()

    @method_accepting_lambdas
    def delete(self, *args, **kwargs):
        raise NotImplementedError()

    @method_accepting_lambdas
    def update(self, *args, **kwargs):
        raise NotImplementedError()

    def truncate(self, *args, **kwargs):
        raise NotImplementedError()

    def _substitute(self, substitutions: Mapping):
        defquery = self.defquery.substitute(substitutions)

        return self.buildfrom(self, defquery=defquery)


# TODO
class TableAs(View):
    __slots__ = ()
