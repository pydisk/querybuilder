from __future__ import annotations
from itertools import chain
from typing import Mapping
import querybuilder.formatting as qbformatting
import querybuilder as qb


class Atom(object):
    __slots__ = ()
    _scopable = False

    @classmethod
    def __init_subclass__(cls, **kwargs):
        if "__slots__" in cls.__dict__ and "__getstate__" not in cls.__dict__:

            def __getstate__(self):
                state = super(cls, self).__getstate__()
                state |= {k: getattr(self, k) for k in cls.__slots__}
                post = cls.__dict__.get("_post_getstate")
                if post:
                    state = post(self, state)
                return state

            setattr(cls, "__getstate__", __getstate__)

    def accept(self, accumulator):
        """Accepts an accumulator that will visit this object."""

        accumulator = accumulator.enter(self)
        accumulator = self.descend(accumulator)
        accumulator = accumulator.exit(self)
        return accumulator

    def descend(self, accumulator):
        return accumulator

    def get_free_with(self):
        """Gets the free queries that appear in this object.

        Returns
        -------
            a WithQueryStore that contains the free with queries of this object
        """
        return self.accept(qb.visitors.stores.WithQueryStore())

    def get_placeholders(self):
        return self.descend(qb.visitors.stores.PlaceholderStore())

    def substitute(self, substitutions: Mapping[Atom, Atom]):
        subs = substitutions.get(self, None)
        if subs:
            return subs
        return self._substitute(substitutions)

    def _substitute(self, substitutions: Mapping[Atom, Atom]):
        return self

    def __getstate__(self):
        return {}

    def __setstate__(self, state):
        for k, v in state.items():
            setattr(self, k, v)

    @classmethod
    def buildfrom(cls, *instances, **kwargs):
        state = {}

        for inst in instances:
            state.update(inst.__getstate__())

        state.update(kwargs)
        self = object.__new__(cls)
        self.__setstate__(state)

        return self

    def __eq__(self, other):
        return (
            self.__class__ == other.__class__
            and self.__getstate__() == other.__getstate__()
        )

    def __hash__(self):
        return hash((self.__class__, *sorted(self.__getstate__().items())))

    def _get_subtokenize_kwargs(self, tokenizer):
        return {}

    def _get_slot_values(self):
        slots = chain.from_iterable(
            getattr(k, "__slots__", []) for k in self.__class__.mro()
        )
        return {k: getattr(self, k) for k in slots}

    def subtokenize(self, tokenizer, scoped=False):
        self = tokenizer.transform(self)
        if scoped and self._scopable:
            return tokenizer.scope_it(self.subtokenize(tokenizer))
        return tokenizer(self, **self._get_subtokenize_kwargs(tokenizer))

    def tokenize(self, tokenizer):
        return tokenizer._post(self.subtokenize(tokenizer))

    def _pretty_subtokenize(
        self,
        tokenizer: qb.drivers.sql.tokenizer.Tokenizer,
        prompt: qbformatting.tokentree.TkTree = (),
    ) -> qbformatting.tokentree.TkTree:
        return (
            (prompt, self.subtokenize(tokenizer)),
            tokenizer.tokenize_values(self.get_placeholders()),
        )

    def _pretty(
        self, prompt: qbformatting.tokentree.TkTree = (), cycle: bool = False
    ) -> str:
        tokenizer = qb.settings["tokenizer"]
        formatter = qb.settings["pretty_formatter"]
        tokens: qbformatting.tokentree.TkTree
        if cycle:
            tokens = qbformatting.preformatter.flatten(
                (
                    prompt,
                    qbformatting.tokentree.TkStr(
                        qbformatting.token.Token.Punctuation.Ellipsis, "…"
                    ).to_seq(),
                )
            )
        else:
            tokens = self._pretty_subtokenize(tokenizer, prompt=prompt)
        formatted = formatter.get_formatted(tokens)

        return formatted

    def __str__(self):
        tokenizer = qb.settings["tokenizer"]
        formatter = qb.settings["raw_formatter"]
        tokens = self.subtokenize(tokenizer)
        return formatter.get_formatted(tokens)
        seq = qbformatting.preformatter.flatten(tokens)
        return str(seq)

    def pretty_print(self, **kwargs):
        print(self._pretty(), **kwargs)

    def _repr_pretty_(self, printer, cycle: bool = False):
        prompt: qbformatting.tokentree.TkTree
        if (
            hasattr(printer, "group_queue")
            and len(getattr(printer.group_queue, "queue", [])) > 2
        ):
            prompt = ()
        else:
            prompt = qbformatting.tokentree.TkStr(
                qbformatting.token.Token.OutPrompt, f"{self.__class__.__name__}:"
            ).to_seq()
        pretty = self._pretty(prompt=prompt, cycle=cycle)
        printer.text(pretty)
