from __future__ import annotations
from typing import Literal, Optional, Union, Mapping
from querybuilder.atoms.atoms import Atom
import querybuilder.utils.constants as qbconstants
import querybuilder.helpers.columns as qbcolhelpers
import querybuilder as qb


class ClauseWrapper(Atom):
    __slots__ = ()


class ColumnClauseWrapper(ClauseWrapper):
    # This is not a column
    __slots__ = ("column",)

    def __init__(self, column: Optional[qb.atoms.columns.Column]):
        self.column = column

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        if self.column:
            kwargs = dict(column=self.column.subtokenize(tokenizer), **kwargs)
        return kwargs


class DistinctColumn(ColumnClauseWrapper):
    __slots__ = ()

    def __init__(self, arg: Union[qb.atoms.columns.Column, Literal[True]]):
        if not arg:
            raise ValueError(arg)
        elif arg is True:
            column = None
        else:
            column = arg
        super().__init__(column=column)


class AliasedColumn(ColumnClauseWrapper):
    # Not a Column
    __slots__ = ("alias",)

    def __init__(self, column: qb.atoms.columns.Column, alias: Optional[str] = None):
        self.alias = alias
        super().__init__(column=column)

    def _get_subtokenize_kwargs(self, tokenizer):
        sup = super()._get_subtokenize_kwargs(tokenizer)
        if self.alias:
            return dict(alias=self.alias, **sup)
        return sup


class TableColumnWrapper(ColumnClauseWrapper):
    def __init__(self, column: qb.atoms.columns.TableColumn):
        self.column = column

    def _get_subtokenize_kwargs(self, tokenizer):
        sup_kwargs = super()._get_subtokenize_kwargs(tokenizer)
        sup_kwargs.pop("column")
        kwargs = {}

        kwargs["constraints"] = tuple(
            c.subtokenize(tokenizer) for c in self.column.constraints
        )

        kwargs["name"] = tokenizer.tokenize_name(name=self.column.name)
        kwargs["sqltype"] = self.column.sqltype

        return dict(kwargs, **sup_kwargs)


class WhereColumn(ColumnClauseWrapper):
    __slots__ = ()


class GroupColumn(ColumnClauseWrapper):
    __slots__ = ()


class HavingColumn(ColumnClauseWrapper):
    __slots__ = ()


class OrderColumn(ColumnClauseWrapper):
    __slots__ = ("how",)

    def __init__(self, column: qb.atoms.columns.Column, how: Union[bool, None] = None):
        self.how = how
        super().__init__(column)


class IndexColumnWrapper(ColumnClauseWrapper):
    __slots__ = ()

    def _get_subtokenize_kwargs(self, tokenizer):
        return {}


class Offset(ClauseWrapper):
    __slots__ = ("offset",)

    def __init__(self, offset: int):
        self.offset = offset

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs = dict(offset=self.offset, **kwargs)
        return kwargs


class Limit(ClauseWrapper):
    __slots__ = ("limit", "with_ties")

    def __init__(self, limit: int, with_ties: bool):
        self.limit = limit
        self.with_ties = with_ties

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs = dict(limit=self.limit, with_ties=self.with_ties, **kwargs)
        return kwargs


class RelationClauseWrapper(ClauseWrapper):
    # This is not a column
    __slots__ = ("relation",)

    def __init__(self, relation: qb.atoms.relations.Fromable):
        self.relation = relation

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs = dict(relation=self.relation.subtokenize(tokenizer), **kwargs)
        return kwargs


class WithClause(ClauseWrapper):
    """A clause that represents the definition of a With query

    Parameters
    ----------

    name: str
        the name of the with query

    query: Query
        the underlying query that is aliased by the With

    materialized: Optional[bool]
        whether the With should be materialized or not.
        If None this value is left blank for the DBMS.

    Examples
    --------

    A WithClause can be obtained seamlessly from a With query:
    >>> from querybuilder.queries.dql import Select
    >>> from querybuilder.atoms.columns import Constant
    >>> s = Select((Constant(int, 1),))
    >>> wc = s.as_with(name="T", materialized=True).to_definition_clause()

    It can then be tokenized as the definition of the with query:
    >>> str(wc)
    'T AS MATERIALIZED (SELECT 1)'

    By default the "MATERIALIZED" field is blank:
    >>> str(s.as_with(name="T2").to_definition_clause())
    'T2 AS (SELECT 1)'
    """

    __slots__ = ("name", "query", "materialized")

    def __init__(
        self,
        name: str,
        query: qb.queries.queries.Query,
        materialized: Optional[bool] = None,
    ):
        self.name = name
        self.query = query
        self.materialized = materialized

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            name=self.name,
            query=self.query.subtokenize(tokenizer),
            materialized=self.materialized,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class SetCombinatorWrapper(ClauseWrapper):
    __slots__ = ("combinator", "all")

    def __init__(
        self, combinator: qbconstants.SetCombinator, all: Optional[bool] = None
    ):
        self.combinator = combinator
        self.all = all
        super().__init__()

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            combinator=self.combinator,
            all=self.all,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class TemporaryWrapper(
    ClauseWrapper
):  # A wrapper for the TEMPORARY property of some queries
    ...  # Implicitly true


class IfNotExistsWrapper(ClauseWrapper): ...  # Implicitly true


class IfExistsWrapper(ClauseWrapper):
    ...  # Implicitly true

    #  TODO: merge both clauses into IfExistsWrapper


class OnChangeWrapper(ClauseWrapper):
    __slots__ = "change", "policy"

    def __init__(self, change, policy):  # TODO: type and import :)
        self.change = change
        self.policy = policy

    def _get_subtokenize_kwargs(self, tokenizer):
        return self._get_slot_values()


class AsQueryWrapper(ClauseWrapper):
    __slots__ = "subquery"

    def __init__(self, subquery):  # TODO: type and import :)
        self.subquery = subquery
        super().__init__()

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            super()._get_subtokenize_kwargs(tokenizer),
            subquery=self.subquery.subtokenize(tokenizer),
        )


class DefaultValuesClause(ClauseWrapper):
    __slots__ = ()

    pass


class SetColumnsClause(ClauseWrapper):
    """Represents the SET clause of an update query."""

    __slots__ = "set_columns"

    def __init__(
        self, set_columns: Mapping[qb.atoms.columns.Named, qb.atoms.columns.Column]
    ):
        self.set_columns = {
            qbcolhelpers.unqualify(k): v for k, v in set_columns.items()
        }

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["set_columns"] = [
            k.eq(v).subtokenize(tokenizer) for (k, v) in self.set_columns.items()
        ]
        return kwargs


class DropCascadeClause(ClauseWrapper):
    """Represents the CASCADE clause of DROP table query."""

    __slots__ = ("cascade",)

    def __init__(self, cascade: Optional[bool]):
        self.cascade = cascade

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["cascade"] = self.cascade
        return kwargs


class DBObjectNameWrapper(ClauseWrapper):
    __slots__ = ("dbobj",)

    def __init__(self, dbobj: qb.atoms.atoms.Atom):
        self.dbobj = dbobj

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(dbobj=self.dbobj)


class UniqueClause(ClauseWrapper):
    __slots__ = ()


class StringWrapper(ClauseWrapper):
    __slots__ = ("string",)

    def __init__(self, string: str):
        self.string = string

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(string=self.string)


class CollationWrapper(StringWrapper):
    __slots__ = ()


class AscWrapper(ClauseWrapper):
    __slots__ = ("asc",)

    def __init__(self, asc: str):
        self.asc = asc

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(asc=self.asc)
