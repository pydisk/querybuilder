from __future__ import annotations
from itertools import chain
from functools import wraps
from typing import (
    Optional,
    Generic,
    Iterable,
    Mapping,
    Any,
    Callable,
    Self,
    TypeVar,
    Union,
    cast,
)
from querybuilder.utils.constants import MISSING, _MISSING_TYPE
from querybuilder.utils.decorators import method_accepting_lambdas
import querybuilder.helpers.columns as qbcolhelpers
from querybuilder.atoms.atoms import Atom
import querybuilder.atoms.constraints as qbconstraints
import querybuilder.atoms.relations as qbrelations
import querybuilder.utils.stores as qbstores
from querybuilder.formatting.token import Error
from querybuilder.formatting.tokentree import TkTree
import querybuilder as qb


Col = TypeVar("Col", bound="Column")


def columnize(f=None, *, constant=None, sqltype=None, sqltyper=None):
    """Method decorator

    The decorator replaces all positional arguments but the first one of
    the method to decorate as follows:  If the argument has an attribute
    column, then this attribute is called and the result is taken instead
    of column.  Otherwise, a Constant column (if constant evaluates to
    True) or an anonymous Value column (otherwise) is created.  The type
    of the created column is controlled by the decorator sqltype parameter
    and determined by the instance self's type (self is thus assumed to be
    a column, or, at least, to enjoy a 'sqltype' attribute) as fallback.

    Parameters
    ----------
    f: callable or None, default=None
        the function to decorate.  If given the decorated function is
        returned.  Otherwise, the parametrized decorator is returned.

    constant: bool, default=False
        whether to build Constant (True) or anonymous Value column (False)
        when a given argument is not a column.

    sqltype: type.Object or str or callable or None, default=None
        how to set the type of the created columns.  If None, the type of
        the first argument (which is thus expected to be a column) is taken.
        If a type.Object then this type is used.  If a string (str), then it
        should be a key of the dialect.types.  Finally, if a callable, then
        it is called with the constant to embed and should return a type.
    """

    def decorator(g):
        @wraps(g)
        def decored(self, *others):
            others = list(others)
            for i, o in enumerate(others):
                if not isinstance(o, Column):
                    others[i] = qbcolhelpers.make_column(
                        o, constant=constant, sqltype=sqltype
                    )
            return g(self, *others)

        return decored

    if f:
        return decorator(f)
    return decorator


# Abstracts
class Column(Atom):
    """Base class for SQL columns

    Parameters
    ----------

    sqltype: type
        The type of the column.

    **kwargs: mapping
        Additional keyworded parameters for super initialization.

    """

    __slots__ = ("sqltype",)
    _scopable = False

    def __init__(self, sqltype: type, **kwargs) -> None:
        self.sqltype = sqltype
        super().__init__(**kwargs)

    def _subrepr(self, pretty=False, **kwargs):
        raise NotImplementedError()
        formatter = (self.dialect.formatter, self.dialect.pretty_formatter)[pretty]
        try:
            return formatter.get_formatted(self.subtokenize())
        except NotImplementedError:
            return formatter.get_formatted(((Error, "<unknown>"),))

    def index(
        self, collation: Optional[str] = None, asc: Optional[bool] = None
    ) -> qb.atoms.schemas.IndexedColumn:
        return qb.atoms.schemas.IndexedColumn(self, collation=collation, asc=asc)

    # BOOLEAN OPERATIONS
    @columnize
    @method_accepting_lambdas
    def and_(self, first, *others):
        """Boolean disjunction of columns

        Same effect can be obtained with the binary operator &, namely,
        'col0 & col1' is equivalent to 'col0.and_(col1)'.
        """
        return BooleanCombination("AND", self, first, *others)

    @columnize
    @method_accepting_lambdas
    def or_(self, first, *others):
        """Boolean disjunction of columns

        Same effect can be obtained with the binary operator |, namely,
        'col0 | col1' is equivalent to 'col0.or_(col1)'.
        """
        return BooleanCombination("OR", self, first, *others)

    def not_(self):
        """Negation of column

        Same effect can be obtained with the unary infix operator ~, namely,
        '~col' is equivalent to 'col.not_()'.
        """
        return Not(self)

    @columnize
    @method_accepting_lambdas
    def coalesce(self, *others, **kwargs):
        return Coalesce(self.pretuple_with(*others), **kwargs)

    def compress(self, *, unsafe=False):
        """Method to compress a column into an equivalent one

        Parameters
        ----------
        unsafe: bool, default=False
            Whether to consider weak or strong equivalence.  In weak equivalence
            a column that would have implicitly cast in an operation or a
            combination, is considered as equivalent to the cast variant, even
            if their type differ.  E.g. '1 AND true' is weakly equivalent to '1'
            but strongly equivalent to 'cast(1 AS BOOLEAN)'.
        """
        return self

    # BOOLEAN TESTS
    @columnize
    @method_accepting_lambdas
    def eq(self, other):
        return Comparison("=", self, other)

    def neq(self, other):
        return self.eq(other).not_()

    @columnize
    @method_accepting_lambdas
    def lt(self, other):
        return Comparison("<", self, other)

    def geq(self, other):
        return self.lt(other).not_()

    @columnize
    @method_accepting_lambdas
    def gt(self, other):
        return Comparison(">", self, other)

    def leq(self, other):
        return self.gt(other).not_()

    def isnull(self):
        return Comparison("IS", self, Null())

    def isnotnull(self):
        return self.isnull().not_()

    @columnize
    @method_accepting_lambdas
    def like(self, other):
        return Comparison("LIKE", self, other)

    def notlike(self, other):
        return self.like(other).not_()

    @columnize
    @method_accepting_lambdas
    def ilike(self, other):
        return Comparison("ILIKE", self, other)

    def notilike(self, other):
        return self.ilike(other).not_()

    @method_accepting_lambdas
    def inset(self, other):
        """The membership SQL condition

        Parameters
        ----------
        other: sqlrelations.Named or sqlqueries.dql.Select or Tuple
            the set of value for which to test membership of self.  If a SELECT
            query, then the query will be nested to form a column of the form:
            "<column> IN (SELECT …)".  Of a named relation (e.g. a table), then
            the resulting column will be of the form: "<column> IN <table name>".
            Finally, if a tuple column, then the result will be of the form:
            "<column> IN (<column1>, <column2>, …)".
        """
        if type(other) in (tuple, list, set, frozenset):
            other = Tuple(*(Value(self.sqltype, o) for o in other))
        elif isinstance(other, qbrelations.Relation):
            return InRelation("IN", self, other)
        return Comparison("IN", self, other)

    def notinset(self, other):
        return self.inset(other).not_()

    # ARITHMETIC
    @columnize(constant=True, sqltype=int)
    @method_accepting_lambdas
    def add(self, *others):
        return ArithmeticOperation("+", self, *others)

    @columnize(constant=True, sqltype=int)
    @method_accepting_lambdas
    def sub(self, *others):
        return ArithmeticOperation("-", self, *others)

    @columnize(constant=True, sqltype=int)
    @method_accepting_lambdas
    def mul(self, *others):
        return ArithmeticOperation("*", self, *others)

    @columnize(constant=True, sqltype=int)
    @method_accepting_lambdas
    def div(self, *others):
        return ArithmeticOperation("/", self, *others)

    def neg(self):
        return Negate(self)

    @columnize(constant=True, sqltype=int)
    @method_accepting_lambdas
    def mod(self, other):
        return Transform("mod", self.tuple_with(other))

    # AGGREGATE
    def count(self, distinct=False):
        return Aggregate("count", self, distinct=distinct, sqltype=int)

    def sum(self, distinct=False):
        return Aggregate("sum", self, distinct=distinct, sqltype=self.sqltype)

    def min(self, distinct=False):
        return Aggregate("min", self, sqltype=self.sqltype)

    def max(self, distinct=False):
        return Aggregate("max", self, sqltype=self.sqltype)

    def any(self, distinct=False):
        return Aggregate("bool_or", self, sqltype=bool)

    def all(self, distinct=False):
        return Aggregate("bool_and", self, sqltype=bool)

    def avg(self, distinct=False):
        return Aggregate("avg", self, distinct=distinct, sqltype=self.sqltype)

    def string_agg(self, sep="\n", distinct=False):
        return Aggregate(
            "string_agg",
            self.tuple_with(Value(str, sep)),
            distinct=distinct,
            sqltype=str,
        )

    # CAST
    def cast(self, sqltype):
        return Cast(self, sqltype)

    # VALUED
    def to_placeholder(self, key: Optional[str | _MISSING_TYPE] = None) -> Placeholder:
        """convert the column to a Placeholder column

        Parameters
        ----------
        self : Constant
            the Constant column to transform into a Placeholder column.

        key : str or None, default=None
            the key of the Placeholder column to build.
        """
        if isinstance(key, _MISSING_TYPE):
            key = None
        return Placeholder(self.sqltype, key=key)

    def to_value(self, value, key: Optional[str | _MISSING_TYPE] = None):
        if MISSING == key:
            key = None
        return Value(self.sqltype, value, key=key)

    def to_constant(self, constant: Any) -> Constant:
        """convert the column to a Constant column."""
        return Constant(self.sqltype, constant)

    # MISC
    def ifnull(self, default):
        return IfNull(self, default)

    @columnize
    def iff(self, then: Column, else_: Column) -> Case:
        return Case((self, then), else_=else_)

    @columnize
    @method_accepting_lambdas
    def pretuple_with(self, *others):
        return Pretuple(self, *others)

    @columnize
    @method_accepting_lambdas
    def tuple_with(self, *others):
        return Tuple(self, *others)

    def transform(self, transformator, sqltype=None):
        return Transform(transformator, self, sqltype=sqltype)

    # TODO: upper, lower, largest…

    # MAGIC SHORTHAND
    def __and__(self, other):
        return self.and_(other)

    __rand__ = __and__

    def __or__(self, other):
        return self.or_(other)

    __ror__ = __or__

    def __invert__(self):
        return self.not_()

    def __add__(self, other):
        return self.add(other)

    def __sub__(self, other):
        return self.sub(other)

    def __mul__(self, other):
        return self.mul(other)

    def __truediv__(self, other):
        return self.div(other)

    def __neg__(self):
        return self.neg()

    def __pos__(self):
        return self

    def __mod__(self, mod):
        return self.mod(mod)


class Named(Column):
    """Class for named columns

    Parameters
    ----------
    sqltype: type
        The type of the column.

    name: str
        The name of the column.

    schema_name: str or None
        The name of the schema to which the column belongs.

    relation_name: str or None
        The name of the table to which the column belongs.

    **kwargs: mapping
        Additional keyworded parameters for super initialization.

    Examples
    --------

    A Named column is formatted as its full name if it has a parent:
    >>> from querybuilder.atoms.columns import Named
    >>> str(Named(int, "c", relation_name="T"))
    'T.c'

    Otherwise it is formatted as its name:
    >>> str(Named(str, "c"))
    'c'

    """

    __slots__ = ("name", "schema_name", "relation_name")

    def __init__(
        self,
        sqltype: type,
        name: str,
        relation_name: Optional[str] = None,
        schema_name: Optional[str] = None,
        **kwargs,
    ) -> None:
        assert not schema_name or relation_name
        self.name = name
        self.schema_name = schema_name
        self.relation_name = relation_name
        super().__init__(sqltype=sqltype, **kwargs)

    def set_name(self, name):
        if self.name == name:
            return self
        return self.buildfrom(self, name=name)

    def set_schema_name(self, schema_name):
        if self.schema_name == schema_name:
            return self
        elif schema_name:
            assert self.relation_name
            # TODO: decide what to raise here
        return self.buildfrom(self, schema_name=schema_name)

    def set_relation_name(self, relation_name):
        if self.relation_name == relation_name:
            return self
        # TODO: if set to None, set schema_name to None as well
        #       (done in __setstate__ for now, see TODO there)
        return self.buildfrom(self, relation_name=relation_name)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            super()._get_subtokenize_kwargs(tokenizer),
            name=tokenizer.tokenize_name(
                schema_name=self.schema_name,
                relation_name=self.relation_name,
                name=self.name,
            ),
        )

    def __setstate__(self, state):
        if "relation_name" in state:
            if state["relation_name"] is None:
                state["schema_name"] = None
        elif state.get("schema_name"):
            state["schema_name"] = None
        # TODO: decide whether expected behavior is to raise here, or not
        super().__setstate__(state)


class TableColumn(Named):
    """Class for columns that belong to a table
    and can have column constraints associated with them

    Parameters
    ----------
    sqltype: type
        The type of the column.

    name: str
        The name of the parent of the column.

    schema_name: str or None
        The name of the schema to which the column belongs.

    relation_name: str
        The name of the table to which the column belongs.

    constraints: Iterable[ColumnConstraint]
        The constraints of the column, empty by default.

    **kwargs: mapping
        Additional keyworded parameters for super initialization.

    Examples
    --------
    A TableColumn is formatted as it were a Named column:
    >>> from querybuilder.atoms.columns import TableColumn
    >>> str(TableColumn(int, "c", "T"))
    'T.c'

    Even if it has constraint(s):
    >>> from querybuilder.atoms.constraints import ColumnUnique
    >>> str(TableColumn(str, "c", "T", constraints=(ColumnUnique(), )))
    'T.c'

    """

    __slots__ = ("constraints",)

    def __init__(
        self,
        sqltype: type,
        name: str,
        relation_name: str,
        schema_name: Optional[str] = None,
        constraints: Iterable[qbconstraints.ColumnConstraint] = (),
        # check constraint exposed as a keyworded parameter so that its expression may
        #       involve the TableColumn to build (main use case)
        check: Union[Column, Callable[[Named], Column], None] = None,
        **kwargs,
    ):
        super().__init__(
            sqltype,
            name,
            schema_name=schema_name,
            relation_name=relation_name,
            **kwargs,
        )
        if callable(check):
            constraints = chain(
                constraints,
                (qbconstraints.ColumnCheck(check(Named(self.sqltype, self.name))),),
            )
        elif check:
            constraints = chain(constraints, (qbconstraints.ColumnCheck(check),))
        cons: Iterable[qb.utils.typing.NamedProto]
        cons = cast(Iterable[qb.utils.typing.NamedProto], constraints)
        self.constraints = qbstores.UNamedStore(cons)


class Placeholder(Column):
    """A Placeholder column

    The placeholder will be replaced by an encoded value provided at execution time, in
    a way that is secured against injections.

    Parameters
    ----------
    sqltype: type
        the type of the column.

    key: Optional[str]
        a mandatory key for the column for tracking expected value that
        should feed placeholders.

    kwargs: mapping
        additional keyworded parameters for super initialization.

    Notes
    -----
    In the Value subclass, the key is allowed to be None.

    """

    # TODO: improve doctest

    __slots__ = ("key",)

    def __init__(self, sqltype: type, key: Optional[str] = None, **kwargs):
        self.key = key
        super().__init__(sqltype=sqltype, **kwargs)

    def to_value(self, value, key: Optional[str | _MISSING_TYPE] = MISSING) -> Value:
        if MISSING == key:
            key = self.key
        return super().to_value(value, key=key)

    def to_placeholder(
        self, key: Optional[str | _MISSING_TYPE] = MISSING
    ) -> Placeholder:
        if MISSING == key:
            key = self.key
        return super().to_placeholder(key=key)

    def set_key(self, key: Optional[str]) -> Placeholder:
        return self.buildfrom(self, key=key)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(key=self.key, **super()._get_subtokenize_kwargs(tokenizer))

    def _pretty_subtokenize(
        self, tokenizer: qb.drivers.sql.tokenizer.Tokenizer, prompt: TkTree = ()
    ) -> TkTree:
        value = getattr(self, "value", MISSING)
        return (
            (prompt, self.subtokenize(tokenizer)),
            tokenizer.tokenize_value(value, with_comment_marker=True),
        )


class Constant(Column):
    """Base class for constant columns

    Parameters
    ----------
    constant: any
        the constant value of the column.

    sqltype: Type
        the type of the constant column.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    A Constant is formatted as its value:
    >>> from querybuilder.atoms.columns import Constant
    >>> str(Constant(int, 42))
    '42'
    >>> str(Constant(str, "foo"))
    "'foo'"

    The sqltype of the Constant is taken into account:
    >>> str(Constant(bool, True))
    'True'
    >>> str(Constant(str, True))
    "'True'"
    >>> str(Constant(int, True))
    '1'
    """

    __slots__ = ("constant",)

    def __init__(self, sqltype: type, constant: Any, **kwargs):
        self.constant = constant
        super().__init__(sqltype=sqltype, **kwargs)

    def to_constant(self, new_constant: Any = MISSING, /) -> Constant:
        if new_constant is MISSING:
            return self
        return super().to_constant(new_constant)

    def to_value(
        self,
        value: Any | _MISSING_TYPE = MISSING,
        key: Optional[str | _MISSING_TYPE] = None,
    ) -> Value:
        if MISSING == value:
            value = self.constant
        return super().to_value(value, key=key)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            sqltype=self.sqltype,
            constant=self.constant,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class Value(Placeholder):
    """Value column, namely placeholder column attached with a value

    The value attached to the placeholder will feed the query at execution time.

    Parameters
    ----------
    value: any
        the constant value of the value column

    sqltype: Type
        the type of the constant column.

    key: str or None
        the optional name of the column.  If None passed, then the name will
        be automatically generated in context.

    kwargs: mapping
        additional keyworded parameters for super initialization.

    Examples
    --------

    A value is formatted as a placeholder
    >>> from querybuilder.atoms.columns import Value
    >>> str(Value(int, 42))
    '?'
    >>> str(Value(int, 42, key="k"))
    ':k'
    """

    __slots__ = ("value",)

    def __init__(self, sqltype, value, key=None, **kwargs):
        self.value = value
        super().__init__(sqltype=sqltype, key=key, **kwargs)

    def set_value(self, value):
        return self.buildfrom(self, value=value)

    def __dir__(self):
        dir = list(super().__dir__())
        dir.remove("to_value")
        return dir

    def to_value(
        self,
        value: Any | _MISSING_TYPE = MISSING,
        key: Optional[str | _MISSING_TYPE] = MISSING,
    ) -> Value:
        if MISSING == key:
            if MISSING == value:
                return self
            key = self.key
        elif MISSING == value:
            value = self.value
        return super().to_value(value, key)

    def to_constant(self, constant: Any | _MISSING_TYPE = MISSING, /) -> Constant:
        if MISSING == constant:
            constant = self.value
        return Constant(self.sqltype, constant)


class True_(Constant):
    """A Constant for the True boolean value

    Examples
    --------

    >>> from querybuilder.atoms.columns import True_
    >>> str(True_())
    'True'
    """

    __slots__ = ()

    def __init__(self, **kwargs):
        super().__init__(sqltype=bool, constant=True, **kwargs)

    @columnize
    @method_accepting_lambdas
    def and_(self, first, *others):
        if others:
            return first.and_(*others)
        return first

    @columnize
    @method_accepting_lambdas
    def or_(self, first, *others):
        return self

    def not_(self):
        return False_()


class False_(Constant):
    """A Constant for the True boolean value

    Examples
    --------

    >>> str(False_())
    'False'
    """

    __slots__ = ()

    def __init__(self, **kwargs):
        super().__init__(sqltype=bool, constant=False, **kwargs)

    @columnize
    @method_accepting_lambdas
    def and_(self, first, *others):
        return self

    @columnize
    @method_accepting_lambdas
    def or_(self, first, *others):
        if others:
            return first.or_(*others)
        return first

    def not_(self):
        return True_()


class Null(Constant):
    """A Constant for the NULL value

    Examples
    --------

    >>> from querybuilder.atoms.columns import Null
    >>> str(Null())
    'NULL'
    """

    __slots__ = ()

    def __init__(self, **kwargs):
        super().__init__(sqltype=type(None), constant=None, **kwargs)

    def and_(self, other):
        if other is None or other == self:
            return self
        return other & self

    def or_(self, other):
        if other is None or other == self:
            return self
        return other | self


def One(constant=True):
    """A constructor that returns the 1 integer value
    as a Value or Constant depending on the parameter.

    Parameters
    ----------
    constant: bool
        True if a Constant should be returned,
        False if a Value should be returned

    Examples
    --------
    >>> from querybuilder.atoms.columns import Constant, One
    >>> str(One())
    '1'

    >>> str(One(constant=False))
    '?'
    """
    return Constant(int, 1) if constant else Value(int, 1)


def Zero(constant=True):
    """A constructor that returns the 0 integer value
    as a Value or Constant depending on the parameter.

    Parameters
    ----------
    constant: bool
        True if a Constant should be returned,
        False if a Value should be returned

    Examples
    --------
    >>> from querybuilder.atoms.columns import Constant, Zero
    >>> str(Zero())
    '0'

    >>> str(Zero(constant=False))
    '?'
    """
    return Constant(int, 0) if constant else Value(int, 0)


class Expression(Column):
    """Abstract class for column expressions

    Column expressions are columns that rely on other columns.

    Parameters
    ----------
    columns: iterable of columns
        the columns on which the column to build rely.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.
    """

    __slots__ = ("columns",)
    _scopable = True

    def __init__(self, columns, **kwargs):
        self.columns = columns
        super().__init__(**kwargs)

    def descend(self, accumulator):
        for c in self.columns:
            accumulator = c.accept(accumulator)
        return accumulator

    def _substitute(self, substitutions: Mapping):
        columns = tuple(c.substitute(substitutions) for c in self.columns)
        return self.buildfrom(self, columns=columns)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            columns=tuple(
                col.subtokenize(tokenizer, scoped=True) for col in self.columns
            ),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


# TUPLE
class Pretuple(Expression, Generic[Col]):
    """A tuple of column

    PreTuple of columns are the simplest concrete tuple expressions:
    they are tuples of columns, not surrounded by parentheses.

    Parameters
    ----------
    *columns: columns
        the columns to be gathered in the PreTuple.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    A Pretuple is formatted without parentheses:
    >>> from querybuilder.atoms.columns import Pretuple, Zero, One, Placeholder
    >>> str(Pretuple(Zero(), One(), Placeholder(int)))
    '0, 1, ?'
    >>> str(Pretuple())
    ''
    """

    __slots__ = ()
    _scopable = False

    def __init__(self, *columns: Col, **kwargs):
        sqltype = tuple((c.sqltype for c in columns))
        super().__init__(columns=columns, sqltype=sqltype, **kwargs)

    def coalesce(self, *others, **kwargs):
        return self.columns[0].__class__.coalesce(*self.columns, *others, **kwargs)

    def to_placeholder(self, key: Optional[str | _MISSING_TYPE] = None) -> Placeholder:
        raise NotImplementedError()

    def to_pretuple_of_placeholders(
        self,
        keys: Mapping[int | Column, str] | Iterable[tuple[int | Column, str]] = (),
    ) -> Pretuple[Col]:  # or -> Pretuple[Placeholder]
        keys = dict(keys)
        cols = (
            c.to_placeholder(key=keys.get(i, keys.get(c, MISSING)))
            for i, c in enumerate(self.columns)
        )
        return type(self)(*cols)

    def to_value(self, *values, key: Optional[str | _MISSING_TYPE] = None) -> Value:
        raise NotImplementedError()

    def to_pretuple_of_values(
        self,
        *values,
        keys: Mapping[int | Column, str] | Iterable[tuple[int | Column, str]] = (),
    ) -> Pretuple[Col]:  # or -> Pretuple[Value]
        keys = dict(keys)
        cols = (
            c.to_value(values[i], key=keys.get(i, keys.get(c, MISSING)))
            for i, c in enumerate(self.columns)
        )
        return type(self)(*cols)

    def to_constant(self, *constants) -> Constant:
        raise NotImplementedError()

    def to_pretuple_of_constants(
        self, *constants
    ) -> Pretuple[Col]:  # or -> Pretuple[Constant]
        cols = (c.to_value(constants[i]) for i, c in enumerate(self.columns))
        return type(self)(*cols)

    @columnize
    @method_accepting_lambdas
    def pretuple_with(self, *others):
        # TODO: Why not done as follows in Column class?
        if not others:
            return self

        def f(c):
            return c.columns if isinstance(c, Pretuple) else (c,)

        columns = map(f, (self, *others))
        return Pretuple(*chain.from_iterable(columns))

    @columnize
    @method_accepting_lambdas
    def tuple_with(self, *others):
        return Tuple(*self.pretuple_with(*others).columns)


class Tuple(Pretuple):
    """A tuple of column

    It represents a tuple of columns, surrounded by parentheses.

    Examples
    --------

    >>> from querybuilder.atoms.columns import Tuple, Zero, One, Placeholder
    >>> str(Tuple(Zero(), One(), Placeholder(int)))
    '(0, 1, ?)'
    >>> str(Tuple())
    '()'
    """

    __slots__ = ()
    _scopable = False

    @columnize
    @method_accepting_lambdas
    def pretuple_with(self, *others):
        if not others:
            return Pretuple(*self.columns)
        return super().pretuple_with(*others)

    @columnize
    @method_accepting_lambdas
    def tuple_with(self, *others):
        if not others:
            return self
        return super().tuple_with(*others)


# BOOLEAN TESTS
class Comparison(Expression):
    """Comparison of two columns

    Parameters
    ----------
    operator: str
        the comparing operator to use.  Usually, a key of the _operators class
        attribute.

    first: column
        the first column of the comparison of columns to build.

    second: column
        the second column of the comparison of columns to build.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    >>> from querybuilder.atoms.columns import Comparison, Placeholder, One
    >>> comp = Comparison('!=', Placeholder(int), One())
    >>> str(comp)
    '? != 1'

    It is possible to negate a comparison that uses a standard operator:
    >>> str(comp.not_())
    '? = 1'
    """

    __slots__ = ("operator",)
    _operators = {  # symmetrically maps operations to their negated variant
        "=": "!=",
        "!=": "=",
        ">": "<=",
        "<=": ">",
        "<": ">=",
        ">=": "<",
        "IS": "IS NOT",
        "IS NOT": "IS",
        "LIKE": "NOT LIKE",
        "NOT LIKE": "LIKE",
        "ILIKE": "NOT ILIKE",
        "NOT ILIKE": "ILIKE",
        "IN": "NOT IN",
        "NOT IN": "IN",
    }
    _scopable = False

    @property
    def left_column(self) -> Column:
        return self.columns[0]

    @property
    def right_column(self) -> Column:
        return self.columns[1]

    def __init__(self, operator: str, left: Column, right: Column, **kwargs):
        self.operator = operator
        super().__init__(columns=(left, right), sqltype=bool, **kwargs)

    def not_(self) -> Comparison:
        return self.buildfrom(
            self, columns=self.columns, operator=self._operators[self.operator]
        )

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            operator=self.operator,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class SubcolumnExpression(Expression):
    """An Expression with only one subcolumn"""

    __slots__ = ()

    def __init__(self, column: Column, sqltype: type, **kwargs):
        super().__init__(columns=(column,), sqltype=sqltype, **kwargs)

    @property
    def subcolumn(self):
        return self.columns[0]


class InRelation(SubcolumnExpression):
    """An expression to check whether a column is (not) in a relation

    Parameters
    ----------
    operator: str
        the comparing operator to use.  Usually, a key of the _operators class
        attribute.

    column: Column
        the column that should appear on the left-hand side of this test.

    relation: Relation
        the second column of the comparison of columns to build.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    >>> from querybuilder.atoms.relations import Named as NamedRel
    >>> rel = NamedRel("foo")
    >>> inr = InRelation(One(), rel, not_in=True)
    >>> str(inr)
    '1 NOT IN foo'

    It is possible to negate an InRelation that uses a standard operator:
    >>> str(inr.not_())
    '1 IN foo'
    """

    __slots__ = ("relation", "operator")
    _operators = {  # symmetrically maps operations to their negated variant
        "IN": "NOT IN",
        "NOT IN": "IN",
    }
    _scopable = True

    def __init__(
        self,
        column: Column,
        relation: qbrelations.Relation,
        not_in: bool = False,
        **kwargs,
    ):
        self.relation = relation
        self.operator = "NOT IN" if not_in else "IN"
        super().__init__(column=column, sqltype=bool, **kwargs)

    def not_(self) -> InRelation:
        return self.buildfrom(
            self, columns=self.columns, operator=self._operators[self.operator]
        )

    def _substitute(self, substitutions):
        relation = self.relation.substitute(substitutions)
        self = self.buildfrom(self, relation=relation)
        return super()._substitute(substitutions)

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        return self.relation.accept(accumulator)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            operator=self.operator,
            relation=self.relation.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class Exists(Column):
    """Boolean column that represents existence of at least one tuple in a relation

    Parameters
    ----------
    relation: Relation
        the relation whose existence should be checked.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------
    >>> from querybuilder.atoms.relations import Named as NamedRel
    >>> str(Exists(NamedRel("foo")))
    'EXISTS (foo)'
    """

    __slots__ = ("query",)
    _scopable = True

    def __init__(self, query, **kwargs):
        super().__init__(sqltype=bool, **kwargs)
        self.query = query

    def descend(self, accumulator):
        accumulator = super().descend(accumulator)
        return self.query.accept(accumulator)

    def _substitute(self, substitutions):
        query = self.query.substitute(substitutions)
        return self.buildfrom(self, query=query)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            query=self.query.subtokenize(tokenizer),
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class BooleanCombination(Expression):
    """Boolean combination of several columns

    They are conjunction or disjunction of, at least 2, columns.

    Parameters
    ----------
    combinator: str
        the Boolean combinator to use.  Usually an item of the '_combinators'
        class attribute.

    first: column
        the first column of the Boolean combination of columns to build.

    second: column
        the second column of the Boolean combination of columns to build.

    *others: columns
        some optional additional columns to use in the boolean combination.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    >>> from querybuilder.atoms.columns import BooleanCombination, False_, True_
    >>> str(BooleanCombination("AND", True_(), False_()))
    'True AND False'

    A BooleanCombination can be applied to more than two columns:
    >>> str(BooleanCombination("AND", True_(), False_(), True_()))
    'True AND False AND True'
    """

    __slots__ = ("combinator",)
    _combinators = {"AND": "OR", "OR": "AND"}
    _neutral = {"AND": True_(), "OR": False_()}
    _scopable = True

    def __init__(
        self, combinator: str, first: Column, second: Column, *others, **kwargs
    ):
        self.combinator = combinator
        super().__init__(columns=(first, second, *others), sqltype=bool, **kwargs)

    def not_(self) -> BooleanCombination:
        return type(self)(
            self._combinators[self.combinator], *(c.not_() for c in self.columns)
        )

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            combinator=self.combinator,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class Not(SubcolumnExpression):
    """Boolean negation of column

    A column expression that simply negate a single column.  It could have
    been implemented as a column transformation (see Transform below), but is
    better identified explicitly.

    Parameters
    ----------
    column: column
        the column to negate.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    >>> from querybuilder.atoms.columns import Not, True_
    >>> str(Not(True_()))
    'NOT True'

    Notice that the method not_ of True_ and False_ columns perform a flattened
    negation.  This should be the preferred way of negating a column.
    >>> str(True_().not_())
    'False'
    """

    __slots__ = ()
    _scopable = True
    combinator = "NOT"

    def __init__(self, column: Column, **kwargs):
        super().__init__(column=column, sqltype=bool, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        super_kwargs = super()._get_subtokenize_kwargs(tokenizer)
        column = super_kwargs["columns"][0]
        super_kwargs.pop("columns")
        return dict(combinator=self.combinator, column=column, **super_kwargs)


# ARITHMETIC OPERATIONS
class ArithmeticOperation(Expression):
    """Operation on several columns

    Parameters
    ----------
    operator: str
        the operation operator to use.  Usually, a key of the _operators class
        attribute.

    first: column
        the first column of the operation sequence.

    second: column
        the second column of the operation sequence.

    others: columns
        further columns for the operation sequence.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------
    >>> str(ArithmeticOperation("+", One(), One()))
    '1 + 1'

    An ArithmeticOperation can be applied to more than two elements:
    >>> str(ArithmeticOperation("+", One(), One(), Zero()))
    '1 + 1 + 0'

    Other operators can be used:
    >>> str(ArithmeticOperation("*", One(), One(), Zero()))
    '1 * 1 * 0'
    >>> str(ArithmeticOperation("-", One(), One(), Zero()))
    '1 - 1 - 0'
    >>> Cnst = lambda x: Constant(int, x)
    >>> str(ArithmeticOperation("/", Cnst(24), Cnst(3), Cnst(2)))
    '24 / 3 / 2'

    """

    __slots__ = ("operator",)
    # mapping operator to triples of tuple (t1, t2, t3) of operators indicating, in
    #      order, the operators which have precedence higher than (t1), same as (t2), or
    #      lower than (t3) the keying operator.
    _operators = {
        "+": (("*", "/"), ("+", "-"), ()),
        "-": (("*", "/"), ("+", "-"), ()),
        "*": ((), ("*", "/"), ("+", "-")),
        "/": ((), ("*", "/"), ("+", "-")),
    }

    @property
    def _scopable(self):
        return self.operator not in "*/"

    def __init__(
        self,
        operator: str,
        first: Column,
        second: Column,
        *others: Column,
        sqltype=None,
        **kwargs,
    ):
        sqltype = sqltype or first.sqltype
        self.operator = operator
        super().__init__(columns=(first, second, *others), sqltype=sqltype, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            operator=self.operator,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class Negate(SubcolumnExpression):
    """Arithmetic negation of a single column

    Parameters
    ----------
    column: Column
        the column to negate.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------

    >>> from querybuilder.atoms.columns import Negate, One
    >>> str(Negate(One()))
    '-1'
    """

    __slots__ = ()
    operator = "-"

    def __init__(self, column: Column, **kwargs):
        super().__init__(column=column, sqltype=column.sqltype, **kwargs)

    def neg(self):
        return self.subcolumn  # should we ensure parent=None, name=None??

    def _get_subtokenize_kwargs(self, tokenizer):
        super_kwargs = super()._get_subtokenize_kwargs(tokenizer)
        column = super_kwargs["columns"][0]
        super_kwargs.pop("columns")
        return dict(operator=self.operator, column=column, **super_kwargs)


# FUNCTIONS
class Transform(SubcolumnExpression):
    """Column transformation

    Parameters
    ----------
    transform: str
        the function to apply to column to get the transformed column.

    column: column
        the column to transform.

    sqltype: Type or None, default=None
        the type of the resulting column.  If None, then the type is obtained
        from the type of column and from transfor (c.f., transform method of
        Type).

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    """

    __slots__ = ("transformator",)
    _scopable = False

    def __init__(self, transformator, column: Column, sqltype=None, **kwargs):
        sqltype = sqltype or column.sqltype
        self.transformator = transformator
        super().__init__(columns=column, sqltype=sqltype, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        return dict(
            transformator=self.transformator,
            **super()._get_subtokenize_kwargs(tokenizer),
        )


class Cast(SubcolumnExpression):
    """Represents the casting of a column as a specific type

    Parameters
    ----------
    column: Column
        the column to cast.

    sqltype: type or None
        the type of the resulting column.

    kwargs: mapping
        additional keyworded parameters to be passed to super initialization.

    Examples
    --------


    >>> str(Cast(One(int), str))
    'CAST(1 AS TEXT)'
    """

    __slots__ = ()

    def __init__(self, column, sqltype, **kwargs):
        super().__init__(column=column, sqltype=sqltype, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        super_kwargs = super()._get_subtokenize_kwargs(tokenizer)
        column = super_kwargs["columns"][0]
        super_kwargs.pop("columns")
        return dict(sqltype=self.sqltype, column=column, **super_kwargs)


class Coalesce(Transform):
    __slots__ = ()

    def __init__(self, column, **kwargs):
        kwargs.setdefault("sqltype", column.columns[0].sqltype)
        super().__init__(transformator="coalesce", column=column, **kwargs)


class IfNull(Coalesce):
    __slots__ = ()

    def __init__(self, column, **kwargs):
        super().__init__(transformator="ifnull", column=column, **kwargs)


class Case(Column):
    __slots__ = ("when", "else_")
    _scopable = True

    def __setstate__(self, state):
        else_ = state.get("else_", None)
        if else_:
            state["sqltype"] = else_.sqltype
        else:
            state["sqltype"] = state["when"][-1][1].sqltype
        super().__setstate__(state)

    def __init__(
        self,
        when_first: tuple[Column, Column],
        *when_tail: tuple[Column, Column],
        else_: Optional[Column] = None,
    ):
        self.when = when = (when_first, *when_tail)
        self.else_ = else_
        sqltype = else_.sqltype if else_ else when[-1][1].sqltype
        super().__init__(sqltype=sqltype)

    def add_when(self, condition, value, *, prepend: bool = False) -> Case:
        if prepend:
            return self.__class__((condition, value), *self.when, else_=self.else_)
        return self.__class__(*self.when, (condition, value), else_=self.else_)

    def set_else(self, value: Optional[Column]) -> Case:
        return self.__class__(*self.when, else_=value)

    def _get_subtokenize_kwargs(self, tokenizer):
        kwargs = super()._get_subtokenize_kwargs(tokenizer)
        kwargs["when"] = tuple(
            tuple(c.subtokenize(tokenizer, scoped=True) for c in w) for w in self.when
        )

        if self.else_:
            kwargs["else_"] = self.else_.subtokenize(tokenizer, scoped=True)

        return kwargs

    def _substitute(self, substitutions: Mapping) -> Self:
        when = tuple(tuple(c.substitute(substitutions) for c in w) for w in self.when)
        else_ = self.else_ and self.else_.substitute(substitutions)
        return self.buildfrom(self, when=when, else_=else_)


# AGGREGATION
class Aggregate(SubcolumnExpression):
    __slots__ = ("aggregator", "distinct")
    _aggregators = {"count", "sum", "min", "max", "any", "all", "avg", "string_agg"}
    _scopable = False

    def __init__(self, aggregator, column, sqltype, distinct=False, **kwargs):
        self.aggregator = aggregator
        self.distinct = distinct
        # TODO: define type!
        super().__init__(column=column, sqltype=sqltype, **kwargs)

    def _get_subtokenize_kwargs(self, tokenizer):
        super_kwargs = super()._get_subtokenize_kwargs(tokenizer)
        column = super_kwargs["columns"][0]
        super_kwargs.pop("columns")
        return dict(
            distinct=self.distinct,
            aggregator=self.aggregator,
            column=column,
            **super_kwargs,
        )
