"""Exceptions for the querybuilder"""


class QueryBuilderError(Exception):
    pass


class SchemaError(QueryBuilderError):
    """Invalid schema construction

    This includes name conflicts in schema.
    """


class TypeError(QueryBuilderError):
    """Invalid type or typed operation"""


class ScopeError(QueryBuilderError):
    pass


class ScopeAmbiguityError(ScopeError):
    pass


class TransactionError(QueryBuilderError):
    pass


class SessionError(QueryBuilderError):
    pass


class APIError(QueryBuilderError):
    pass
