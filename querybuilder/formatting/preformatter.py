from __future__ import annotations
from typing import Callable, cast
from querybuilder.formatting.token import Punctuation, Whitespace
from querybuilder.formatting.tokentree import TkStr, TkSeq, TkInd, TkTree


# Tree manipulation functions
def transform(func: Callable[[TkTree], TkTree], tree: TkTree) -> TkTree:
    if isinstance(tree, TkSeq):
        return func(tree)

    newtree: list[TkTree] = []
    for subtree in tree:
        subtree = transform(func, subtree)
        newtree.append(subtree)

    return func(cast(TkTree, tuple(newtree)))


def append_to_tree(tree: TkTree, seq: TkSeq, to_left: bool = False) -> TkTree:
    """Appends seq to the right (or left) of the right-most (or left-most) leaf of tree

    Parameters
    ----------
    tree: TkTree
        the TkTree to which the TkSeq seq should be appended or prepended.

    seq: TkSeq
        the TkSeq to prepend or append to the TkTree tree.

    to_left: bool, default=False
        whether to append or prepend the TkSeq seq to the TkTree tree.  Default is to
        append, that is, seq is appended to the right-most TkSeq leaf of tree.
    """
    if isinstance(tree, TkInd):
        raise ValueError("Can't append on TkInd")
    if not tree:
        return seq
    elif to_left:
        if isinstance(tree, TkSeq):
            return TkSeq(seq + tree)
        tree = cast(
            tuple[TkTree], tree
        )  # TODO: why does mypy fail to infer type without this cast?
        return (append_to_tree(tree[0], seq, to_left=to_left), *tree[1:])
    else:
        if isinstance(tree, TkSeq):
            return TkSeq(tree + seq)
        tree = cast(
            tuple[TkTree], tree
        )  # TODO: why does mypy fail to infer type without this cast?
        return (*tree[:-1], append_to_tree(tree[-1], seq, to_left=to_left))


def flatten(tree: TkTree) -> TkSeq:
    """Non-recursive version of flatten

    Parameters
    ----------
    sequences: Union[TkSeq, tuple[TkSeq]]
        either a tree leaf (TkSeq), or a tuple of already-flattened subtrees
        (tuple[TkSeq]).

    """
    if isinstance(tree, TkSeq):
        return tree

    sep = TkStr(Whitespace, " ").to_seq()

    seq: TkSeq = TkSeq()
    for subseq in filter(
        bool, map(flatten, tree)
    ):  # makes _flatten1 be recursive, and equivalent to flatten
        if not seq:
            seq = subseq
        elif seq[-1].type in Punctuation and seq[-1].value in set("(:.\n"):
            seq += subseq
        elif subseq[0].type in Punctuation and subseq[0].value in set(",;.:!)\n"):
            seq += subseq
        elif seq[-1].type in Whitespace:
            seq += subseq
        elif subseq[0].type in Whitespace:
            seq += subseq
        else:
            seq += sep + subseq

    return seq


def prune_empty(tree: TkTree) -> TkTree:
    """Unused function for cleaning a tree

    The function outputs an equivalent tree in which every empty leaves (including TkInd
    instances) have been removed, as well as empty inner nodes (namely, inner nodes
    without children).  The empty tree (empty tuple) is returned if and only if the
    input tree has no non-empty leaves.

    Parameters
    ----------
    tree: TkTree
        the tree to clean.

    """

    def f(t: TkTree) -> TkTree:
        return t or cast(TkTree, ())

    return transform(f, tree)


def break_lines(
    tree: TkTree,
    maxlen: int = 88,
    indent: int = 0,
    flattener: Callable[[TkTree], TkSeq] = flatten,
) -> TkTree:
    """Pretty multiline PreFormatter

    A PreFormatter which, besides flattening while inserting relevant spaces, breaks and
    indent lines.

    Parameters
    ----------
    tree: TkTree
        the token tree to flatten

    maxlen: int, default=88
        the desired maximal length of an output line.  This length does not take
        indentation into account.  Unless when the associated string of some leaf of the
        given tree already exceed maxlen, the preformatter ensures that no line exceed
        it.

    indent: int, default=0
        the initial indentation level (mostly intended for internal use, on recursive
        calls).

    flattener: Callable[[TkTree], TkSeq], default=flatten
        the flattener to use, which defined how spaces should be inserted.
    """
    flattree: TkSeq = flattener(tree).indent(indent)

    if isinstance(tree, TkSeq):
        return flattree
    elif len(str(flattree)) <= maxlen:
        return flattree

    sep = TkStr(Punctuation.Linebreak, "\n").to_seq()

    flattree = TkSeq()
    for subtree in tree:
        flat_subtree = break_lines(
            subtree, maxlen=maxlen, indent=indent, flattener=flattener
        )
        if flat_subtree == TkSeq(()):
            continue
        if isinstance(subtree, TkInd):
            indent += 1
            continue
        if flattree:
            flattree += sep
        flattree += flat_subtree
    return flattree
