from typing import Generic, TypeVar
from abc import ABC, abstractmethod

K = TypeVar("K")


class KeyedParamStyle(ABC, Generic[K]):
    @abstractmethod
    def __call__(self, key: K) -> str: ...


class NamedParamStyle(KeyedParamStyle[str]):
    def __call__(self, key: str) -> str:
        return f":{key}"


class PyformatParamStyle(KeyedParamStyle[str]):
    def __call__(self, key: str) -> str:
        return f"%({key})s"


class NumericParamStyle(KeyedParamStyle[int]):
    def __call__(self, key: int) -> str:
        return f":{key}"


class UnkeyedParamStyle(ABC):
    @abstractmethod
    def __call__(self) -> str: ...


class FormatParamStyle(UnkeyedParamStyle):
    def __call__(self) -> str:
        return "%s"


class QmarkParamStyle(UnkeyedParamStyle):
    def __call__(self) -> str:
        return "?"
