from __future__ import annotations
from typing import (
    Any,
    Callable,
    IO,
    Iterable,
    MutableMapping,
    Mapping,
    Union,
    cast,
)
from io import StringIO
import pygments.formatters as pygments_formatters
from querybuilder.utils.stores import Frozenmap
from querybuilder.formatting.tokentree import TkStr, TkSeq, TkTree
import querybuilder.formatting.preformatter as qbpreformatter
import querybuilder.formatting.style as qbstyle
import querybuilder as qb


class Formatter:
    __slots__ = ("_preformatter",)

    def __init__(
        self,
        preformatter: qb.utils.typing.PreFormatter = cast(
            qb.utils.typing.PreFormatter, qbpreformatter.flatten
        ),
    ) -> None:
        self._preformatter: qb.utils.typing.PreFormatter = preformatter

    def preformat(self, tree: TkTree) -> TkSeq:
        return self._preformatter(tree)

    def format(self, tree: TkTree, file: IO) -> None:
        file.write(self.get_formatted(tree))

    def get_formatted(self, tree: TkTree) -> str:
        tokens: TkSeq = self.preformat(tree)
        return str(tokens)


class StandardFormatter(Formatter):
    """A pretty formatter not relying on pygment's formatters

    Parameters
    ----------
    styles: Any, default=()
        any positional parameter accepted by the style_factory.  With
        the default factory `Style`, it can be a Mapping whose values
        are string transformers (e.g., str.upper) and whose keys are
        strings, _TokenKind, or _TokenType, or it can be an Iterable
        of such key/value pairs.

    **kwargs:
        keyworded parameters that populate either the breakinglines
        default parameter mapping if the key correspond to a key
        expected by the `breakinglines` method, or the local `styles`
        attribute thus completing the `styles` parameter, otherwise.

    Examples
    --------
    Let us consider this simple token sequence:
    >>> from querybuilder.formatting.token import Token
    >>> tokens = TkSeq((
    ...     TkStr(Token.Keyword, "let"),
    ...     TkStr(Token.Text.Whitespace, " "),
    ...     TkStr(Token.Name, "i"),
    ...     TkStr(Token.Operator, "="),
    ...     TkStr(Token.Literal, "3"),
    ...     TkStr(Token.Text.Whitespace, " "),
    ...     TkStr(Token.Keyword, "in"),
    ...     TkStr(Token.Text.Whitespace, " "),
    ...     TkStr(Token.Name, "i"),
    ...     TkStr(Token.Operator, "*"),
    ...     TkStr(Token.Literal, "3"),
    ...     TkStr(Token.Punctuation, ";"),
    ...     TkStr(Token.Punctuation.Linebreak, "\\n")
    ... ))

    We can get a default basic StandardFormatter like this:
    >>> frmt1 = StandardFormatter()

    This formatter can be used to format our token stream.
    >>> frmt1.get_formatted(tokens)
    'LET i=3 IN i*3;\\n'

    Notice that the keywords 'LET' and 'IN' have been uppercased.  This is because the
    `default_styles` attribute of the standard formatter class defines the style for
    Keyword tokens to be uppercased strings:
    >>> frmt1.default_styles['Keyword']
    <method 'upper' of 'str' objects>

    It is possible to locally change the style as follows:
    >>> frmt1.styles['Keyword'] = str.lower
    >>> frmt1.get_formatted(tokens)
    'let i=3 in i*3;\\n'
    >>> frmt1.styles['Keyword'] = str.upper
    >>> frmt1.get_formatted(tokens)
    'LET i=3 IN i*3;\\n'

    We could also have specified the wanted style at instanciation time:
    >>> frmt2 = StandardFormatter(dict(Keyword=str.lower))
    >>> frmt2.get_formatted(tokens)
    'let i=3 in i*3;\\n'

    This local changes of styles do not affect the class default styles:
    >>> frmt3 = StandardFormatter()
    >>> frmt3.get_formatted(tokens)
    'LET i=3 IN i*3;\\n'

    A style can be any string transformer, namely a callable which takes a
    string as unique argument and returns a string.
    >>> frmt1.styles['Operator'] = " {} ".format
    >>> frmt1.get_formatted(tokens)
    'LET i = 3 IN i * 3;\\n'

    If a style is not found, style for parent is looked for:
    >>> Token.Text.Whitespace
    Token.Text.Whitespace
    >>> frmt1.styles['Token.Text'] = " ".__add__
    >>> frmt1.get_formatted(tokens)
    'LET  i = 3  IN  i * 3;\\n'

    """

    __slots__ = ("styles",)
    style_factory = qbstyle.StandardStyle
    default_styles: MutableMapping[str, Callable[[str], str]] = dict(
        Keyword=str.upper, Name=str.lower
    )

    @staticmethod
    def default_styling(s: str) -> str:
        return s

    def __init__(
        self,
        styles: Mapping[str, Callable[[str], str]] = Frozenmap(),
        preformatter: qb.utils.typing.PreFormatter = cast(
            qb.utils.typing.PreFormatter, qbpreformatter.break_lines
        ),
        /,
        **kwargs,
    ) -> None:
        self.styles = self.style_factory(dict(styles), self.default_styles)
        super().__init__(preformatter=preformatter)

    def get_formatted(self, tree: TkTree) -> str:
        preformatted = self.preformat(tree)
        strings = []
        for tk, s in preformatted:
            styling = self.styles.get(tk, self.default_styling)
            strings.append(styling(s))
        return "".join(strings)


class WrapStreamFormatter(Formatter):
    __slots__ = ("_streamformatter",)

    def __init__(
        self,
        streamformatter: qb.utils.typing.StreamFormatter = cast(
            qb.utils.typing.StreamFormatter, Formatter()
        ),
        preformatter: qb.utils.typing.PreFormatter = cast(
            qb.utils.typing.PreFormatter, qbpreformatter.flatten
        ),
        **kwargs,
    ) -> None:
        self._streamformatter: qb.utils.typing.StreamFormatter = streamformatter
        super().__init__(preformatter=preformatter, **kwargs)

    def streamformat(self, tokens: Iterable[TkStr], file: IO) -> None:
        self._streamformatter.format(tokens, file)

    def format(self, tree: TkTree, file: IO) -> None:
        tokens: Iterable[TkStr] = self.preformat(tree)
        self.streamformat(tokens, file)

    def get_formatted(self, tree: TkTree) -> str:
        with StringIO() as file:
            self.format(tree, file)
            return file.getvalue()


class PygmentsFormatter(WrapStreamFormatter):
    """

    Examples
    --------
    >>> from querybuilder.formatting.token import Token
    >>> tokens = TkSeq((
    ...     TkStr(Token.Keyword, "let"),
    ...     TkStr(Token.Text.Whitespace, " "),
    ...     TkStr(Token.Name, "i"),
    ...     TkStr(Token.Operator, "="),
    ...     TkStr(Token.Literal, "3"),
    ...     TkStr(Token.Text.Whitespace, " "),
    ...     TkStr(Token.Keyword, "in"),
    ...     TkStr(Token.Text.Whitespace, " "),
    ...     TkStr(Token.Name, "i"),
    ...     TkStr(Token.Operator, "*"),
    ...     TkStr(Token.Literal, "3"),
    ...     TkStr(Token.Punctuation, ";"),
    ...     TkStr(Token.Punctuation.Linebreak, "\\n")
    ... ))
    >>> frmt = PygmentsFormatter()
    >>> formatted = frmt.get_formatted(tokens)
    >>> type(formatted)
    <class 'str'>
    >>> for infix in (ts[1] for ts in tokens):
    ...     assert infix in formatted, (infix, formatted)

    """

    __slots__ = ()
    streamformatter_factory = pygments_formatters.Terminal256Formatter
    default_streamformatter_kwargs: dict[str, Any] = dict(
        style=qbstyle.PygmentsDefaultStyle
    )

    @classmethod
    def __issubclass_hook__(cls, other):
        if issubclass(cls.streamformatter_factory, other):
            return True
        return NotImplemented

    def __init__(
        self,
        streamformatter_kwargs: Union[
            Iterable[tuple[str, Any]], Mapping[str, Any]
        ] = (),
        preformatter: qb.utils.typing.PreFormatter = cast(
            qb.utils.typing.PreFormatter, qbpreformatter.break_lines
        ),
        **kwargs,
    ) -> None:
        streamformatter = self.streamformatter_factory(
            **(self.default_streamformatter_kwargs | dict(streamformatter_kwargs))
        )
        super().__init__(streamformatter, preformatter=preformatter, **kwargs)
