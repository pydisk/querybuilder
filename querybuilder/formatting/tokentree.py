from __future__ import annotations
from typing import NamedTuple, SupportsIndex, TypeAlias, Union, overload
import querybuilder.formatting as qbformatting


class TkStr(NamedTuple):
    type: qbformatting.token._TokenType
    value: str

    def __str__(self) -> str:
        return self.value

    def to_seq(self) -> TkSeq:
        return TkSeq((self,))


class TkSeq(tuple[TkStr, ...]):
    __slots__ = ()

    def __str__(self) -> str:
        return "".join(map(str, self))

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}({super().__repr__()})"

    def __add__(self, other):
        if not isinstance(other, TkSeq):
            return NotImplemented
        return self.__class__(super().__add__(other))

    def __eq__(self, other) -> bool:
        return self.__class__ == other.__class__ and super().__eq__(other)

    def __hash__(self):
        return super().__hash__()

    def indent(self, indent: int = 1) -> TkSeq:
        if not indent or not self:
            return self
        indentation = TkStr(
            qbformatting.token._TokenType(
                (*qbformatting.token.Whitespace, "Indentation")
            ),
            "  " * indent,
        ).to_seq()
        return indentation + self

    @overload
    def __getitem__(self, index: SupportsIndex) -> TkStr: ...

    @overload
    def __getitem__(self, index: slice) -> TkSeq: ...

    def __getitem__(self, index: SupportsIndex | slice) -> TkStr | TkSeq:
        if isinstance(index, slice):
            tuple_of_tkstr = super().__getitem__(index)
            return self.__class__(tuple_of_tkstr)
        return super().__getitem__(index)


class TkInd(TkSeq):
    """Special marker in TkTree's that indicate indentations

    TkInd subclasses TkSeq whence it subclasses tuple[TkStr].  However, TkInd instances
    always have empty underlying tuple (and there are thus all equal).

    Examples
    --------
    >>> m = TkInd()
    >>> m
    TkInd()
    >>> str(m)
    ''
    >>> bool(m)
    False
    >>> isinstance(m, TkSeq)
    True
    >>> isinstance(m, TkInd)
    True
    >>> m == TkSeq(())
    False
    >>> m == TkInd()
    True
    >>> TkInd(3)
    Traceback (most recent call last):
        ...
    TypeError: ...__new__() takes 1 positional argument but 2 were given
    >>> TkInd(())
    Traceback (most recent call last):
        ...
    TypeError: ...__new__() takes 1 positional argument but 2 were given

    """

    __slots__ = ()

    @staticmethod
    def __new__(cls):
        return TkSeq.__new__(cls)

    def __init__(self):
        pass

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}()"


TkTree: TypeAlias = Union[TkSeq, tuple["TkTree", ...]]
