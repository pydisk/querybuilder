from pygments.token import (
    _TokenType,
    Token,
    Comment,
    Error,
    Keyword,
    Literal,
    Name,
    Operator,
    Punctuation,
    Text,
    Whitespace,
)
