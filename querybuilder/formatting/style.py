from __future__ import annotations
from typing import Callable, ChainMap, Optional, TypeVar, overload
import pygments.styles as pygments_styles
import pygments.styles.default
import querybuilder.formatting as qbformatting


T = TypeVar("T")


# STYLES
class StandardStyle(ChainMap[str, Callable[[str], str]]):
    """Mutable mapping handling storing styling functions

    A styling function is a function that maps a TkStr str value to a str.  The present
    mutable mapping stores such functions.  The keys are stringified TkStr type.
    """

    __slots__ = ()

    @staticmethod
    def _rekey(key: qbformatting.token._TokenType | str, /) -> str:
        if not isinstance(key, str):
            return repr(key)
        return key

    def _get_longest_key_infix(
        self, key: str, /, prefix: bool = False
    ) -> Optional[str]:
        if super().__contains__(key):
            return key
        elif "." not in key:
            return None
        elif prefix:
            return self._get_longest_key_infix(
                key.rsplit(".", maxsplit=1)[0], prefix=prefix
            )
        else:
            return self._get_longest_key_infix(
                key.split(".", maxsplit=1)[1], prefix=prefix
            )

    def __getitem__(
        self, key: qbformatting.token._TokenType | str, /
    ) -> Callable[[str], str]:
        r = self.get(key)
        if r is None:
            raise KeyError(key)
        return r

    def __missing__(self, key):
        for i in range(1, len(self.maps)):
            tailmap = self.maps[i]
            try:
                return tailmap[key]
            except KeyError:
                pass
        raise KeyError(key)

    def __setitem__(
        self,
        key: qbformatting.token._TokenType | str,
        val: Callable[[str], str],
        /,
    ) -> None:
        super().__setitem__(self._rekey(key), val)

    def __delitem__(self, key: qbformatting.token._TokenType | str, /) -> None:
        super().__delitem__(self._rekey(key))

    @overload
    def get(
        self, key: qbformatting.token._TokenType | str, /, default: None = None
    ) -> Callable[[str], str] | None: ...

    @overload
    def get(
        self,
        key: qbformatting.token._TokenType | str,
        /,
        default: T,
    ) -> Callable[[str], str] | T: ...

    def get(
        self, key: qbformatting.token._TokenType | str, /, default: Optional[T] = None
    ) -> Callable[[str], str] | T | None:
        if not isinstance(key, str):
            return self.get(self._rekey(key), default=default)
        lkinf = self._get_longest_key_infix(key, prefix=False)
        if lkinf is not None:
            try:
                return super().__getitem__(lkinf)
            except KeyError:
                pass
        lkinf = self._get_longest_key_infix(key, prefix=True)
        if lkinf is not None:
            try:
                return super().__getitem__(lkinf)
            except KeyError:
                pass
        return default


class PygmentsDefaultStyle(pygments_styles.default.DefaultStyle):
    default_style = pygments.styles.default.DefaultStyle.styles
    styles = dict(pygments.styles.default.DefaultStyle.styles)
    styles.update(
        {
            qbformatting.token.Keyword: "bold #3344cc",
            qbformatting.token.Comment: "italic #999999",
            qbformatting.token.Name: "#11bb00",
            qbformatting.token.Name.Function: "#00ff00",
            qbformatting.token.Punctuation: "#7777cc",
            qbformatting.token.Operator: "#999999",
            qbformatting.token.Token.OutPrompt: "#999999",
        }
    )
