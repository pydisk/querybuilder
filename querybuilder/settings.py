from collections import ChainMap
from querybuilder.formatting.preformatter import flatten
from querybuilder.formatting.formatter import Formatter, PygmentsFormatter
from querybuilder.drivers.sql.tokenizer import Tokenizer
from querybuilder.utils.typing import PreFormatter
from typing import Any, cast

settings: ChainMap[str, Any] = ChainMap(
    {},
    dict(
        raw_formatter=Formatter(preformatter=cast(PreFormatter, flatten)),
        pretty_formatter=PygmentsFormatter(),
        log_formatter=PygmentsFormatter(),
        tokenizer=Tokenizer(),
        pretty_styles=PygmentsFormatter.default_streamformatter_kwargs["style"].styles,
    ),
)
