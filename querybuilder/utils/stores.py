from __future__ import annotations
from functools import wraps
from itertools import chain
from typing import (
    Callable,
    ClassVar,
    Generic,
    Iterable,
    Iterator,
    Mapping,
    Optional,
    Sequence,
    Self,
    SupportsIndex,
    TypeVar,
    cast,
    overload,
)
from querybuilder.utils.typing import NamedProto, PrettyPrinter, E, K, V

T = TypeVar("T")


# Abstracts
class KeyedTuple(tuple[E, ...], Generic[E]):
    """Tuple whose elements are accessible by attribute lookup when unambiguously keyed

    The class method `_get_key` is expected to return a hashable key for each element
    of the tuple.  Whenever the returned key is a string `s` which uniquely identify the
    element, this element can be accessed as attribute `s` of the KeyedTuple.

    Parameters
    ----------
    iterable: Iterable[E], default=()
        the elements to gather within the KeyedTuple.

    Class attributes
    ----------------
    _get_key: Callable[[E], str]
        a classmethod defining how a key is returned from an element.

    _to_key: Callable[[str], str]
        a classmethod allowing to reshape a key (e.g., str.lower, str.strip).

    _key_str: str, default='key'
        the meta-name of the key, to be used in exception messages (e.g., for speaking
        of "ambiguous key").

    _prepare: Callable[Iterable[E], Iterable[E]]
        a hook that applies on the unique parameter passed to the class constructor, in
        order to filter accepted values.  A typical use is to define it in such a way
        that ambiguity is forbidden.  See concretes implementations below for examples.

    Examples
    --------
    >>> class AlphaT(KeyedTuple[list[str]]):
    ...     @classmethod
    ...     def _get_key(cls, elt: list[str], /) -> Optional[str]:
    ...         return ''.join(elt)
    >>> t = AlphaT((['a', 'bc'], ['a'], ['ab', 'cd'], ['a']))

    Now, `t` is a tuple…
    >>> len(t)
    4
    >>> ['a', 'bc'] in t
    True
    >>> ['abc'] in t
    False
    >>> tuple(t)
    (['a', 'bc'], ['a'], ['ab', 'cd'], ['a'])
    >>> t[2]
    ['ab', 'cd']
    >>> t[-1]
    ['a']
    >>> t[2] is t[-1]
    False

    … but it is an AlphaT…
    >>> type(t).__qualname__
    'AlphaT'
    >>> repr(t)
    "AlphaT((['a', 'bc'], ['a'], ['ab', 'cd'], ['a']))"

    … this allows us to access its (list-of-strings) elements by the corresponding key
    (which is the concatenation of its elements in this example)…
    >>> t.abc
    ['a', 'bc']
    >>> t.abcd
    ['ab', 'cd']
    >>> t.b # no element is keyed by 'b'
    Traceback (most recent call last):
        ...
    AttributeError: b
    >>> l = t.abc
    >>> l.append('d')
    >>> l
    ['a', 'bc', 'd']
    >>> t.abc
    Traceback (most recent call last):
        ...
    AttributeError: abc
    >>> t.abcd # several distinct elements are keyed by 'abcd'
    Traceback (most recent call last):
        ...
    AttributeError: abcd
    >>> t.a # several elements are keyed by 'a', and they are all equal
    Traceback (most recent call last):
        ...
    AttributeError: a

    KeyedTuple can be concatenated, by using the bit-or operator (`|`).
    >>> s0 = AlphaT((['a'], ['a', 'bc']))
    >>> s1 = AlphaT((['a'], ['d', 'e']))
    >>> s2 = s0 | s1
    >>> type(s2).__qualname__
    'AlphaT'
    >>> repr(s2)
    "AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))"
    >>> s2.abc
    ['a', 'bc']
    >>> s2.de
    ['d', 'e']

    Actually the right tuple does not need to be an instance of AlphaT.
    >>> s3 = s0 | (['a'], ['d', 'e'])
    >>> repr(s3)
    "AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))"

    It may even be an arbitrary iterable.
    >>> s0 | (e for e in s1)
    AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))

    If the left side object is an iterable that does not defined how to be or-ed with
    AlphaT instances, it is still possible to concatenate it with such an instance.
    >>> tuple(s0) | s1
    AlphaT((['a'], ['a', 'bc'], ['a'], ['d', 'e']))

    Finally, you can get the set of elements keys through the `_keys` property.
    >>> type(s2._keys).__qualname__
    'Frozenmap'
    >>> sorted(s2._keys)
    ['a', 'abc', 'de']

    The unambiguous ones populate the dir list.
    >>> hasattr(s2, 'a')
    False
    >>> sorted(x for x in dir(s2) if not x in dir(type(s2)))
    ['abc', 'de']

    """

    __slots__ = ()
    _key_attr: ClassVar[str] = "key"

    @classmethod
    def _get_key(cls, element, /) -> Optional[str]:
        return None

    @classmethod
    def _to_key(cls, key: str) -> str:
        return key

    @classmethod
    def _prepare(cls, iterable: Iterable[E]) -> Iterable[E]:
        return iterable

    @property
    def _(self) -> UKeyedMappingView[E]:
        return UKeyedMappingView(self)

    @staticmethod
    def __new__(
        cls: type[KeyedTuple[E]], iterable: Iterable[E] = (), /
    ) -> KeyedTuple[E]:
        itr: Iterable[E] = cls._prepare(iterable)
        return tuple.__new__(cls, itr)  # type: ignore

    def __getattr__(self, attr: str) -> E:
        attr = self._to_key(attr)
        found = self._keys.get(attr, ())
        if not len(found):
            return self._missing_(attr)
        elif len(found) > 1:
            return self._ambiguous_(attr)
        return found[0]

    def _missing_(self, attr) -> E:
        raise AttributeError(attr)

    def _ambiguous_(self, attr) -> E:
        raise AttributeError(attr)

    def __or__(self, other: Iterable[E]) -> Self:
        if not isinstance(other, Iterable):
            return NotImplemented
        elif not other:
            return self
        return self.__class__(chain(self, other))

    def __ror__(self, other: Iterable[E]) -> Self:
        # TODO: remove this method (useless?)
        if not isinstance(other, Iterable):
            return NotImplemented
        return self.__class__(chain(other, self))

    def __sub__(self, other: Iterable[E]) -> KeyedTuple[E]:
        if not isinstance(other, Iterable):
            return NotImplemented

        return self.__class__((e for e in self if e not in other))

    def __dir__(self) -> list[str]:
        d = list(super().__dir__())
        d.extend(n for n, v in self._keys.items() if isinstance(n, str) and len(v) == 1)
        return d

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}({super().__repr__()})"

    @property
    def _keys(self) -> Frozenmap[Optional[str], tuple[E, ...]]:
        d: dict[Optional[str], tuple[E, ...]] = {}
        for e in self:
            k = self._get_key(e)
            if k:
                d[k] = (*d.get(k, ()), e)
        return Frozenmap(d)

    def _repr_pretty_(self, printer: PrettyPrinter, cycle: bool = False):
        opening = f"{self.__class__.__qualname__}("
        with printer.group(len(opening), opening, ")"):
            printer.type_pprinters[tuple](self, printer, cycle)

    @overload
    def __getitem__(self, i: SupportsIndex, /) -> E: ...

    @overload
    def __getitem__(self, i: slice, /) -> KeyedTuple[E]: ...

    @overload
    def __getitem__(self, i: list[int | str], /) -> list[E]: ...

    def __getitem__(
        self, i: SupportsIndex | slice | list[int | str], /
    ) -> E | KeyedTuple[E] | list[E]:
        if isinstance(i, list):
            # return list rather than self._store.__class__ to allow duplicates
            return [self.resolve(j) for j in i]
        elif isinstance(i, slice):
            res = super().__getitem__(i)
            return self.__class__(res)
        else:
            return super().__getitem__(i)

    def resolve(self, spec: int | str) -> E:
        if isinstance(spec, int):
            return self[spec]
        return self._[spec]


class UKeyedTuple(KeyedTuple[E]):
    """KeyedTuple, where non-None keys should unambiguously identify the elements"""

    __slots__ = ()

    @classmethod
    def _prepare(cls, iterable: Iterable[E] = (), /) -> Iterable[E]:
        seen: dict[str, E] = {}

        def fltr(e: E) -> bool:
            k = cls._get_key(e)
            if k is None:
                return True
            prev = seen.get(k)
            if prev is None:
                seen[k] = e
                return True
            raise ValueError(f"Ambiguous {cls._key_attr} {k!r}, got {prev!r} and {e!r}")

        return filter(fltr, iterable)


# Concretes
class NamedStore(KeyedTuple[NamedProto]):
    """KeyedTuple that uses the case-insensitive 'name' attribute as key"""

    __slots__ = ()
    _key_attr = "name"

    @classmethod
    def _to_key(cls, key: str) -> str:
        return key.lower()

    @classmethod
    def _get_key(cls, e: NamedProto, /) -> Optional[str]:
        name = getattr(e, "name", None)
        if name:
            return cls._to_key(name)
        return name


class UNamedStore(NamedStore, UKeyedTuple[NamedProto]):
    """NamedStore that does not allow two named elements to share the same name

    Such stores are mostly used in schema objects such as Schema, Table or TableColumn.
    """

    __slots__ = ()


class StoreFilter(Sequence[E]):
    """View on a KeyedTuple filtered by a function

    Parameters
    ----------
    func: Callable[[E], bool]
        the function used for filtering the elements of the underlying store

    store: KeyedTuple[E]
        the source tuple storing the elements

    """

    __slots__ = ("_filter", "_store")

    def __init__(self, func: Callable[[E], bool], store: KeyedTuple[E]):
        self._filter = func
        self._store = store

    @property
    def _get_key(self) -> Callable[[E], Optional[str]]:
        return self._store._get_key

    @property
    def _key_attr(self) -> str:
        return self._store._key_attr

    @property
    def _keys(self) -> Frozenmap[Optional[str], tuple[E, ...]]:
        d = {}
        for k, v in self._store._keys.items():
            v = tuple(filter(self._filter, v))
            if v:
                d[k] = v
        return Frozenmap(d)

    def __getattr__(self, attr: str):
        storeattr = getattr(self._store.__class__, attr, None)
        __get__ = getattr(storeattr, "__get__", None)
        if __get__:
            return __get__(self, self.__class__)
        elif storeattr:
            return storeattr
        attr = self._to_key(attr)
        found = self._keys.get(attr, ())
        if len(found) != 1:
            raise AttributeError(attr)
        return found[0]

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}({self._filter!r}, {self._store!r})"

    def _repr_pretty_(self, printer: PrettyPrinter, cycle: bool = False):
        return KeyedTuple._repr_pretty_(cast(KeyedTuple[E], self), printer, cycle)

    def __dir__(self) -> list[str]:
        d = set(super().__dir__())
        d.update(e for e, v in self._keys.items() if isinstance(e, str) and len(v) == 1)
        return list(d)

    def __iter__(self) -> Iterator[E]:
        return filter(self._filter, self._store)

    def __len__(self) -> int:
        return sum(1 for _ in self)

    @overload
    def __getitem__(self, i: int, /) -> E: ...

    @overload
    def __getitem__(self, i: slice, /) -> KeyedTuple[E]: ...

    @overload
    def __getitem__(self, i: list[int], /) -> list[E]: ...

    def __getitem__(self, i) -> E | KeyedTuple[E] | list[E]:
        if isinstance(i, list):
            elts = list(self)
            # return list rather than self._store.__class__ to allow duplicates
            return [elts[j] for j in i]
        elif isinstance(i, slice):
            elts = list(self)
            return self._store.__class__(elts[i])
        elif not isinstance(i, int):
            raise TypeError(type(i))
        else:
            j = (len(self) + i) if i < 0 else i
            for e in self:
                if not j:
                    return e
                j -= 1
            raise IndexError(i)


# MappingView on unambiguously keyed elements of KeyedTuple
class UKeyedMappingView(Mapping[str, E]):
    __slots__ = ("_store",)

    def __init__(self, store: KeyedTuple):
        self._store = store

    @overload
    def __getitem__(self, key: str, /) -> E: ...

    @overload
    def __getitem__(self, keys: list[str], /) -> list[E]: ...

    def __getitem__(self, key: str | list[str]) -> E | list[E]:
        if isinstance(key, list):
            # return list rather than self._store.__class__ to allow duplicates
            return [self[k] for k in key]
        res = self.get(key)
        if res is None:
            raise KeyError(key)
        return res

    @overload
    def get(self, key: str, /) -> Optional[E]: ...

    @overload
    def get(self, key: str, /, default: E | T) -> E | T: ...

    def get(self, key: str, /, default: Optional[T] = None) -> E | Optional[T]:
        return getattr(self._store, key, default)

    def __len__(self) -> int:
        return sum((1 for s in self._store._keys.values() if len(s) == 1), start=0)

    def __iter__(self) -> Iterator[str]:
        for k, v in self._store._keys.items():
            if k is not None and len(v) == 1:
                yield k


# Frozenmap
class Frozenmap(Mapping[K, V]):
    __slots__ = ("_keys", "_values")
    _keys: tuple[K]
    _values: tuple[V]

    @wraps(dict[K, V].__init__)  # type: ignore
    def __init__(self, E=(), /, **F: V):
        d = dict(E, **F)
        if d:
            self._keys, self._values = zip(*d.items())
        else:
            self._keys = cast(tuple[K], ())
            self._values = cast(tuple[V], ())

    def __getitem__(self, key: K) -> V:
        try:
            i = self._keys.index(key)
        except ValueError:
            return self.__missing__(key)
        return self._values[i]

    def __missing__(self, key: K) -> V:
        raise KeyError(key) from None

    def __len__(self) -> int:
        return len(self._keys)

    def __iter__(self) -> Iterator[K]:
        return iter(self._keys)

    def __hash__(self) -> int:
        return hash(frozenset(self.items()))

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({dict(self)!r})"

    def _repr_pretty_(self, printer: PrettyPrinter, cycle: bool = False):
        if cycle:
            printer.text("…")
            return
        opening = f"{self.__class__.__name__}("
        with printer.group(len(opening), opening, ")"):
            printer.type_pprinters[dict](self, printer, cycle)

    def __or__(self, other: Iterable[tuple[K, V]] | Mapping[K, V]) -> Frozenmap[K, V]:
        d = dict(self)
        d.update(other)
        return self.__class__(d)
