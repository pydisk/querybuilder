from __future__ import annotations
from collections import deque
import sys
from typing import (
    Any,
    Iterable,
    Mapping,
    MutableSequence,
    Optional,
    TextIO,
)
import querybuilder.formatting.formatter as qbformatter
import querybuilder.formatting.token as qbtoken
import querybuilder.formatting.tokentree as qbtokentree
import querybuilder as qb


class Logger:
    """Query logger

    Parameters
    ----------
    tokenizer: qb.drivers.sql.tokenizer.Tokenizer
        the tokenizer to use for tokenizing queries.

    file: TextIO, default=sys.stdout
        the opened file in which to write the log.

    formatter: Formatter, default=qbformatter.PygmentsFormatter
        the formatter to use to format the queries.  Designed for pygments formatters
        (see qbformatter), it accept any object enjoying a format method.  If
        None, then the query is not formatted, and the `rawformat` result is written by
        the logger.

    active: bool
        whether to log or not.  Inactive logger will not print anything, but will still
        count and/or track (see query_log parameter below) the executed queries.

    bunch_step: int, default=16
        how often to update the log during an execution of a bunch query.

    query_log: type[MutableSequence], default=deque(maxlen=0)
        a MutableSequence concrete factory, for saving, within the logger, all or some
        or none of the last queries (default is to save none of the them).
    """

    __slots__ = (
        "file",
        "tokenizer",
        "formatter",
        "active",
        "query_counter",
        "query_log",
        "bunch_step",
    )

    def __init__(
        self,
        tokenizer: qb.drivers.sql.tokenizer.Tokenizer,
        file: TextIO = sys.stdout,
        formatter: qbformatter.Formatter = qbformatter.PygmentsFormatter(),
        active: bool = True,
        bunch_step: int = 16,
        query_log: MutableSequence[qb.queries.queries.Query] = deque(maxlen=0),
    ):
        self.file = file
        self.tokenizer = tokenizer
        self.formatter = formatter
        self.active = active
        self.query_counter = 0
        self.query_log = query_log
        self.bunch_step = bunch_step

    def print_last_queries(self, stop=None, formatter=None, file=sys.stdout):
        qloglen = len(self.query_log)
        if stop is None:
            stop = qloglen
        for i in range(max(qloglen - stop, 0), qloglen):
            q = self.query_log[i]
            self.log(q, formatter=formatter, file=file)

    def __call__(
        self,
        query: qb.queries.queries.Query,
        parameters: Optional[Mapping[str, Any]] = None,
    ) -> None:
        if not self.active:
            return

        self.query_counter += 1
        self.query_log.append(query)

        tokens = query.tokenize(self.tokenizer)
        finaltokens: tuple[qbtokentree.TkSeq, ...] = ()
        TkStr = qbtokentree.TkStr
        if parameters:
            finaltokens += (
                TkStr(qbtoken.Punctuation.Linebreak, "\n").to_seq(),
                TkStr(qbtoken.Comment.Args, f"  --  ↖{parameters}").to_seq(),
            )
        finaltokens += (TkStr(qbtoken.Punctuation.Semicolon, ";").to_seq(),)
        tokens = (tokens, finaltokens)
        self.formatter.format(tokens, self.file)
        self.file.write("\n")

    def wrap_iterable(
        self,
        iterable: Iterable[tuple],
        size_hint: Optional[int] = None,
    ) -> Iterable[tuple]:
        """Returns a wrapped iterable producing logging outputs as it is iterated upon

        The elements of `iterable` are yielded unchanged.
        """
        if not self.active:
            yield from iterable

        if self.file.seekable():
            seek_pos = self.file.tell()

        if size_hint is None:
            size_hint_str = "???"
        else:
            size_hint_str = str(size_hint)
        counter = 0
        prevline_len = 0

        def padd_counter(i):
            return str(i).rjust(len(str(size_hint_str)) + 1, "0")

        def cover_prevline(line, prevline_len):
            curline_len = len(line)
            if curline_len < prevline_len:
                line = line.ljust(prevline_len, " ")
            return line, curline_len

        for args in iterable:
            if not (counter % self.bunch_step):
                line = (
                    f"  --  ↖:many ({padd_counter(counter)}/~{size_hint_str}): {args}"
                )
                line, prevline_len = cover_prevline(line, prevline_len)
                argtokens = qbtokentree.TkStr(qbtoken.Comment.Args, line).to_seq()
                self.formatter.format(argtokens, self.file)
                self.file.flush()
                if self.file.seekable():
                    self.file.seek(seek_pos)
                elif self.file.isatty():
                    self.file.write("\r")
                else:
                    self.file.write("\n")
            counter += 1
            yield args

        if counter > 0:
            line = f"  --  ↖:many ({counter}/{counter}): {args}"
            line, prevline_len = cover_prevline(line, prevline_len)
            argtokens = qbtokentree.TkStr(qbtoken.Comment.Args, line).to_seq()
            self.formatter.format(argtokens, self.file)
            self.file.write("\n")
            self.file.flush()
