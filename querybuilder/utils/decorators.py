from __future__ import annotations
from functools import wraps
from types import MappingProxyType, MethodType
from typing import (
    Any,
    Callable,
    Generic,
    Mapping,
    Optional,
    TypeVar,
    Union,
    overload,
    TYPE_CHECKING,
)
from typing_extensions import ParamSpec, Concatenate

if TYPE_CHECKING:
    import querybuilder.drivers.sql.tokenizer as qbtokenizer

P = ParamSpec("P")
R = TypeVar("R")
Tknzr = TypeVar("Tknzr", bound="qbtokenizer.Tokenizer")


class TypeDispatch(Generic[P, R, Tknzr]):
    __slots__ = ("__func__", "_registry")

    def __init__(self, func: Callable[Concatenate[Tknzr, type, P], R]):
        self.__func__: Callable[Concatenate[Tknzr, type, P], R] = func
        self._registry: dict[type, Callable[Concatenate[Tknzr, type, P], R]] = {}
        self.register(object, func)

    @property
    def registry(self) -> Mapping[type, Callable[Concatenate[Tknzr, type, P], R]]:
        return MappingProxyType(self._registry)

    @overload
    def register(
        self, cls: type, func: None = None
    ) -> Callable[
        [Callable[Concatenate[Tknzr, type, P], R]],
        Callable[Concatenate[Tknzr, type, P], R],
    ]: ...

    @overload
    def register(
        self, cls: type, func: Callable[Concatenate[Tknzr, type, P], R]
    ) -> Callable[Concatenate[Tknzr, type, P], R]: ...

    def register(
        self,
        cls: type,
        func: Optional[Callable[Concatenate[Tknzr, type, P], R]] = None,
    ) -> Union[
        Callable[Concatenate[Tknzr, type, P], R],
        Callable[
            [Callable[Concatenate[Tknzr, type, P], R]],
            Callable[Concatenate[Tknzr, type, P], R],
        ],
    ]:
        if func is None:

            def decor(
                f: Callable[Concatenate[Tknzr, type, P], R],
            ) -> Callable[Concatenate[Tknzr, type, P], R]:
                return self.register(cls, f)

            return decor
        self._registry[cls] = func
        return func

    def dispatch(self, cls: type) -> Callable[Concatenate[Tknzr, type, P], R]:
        for t in cls.mro():
            impl: Optional[Callable[Concatenate[Tknzr, type, P], R]] = (
                self.registry.get(t)
            )
            if impl:
                return impl
        raise TypeError(cls)

    def __call__(self, tknzr: Tknzr, cls: type, *args: P.args, **kwargs: P.kwargs) -> R:
        impl: Callable[Concatenate[Tknzr, type, P], R] = self.dispatch(cls)
        return impl(tknzr, cls, *args, **kwargs)

    def __get__(self, obj: Any, cls: Optional[type] = None):
        if not obj:
            return self
        return MethodType(self, obj)


def method_accepting_lambdas(func: Callable[..., R]) -> Callable[..., R]:
    # Sadly, typing couldn't be more precise
    @wraps(func)
    def decored(self, *args, **kwargs) -> R:
        arguments = list(args)
        for i, a in enumerate(arguments):
            if callable(a):
                arguments[i] = a(self)
        for k, v in kwargs.items():
            if callable(v):
                kwargs[k] = v(self)
        return func(self, *arguments, **kwargs)

    setattr(decored, "__func__", func)
    return decored
