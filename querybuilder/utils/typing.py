from __future__ import annotations
from typing import (
    Any,
    Callable,
    ContextManager,
    IO,
    Iterable,
    Mapping,
    Optional,
    Protocol,
    Sequence,
    TypeVar,
    Union,
)
import querybuilder.formatting as qbformatting


class PreFormatter(Protocol):
    def __call__(
        self, tree: qbformatting.tokentree.TkTree, **kwargs
    ) -> qbformatting.tokentree.TkSeq: ...


class StreamFormatter(Protocol):
    def format(
        self, tokens: Iterable[qbformatting.tokentree.TkStr], file: IO
    ) -> None: ...


E = TypeVar("E")
K = TypeVar("K")
V = TypeVar("V")

DictArg = Union[Mapping[K, V], Iterable[tuple[K, V]]]


class NamedProto(Protocol):
    @property
    def name(self) -> str: ...


class KeyedProto(Protocol):
    @property
    def key(self) -> Optional[str]: ...


class PrettyPrinter(Protocol):
    def pretty(self, obj: Any) -> None: ...

    def text(self, text: str) -> None: ...

    def indent(self, indent: int) -> None: ...

    def group(
        self, indent: int = 0, open: str = "", close: str = ""
    ) -> ContextManager: ...

    def breakable(self, sep: str = " ") -> None: ...

    def break_(self, sep: str = " ") -> None: ...

    def begin_group(self, indent: int = 0, close: str = "") -> None: ...

    def end_group(self, dedent: int = 0, close: str = "") -> None: ...

    def flush(self) -> None: ...

    @property
    def type_pprinters(
        self,
    ) -> Mapping[type, Callable[[Any, PrettyPrinter, bool], None]]: ...

    @property
    def singleton_pprinters(
        self,
    ) -> Mapping[int, Callable[[Any, PrettyPrinter, bool], None]]: ...


# DBAPI
class DBAPI_ConnectProto(Protocol):
    def close(self): ...

    def commit(self): ...

    def rollback(self): ...

    def cursor(self) -> DBAPI_CursorProto: ...


class DBAPI_CursorProto(Protocol, Iterable[tuple]):
    # following: https://peps.python.org/pep-0249/#cursor-objects
    @property
    # typing was not clearly specified in DBAPI documentation
    def description(
        self,
    ) -> Sequence[
        tuple[
            str,  # name
            Optional[Callable[..., Any] | type | int],  # type_code
            Optional[int],  # display_size
            Optional[int],  # internal_size
            Optional[float | int],  # precision
            Optional[float | int],  # scale
            Optional[bool],  # null_ok
        ]
    ]:
        # About type_code:
        #   in DBAPI, type_code should be a Type Objects (Callable[..., ] | type)
        #   in sqlite3, type_code is None
        #   in psycopg, type_code seems to be an integer (oid of type)
        ...

    @property
    def rowcount(self) -> int: ...

    @property
    def arraysize(self) -> int: ...

    @arraysize.setter
    def arraysize(self, value, /): ...

    def close(self): ...

    def execute(
        self,
        operation: str,
        parameters: Optional[Sequence | Mapping[int | str, Any]] = None,
    ) -> Any: ...

    def executemany(
        self,
        operation: str,
        seq_of_parameters: Iterable[Sequence | Mapping[int | str, Any]],
    ) -> Any: ...

    def fetchone(self) -> Optional[Sequence]: ...

    def fetchmany(self, size: Optional[int] = None) -> Sequence[Sequence]:
        # default size should be self.arraysize
        ...

    def fetchall(self) -> Sequence[Sequence]: ...

    def setinputsizes(self, sizes: Sequence[Callable | int | None]): ...

    def setoutputsize(self, size: int, column: Optional[int] = None): ...
