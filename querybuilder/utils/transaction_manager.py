from __future__ import annotations
import functools
from typing import Union, Optional, Any, Mapping, Iterable
import querybuilder.queries.tcl as tcl
from querybuilder.exception import TransactionError, SessionError
import querybuilder.drivers.sql.connector as qbconnector
import querybuilder as qb


class TransactionManager:
    """Transaction manager.  Every Connector should have one.

    Mutable transaction manager, which support contextualization.

    Parameters
    ----------
    connector : Connector
        The connector for which to manage transactions.

    **kwargs : dict
        keyworded parameters to be passed to the set method at
        initialization end.

    Glossary
    --------
    transaction control query
        One of the "BEGIN", "COMMIT", "ROLLBACK", "SAVEPOINT", and "RELEASE
        SAVEPOINT" queries or variants, which allows transaction control.
        It should be an explicit query, i.e., not, e.g., a commit through
        the backend connector's `commit` method.

    active query
        A query, not being a transaction control query, that has been
        executed but neither committed nor rolled back.

    explicit (active) transaction
        A sequence of active queries that was preceded by a "BEGIN"
        transaction control query.

    implicit (active) transaction
        A sequence of active queries that was not preceded by a "BEGIN"
        query.  When the sequence is nonempty, it is not possible to start
        a transaction segment by firing some transaction control query.
        Indeed, the implicit transaction should be committed or rolled back
        first.

    active savepoint
        A savepoint that has been created and not released, within an
        explicit active transaction.

    transaction segment
        A maximal sequence of consecutive active queries, delimited by an
        explicit or implicit transaction start, or the creation of an active
        savepoint.  There always is a (possibly empty) transaction segment,
        which correspond to an implicit transaction.  When an explicit
        transaction is started (by a "BEGIN" query), we consider that there
        are two transactions segments:  a first one for the implicit active
        queries that occurs before the just-started explicit one, and one
        for the explicit transaction, although the start of such an explicit
        transaction implies a commit of the implicit transaction.

    Attributes
    ----------
    active : bool (property)
        Whether there currently is an explicit active transaction.  This is
        equivalent to having the list attribute `query_counters` longer than
        1.

    connector : Connector
        The connector for which to manage transactions.

    query_counters : list[int]
        Counters of active queries, by transaction segments.  The first item
        always exists and equals the number of queries performed out of the
        current explicit running transaction — it is reset on each explicit
        transaction start, so it might be nonzero only if the list has
        length exactly one.  In particular, in case there is no active
        transaction, this item is the only item of the list.  When a
        transaction is active, then there is a second item which equals the
        number of active queries executed after the transaction start and
        before the first active savepoint, if any, or until now, otherwise.
        The subsequent items indicate the numbers of active queries executed
        between the consecutive active savepoints.  In particular, if no
        savepoint has been created but a transaction is active, the list has
        length exactly 2.  Notice that rollbacking to a savepoint does not
        release it, that is, the savepoint remains active after this partial
        rollback.  However, this operation releases every active savepoint
        that have been created after this specific savepoint creation,
        possibly unshadowing some preceding ones.

    savepoints : list[str]
        The ordered list of active savepoints of the running transaction.
        The list length always equals the length of `query_counters` minus 2
        unless there currently is no explicit transaction.  Indeed, within
        an explicit transaction, each savepoint starts a new transaction
        segment, and there are two more transaction segments:  the first one
        (corresponding to an implicit transaction out from the explicit
        transaction), and the second one (which has been started by the
        "BEGIN" query that initialized the current explicit transaction).
        In absence of explicit transaction, the list is empty:  savepoints
        can exist only in explicit transactions.

    context_savepoint_format : str
        The format of the names of the savepoints created on context
        management.  The format (str) may include the '{index}' placeholder
        to be replaced by the depth (int) of the managed context.

    subcontexts : list[int or None]
        For each current context (which can be nested whence the list), the
        specification of the transaction segment start that occur at context
        entrance or just before.  The value `-1` indicates that the
        transaction segment that was started at context entrance is the
        current transaction beginning.  Other integer value indicate the
        index in `savepoints` of the active savepoint that has been created
        by the context management (i.e., within `__enter__` method) for the
        context.  Finally, the value `None` indicates that no transaction
        segment is under control for the context.

    mode : dictable
        The SQL transaction mode with which the active transaction has been
        started, if there is an active transaction, or with which to start
        the next transaction, otherwise.  The mode is a set of keyworded
        parameters, accepted by the `self.queries.tcl.Begin` initializer.
        After the next end of transaction, the mode is reset to the empty
        mapping.

    on_next_context : int
        Specifies how transaction is controlled when entering a context.  If
        `0` then no transaction control is performed.  If `1` then a minimal
        transaction control is performed, namely, nothing more than starting
        a transaction if none is active is performed.  If `2` then a
        intermediate transaction control is performed:  a transaction is
        started if none is active, a savepoint is created otherwise, unless
        no queries has been executed in the current transaction segment.  In
        which case, there is indeed no need to create a savepoint, as
        rolling back to the preceding savepoint if any, or the transaction
        start, otherwise, does the rollback job.  Finally, if `3` or more,
        then a full transaction control is performed:  each context entrance
        produces a transaction start or a savepoint creation, according to
        whether no transaction is active.  The `on_next_context` applies to
        the next context to enter only.  After use, it is set to the value
        of the `on_all_contexts` attribute.

    on_all_contexts : int
        Default value for `on_next_context` attribute, when missing or for
        resetting the `on_next_context` attribute after a context entrance.

    deferred : list[int]
        The ordered list of deferred transaction segment starts.  If `-1`, the
        deferred transaction segment start is a transaction begin.  Otherwise,
        it is the savepoint creation at corresponding index in `savepoints`.

    deferrable : bool
        Whether transaction start and savepoint creations are deferred to avoid
        empty transactions or transaction segments, or not.

    NOTES
    -----
    Invariants:
        `max(0, len(query_counters)-2) == len(savepoints)`
        `len(deferred) < len(savepoints)+1 < len(query_counters)`

    """

    # TODO: remove this sanity check method and move its contents to doctests
    def _invariant_check(self):
        if self.active:
            assert len(self.query_counters) == len(self.savepoints) + 2
            assert len(self.deferred) <= len(self.savepoints) + 1
            assert all(
                ctxt is None or -1 <= ctxt < len(self.savepoints)
                for ctxt in self.subcontexts
            )
            assert all(-1 <= dfrd < len(self.savepoints) for dfrd in self.deferred)
            assert not self.deferred or not self.query_counters[-1]
        else:
            assert not self.savepoints
            assert not self.deferred
            assert len(self.query_counters) == 1
            assert all(ctxt is None for ctxt in self.subcontexts)

    @property
    def active(self):
        return len(self.query_counters) > 1

    def __init__(
        self,
        connector: qbconnector.Connector,
        *,
        on_all_contexts: int = 1,
        deferrable: bool = True,
        all_context_savepoint_format: str = "context_savepoint_{index}",
        **kwargs,
    ):
        self.connector = connector

        self.chainwithprevious = False
        self.chainwithnext = False
        self.deferrable = deferrable

        self.subcontexts: list[Union[int, None]] = []
        self.on_all_contexts = on_all_contexts
        self.all_context_savepoint_format = all_context_savepoint_format

        self.reset()
        self.set(**kwargs)

    def reset(self):
        self.mode = {}
        self.deferred = []
        self.on_next_context = self.on_all_contexts
        self.next_context_savepoint_format = self.all_context_savepoint_format
        self.savepoints = []
        self.query_counters = [0]
        self._invariant_check()

    def set(
        self,
        mode=None,
        chainwithnext=None,
        deferrable=None,
        on_next_context=None,
        on_all_contexts=None,
        next_context_savepoint_format=None,
        all_context_savepoint_format=None,
    ):
        """
        Parameters
        ----------
        mode : dictable or None, default=None
            Set the mode of the next transaction.  This is possible only if no
            transaction is active.  If None, the current value is kept.

        chainwithnext : bool or None, default=None
            Whether to chain the current transaction (or the next one, if none
            is active) with its successor one.  If `None` the current instance
            value is kept unchanged.

        on_next_context : int or None, default=None
            If not None, sets the `on_next_context` attribute, for controlling how
            the next context to enter should manage transaction.  It is interpreted
            with the following meaning, applied to the next context:
            · `0`: no transaction control is performed;
            · `1`: a transaction is started if none is active, otherwise as `0`;
            · `2`: if a transaction is active, a savepoint is created if there
                have been some queries executed since the last transaction segment
                start, otherwise as `1`;
            · `3` (or more): a transaction start is always ensured, which might
                be a transaction beginning if no transaction is already active, or
                a savepoint creation otherwise…

        on_all_contexts : int or None, default=None
            If not None, sets the `on_all_contexts` attribute as well as the
            `on_next_context` attribute unless the `on_next_context` parameter has
            been given.  These attributes control the transaction control on contexts
            (`on_all_contexts` sets the default control level for all context, while
            `on_next_context` applies, with higher priority, to the next context only)
            — see documentations of `on_next_context` parameter and attribute for
            details.

        next_context_savepoint_format : str or None, default=None
            If not None, sets the `next_context_savepoint_format` attribute, which
            specifies how to name the savepoint to be created on next context entrance,
            if any.  This format may include the special '{index}' placeholder to be
            replaced by the depth (int) of the managed context.

        all_context_savepoint_format : str pr None, default=None
            If not None, then it sets the `all_context_savepoint_format` attribute as
            well as the `next_context_savepoint_format` attribute unless the same-named
            parameter has been given.  These attributes control the naming of savepoints
            created for transaction control on next, or all, context entrance,
            respectively — see documentations of `next_context_savepoint_format`
            paramater and attribute for details.

        deferrable : None or bool
            Whether transaction start should be deferred or not.  If deferred, their
            corresponding TCL query is executed only if needed, namely, only if a query
            is executed in the transaction segment or if an inner transaction segment.
            If None, the current value is kept.
        """
        self._invariant_check()
        if mode is not None:
            if self.active:
                raise TransactionError(
                    f"Cannot change mode of active transaction to {mode}"
                )
            self.mode = mode
        if chainwithnext is not None:
            self.chainwithnext = chainwithnext
        if deferrable is not None:
            self.deferrable = deferrable
        if on_all_contexts is not None:
            self.on_all_contexts = on_all_contexts
            self.on_next_context = on_all_contexts
        if on_next_context is not None:
            self.on_next_context = on_next_context
        if all_context_savepoint_format is not None:
            self.all_context_savepoint_format = all_context_savepoint_format
            self.next_context_savepoint_format = all_context_savepoint_format
        if next_context_savepoint_format is not None:
            self.next_context_savepoint_format = next_context_savepoint_format
        self._invariant_check()

    @functools.wraps(set)
    def __call__(self, *args, **kwargs):
        self.set(*args, **kwargs)
        return self

    def __enter__(self):
        if not hasattr(self.connector, "session"):
            raise SessionError("Connector has no active session")
        self._invariant_check()
        # get previous transaction segment start for context if any, None otherwise
        prv_ctxt = None
        for k in self.subcontexts:
            if k is not None:
                prv_ctxt = k
        if self.on_next_context <= 0:
            ctxt = None
        elif not self.active:
            self.begin(mode=self.mode, implicit=True, deferred=self.deferrable)
            ctxt = -1
        elif self.on_next_context < 2:
            ctxt = None
        elif (
            self.on_next_context == 2
            and prv_ctxt == len(self.savepoints) - 1
            and not self.query_counters[-1]
        ):
            ctxt = prv_ctxt
        else:
            ctxt = len(self.savepoints)
            self.savepoint(
                self.next_context_savepoint_format.format(index=len(self.subcontexts)),
                deferred=self.deferrable,
            )
        self.subcontexts.append(ctxt)
        self.on_next_context = self.on_all_contexts
        self.next_context_savepoint_format = self.all_context_savepoint_format
        self._invariant_check()
        return self

    def __exit__(self, exc, arg, tb):
        if not isinstance(exc, AssertionError):
            self._invariant_check()
        ctxt = self.subcontexts.pop()
        if exc:
            keep = [i for i, c in enumerate(self.subcontexts) if c == ctxt]
            if ctxt is None:
                self.rollback(implicit=True)
            elif ctxt == -1:
                self.rollback(implicit=True)
                if keep:
                    # restart the transaction segment, deferred, for outer-contexts…
                    self.begin(mode=self.mode, deferred=True, implicit=True)
                    for i in keep:
                        self.subcontexts[i] = ctxt
            else:
                self._rollback_to_savepoint(ctxt, release=not keep)
        else:
            if ctxt is None:
                pass
            elif ctxt in self.subcontexts:
                pass
            elif ctxt == -1:
                try:
                    self.commit(implicit=True)
                except Exception:
                    self.rollback(implicit=True)
                    raise
            else:
                self._release_savepoint(ctxt)
        self._invariant_check()

    def apply_deferred(self):
        while self.deferred:
            trans = self.deferred.pop(0)
            if trans == -1:
                q = tcl.Begin(**self.mode)
            else:
                sp = self.savepoints[trans]
                q = tcl.CreateSavepoint(sp)
            self._execute(q)
        self._invariant_check()

    def begin(self, mode=None, *, implicit=True, deferred=False):
        self._invariant_check()
        mode = self.mode if mode is None else mode
        if self.active:
            if self.chainwithprevious:
                self.chainwithprevious = False
                return
            raise TransactionError(
                "Cannot start an explicit transaction"
                "within an already-existing explicit transaction"
            )
        if self.query_counters[0]:
            if not implicit:
                raise TransactionError(
                    "Cannot start an explicit transaction"
                    "since an implicit one is running"
                )
            self.query_counters[0] = 0
        self.connector.session.commit()
        self.mode = mode
        self.query_counters.append(0)
        if deferred:
            self.deferred.append(-1)
        else:
            q = tcl.Begin(**mode)
            self.apply_deferred()
            self._execute(q)
        self._invariant_check()

    def commit(self, *, implicit=False):
        self._invariant_check()
        if not self.active:
            if not implicit:
                self.reset()
                raise TransactionError("No transaction to commit")
            self.connector.session.commit()
        elif self.chainwithnext:
            self.chainwithnext = False
            self.chainwithprevious = True
        elif not self.deferred or self.deferred[0] != -1:
            q = tcl.Commit()
            self._execute(q)
            self.connector.session.commit()
        self.subcontexts = [None] * len(self.subcontexts)
        self.reset()
        self._invariant_check()

    def _rollback_to_savepoint(self, idx, release=False):
        self._invariant_check()
        name = self.savepoints[idx]
        for i in range(len(self.savepoints) - 1, idx, -1):
            if self.savepoints[i] == name:
                self._rollback_to_unique_savepoint(idx, release=True)
        self._rollback_to_unique_savepoint(idx, release=release)
        self._invariant_check()

    def _rollback_to_unique_savepoint(self, idx, release=False):
        # assumed: no savepoints in `self.savepoints[idx+1:]` have same name as
        #          `self.savepoints[idx]`
        self._invariant_check()
        name = self.savepoints[idx]
        # several savepoints might be implicitly released
        self.savepoints = self.savepoints[: idx + 1]
        self.query_counters = self.query_counters[: idx + 2] + [0]
        for i, ctxt in enumerate(self.subcontexts):
            if ctxt is not None and ctxt > idx:
                self.subcontexts[i] = None
        if idx in self.deferred:
            n = len(self.deferred)
            j = self.deferred.index(idx)
            self.deferred = self.deferred[: -(n - j) - (not release)]
            if release:
                self.savepoints.pop()
                self.query_counters.pop()
        else:
            q = tcl.RollbackToSavepoint(name)
            self._execute(q)
            if release:
                self._release_unique_savepoint(idx)
        self._invariant_check()

    def rollback(self, to_savepoint=None, release=False, *, implicit=False):
        self._invariant_check()
        if to_savepoint is True:
            to_savepoint = len(self.savepoints) - 1 if self.savepoints else None
        if not self.active:
            if not implicit:
                self.reset()
                raise TransactionError("No transaction to rollback")
            self.query_counters[0] = 0
            self.connector.session.rollback()
        elif to_savepoint is not None:
            if isinstance(to_savepoint, int):
                idx = to_savepoint
            elif to_savepoint not in self.savepoints:
                raise TransactionError("No savepoint {to_savepoint} is known")
            else:
                revsavepoints = list(reversed(self.savepoints))
                idx = len(revsavepoints) - 1 - revsavepoints.index(to_savepoint)
            self._rollback_to_unique_savepoint(idx, release=release)
        else:
            if not self.deferred or self.deferred[0] != -1:
                q = tcl.Rollback()
                self._execute(q)
                self.connector.session.rollback()
            self.subcontexts = [None] * len(self.subcontexts)
            self.reset()
        self._invariant_check()

    def get_new_indexed_savepoint(self, name="savepoint"):
        savepoints = set(self.savepoints)
        i = 0
        while f"{name}{i}" in savepoints:
            i += 1
        name = f"{name}{i}"
        return name

    def savepoint(self, name, autoindex=False, *, deferred=False):
        """
        +    name
            a string, being a savepoint name or basename (c.f. index).
        +    index
            a Boolean. If `True`, then the smallest integer index `i`
            such that `f"{name}{i}"` is not an existing savepoint name
            is append to `name`. This happen even if `name` is not an
            existing savepoint name (whence, the first indexed
            savepoint name of root `name` is `f"{name}0"`). If `False`
            {name} is used unchanged, whence an error is raised if the
            name is already used by some existing savepoint. In every
            case, the method returns the created savepoint name, so
            that the indexed version can be saved by the caller.
        """
        self._invariant_check()
        if not self.active:
            spname = f"{name}{'N' if autoindex else ''}"
            raise TransactionError(
                f"No transaction within which to create savepoint {spname}"
            )
        if autoindex:
            name = self.get_new_indexed_savepoint(name=name)
        self.savepoints.append(name)
        self.query_counters.append(0)
        q = tcl.CreateSavepoint(name)
        if deferred:
            self.deferred.append(len(self.savepoints) - 1)
        else:
            self.apply_deferred()
            self._execute(q)
        return name
        self._invariant_check()

    def _release_unique_savepoint(self, idx):
        """Release active savepoint specified by index assuming its name is not shadowed

        Parameters
        ----------
        idx: int
            The index in `self.savepoints` of the savepoint to release.  It is assumed
            that no savepoint in `self.savepoints[idx+1:]` has same name as this one.

        """
        self._invariant_check()
        name = self.savepoints[idx]
        # assumed: idx is the rightmost occurrence index of name in self.savepoints
        self.savepoints = self.savepoints[:idx]
        for i, ctxt in enumerate(self.subcontexts):
            if ctxt is None or ctxt < 0:
                continue
            elif ctxt >= idx:
                self.subcontexts[i] = None
        if idx in self.deferred:
            n = len(self.deferred)
            i = self.deferred.index(idx)
            self.query_counters = self.query_counters[: -(n - i)]
            self.deferred = self.deferred[: -(n - i)]
        else:
            qbefore = self.query_counters[: idx + 1]
            qafter = [sum(self.query_counters[idx + 1 :])]
            self.query_counters = qbefore + qafter
            self.deferred = []
            q = tcl.ReleaseSavepoint(name)
            self._execute(q)
        self._invariant_check()

    def _release_savepoint(self, idx):
        """Release active savepoint specified by index.

        If the name of the savepoint specified by `idx` is shared by other
        active after-created savepoints, then they are released as well.

        Parameters
        ----------
        idx : int
            The index of the active savepoint to release in `self.savepoints`.
        """
        self._invariant_check()
        name = self.savepoints[idx]
        n = len(self.savepoints)
        for i in range(n - 1, idx - 1, -1):
            if self.savepoints[i] == name:
                self._release_unique_savepoint(i)
        self._invariant_check()

    def release_savepoint(self, name=True):
        """Release active savepoint by name.

        Notice that several active savepoints may have same name, the newest
        shadowing the oldest.  Thus, only the newest active savepoint of name
        `name` is released.

        Parameters
        ----------
        name : str or bool
            The name of the active savepoint to release.  If `True`, then the
            newest active savepoint name is taken.  If `False`, no savepoint is
            released (thus nothing is done).

        Raises
        ------
        TransactionError
            If the specified savepoint does not exists.  This includes cases in
            which `name` is `True` but there is no active savepoint, or in which
            there is no active transaction at all.
        """
        self._invariant_check()
        if name is True:
            if not self.savepoints:
                raise TransactionError("No savepoint in current transaction")
            idx = len(self.savepoints) - 1
        elif name is False:
            return
        elif not self.active:
            raise TransactionError(
                f"Cannot release savepoint {name} in ended transaction"
            )
        elif name not in self.savepoints:
            raise TransactionError(
                f"No savepoint named {name} known in the current transaction"
            )
        else:
            revsavepoints = list(reversed(self.savepoints))
            idx = len(revsavepoints) - 1 - revsavepoints.index(name)
        self._release_unique_savepoint(idx)
        self._invariant_check()

    def execute(
        self,
        query: qb.queries.queries.Query,
        parameters: Optional[Mapping[str, Any]] = None,
    ):
        self._invariant_check()
        self.apply_deferred()
        self.query_counters[-1] += 1
        res = self._execute(query, parameters=parameters)
        self._invariant_check()
        return res

    def _execute(
        self,
        query: qb.queries.queries.Query,
        parameters: Optional[Mapping[str, Any]] = None,
    ) -> Any:
        self._invariant_check()

        if not parameters:
            parameters = {}

        # query = query.autokey_placeholders()
        phs = query.get_placeholders()  # tuple of placeholders
        # key-to-placeholder map (currently, autokeying anonymous placeholders)
        keyed_phs = phs.to_keyed_map()
        allargs = phs.to_valued_map(keyed_phs, **parameters)  # key-to-value map
        # TODO: allow positional placeholders (⇒ allargs as a tuple) in some cases
        #       (when? how?) // how ⇒ use to_value_tuple method of PlaceholderStore

        if hasattr(self.connector, "cast_values"):
            allargs = self.connector.cast_values(allargs)

        q = query.rawformat(self.connector.tokenizer)
        if self.connector.logger is not None:
            # TODO: ⇒ second pass on tokenizing
            self.connector.logger(query, allargs)

        cursor = self.connector.cursor()
        res = cursor.execute(q, allargs)

        res = iter(res)
        self._invariant_check()

        # TODO: decode return items
        return res

    def executemany(
        self,
        query: qb.queries.queries.Query,
        parameters: Iterable[Mapping[str | int, Any]],
        size_hint=None,
    ) -> Iterable[tuple]:
        self._invariant_check()
        self.apply_deferred()
        self.query_counters[-1] += 1
        res = self._executemany(query, parameters=parameters, size_hint=size_hint)
        self._invariant_check()
        return res

    def _executemany(
        self,
        query: qb.queries.queries.Query,
        parameters: Iterable[Mapping[str | int, Any]],
        size_hint=None,
    ) -> Iterable[tuple]:
        """

        Parameters
        ----------
        query: queries.dml.Object
            the DML query to execute.

        parameters: iterable, default=()
            the iterable of values to feed keyed placeholders.  Each element
            is a dict if key_order is None or a tuple of same length as the
            key_order parameter value otherwise.


        size_hint: int or None, default=None
            a hint on the size of the iterable I, mostly used for logging.  If
            None, the method attempts to get a length hint from I, by looking
            at `I.__len__()` and `I.__length_hint__()`.
        """

        if size_hint is None:
            if hasattr(parameters, "__len__"):
                size_hint = parameters.__len__()  # using __len__ because of mypy
            elif hasattr(parameters, "__length_hint__"):
                size_hint = parameters.__length_hint__()
        self._invariant_check()

        phs = query.get_placeholders()  # tuple of placeholders
        keyed_phs = phs.to_keyed_map()
        parameters = map(lambda m: phs.to_valued_map(keyed_phs, **m), parameters)

        if hasattr(self.connector, "cast_values"):
            parameters = map(self.connector.cast_values, parameters)

        logger = self.connector.logger
        if logger is not None:
            # TODO: ⇒ second pass on tokenizing
            logger(query)
            parameters = self.connector.logger.wrap_iterable(parameters)

        cursor = self.connector.cursor()
        cursor.executemany(query.rawformat(self.connector.tokenizer), parameters)
        # TODO: why is casting required by mypy?
        # prmtrs = cast(Iterable[Mapping[Union[int, str], Any]], parameters)
        # cursor.executemany(query.rawformat(self.connector.tokenizer), prmtrs)
        res = cursor
        self._invariant_check()
        return res
