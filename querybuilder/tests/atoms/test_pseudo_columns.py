from mock import Mock
import querybuilder.atoms.pseudo_columns as qbpseudo_columns


class TestStar:
    def test_get_subtokenize_kwargs_with_relation(self):
        relation_tok = "rel_name"
        relation = Mock()
        relation.subtokenize = Mock(return_value=relation_tok)

        star = qbpseudo_columns.Star(relation=relation)

        kwargs = star._get_subtokenize_kwargs(None)

        expected = {"relation": "rel_name"}

        assert expected == kwargs

    def test_get_subtokenize_kwargs_without_relation(self):
        star = qbpseudo_columns.Star()

        kwargs = star._get_subtokenize_kwargs(None)

        expected = {"relation": None}

        assert expected == kwargs


class TestDefault:
    def test_get_subtokenize_kwargs(self):
        default = qbpseudo_columns.Default()

        assert {} == default._get_subtokenize_kwargs(None)
