import pytest
from mock import Mock
from querybuilder.atoms.atoms import Atom


class ScopableAtom(Atom):
    _scopable = True


class TestAtom:
    @pytest.fixture
    def mock_tokens(self):
        return "tokens"

    @pytest.fixture
    def mock_scoped_tokens(self):
        return "(tokens)"

    @pytest.fixture
    def mock_tokenizer(self, mocker, mock_tokens, mock_scoped_tokens):
        tk = Mock(name="tokenizer", return_value=mock_tokens)
        tk.scope_it = Mock(name="scope_it", return_value=mock_scoped_tokens)
        tk.transform = lambda x: x

        return tk

    def test_scopable_atom_with_scoped(
        self, mock_tokenizer, mock_tokens, mock_scoped_tokens
    ):
        atom = ScopableAtom()

        result = atom.subtokenize(mock_tokenizer, scoped=True)

        assert result == mock_scoped_tokens
        mock_tokenizer.scope_it.assert_called_with(mock_tokens)

    def test_scopable_atom_without_scoped(
        self, mock_tokenizer, mock_tokens, mock_scoped_tokens
    ):
        atom = ScopableAtom()

        result = atom.subtokenize(mock_tokenizer)

        assert result == mock_tokens
        mock_tokenizer.scope_it.assert_not_called

    def test_unscopable_atom_with_scoped(
        self, mock_tokenizer, mock_tokens, mock_scoped_tokens
    ):
        atom = Atom()

        result = atom.subtokenize(mock_tokenizer, scoped=True)

        assert result == mock_tokens
        mock_tokenizer.scope_it.assert_not_called

    def test_unscopable_atom_without_scoped(
        self, mock_tokenizer, mock_tokens, mock_scoped_tokens
    ):
        atom = ScopableAtom()

        result = atom.subtokenize(mock_tokenizer)

        assert result == mock_tokens
        mock_tokenizer.scope_it.assert_not_called

    def test_substitute_with_self_in_substitution(self) -> None:
        input_atom = object.__new__(Atom)
        expected_atom = object.__new__(Atom)
        substitution = {input_atom: expected_atom}

        result = input_atom.substitute(substitution)

        assert result is expected_atom

    def test_substitute_without_self_in_substitution(self) -> None:
        input_atom = object.__new__(Atom)

        substitution = {}

        result = input_atom.substitute(substitution)

        assert result is input_atom
