from uuid import UUID
import pytest
from mock import Mock
import querybuilder.atoms.columns as qbcolumns
from querybuilder.helpers import make_column
import querybuilder as qb


class TestColumns:
    @pytest.mark.parametrize(
        "col",
        [
            qbcolumns.Constant(int, 42),
            qbcolumns.Constant(str, "42"),
            qbcolumns.Constant(str, 42),
            qbcolumns.Constant(UUID, 0),
            qbcolumns.False_(),
            qbcolumns.True_(),
            qbcolumns.Null(),
            qbcolumns.Value(int, 42, key="k"),
            qbcolumns.Value(int, 42),
        ],
    )
    def test_equality(self, col):
        assert col == col.buildfrom(col)
        assert hash(col) == hash(col.buildfrom(col))

    @pytest.mark.parametrize(
        "col, str_",
        [
            (qbcolumns.Constant(int, 42), "42"),
            (qbcolumns.Constant(str, "42"), "'42'"),
            (qbcolumns.Constant(str, "42"), "'42'"),
            (qbcolumns.Constant(UUID, 0), repr("00000000-0000-0000-0000-000000000000")),
            (qbcolumns.False_(), "False"),
            (qbcolumns.True_(), "True"),
            (qbcolumns.Null(), "NULL"),
            (qbcolumns.Value(int, 42, key="k"), ":k"),
            (qbcolumns.Value(int, 42), "?"),
        ],
    )
    def test_str(self, col, str_):
        assert str_ == str(col)

    @pytest.mark.parametrize(
        "col, args",
        [
            (qbcolumns.Constant(int, 42), [(42, {}), (42, {"sqltype": int})]),
            (qbcolumns.Constant(int, "42"), [("42", {"sqltype": int})]),
            (
                qbcolumns.Constant(str, "42"),
                [
                    ("42", {"constant": True}),
                    ("42", {"sqltype": str, "constant": True}),
                ],
            ),
            (qbcolumns.Value(str, "42"), [("42", {}), ("42", {"sqltype": str})]),
            (qbcolumns.Value(int, 42), [(42, {"sqltype": int, "constant": False})]),
            (qbcolumns.False_(), [(False, {}), (False, {"sqltype": bool})]),
            (qbcolumns.True_(), [(True, {}), (True, {"sqltype": bool})]),
            (qbcolumns.Null(), [(None, {}), (None, {"sqltype": type(None)})]),
        ],
    )
    def test_make_column(self, col, args):
        for arg, kwarg in args:
            assert col == make_column(arg, **kwarg)

    def assert_correct_operation(
        self, op_gen, op_column, op_str, l_column, r_param, r_column
    ):
        result = op_gen(l_column, r_param)

        expected = op_column(op_str, l_column, r_column)

        assert expected == result

    @pytest.mark.parametrize(
        "op_gen, op_column, op_str",
        [
            (lambda lft, rght: lft.add(rght), qbcolumns.ArithmeticOperation, "+"),
            (lambda lft, rght: lft.mul(rght), qbcolumns.ArithmeticOperation, "*"),
            (lambda lft, rght: lft.sub(rght), qbcolumns.ArithmeticOperation, "-"),
            (lambda lft, rght: lft.div(rght), qbcolumns.ArithmeticOperation, "/"),
        ],
    )
    @pytest.mark.parametrize(
        "l_column",
        [
            qbcolumns.Named(int, "foo"),
            qbcolumns.Constant(int, 24),
        ],
    )
    @pytest.mark.parametrize(
        "r_param, r_column",
        [
            (42, qbcolumns.Constant(int, 42)),
            (qbcolumns.Constant(int, 42), qbcolumns.Constant(int, 42)),
        ],
    )
    def test_arithmetic_operation(
        self, op_gen, op_column, op_str, l_column, r_param, r_column
    ):
        self.assert_correct_operation(
            op_gen, op_column, op_str, l_column, r_param, r_column
        )

    @pytest.mark.parametrize(
        "op_gen, op_column, op_str",
        [
            (lambda lft, rght: lft.eq(rght), qbcolumns.Comparison, "="),
            (lambda lft, rght: lft.neq(rght), qbcolumns.Comparison, "!="),
            (lambda lft, rght: lft.geq(rght), qbcolumns.Comparison, ">="),
            (lambda lft, rght: lft.leq(rght), qbcolumns.Comparison, "<="),
            (lambda lft, rght: lft.lt(rght), qbcolumns.Comparison, "<"),
            (lambda lft, rght: lft.gt(rght), qbcolumns.Comparison, ">"),
            (lambda lft, rght: lft.like(rght), qbcolumns.Comparison, "LIKE"),
            (lambda lft, rght: lft.notlike(rght), qbcolumns.Comparison, "NOT LIKE"),
            (lambda lft, rght: lft.ilike(rght), qbcolumns.Comparison, "ILIKE"),
            (lambda lft, rght: lft.notilike(rght), qbcolumns.Comparison, "NOT ILIKE"),
        ],
    )
    @pytest.mark.parametrize(
        "l_column",
        [
            qbcolumns.Named(int, "foo"),
            qbcolumns.Constant(int, 24),
            qbcolumns.ArithmeticOperation(
                "+", qbcolumns.Named(int, "oof"), qbcolumns.Constant(int, 24)
            ),
        ],
    )
    @pytest.mark.parametrize(
        "r_param, r_column",
        [
            (42, qbcolumns.Constant(int, 42)),
            ("bar", qbcolumns.Value(str, "bar")),
            (qbcolumns.Constant(int, 42), qbcolumns.Constant(int, 42)),
        ],
    )
    def test_boolean_operation(
        self, op_gen, op_column, op_str, l_column, r_param, r_column
    ):
        self.assert_correct_operation(
            op_gen, op_column, op_str, l_column, r_param, r_column
        )

    @pytest.mark.parametrize(
        "left, right, result",
        [
            (qbcolumns.True_(), qbcolumns.True_(), qbcolumns.True_()),
            (qbcolumns.False_(), qbcolumns.True_(), qbcolumns.False_()),
            (qbcolumns.False_(), qbcolumns.False_(), qbcolumns.False_()),
            (qbcolumns.True_(), qbcolumns.Null(), qbcolumns.Null()),
            (qbcolumns.False_(), qbcolumns.Null(), qbcolumns.False_()),
            (qbcolumns.Null(), qbcolumns.Null(), qbcolumns.Null()),
            (qbcolumns.True_(), None, qbcolumns.Null()),
            (qbcolumns.False_(), None, qbcolumns.False_()),
            (qbcolumns.Null(), None, qbcolumns.Null()),
        ],
    )
    def test_and_of_simple_boolean_columns_or_null(self, left, right, result):
        assert left.and_(right) == result
        assert (left & right) == result
        if right is not None:
            assert right.and_(left) == result
            assert (right & left) == result

    @pytest.mark.parametrize(
        "left, right, result",
        [
            (qbcolumns.True_(), qbcolumns.True_(), qbcolumns.True_()),
            (qbcolumns.True_(), qbcolumns.False_(), qbcolumns.True_()),
            (qbcolumns.False_(), qbcolumns.False_(), qbcolumns.False_()),
            (qbcolumns.True_(), None, qbcolumns.True_()),
            (qbcolumns.False_(), None, qbcolumns.Null()),
            (qbcolumns.Null(), None, qbcolumns.Null()),
        ],
    )
    def test_or_of_simple_boolean_columns_or_null(self, left, right, result):
        assert left.or_(right) == result
        assert (left | right) == result
        if right is not None:
            assert right.or_(left) == result
            assert (right | left) == result


class TestAggregation:
    @pytest.mark.parametrize(
        "method, aggregator, typer",
        [
            ("count", "count", lambda c: int),
            ("sum", "sum", lambda c: c.sqltype),
            ("min", "min", lambda c: c.sqltype),
            ("max", "max", lambda c: c.sqltype),
            ("any", "bool_or", lambda c: bool),
            ("all", "bool_and", lambda c: bool),
        ],
    )
    def test_count(self, method, aggregator, typer):
        col = qbcolumns.Constant(str, "foobar")
        sqltype = typer(col)
        ccol0 = qbcolumns.Aggregate(aggregator, col, sqltype)
        ccol1 = getattr(col, method)()
        for ccol in (ccol0, ccol1):
            assert ccol.sqltype is sqltype
            assert isinstance(ccol, qbcolumns.Aggregate)
            assert ccol.aggregator == aggregator
            assert ccol.subcolumn is col
        assert ccol0 == ccol1


class TestCase:
    @pytest.fixture
    def int_when(self):
        return [(qbcolumns.True_(), qbcolumns.Constant(int, i)) for i in range(4)]

    @pytest.fixture
    def non_uniform_type_when(self):
        return [
            (qbcolumns.True_, c)
            for c in [
                qbcolumns.Constant(str, "a"),
                qbcolumns.Constant(bool, False),
                qbcolumns.Constant(float, 3.0),
            ]
        ]

    def test_case_without_else(self, int_when):
        when = int_when
        col = qbcolumns.Case(*when)

        assert not col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)

    def test_case_with_else(self, int_when):
        when = int_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when, else_=else_)
        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)

    def test_case_distinct_types_without_else(self, non_uniform_type_when):
        when = non_uniform_type_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when, (qbcolumns.True_(), else_))
        assert not col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when) + 1

    def test_case_distinct_types_with_else(self, non_uniform_type_when):
        when = non_uniform_type_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when, else_=else_)
        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)

    def test_case_without_else_add_when(self, int_when):
        when = int_when
        col = qbcolumns.Case(*when[:-1])
        col = col.add_when(*when[-1])

        assert not col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.when[-1] == when[-1]

    def test_case_without_else_add_when_prepended(self, int_when):
        when = int_when
        col = qbcolumns.Case(*when[1:])
        col = col.add_when(*when[0], prepend=True)

        assert not col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.when[0] == when[0]

    def test_case_with_else_add_when(self, int_when):
        when = int_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when[:-1], else_=else_)
        col = col.add_when(*when[-1])

        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.when[-1] == when[-1]

    def test_case_with_else_add_when_prepended(self, int_when):
        when = int_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when[1:], else_=else_)
        col = col.add_when(*when[0], prepend=True)

        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.when[0] == when[0]

    def test_case_without_else_add_when_distinct_types(self, non_uniform_type_when):
        when = non_uniform_type_when
        col = qbcolumns.Case(*when)
        cond = qbcolumns.True_()
        val = qbcolumns.Constant(int, -1)
        col = col.add_when(cond, val)

        assert not col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when) + 1
        assert col.when[-1] == (cond, val)

    def test_case_without_else_add_when_distinct_types_prepended(
        self, non_uniform_type_when
    ):
        when = non_uniform_type_when
        col = qbcolumns.Case(*when)
        cond = qbcolumns.True_()
        val = qbcolumns.Constant(int, -1)
        col = col.add_when(cond, val, prepend=True)

        assert not col.else_
        assert col.sqltype is float
        assert len(col.when) == len(when) + 1
        assert col.when[0] == (cond, val)

    def test_case_distinct_types_with_else_add_when(self, non_uniform_type_when):
        when = non_uniform_type_when
        else_ = qbcolumns.Constant(float, -1.0)
        col = qbcolumns.Case(*when, else_=else_)
        cond = qbcolumns.True_()
        val = qbcolumns.Constant(str, "foo")
        col = col.add_when(cond, val)

        assert col.else_
        assert col.sqltype is float
        assert len(col.when) == len(when) + 1
        assert col.when[-1] == (cond, val)

    def test_case_distinct_types_prepended_with_else_add_when(
        self, non_uniform_type_when
    ):
        when = non_uniform_type_when
        else_ = qbcolumns.Constant(float, -1.0)
        col = qbcolumns.Case(*when, else_=else_)
        cond = qbcolumns.True_()
        val = qbcolumns.Constant(int, -1)
        col = col.add_when(cond, val, prepend=True)

        assert col.else_
        assert col.sqltype is float
        assert len(col.when) == len(when) + 1
        assert col.when[0] == (cond, val)

    def test_case_without_else_set_else(self, int_when):
        when = int_when
        col = qbcolumns.Case(*when)
        else_ = qbcolumns.Constant(int, -1)
        col = col.set_else(else_)

        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.else_ == else_

    def test_case_with_else_set_else(self, int_when):
        when = int_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when, else_=else_)
        else_ = qbcolumns.Constant(int, -2)
        col = col.set_else(else_)

        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.else_ == else_

    def test_case_distinct_types_without_else_set_else(self, non_uniform_type_when):
        when = non_uniform_type_when
        col = qbcolumns.Case(*when)
        else_ = qbcolumns.Constant(int, -1)
        col = col.set_else(else_)

        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.else_ == else_

    def test_case_distinct_types_with_else_set_else(self, non_uniform_type_when):
        when = non_uniform_type_when
        else_ = qbcolumns.Constant(float, -1.0)
        col = qbcolumns.Case(*when, else_=else_)
        else_ = qbcolumns.Constant(int, -2)
        col = col.set_else(else_)

        assert col.else_
        assert col.sqltype is int
        assert len(col.when) == len(when)
        assert col.else_ == else_

    def test_substitute_column_in_case_depth_0(self, int_when):
        when = int_when
        else_ = qbcolumns.Constant(int, -1)
        col = qbcolumns.Case(*when, else_=else_)
        col_expected = qbcolumns.Constant(int, 42)
        col_substitued = col.substitute({col: col_expected})
        assert col_substitued == col_expected

    def test_substitute_column_in_case_depth_1(self, int_when):
        when = int_when
        colk = qbcolumns.Constant(int, 42)
        colv = qbcolumns.Constant(int, -42)
        T = qbcolumns.True_()
        substitutions = {colk: colv}

        col = qbcolumns.Case(
            (T, colk), *when[:2], (T, colk), (T, colv), *when[2:], (T, colk), else_=colk
        )
        col_expected = qbcolumns.Case(
            (T, colv), *when[:2], (T, colv), (T, colv), *when[2:], (T, colv), else_=colv
        )
        col_substitued = col.substitute(substitutions)

        assert col_expected == col_substitued

    def test_substitute_column_in_case_deeper(self, int_when):
        when = int_when
        colk = qbcolumns.Constant(int, 42)
        colv = qbcolumns.Constant(int, -42)
        T = qbcolumns.True_()
        substitutions = {colk: colv}

        col = qbcolumns.Case(
            (T, colk.isnull()),
            *when[:2],
            (T, colk.eq(colv)),
            (T, colv.gt(colk)),
            *when[2:],
            (T, colk.not_()),
            else_=colk.iff(colk, colv),
        )
        col_expected = qbcolumns.Case(
            (T, colv.isnull()),
            *when[:2],
            (T, colv.eq(colv)),
            (T, colv.gt(colv)),
            *when[2:],
            (T, colv.not_()),
            else_=colv.iff(colv, colv),
        )
        col_substitued = col.substitute(substitutions)

        assert col_expected == col_substitued

    def test_add_when_with_prepend_True(self):
        case = qbcolumns.Case(
            (make_column(True), make_column(1)), (make_column(True), make_column(2))
        )

        case = case.add_when(make_column(False), make_column(0), prepend=True)

        expected_case = qbcolumns.Case(
            (make_column(False), make_column(0)),
            (make_column(True), make_column(1)),
            (make_column(True), make_column(2)),
        )

        assert expected_case == case

    def test_add_when_with_prepend_False(self):
        case = qbcolumns.Case(
            (make_column(False), make_column(0)), (make_column(False), make_column(1))
        )

        case = case.add_when(make_column(True), make_column(2), prepend=False)

        expected_case = qbcolumns.Case(
            (make_column(False), make_column(0)),
            (make_column(False), make_column(1)),
            (make_column(True), make_column(2)),
        )

        assert expected_case == case

    def test_set_else_with_column(self):
        case = qbcolumns.Case(
            (make_column(False), make_column(0)), else_=make_column(41)
        )

        case = case.set_else(make_column(42))

        expected_case = qbcolumns.Case(
            (make_column(False), make_column(0)), else_=make_column(42)
        )

        assert expected_case == case

    def test_set_else_with_None(self):
        case = qbcolumns.Case(
            (make_column(False), make_column(0)), else_=make_column(41)
        )

        case = case.set_else(None)

        expected_case = qbcolumns.Case((make_column(False), make_column(0)))

        assert expected_case == case

    def test_substitute_without_else(self):
        col_1 = make_column(1)
        col_2 = make_column(2)
        col_3 = make_column(3)

        pre_case = qbcolumns.Case((col_1, col_2), (col_2, col_3))

        substitution = {col_1: col_2, col_2: col_3, col_3: col_1}

        result = pre_case.substitute(substitution)

        expected_case = qbcolumns.Case((col_2, col_3), (col_3, col_1))

        assert expected_case == result

    def test_substitute_with_else(self):
        col_1 = make_column(1)
        col_2 = make_column(2)
        col_3 = make_column(3)

        pre_case = qbcolumns.Case((col_1, col_2), (col_2, col_3), else_=col_1)

        substitution = {col_1: col_2, col_2: col_3, col_3: col_1}

        result = pre_case.substitute(substitution)

        expected_case = qbcolumns.Case((col_2, col_3), (col_3, col_1), else_=col_2)

        assert expected_case == result

    def mock_column(self, subtokenize_value):
        column = Mock()
        column.subtokenize = Mock(return_value=subtokenize_value)

        return column

    def test_get_subtokenize_kwargs_with_else(self, mocker):
        case = qbcolumns.Case(
            (self.mock_column("c0"), self.mock_column("c1")),
            (self.mock_column("c2"), self.mock_column("c3")),
            else_=self.mock_column("c4"),
        )

        expected = {"when": (("c0", "c1"), ("c2", "c3")), "else_": "c4"}

        result = case._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs_without_else(self, mocker):
        case = qbcolumns.Case(
            (self.mock_column("c0"), self.mock_column("c1")),
            (self.mock_column("c2"), self.mock_column("c3")),
        )

        expected = {"when": (("c0", "c1"), ("c2", "c3"))}

        result = case._get_subtokenize_kwargs(None)

        assert expected == result


class TestExpression:
    def test_substitute_column_in_expression(self):
        col_1 = make_column(1)
        col_2 = make_column(2)
        col_4 = make_column(4)

        input_expression = col_1 + col_2
        expected_expression = col_4 + col_2

        substitutions = {col_1: col_4}

        result = input_expression.substitute(substitutions)

        assert result == expected_expression


class TestInRelation:
    def test_substitute_relation_and_column(self):
        pre_relation = qb.atoms.relations.Named("foo")
        post_relation = qb.atoms.relations.Named("bar")
        pre_col = qbcolumns.Named(str, "clo")
        post_col = qbcolumns.Named(str, "col")

        in_relation = qbcolumns.InRelation(pre_col, pre_relation)

        substitutions = {pre_relation: post_relation, pre_col: post_col}

        result = in_relation.substitute(substitutions)

        assert result.relation == post_relation
        assert result.subcolumn == post_col


class TestExists:
    def test_substitute_query(self):
        pre_query = qb.queries.dql.Select((make_column(1),))
        post_query = qb.queries.dql.Select((make_column(2),))

        exists = qbcolumns.Exists(pre_query)

        result = exists.substitute({pre_query: post_query})

        assert post_query == result.query
