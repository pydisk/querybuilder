import pytest
import querybuilder.atoms.constraints as qbconstraints
from querybuilder.helpers import make_column


class TestSubstitute:
    @pytest.mark.parametrize(
        "cls",
        [
            qbconstraints.ColumnDefault,
            qbconstraints.ColumnCheck,
            qbconstraints.ColumnGeneratedAlwaysAs,
            qbconstraints.ColumnReferences,
            qbconstraints.TableUnique,
            qbconstraints.TableCheck,
            qbconstraints.TablePrimaryKey,
        ],
    )
    def test_substitute_for_subcolumn_constraints(self, cls):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_constraint = cls(pre_col)

        post_constraint = pre_constraint.substitute({pre_col: post_col})

        assert post_col == post_constraint.subcolumn

    def test_substitute_in_table_foreign_key(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_constraint = qbconstraints.TableForeignKey(pre_col, post_col)
        substitutions = {pre_col: post_col, post_col: pre_col}

        post_constraint = pre_constraint.substitute(substitutions)

        assert post_col == post_constraint.subcolumn
        assert pre_col == post_constraint.refcolumn
