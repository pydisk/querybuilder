import pytest
from mock import Mock

from querybuilder.helpers import table, ColumnSpec, unqualify, make_column
from querybuilder.tests.utils import create_subtokenizable_mock
import querybuilder as qb


class TestRelation:
    @pytest.fixture
    def tables(self, person_table):
        L = [person_table]

        @L.append
        @table(schema_name="sch")
        class Person:
            id: int
            name: str
            birth_year: int

        @L.append
        @table
        class Person2:
            id: int
            name: str
            birth_year: int

        @L.append
        @table
        class Person:  # noqa: F811
            id: int
            lastname: str
            birth_year: int

        @L.append
        @table
        class Person:  # noqa: F811
            id: int
            name: str
            birth_year: str

        @L.append
        @table
        class Person:  # noqa: F811
            id: int
            name: str
            surname: str
            birth_year: int

        @L.append
        @table(name="Person3")
        class Person:  # noqa: F811
            id: int
            name: str
            birth_year: str

        @L.append
        @table
        class Person:  # noqa: F811
            id: int
            birth_year: int

        @L.append
        @table
        class Person:  # noqa: F811
            id: ColumnSpec(int, primary_key=True)
            name: str
            birth_year: int

        return L

    def test_insert_values_with_default_args(self, person_table):
        result = person_table.insert_values()

        expected_subquery = None
        expected_in_columns = ()

        assert expected_subquery == result.query
        assert expected_in_columns == result.in_columns

    def test_insert_values_with_in_columns(self, person_table):
        in_columns = person_table.c[1:]
        result = person_table.insert_values(in_columns)

        expected_subquery = None
        expected_in_columns = in_columns

        assert expected_subquery == result.query
        assert expected_in_columns == result.in_columns

    def test_insert_values_with_proper_types(self, person_table):
        values = [("foo", 42), ("bar", 1337)]
        in_columns = person_table.c[1:]

        result = person_table.insert_values(in_columns, values=values)

        expected_in_columns = in_columns
        expected_values = [
            (qb.atoms.columns.Value(str, "foo"), qb.atoms.columns.Constant(int, 42)),
            (qb.atoms.columns.Value(str, "bar"), qb.atoms.columns.Constant(int, 1337)),
        ]

        assert expected_in_columns == result.in_columns
        for i, tup in enumerate(result.query.values):
            for j, col in enumerate(tup):
                assert expected_values[i][j] == col

    def test_insert_values_with_different_type_from_table_column(self, person_table):
        values = [("foo", 42), (1234, 1337)]
        in_columns = person_table.c[1:]

        result = person_table.insert_values(in_columns, values=values)

        expected_in_columns = in_columns
        expected_values = [
            (qb.atoms.columns.Value(str, "foo"), qb.atoms.columns.Constant(int, 42)),
            (qb.atoms.columns.Value(str, 1234), qb.atoms.columns.Constant(int, 1337)),
        ]

        assert expected_in_columns == result.in_columns
        for i, tup in enumerate(result.query.values):
            for j, col in enumerate(tup):
                assert expected_values[i][j] == col

    def test_insert_many_without_keys(self, person_table):
        result = person_table.insert_many(person_table.c[1:])

        expected_in_columns = person_table.c[1:]
        expected_values = [
            (
                qb.atoms.columns.Placeholder(str, "name"),
                qb.atoms.columns.Placeholder(int, "age"),
            ),
        ]

        assert expected_in_columns == result.in_columns
        for i, tup in enumerate(result.query.values):
            for j, col in enumerate(tup):
                assert expected_values[i][j] == col

    def test_insert_many_with_key(self, person_table):
        result = person_table.insert_many(person_table.c[1:], {1: "k_age"})

        expected_in_columns = person_table.c[1:]
        expected_values = [
            (
                qb.atoms.columns.Placeholder(str, "name"),
                qb.atoms.columns.Placeholder(int, "k_age"),
            ),
        ]

        assert expected_in_columns == result.in_columns
        for i, tup in enumerate(result.query.values):
            for j, col in enumerate(tup):
                assert expected_values[i][j] == col

    @pytest.mark.parametrize(
        "left, right",
        [
            (lambda rel: rel.select(), lambda rel: rel.select()),
            (
                lambda rel: rel.select(where=rel.c[0].eq(0)),
                lambda rel: rel.select(where=rel.c[0].eq(0)),
            ),
            (
                lambda rel: rel.select().add_where(rel.c[0].eq(0)),
                lambda rel: rel.select(where=rel.c[0].eq(0)),
            ),
            (
                lambda rel: rel.select().add_where(rel.c[0].eq(0)),
                lambda rel: rel.select().add_where(rel.c[0].eq(0)),
            ),
            (lambda rel: rel, lambda rel: rel),
            (lambda rel: rel.insert(), lambda rel: rel.insert()),
        ],
    )
    def test_equality(self, tables, left, right):
        for t in tables:
            assert left(t) == right(t)
            assert right(t) == left(t)

    @pytest.mark.parametrize(
        "left, right",
        [
            (lambda rel: rel.select(), lambda rel: rel),
            (lambda rel: rel.select(columns=rel.c[1:]), lambda rel: rel.select()),
            (lambda rel: rel.select(aliases={1: "foo"}), lambda rel: rel.select()),
        ],
    )
    def test_inequality(self, tables, left, right):
        for t in tables:
            assert left(t) != right(t)
            assert right(t) != left(t)

    def test_inequality_table(self, tables):
        for t0 in tables:
            for t1 in tables:
                if t0 is t1:
                    continue
                assert t0 != t1

    def test_update_with_dict(self, person_table):
        query = person_table.update(
            {person_table.c[1]: "foo", "age": qb.atoms.columns.Constant(int, 8)}
        )

        expected_set_columns = {
            person_table.c[1]: qb.atoms.columns.Value(str, "foo"),
            person_table.c[2]: qb.atoms.columns.Constant(int, 8),
        }

        assert expected_set_columns == query.set_columns

    def test_update_with_kwargs(self, person_table):
        query = person_table.update(name="foo", age=qb.atoms.columns.Constant(int, 8))

        expected_set_columns = {
            person_table.c[1]: qb.atoms.columns.Value(str, "foo"),
            person_table.c[2]: qb.atoms.columns.Constant(int, 8),
        }

        assert expected_set_columns == query.set_columns

    def test_update_with_dict_and_kwargs(self, person_table):
        query = person_table.update(
            {person_table.c[1]: "foo", "age": qb.atoms.columns.Constant(int, 8)}, id=42
        )

        expected_set_columns = {
            person_table.c[0]: qb.atoms.columns.Constant(int, 42),
            person_table.c[1]: qb.atoms.columns.Value(str, "foo"),
            person_table.c[2]: qb.atoms.columns.Constant(int, 8),
        }

        assert expected_set_columns == query.set_columns

    def test_update_with_dict_and_kwargs_with_collisions(self, person_table):
        with pytest.raises(KeyError):
            person_table.update(
                {person_table.c[1]: "foo", "age": qb.atoms.columns.Constant(int, 8)},
                age=42,
            )

    def test_update_proper_types(self, person_table):
        query = person_table.update(
            {person_table.c[1]: 1, "age": qb.atoms.columns.Constant(int, 8)}, id="42"
        )

        expected_set_columns = {
            person_table.c[0]: qb.atoms.columns.Constant(int, "42"),
            person_table.c[1]: qb.atoms.columns.Value(str, 1),
            person_table.c[2]: qb.atoms.columns.Constant(int, 8),
        }

        assert expected_set_columns == query.set_columns


class TestNamed:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qb.atoms.relations.Named("foo", columns=(pre_col,))
        post_rel = pre_rel.substitute({pre_col: post_col})

        assert tuple(post_rel.columns) == (post_col,)

    def test_star(self):
        rel = qb.atoms.relations.Named("foo")

        star = rel.star()

        assert isinstance(star, qb.atoms.pseudo_columns.Star)
        assert rel == star.relation


class TestAliased:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_subrel = qb.atoms.relations.Named("sub_foo", columns=(pre_col,))
        post_subrel = qb.atoms.relations.Named("sub_bar", columns=(pre_col,))

        pre_rel = qb.atoms.relations.Aliased(pre_subrel, "foo")
        post_rel = pre_rel.substitute({pre_col: post_col, pre_subrel: post_subrel})

        assert post_subrel == post_rel.subrelation

    def test_columns_without_aliases(self):
        col_1 = make_column(1)
        col_2 = make_column(2)

        columns = (col_1, col_2)

        subrel = qb.atoms.relations.Named("foo", columns=columns)

        rel = qb.atoms.relations.Aliased(subrel, "bar")

        assert columns == rel.columns

    @pytest.mark.parametrize("aliases", [{0: "a", 2: "c"}, ((2, "c"), (0, "a"))])
    def test_columns_aliases(self, aliases):
        columns = (
            qb.atoms.columns.Named(int, "x"),
            qb.atoms.columns.Named(int, "b"),
            make_column(42),
        )

        subrel = qb.atoms.relations.Named("foo", columns=columns)

        rel = qb.atoms.relations.Aliased(subrel, "bar", aliases)

        expected_columns = (
            qb.atoms.columns.Named(int, "a"),
            qb.atoms.columns.Named(int, "b", relation_name="bar"),
            qb.atoms.columns.Named(int, "c"),
        )

        assert expected_columns == rel.columns

    def test_unnamed_columns_must_be_aliased(self):
        columns = (
            qb.atoms.columns.Named(int, "x"),
            qb.atoms.columns.Named(int, "b"),
            make_column(42),
        )

        subrel = qb.atoms.relations.Named("foo", columns=columns)

        aliases = {0: "a"}

        with pytest.raises(ValueError):
            qb.atoms.relations.Aliased(subrel, "bar", aliases)

    def test_get_subtokenize_kwargs_with_aliases(self, mocker):
        subrelation_tok = "subrelation foo"
        name_tok = "bar"
        column_tok = "column"

        name = "foo"

        subrelation = Mock()
        subrelation.subtokenize = Mock(return_value=subrelation_tok)
        subrelation.columns = (
            qb.atoms.columns.Named(int, "col_0"),
            qb.atoms.columns.Named(int, "col_1"),
        )

        tokenizer = Mock()
        tokenizer.tokenize_name = Mock(return_value=name_tok)

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column_tok
        )

        rel = qb.atoms.relations.Aliased(
            subrelation,
            name,
            column_aliases={1: "alias"},
        )

        expected = {
            "subrelation": subrelation_tok,
            "name": name_tok,
            "columns": (column_tok, column_tok),
        }

        result = rel._get_subtokenize_kwargs(tokenizer)

        assert expected == result
        tokenizer.tokenize_name.assert_called_once_with(
            schema_name=None, relation_name=name
        )
        subrelation.subtokenize.assert_called_once_with(tokenizer, scoped=True)

        AC = qb.atoms.clauses.AliasedColumn
        for c in rel.columns:
            AC.assert_any_call(unqualify(c))

    def test_get_subtokenize_kwargs_without_aliases(self, mocker):
        subrelation_tok = "subrelation foo"
        name_tok = "bar"

        name = "foo"

        subrelation = Mock()
        subrelation.subtokenize = Mock(return_value=subrelation_tok)
        subrelation.columns = (
            qb.atoms.columns.Named(int, "col_0"),
            qb.atoms.columns.Named(int, "col_1"),
        )

        tokenizer = Mock()
        tokenizer.tokenize_name = Mock(return_value=name_tok)

        rel = qb.atoms.relations.Aliased(
            subrelation,
            name,
        )

        expected = {"subrelation": subrelation_tok, "name": name_tok, "columns": ()}

        result = rel._get_subtokenize_kwargs(tokenizer)

        assert expected == result
        tokenizer.tokenize_name.assert_called_once_with(
            schema_name=None, relation_name=name
        )
        subrelation.subtokenize.assert_called_once_with(tokenizer, scoped=True)


class TestWith:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_subrel = qb.atoms.relations.Named("sub_foo", columns=(pre_col,))
        post_subrel = qb.atoms.relations.Named("sub_bar", columns=(pre_col,))

        pre_rel = qb.atoms.relations.With(pre_subrel, "foo")
        post_rel = pre_rel.substitute({pre_col: post_col, pre_subrel: post_subrel})

        assert post_subrel == post_rel.subrelation

    def test_recursive(self):
        NamedColumn = qb.atoms.columns.Named
        columns = [
            NamedColumn(int, 3),
            NamedColumn(str, "foo"),
            NamedColumn(bool, True),
        ]
        base = qb.queries.dql.Select(columns)
        recnamed = qb.atoms.relations.RecursiveNamed("recrel", columns=columns)
        recquery = base.union(
            recnamed.select(
                [
                    recnamed.columns[0].add(1),
                    recnamed.columns[1],
                    NamedColumn(bool, False),
                ]
            )
        )
        recwithrel = recquery.as_with("bar")
        assert recwithrel.recursive


class TestCartesianProduct:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_subrel = qb.atoms.relations.Named("sub_foo", columns=(pre_col,))
        post_subrel = qb.atoms.relations.Named("sub_bar", columns=(pre_col,))
        other_subrel = qb.atoms.relations.Named("other", columns=(post_col,))

        pre_rel = qb.atoms.relations.CartesianProduct((pre_subrel, other_subrel))
        post_rel = pre_rel.substitute({pre_col: post_col, pre_subrel: post_subrel})

        expected_subrelations = (post_subrel, other_subrel)
        assert expected_subrelations == tuple(post_rel.subrelations)


class TestJoin:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_subrel = qb.atoms.relations.Named("sub_foo", columns=(pre_col,))
        post_subrel = qb.atoms.relations.Named("sub_bar", columns=(pre_col,))
        other_subrel = qb.atoms.relations.Named("other", columns=(post_col,))

        pre_rel = qb.atoms.relations.Join(
            "INNER", pre_subrel, other_subrel, on=pre_col.eq(post_col)
        )
        post_rel = pre_rel.substitute({pre_col: post_col, pre_subrel: post_subrel})

        expected_on = post_col.eq(post_col)
        assert expected_on == post_rel.on
        assert post_subrel == post_rel.left

    def test_substitute_without_on(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_subrel = qb.atoms.relations.Named("sub_foo", columns=(pre_col,))
        post_subrel = qb.atoms.relations.Named("sub_bar", columns=(pre_col,))
        other_subrel = qb.atoms.relations.Named("other", columns=(post_col,))

        pre_rel = qb.atoms.relations.Join("INNER", pre_subrel, other_subrel)
        post_rel = pre_rel.substitute({pre_col: post_col, pre_subrel: post_subrel})

        assert post_subrel == post_rel.left


class TestView:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = qb.queries.dql.Select((pre_col, pre_col))
        post_query = qb.queries.dql.Select((pre_col, post_col))

        pre_view = qb.atoms.relations.View("foo", pre_query)
        post_view = pre_view.substitute({pre_col: post_col, pre_query: post_query})

        assert post_query == post_view.defquery


class TestTable:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_constraint = qb.atoms.constraints.ColumnNotNull()
        post_constraint = qb.atoms.constraints.ColumnUnique()

        pre_table = qb.atoms.relations.Table(
            "foo", (post_col, pre_col), constraints=(pre_constraint,)
        )

        subs = {pre_col: post_col, pre_constraint: post_constraint}
        post_table = pre_table.substitute(subs)

        expected_columns = (post_col, post_col)
        assert expected_columns == post_table.columns
        assert (post_constraint,) == tuple(post_table.constraints)

    def test_substitute_without_constraints(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_table = qb.atoms.relations.Table("foo", (post_col, pre_col))

        subs = {pre_col: post_col}
        post_table = pre_table.substitute(subs)

        expected_columns = (post_col, post_col)
        assert expected_columns == post_table.columns
