import pytest
from mock import Mock
from querybuilder.atoms.schemas import Index, IndexedColumn, Schema
from querybuilder.helpers import make_column
from querybuilder.tests.utils import create_subtokenizable_mock
import querybuilder.atoms.relations as qbrelations
import querybuilder.atoms.columns as qbcolumns


class TestIndexedColumn:
    def test_subtokenize_kwargs(self, mocker):
        column = "column"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )

        idxcol = IndexedColumn(
            qbcolumns.Named(int, "col"),
        )

        expected = dict(column=column)

        result = idxcol._get_subtokenize_kwargs(None)

        assert expected == result

    def test_subtokenize_kwargs_with_asc(self, mocker):
        column = "column"
        asc = "ASC"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )
        create_subtokenizable_mock(mocker, "querybuilder.atoms.clauses.AscWrapper", asc)

        idxcol = IndexedColumn(qbcolumns.Named(int, "col"), asc=True)

        expected = dict(column=column, asc=asc)

        result = idxcol._get_subtokenize_kwargs(None)

        assert expected == result

    def test_subtokenize_kwargs_with_asc_False(self, mocker):
        column = "column"
        asc = "DESC"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )
        create_subtokenizable_mock(mocker, "querybuilder.atoms.clauses.AscWrapper", asc)

        idxcol = IndexedColumn(qbcolumns.Named(int, "col"), asc=False)

        expected = dict(column=column, asc=asc)

        result = idxcol._get_subtokenize_kwargs(None)

        assert expected == result

    def test_subtokenize_kwargs_with_collation(self, mocker):
        column = "column"
        collation = "'collation'"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.CollationWrapper", collation
        )

        idxcol = IndexedColumn(qbcolumns.Named(int, "col"), collation="foo")

        expected = dict(column=column, collation=collation)

        result = idxcol._get_subtokenize_kwargs(None)

        assert expected == result

    def test_subtokenize_kwargs_with_asc_and_collation(self, mocker):
        column = "column"
        collation = "'collation'"
        asc = "ASC"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.CollationWrapper", collation
        )
        create_subtokenizable_mock(mocker, "querybuilder.atoms.clauses.AscWrapper", asc)

        idxcol = IndexedColumn(
            qbcolumns.Named(int, "col"), collation="collation", asc=True
        )

        expected = dict(column=column, collation=collation, asc=asc)

        result = idxcol._get_subtokenize_kwargs(None)

        assert expected == result

    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_indexed = IndexedColumn(pre_col)

        subs = {pre_col: post_col}
        post_indexed = pre_indexed.substitute(subs)

        assert post_col == post_indexed.column


class TestIndex:
    @pytest.fixture
    def mock_indexed_column_subtoken(self):
        return "indexed col"

    @pytest.fixture
    def mock_indexed_column(self, mocker, mock_indexed_column_subtoken):
        idx_col = Mock()
        subtokenize = Mock(return_value=mock_indexed_column_subtoken)
        idx_col.attach_mock(subtokenize, "subtokenize")
        return idx_col

    @pytest.fixture
    def mock_column(self, mocker, mock_indexed_column):
        col = Mock()

        index_fct = Mock(return_value=mock_indexed_column)

        col.attach_mock(index_fct, "index")

        return col

    def test_creation_kwargs_with_unique_and_where(
        self, mocker, mock_column, mock_indexed_column, mock_indexed_column_subtoken
    ):
        schema_relation = "relation"
        unique = "unique"
        where = "where ..."

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", schema_relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.UniqueClause", unique
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.WhereColumn", where
        )

        col = mock_column

        index = Index(
            "index",
            qbrelations.Named("rel"),
            (col,),
            unique=True,
            where=col.eq(2),
        )

        expected_columns = (mock_indexed_column_subtoken,)
        expected = dict(schema_relation=schema_relation, unique=unique, where=where)

        result = index._get_creation_kwargs(None)

        actual_columns = tuple(result.pop("columns"))

        assert expected_columns == actual_columns
        assert expected == result
        mock_column.index.assert_called_once()
        mock_indexed_column.subtokenize.assert_called_once()

    def test_creation_kwargs_with_unique(
        self, mocker, mock_column, mock_indexed_column, mock_indexed_column_subtoken
    ):
        schema_relation = "relation"
        unique = "unique"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", schema_relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.UniqueClause", unique
        )

        col = mock_column

        index = Index(
            "index",
            qbrelations.Named("rel"),
            (col,),
            unique=True,
        )

        expected_columns = (mock_indexed_column_subtoken,)
        expected = dict(schema_relation=schema_relation, unique=unique)

        result = index._get_creation_kwargs(None)

        actual_columns = tuple(result.pop("columns"))

        assert expected_columns == actual_columns
        assert expected == result
        mock_column.index.assert_called_once()
        mock_indexed_column.subtokenize.assert_called_once()

    def test_creation_kwargs_with_where(
        self, mocker, mock_column, mock_indexed_column, mock_indexed_column_subtoken
    ):
        schema_relation = "relation"
        where = "where ..."

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", schema_relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.WhereColumn", where
        )

        col = mock_column

        index = Index("index", qbrelations.Named("rel"), (col,), where=col.eq(2))

        expected_columns = (mock_indexed_column_subtoken,)
        expected = dict(schema_relation=schema_relation, where=where)

        result = index._get_creation_kwargs(None)

        actual_columns = tuple(result.pop("columns"))

        assert expected_columns == actual_columns
        assert expected == result
        mock_column.index.assert_called_once()
        mock_indexed_column.subtokenize.assert_called_once()

    def test_creation_kwargs(
        self, mocker, mock_column, mock_indexed_column, mock_indexed_column_subtoken
    ):
        schema_relation = "relation"

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", schema_relation
        )

        col = mock_column

        index = Index(
            "index",
            qbrelations.Named("rel"),
            (col,),
        )

        expected_columns = (mock_indexed_column_subtoken,)
        expected = dict(schema_relation=schema_relation)

        result = index._get_creation_kwargs(None)

        actual_columns = tuple(result.pop("columns"))

        assert expected_columns == actual_columns
        assert expected == result
        mock_column.index.assert_called_once()
        mock_indexed_column.subtokenize.assert_called_once()

    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_index = Index("index", pre_rel, (pre_col, post_col), where=pre_col)

        subs = {pre_col: post_col, pre_rel: post_rel}
        post_index = pre_index.substitute(subs)

        assert (post_col, post_col) == tuple(post_index.columns)
        assert post_rel == post_index.schema_relation
        assert post_col == post_index.where

    def test_substitute_without_where(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_index = Index("index", pre_rel, (pre_col, post_col))

        subs = {pre_col: post_col, pre_rel: post_rel}
        post_index = pre_index.substitute(subs)

        assert (post_col, post_col) == tuple(post_index.columns)
        assert post_rel == post_index.schema_relation


class TestSchema:
    def test_substitute(self):
        name = "schema"

        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo", schema_name=name)
        post_rel = qbrelations.Named("bar", schema_name=name)

        pre_index = Index("index", pre_rel, (pre_col, post_col), schema_name=name)
        post_index = Index("index", pre_rel, (pre_col, post_col), schema_name=name)

        pre_schema = Schema(
            name,
            (
                pre_rel,
                pre_index,
            ),
        )

        subs = {pre_col: post_col, pre_index: post_index, pre_rel: post_rel}
        post_schema = pre_schema.substitute(subs)

        assert (post_rel, post_index) == tuple(post_schema.objects)
