import querybuilder.atoms.clauses as qbclauses
import querybuilder.atoms.columns as qbcolumns
import querybuilder as qb


class TestSetColumnsClause:
    def test_set_columns_are_unqualified(self):
        val_col1 = qbcolumns.Constant(int, 42)
        val_col2 = qbcolumns.Value(str, "foo")
        name1 = "age"
        name2 = "name"

        set_columns = {
            qbcolumns.Named(
                int, name1, relation_name="Person", schema_name="Public"
            ): val_col1,
            qbcolumns.Named(str, name2, relation_name="Person"): val_col2,
        }

        clause = qbclauses.SetColumnsClause(set_columns)

        expected_set_columns = {
            qbcolumns.Named(int, name1): val_col1,
            qbcolumns.Named(str, name2): val_col2,
        }

        assert expected_set_columns == clause.set_columns


class TestDBObjectNameWrapper:
    def test_get_subtokenize_kwargs(self):
        index = qb.atoms.schemas.Schema("foo")
        wrapper = qbclauses.DBObjectNameWrapper(index)

        result = wrapper._get_subtokenize_kwargs(None)
        expected = dict(dbobj=index)

        assert expected == result


class TestCollationWrapper:
    def test_subtokenize_kwargs(self):
        string = "foo"
        str_wrapper = qbclauses.CollationWrapper(string)

        expected = dict(string=string)

        result = str_wrapper._get_subtokenize_kwargs(None)

        assert expected == result


class TestAscWrapper:
    def test_subtokenize_kwargs(self):
        asc = True
        str_wrapper = qbclauses.AscWrapper(asc)

        expected = dict(asc=asc)

        result = str_wrapper._get_subtokenize_kwargs(None)

        assert expected == result
