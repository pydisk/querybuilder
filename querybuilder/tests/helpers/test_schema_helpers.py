# type: ignore

from datetime import date
from querybuilder.helpers import table, schema, ColumnSpec
from querybuilder.formatting.formatter import Formatter
import querybuilder as qb


class TestHelper:
    tokenizer = qb.drivers.sql.tokenizer.Tokenizer()
    formatter = Formatter()

    @classmethod
    def _check(cls, obj: qb.atoms.atoms.Atom, expected: str):
        tokens = obj.tokenize(cls.tokenizer)
        formatted = cls.formatter.get_formatted(tokens)
        assert formatted == expected


class TestColumnSpec(TestHelper):
    def test_column_spec_type(self):
        c = ColumnSpec(
            int, name="bar", relation_name="foo", schema_name="public", primary_key=True
        )
        col = c.to_table_column()
        assert type(col) == qb.atoms.columns.TableColumn
        assert col.name == "bar"
        assert col.relation_name == "foo"
        assert col.schema_name == "public"
        assert col.sqltype is int
        assert len(col.constraints) == 1
        self._check(col, "public.foo.bar")


class TestTableHelper(TestHelper):
    def test_table_decorator_simple(self):
        @table
        class mytable:
            id: int
            firstname: str
            lastname: str
            birthday: date

        assert type(mytable) == qb.atoms.relations.Table
        assert mytable.name == "mytable"
        assert mytable.schema_name is None
        assert len(mytable.columns) == 4
        assert all(isinstance(c, qb.atoms.columns.TableColumn) for c in mytable.columns)
        assert all(c.relation_name == "mytable" for c in mytable.columns)
        assert all(c.schema_name is None for c in mytable.columns)
        assert not mytable.constraints

        self._check(mytable, "mytable")
        self._check(
            mytable.create(),
            "CREATE TABLE mytable (id INTEGER, firstname TEXT, lastname TEXT, birthday DATE)",  # noqa: E501
        )
        self._check(
            mytable.select(),
            "SELECT mytable.id, mytable.firstname, mytable.lastname, mytable.birthday FROM mytable",  # noqa: E501
        )

    def test_table_decorator_with_schema_name(self):
        @table(schema_name="main")
        class mytable:
            id: int
            firstname: str
            lastname: str
            birthday: date

        assert type(mytable) == qb.atoms.relations.Table
        assert mytable.name == "mytable"
        assert mytable.schema_name == "main"
        assert len(mytable.columns) == 4
        assert all(isinstance(c, qb.atoms.columns.TableColumn) for c in mytable.columns)
        assert all(c.relation_name == "mytable" for c in mytable.columns)
        assert all(c.schema_name == "main" for c in mytable.columns)
        assert not mytable.constraints
        self._check(mytable, "main.mytable")
        self._check(
            mytable.create(),
            "CREATE TABLE main.mytable (id INTEGER, firstname TEXT, lastname TEXT, birthday DATE)",  # noqa: E501
        )
        self._check(
            mytable.select(),
            "SELECT main.mytable.id, main.mytable.firstname, main.mytable.lastname, main.mytable.birthday FROM main.mytable",  # noqa: E501
        )

    def test_table_decorator_with_alternative_name(self):
        @table(name="break")
        class mytable:
            id: int
            firstname: str
            lastname: str
            birthday: date

        assert type(mytable) == qb.atoms.relations.Table
        assert mytable.name == "break"
        assert mytable.schema_name is None
        assert len(mytable.columns) == 4
        assert all(isinstance(c, qb.atoms.columns.TableColumn) for c in mytable.columns)
        assert all(c.relation_name == "break" for c in mytable.columns)
        assert all(c.schema_name is None for c in mytable.columns)
        assert not mytable.constraints
        self._check(mytable, "break")
        self._check(
            mytable.create(),
            "CREATE TABLE break (id INTEGER, firstname TEXT, lastname TEXT, birthday DATE)",  # noqa: E501
        )
        self._check(
            mytable.select(),
            "SELECT break.id, break.firstname, break.lastname, break.birthday FROM break",  # noqa: E501
        )

    def test_table_decorator_with_both_alternative_name_and_schema_name(self):
        @table(schema_name="main", name="break")
        class mytable:
            id: int
            firstname: str
            lastname: str
            birthday: date

        assert type(mytable) == qb.atoms.relations.Table
        assert mytable.name == "break"
        assert mytable.schema_name == "main"
        assert len(mytable.columns) == 4
        assert all(isinstance(c, qb.atoms.columns.TableColumn) for c in mytable.columns)
        assert all(c.relation_name == "break" for c in mytable.columns)
        assert all(c.schema_name == "main" for c in mytable.columns)
        assert not mytable.constraints
        self._check(mytable, "main.break")
        self._check(
            mytable.create(),
            "CREATE TABLE main.break (id INTEGER, firstname TEXT, lastname TEXT, birthday DATE)",  # noqa: E501
        )
        self._check(
            mytable.select(),
            "SELECT main.break.id, main.break.firstname, main.break.lastname, main.break.birthday FROM main.break",  # noqa: E501
        )

    def test_table_decorator_with_rich_column_specifications(self):
        @table
        class mytable:
            id: ColumnSpec(int, primary_key=True, generated_as_identity=True)
            firstname: ColumnSpec(str, not_null=True)
            lastname: str
            birthday: ColumnSpec(date, not_null=True)
            is_dead: ColumnSpec(bool, not_null=True, default=False)

        self._check(mytable, "mytable")
        self._check(
            mytable.create(),
            "CREATE TABLE mytable ("
            "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
            "firstname TEXT NOT NULL, "
            "lastname TEXT, "
            "birthday DATE NOT NULL, "
            "is_dead BOOLEAN NOT NULL DEFAULT False"
            ")",
        )
        self._check(
            mytable.select(),
            "SELECT mytable.id, mytable.firstname, mytable.lastname, mytable.birthday, mytable.is_dead FROM mytable",  # noqa: E501
        )

    def test_table_decorator_with_rich_column_specifications_and_names(self):
        @table(name="t", schema_name="main")
        class mytable:
            id: ColumnSpec(int, primary_key=True, generated_as_identity=True)
            firstname: ColumnSpec(str, not_null=True)
            lastname: str
            birthday: ColumnSpec(date, not_null=True)
            is_dead: ColumnSpec(bool, not_null=True, default=False)

        self._check(mytable, "main.t")
        self._check(
            mytable.create(),
            "CREATE TABLE main.t ("
            "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
            "firstname TEXT NOT NULL, "
            "lastname TEXT, "
            "birthday DATE NOT NULL, "
            "is_dead BOOLEAN NOT NULL DEFAULT False"
            ")",
        )
        self._check(
            mytable.select(),
            "SELECT main.t.id, main.t.firstname, main.t.lastname, main.t.birthday, main.t.is_dead FROM main.t",  # noqa: E501
        )

    def test_table_decorator_with_table_constraints(self):
        @table
        class edges:
            id: int
            source: int
            target: int

            constraints = table.collector()

            @constraints.primary_key
            def _(tbl):
                return tbl.columns.id

            @constraints.foreign_key(order="A")
            def _(tbl, sch=None):
                return (tbl.columns.source, "nodes.id")

            @constraints.foreign_key(order="B")
            def _(tbl, sch=None):
                return (tbl.columns.target, "nodes.id")

            @constraints.unique(order="C")
            def _(tbl):
                return dict(
                    name="edge_uniqueness",
                    column=tbl.columns.source.tuple_with(tbl.columns.target),
                )

        self._check(edges, "edges")
        # TODO: check that qualified named in table constraints are expected in SQL
        #       (tokenizer fix?)
        self._check(
            edges.create(),
            "CREATE TABLE edges ("
            "id INTEGER, source INTEGER, target INTEGER, "
            "PRIMARY KEY id, "
            "FOREIGN KEY source REFERENCES nodes(id), "
            "FOREIGN KEY target REFERENCES nodes(id), "
            "CONSTRAINT edge_uniqueness UNIQUE (source, target))",
        )
        self._check(
            edges.select(), "SELECT edges.id, edges.source, edges.target FROM edges"
        )


class TestSchemaHelper(TestHelper):
    def test_schema_decorator_simple(self):
        @schema
        class main:
            class players:
                id: int
                firstname: str
                lastname: str
                birthday: date

            class teams:
                name: str
                leader: int
                active: bool

            class team_members:
                team: str
                player: int
                since: date

        assert type(main) == qb.atoms.schemas.Schema
        assert len(main.objects) == 3
        self._check(main, "main")

        # test main.players
        self._check(main.relations.players, "main.players")
        self._check(
            main.relations.players.create(),
            "CREATE TABLE main.players (id INTEGER, firstname TEXT, lastname TEXT, birthday DATE)",  # noqa: E501
        )
        self._check(
            main.relations.players.select(),
            "SELECT main.players.id, main.players.firstname, main.players.lastname, main.players.birthday FROM main.players",  # noqa: E501
        )

        # test main.teams
        self._check(main.relations.teams, "main.teams")
        self._check(
            main.relations.teams.create(),
            "CREATE TABLE main.teams (name TEXT, leader INTEGER, active BOOLEAN)",
        )
        self._check(
            main.relations.teams.select(),
            "SELECT main.teams.name, main.teams.leader, main.teams.active FROM main.teams",  # noqa: E501
        )

        # test main.team_members
        self._check(main.relations.team_members, "main.team_members")
        self._check(
            main.relations.team_members.create(),
            "CREATE TABLE main.team_members (team TEXT, player INTEGER, since DATE)",
        )
        self._check(
            main.relations.team_members.select(),
            "SELECT main.team_members.team, main.team_members.player, main.team_members.since FROM main.team_members",  # noqa: E501
        )

    def test_schema_passed_to_inner_table_decorator(self):
        @schema
        class main:
            class t:
                id: int

            class s:
                fid: "t.id"  # noqa: F821

        assert len(main.relations.s.columns.fid.constraints) == 1
        assert isinstance(
            main.relations.s.columns.fid.constraints[0],
            qb.atoms.constraints.ColumnReferences,
        )
        assert str(main.relations.t.columns.id) == str(
            main.relations.s.columns.fid.constraints[0].refcolumn
        )
        assert (
            main.relations.t.columns.id.sqltype
            == main.relations.s.columns.fid.constraints[0].refcolumn.sqltype
        )

    def test_schema_decorator_complex(self):
        @schema
        class main:
            class players:
                id: ColumnSpec(int, primary_key=True, generated_as_identity=True)
                firstname: ColumnSpec(str, not_null=True)
                lastname: str
                birthday: date

            class teams:
                name: ColumnSpec(str, primary_key=True)
                leader: ColumnSpec(
                    int, references="main.players.id", not_null=True, unique=True
                )
                active: ColumnSpec(bool, not_null=True, default=False)

            class team_members:
                team: ColumnSpec(str, references="main.teams.name", not_null=True)
                player: ColumnSpec(int, references="main.players.id", not_null=True)
                since: ColumnSpec(date, not_null=True)
                until: date
                current: ColumnSpec(bool, not_null=True)

                with table.collector() as coll:

                    @coll.unique
                    def _(tbl):
                        return tbl.columns.team.tuple_with(tbl.columns.player)

                    @coll.check
                    def _(tbl):
                        return tbl.columns.since.leq(tbl.columns.until)

                    @coll.check
                    def _(tbl):
                        return tbl.columns.current.not_().or_(
                            tbl.columns.until.isnotnull()
                        )

            @schema.view(order=0)
            def team_players(sch):
                return sch.relations.players.left_join(
                    sch.relations.team_members,
                    on=sch.relations.players.columns.id.eq(
                        sch.relations.team_members.columns.player
                    ).and_(sch.relations.team_members.columns.current),
                ).select(columns=lambda f: f.columns[:5])

            @schema.view(order=1)
            def teammates(sch):
                p1 = sch.relations.team_players.alias("p1")
                p2 = sch.relations.team_players.alias("p2")
                j = p1.inner_join(
                    p2,
                    on=p1.columns.team.eq(p2.columns.team).and_(
                        p1.columns.id.neq(p2.columns.id)
                    ),
                )
                return j.select(
                    columns=(*j.columns[:3], *j.columns[5:8], j.columns[4]),
                    aliases=dict(
                        enumerate(("id1", "fn1", "ln1", "id2", "fn2", "ln2", "team"))
                    ),
                )

            # TODO: test index

        assert type(main) == qb.atoms.schemas.Schema
        assert len(main.objects) == 5
        assert len(main.relations) == 5
        self._check(main, "main")

        # test main.players
        self._check(main.relations.players, "main.players")
        self._check(
            main.relations.players.create(),
            "CREATE TABLE main.players ("
            "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, "
            "firstname TEXT NOT NULL, "
            "lastname TEXT, "
            "birthday DATE"
            ")",
        )
        self._check(
            main.relations.players.select(),
            "SELECT main.players.id, main.players.firstname, main.players.lastname, main.players.birthday FROM main.players",  # noqa: E501
        )

        # test main.teams
        self._check(main.relations.teams, "main.teams")
        self._check(
            main.relations.teams.create(),
            "CREATE TABLE main.teams ("
            "name TEXT PRIMARY KEY, "
            "leader INTEGER REFERENCES main.players(id) UNIQUE NOT NULL, "
            "active BOOLEAN NOT NULL DEFAULT False"
            ")",
        )
        self._check(
            main.relations.teams.select(),
            "SELECT main.teams.name, main.teams.leader, main.teams.active FROM main.teams",  # noqa: E501
        )

        # test main.team_members
        self._check(main.relations.team_members, "main.team_members")
        self._check(
            main.relations.team_members.create(),
            "CREATE TABLE main.team_members ("
            "team TEXT REFERENCES main.teams(name) NOT NULL, "
            "player INTEGER REFERENCES main.players(id) NOT NULL, "
            "since DATE NOT NULL, "
            "until DATE, "
            "current BOOLEAN NOT NULL, "
            "UNIQUE (team, player), "
            "CHECK (since <= until), "
            "CHECK ((NOT current) OR until IS NOT NULL)"
            ")",
        )
        self._check(
            main.relations.team_members.select(),
            "SELECT main.team_members.team, main.team_members.player, main.team_members.since, main.team_members.until, main.team_members.current FROM main.team_members",  # noqa: E501
        )

        # test main.team_players
        self._check(main.relations.team_players, "main.team_players")
        if False:
            # TODO: active the test once create method of View has been implemented
            self._check(
                main.relations.team_players.create(),
                "CREATE VIEW main.team_players AS (" "SELECT " ")",
            )
        self._check(
            main.relations.team_players.select(),
            "SELECT main.team_players.id, main.team_players.firstname, main.team_players.lastname, main.team_players.birthday, main.team_players.team FROM main.team_players",  # noqa: E501
        )
