import pytest
from querybuilder.helpers import table
from typing import cast
import querybuilder as qb


@pytest.fixture
def person_table() -> qb.atoms.relations.Table:
    @table
    class Person:
        id: int
        name: str
        age: int

    return cast(qb.atoms.relations.Table, Person)
