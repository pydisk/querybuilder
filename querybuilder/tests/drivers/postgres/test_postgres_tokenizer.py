import pytest
import querybuilder as qb
from querybuilder.formatting import token as qbtoken
from querybuilder.formatting.tokentree import TkStr, TkInd
import querybuilder.tests.drivers.sql.test_tokenizer as parent_suite

# can't import TestSQLTokenizer as it would rerun the SQL tests


class TestPostgresTokenizer(parent_suite.TestSQLTokenizer):
    tk = qb.drivers.postgres.tokenizer.Tokenizer()

    def test_create_view_without_columns(self):  # override
        name = self.get_dummy_tkseq(value="view")
        as_query = self.get_dummy_tkseq(value="SELECT ...")
        temporary = self.get_dummy_tkseq(value="temporary")
        if_not_exists = self.get_dummy_tkseq(value="if not exists")

        with pytest.warns():
            result = self.tk(
                self.get_empty_instance(qb.queries.ddl.CreateView),
                name=name,
                as_query=as_query,
                if_not_exists=if_not_exists,
                temporary=temporary,
            )

        expected = (
            (
                (
                    (
                        TkStr(qbtoken.Token.Keyword, "CREATE").to_seq(),
                        temporary,
                        TkStr(qbtoken.Token.Keyword, "VIEW").to_seq(),
                    ),
                    (),
                ),
                name,
            ),
            as_query,
        )

        assert expected == result

    def test_create_view_with_columns(self):  # override
        name = self.get_dummy_tkseq(value="view")
        as_query = self.get_dummy_tkseq(value="SELECT ...")
        temporary = self.get_dummy_tkseq(value="temporary")
        if_not_exists = self.get_dummy_tkseq(value="if not exists")
        columns = self.get_dummy_columns(3)

        with pytest.warns():
            result = self.tk(
                self.get_empty_instance(qb.queries.ddl.CreateView),
                name=name,
                as_query=as_query,
                columns=columns,
                if_not_exists=if_not_exists,
                temporary=temporary,
            )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            (
                (
                    (
                        (
                            TkStr(qbtoken.Token.Keyword, "CREATE").to_seq(),
                            temporary,
                            TkStr(qbtoken.Token.Keyword, "VIEW").to_seq(),
                        ),
                        (),
                    ),
                    name,
                ),
                (
                    TkStr(qbtoken.Punctuation, "(").to_seq(),
                    (
                        TkInd(),
                        (
                            columns[0] + sepseq,
                            columns[1] + sepseq,
                            columns[2],
                        ),
                    ),
                    TkStr(qbtoken.Punctuation, ")").to_seq(),
                ),
            ),
            as_query,
        )

        assert expected == result

    def test_create_view_does_not_warn_without_if_not_exists(self, recwarn):
        name = self.get_dummy_tkseq(value="view")
        as_query = self.get_dummy_tkseq(value="SELECT ...")
        temporary = self.get_dummy_tkseq(value="temporary")

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.CreateView),
            name=name,
            temporary=temporary,
            as_query=as_query,
        )

        expected = (
            (
                (
                    (
                        TkStr(qbtoken.Token.Keyword, "CREATE").to_seq(),
                        temporary,
                        TkStr(qbtoken.Token.Keyword, "VIEW").to_seq(),
                    ),
                    (),
                ),
                name,
            ),
            as_query,
        )

        assert expected == result
        assert len(recwarn) == 0

    def test_placeholder_with_key_provided(self):  # override
        key = "42"
        result = self.tk(self.get_empty_instance(qb.atoms.columns.Placeholder), key=key)

        expected = TkStr(qbtoken.Token.Name.Variable, f"%({key})s").to_seq()

        assert expected == result

    def test_placeholder_with_no_key_provided(self):  # override
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Placeholder),
        )

        expected = TkStr(qbtoken.Token.Name.Variable, "%s").to_seq()

        assert expected == result
