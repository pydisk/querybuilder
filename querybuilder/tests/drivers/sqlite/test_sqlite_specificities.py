import pytest
from querybuilder.formatting.formatter import StandardFormatter
import querybuilder as qb


class TestSqliteSpecificities:
    tk = qb.drivers.sqlite.tokenizer.Tokenizer()
    formatter = StandardFormatter()

    def test_schema_removed_in_create(self):
        col1 = qb.atoms.columns.TableColumn(
            int, "x", schema_name="main", relation_name="A"
        )
        qb.atoms.relations.Table("A", schema_name="main", columns=(col1,))
        col2 = qb.atoms.columns.TableColumn(
            int,
            "y",
            schema_name="main",
            relation_name="B",
            constraints=(qb.atoms.constraints.ColumnReferences(col1),),
        )
        B = qb.atoms.relations.Table("B", schema_name="main", columns=(col2,))
        result = self.formatter.get_formatted(B.create().tokenize(self.tk))
        expected = "CREATE TABLE main.b (y INTEGER REFERENCES a(x))"
        assert expected == result

    def test_autoincrement_in_create(self):
        col1 = qb.atoms.columns.TableColumn(
            int,
            "x",
            relation_name="A",
            constraints=(qb.atoms.constraints.ColumnGeneratedAsIdentity(),),
        )
        A = qb.atoms.relations.Table("A", columns=(col1,))
        with pytest.warns(UserWarning):
            result = self.formatter.get_formatted(A.create().tokenize(self.tk))
        expected = "CREATE TABLE a (x INTEGER AUTOINCREMENT)"

        assert expected == result
