from mock import Mock
import querybuilder.drivers.sqlite.specifics as specifics


class TestScopedDQL:
    def test_get_subtokenize_kwargs(self):
        query_tok = "query"
        tk = Mock()
        query = Mock()
        query.subtokenize = Mock(return_value=query_tok)
        query.columns = []

        scoped = specifics.ScopedDQL(query)

        kwargs = scoped._get_subtokenize_kwargs(tk)

        expected_kwargs = {"query": query_tok}

        assert expected_kwargs == kwargs
        query.subtokenize.assert_called_with(tk, scoped=True)
