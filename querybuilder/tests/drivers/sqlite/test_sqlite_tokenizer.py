import pytest
import querybuilder as qb
from querybuilder.formatting import token as qbtoken
from querybuilder.helpers import make_column
from querybuilder.formatting.tokentree import TkStr, TkSeq, TkInd
import querybuilder.tests.drivers.sql.test_tokenizer as parent_suite
import querybuilder.atoms.relations as qbrelations
import querybuilder.atoms.columns as qbcolumns
import querybuilder.queries.dql as qbdql


# can't import TestSQLTokenizer as it would rerun the SQL tests


class TestSqliteTokenizer(parent_suite.TestSQLTokenizer):
    tk = qb.drivers.sqlite.tokenizer.Tokenizer()

    def test_limit_one_row_only(self):  # override
        limit = 1
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.Limit),
            limit=limit,
            with_ties=False,
        )

        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "LIMIT"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Literal.Number, str(limit)),
            )
        )

        assert expected == result

    def test_limit_some_rows_only(self):  # override
        limit = 42
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.Limit),
            limit=limit,
            with_ties=False,
        )

        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "LIMIT"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Literal.Number, str(limit)),
            )
        )

        assert expected == result

    def test_limit_some_rows_with_ties(self):  # override
        limit = 42

        with pytest.raises(NotImplementedError):
            self.tk(
                self.get_empty_instance(qb.atoms.clauses.Limit),
                limit=limit,
                with_ties=True,
            )

    def test_order_columns_with_none_how(self):
        column = self.get_dummy_tkseq(value="column")
        scoped_column = self.tk.scope_it(column, noindent=True)
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.OrderColumn),
            column=scoped_column,
            how=None,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "ORDER"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "BY"),
                    TkStr(qbtoken.Whitespace, " "),
                )
            ),
            TkInd(),
            column,
        )

        assert expected == result

    def test_transform_aliased_named_relation_with_column_aliases(self):
        column_names = ("x", "b", "z")
        columns = tuple(qbcolumns.Named(int, name) for name in column_names)

        subrel = qbrelations.Named("foo", columns=columns)

        relation_alias = "bar"
        column_aliases = {1: "y"}

        rel = qbrelations.Aliased(subrel, relation_alias, column_aliases)
        result = self.tk.transform(rel)

        assert relation_alias == result.name

        result_subrel = result.subrelation.right.from_
        assert subrel == result_subrel
        assert tuple(subrel.columns) == tuple(result.subrelation.right.columns)

        expected_column_names = ("x", "y", "z")
        naming_relation = result.subrelation.left
        assert qbcolumns.False_() == naming_relation.where
        for i, c in enumerate(naming_relation.columns):
            assert expected_column_names[i] == c.name

    def test_transform_aliased_unnamed_relation_with_column_aliases(self):
        columns = tuple(make_column(i) for i in range(3))

        subrel = qbdql.Select(columns)

        relation_alias = "bar"
        column_aliases = {0: "x", 1: "y", 2: "z"}

        rel = subrel.alias(relation_alias, column_aliases=column_aliases)
        result = self.tk.transform(rel)

        assert relation_alias == result.name

        result_subrel = result.subrelation.right.from_
        assert subrel == result_subrel.subrelation
        assert relation_alias == result_subrel.name
        assert tuple(subrel.columns) == tuple(result.subrelation.right.columns)

        expected_column_names = ("x", "y", "z")
        naming_relation = result.subrelation.left
        assert qbcolumns.False_() == naming_relation.where
        for i, c in enumerate(naming_relation.columns):
            assert expected_column_names[i] == c.name

    def test_transform_aliased_relation_with_column_aliases(self):
        column_names = ("x", "b", "z")
        columns = tuple(qbcolumns.Named(int, name) for name in column_names)

        subrel = qbrelations.Named("foo", columns=columns)

        rel = qbrelations.Aliased(subrel, "bar", column_aliases={1: "col"})

        transrel = self.tk.transform(rel)

        assert rel.name == transrel.name
        assert rel.schema_name == transrel.schema_name

    def test_set_combinator_wrapper_with_False_all(self):
        # sqlite does not support UNION/INTERSECT/EXCEPT DISTINCT
        # ⟶  fallback to UNION/INTERSECT/EXCEPT
        combinator = qb.utils.constants.SetCombinator.UNION
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.SetCombinatorWrapper),
            combinator=combinator,
        )

        expected = (TkSeq((TkStr(qbtoken.Keyword, combinator.value),)),)

        assert expected == result

    def test_scoped_dql(self):
        query = self.get_dummy_tkseq("query")
        result = self.tk(
            self.get_empty_instance(qb.drivers.sqlite.specifics.ScopedDQL),
            query=query,
        )

        assert query == result
