import pytest
import querybuilder as qb
from querybuilder.formatting import token as qbtoken
from querybuilder.formatting.tokentree import TkStr, TkSeq, TkInd
import querybuilder.utils.constants as qbconstants
import querybuilder.atoms.columns as qbcolumns
import querybuilder.atoms.relations as qbrelations


class TestSQLTokenizer:
    tk = qb.drivers.sql.tokenizer.Tokenizer()

    def get_empty_instance(self, typ: type):
        return object.__new__(typ)

    def get_dummy_tkseq(self, token_type=qbtoken.Keyword, value="DUMMY"):
        return self.get_dummy_tkstr(token_type=token_type, value=value).to_seq()

    def get_dummy_tkstr(self, token_type=qbtoken.Keyword, value="DUMMY"):
        return TkStr(token_type, value)

    def get_dummy_columns(self, number, token_type=qbtoken.Keyword):
        return tuple(
            self.get_dummy_tkseq(value=f"column_{i}") for i in range(0, number)
        )

    def test_base_call(self):
        class DummyClass:
            pass

        with pytest.raises(TypeError):
            self.tk(DummyClass())

    def test_tokenize_default(self):
        result = self.tk(qb.atoms.pseudo_columns.Default())

        expected = TkStr(qbtoken.Keyword, "DEFAULT").to_seq()

        assert expected == result

    def test_tokenize_exists(self):
        query = self.get_dummy_tkseq(value="QUERY")
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Exists),
            query=query,
        )
        expected = (
            TkStr(qbtoken.Keyword, "EXISTS").to_seq(),
            (
                TkStr(qbtoken.Punctuation, "(").to_seq(),
                (TkInd(), query),
                TkStr(qbtoken.Punctuation, ")").to_seq(),
            ),
        )

        assert expected == result

    def test_tokenize_aggregate_with_distinct(self):
        column = self.get_dummy_tkseq("COLUMN")
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Aggregate),
            aggregator="avg",
            column=column,
            distinct=True,
        )
        expected = (
            TkStr(qbtoken.Token.Name.Function.Aggregator, "avg").to_seq(),
            (
                TkStr(qbtoken.Punctuation, "(").to_seq(),
                (
                    TkInd(),
                    (TkStr(qbtoken.Keyword, "DISTINCT").to_seq(), column),
                ),
                TkStr(qbtoken.Punctuation, ")").to_seq(),
            ),
        )

        assert expected == result

    def test_tokenize_aggregate_without_distinct(self):
        column = self.get_dummy_tkseq("column")
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Aggregate),
            aggregator="avg",
            column=column,
            distinct=False,
        )
        expected = (
            TkStr(qbtoken.Token.Name.Function.Aggregator, "avg").to_seq(),
            (
                TkStr(qbtoken.Punctuation, "(").to_seq(),
                (TkInd(), (column,)),
                TkStr(qbtoken.Punctuation, ")").to_seq(),
            ),
        )

        assert expected == result

    def test_tokenize_case_without_else(self):
        case_column = self.get_empty_instance(qb.atoms.columns.Case)
        when = [
            (
                self.get_dummy_tkseq(value=f"COND{i}"),
                self.get_dummy_tkseq(value=f"VAL{i}"),
            )
            for i in range(4)
        ]
        else_ = None
        result = self.tk(case_column, when=when, else_=else_)
        Token = qbtoken.Token
        expected = (
            TkSeq((TkStr(type=Token.Keyword, value="CASE"),)),
            (
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND0"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL0"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND1"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL1"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND2"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL2"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND3"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL3"),)),
                ),
            ),
            TkSeq((TkStr(type=Token.Keyword, value="END"),)),
        )
        assert result == expected

    def test_tokenize_case_with_else(self):
        case_column = self.get_empty_instance(qb.atoms.columns.Case)
        when = [
            (
                self.get_dummy_tkseq(value=f"COND{i}"),
                self.get_dummy_tkseq(value=f"VAL{i}"),
            )
            for i in range(4)
        ]
        else_ = self.get_dummy_tkseq(value="ELSE_VAL")
        result = self.tk(case_column, when=when, else_=else_)
        Token = qbtoken.Token
        expected = (
            TkSeq((TkStr(type=Token.Keyword, value="CASE"),)),
            (
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND0"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL0"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND1"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL1"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND2"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL2"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="WHEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="COND3"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="THEN"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="VAL3"),)),
                ),
                (
                    TkSeq((TkStr(type=Token.Keyword, value="ELSE"),)),
                    TkSeq((TkStr(type=Token.Keyword, value="ELSE_VAL"),)),
                ),
            ),
            TkSeq((TkStr(type=Token.Keyword, value="END"),)),
        )
        assert result == expected

    def test_tokenize_cast(self):
        column = self.get_dummy_tkseq(value="COLUMN")
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Cast),
            column=column,
            sqltype=str,
        )
        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Token.Name.Function, "CAST"),
                    TkStr(qbtoken.Punctuation, "("),
                )
            ),
            (
                TkInd(),
                (
                    column,
                    (
                        TkStr(qbtoken.Keyword, "AS").to_seq(),
                        TkStr(qbtoken.Name.Builtin, "TEXT").to_seq(),
                    ),
                ),
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        assert expected == result

    def test_tokenize_transform(self):
        columns = self.get_dummy_columns(3)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Transform),
            columns=columns,
            transformator="f",
        )
        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Token.Name.Function, "f"),
                    TkStr(qbtoken.Punctuation, "("),
                )
            ),
            (
                TkInd(),
                columns,
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        assert expected == result

    def test_tokenize_negate(self):
        tkval = "COLUMN"
        column = self.get_dummy_tkseq(value=tkval)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Negate),
            operator="-",
            column=column,
        )
        expected = TkSeq(
            (
                TkStr(qbtoken.Token.Operator, "-"),
                self.get_dummy_tkstr(value=tkval),
            )
        )
        assert expected == result

    def test_arithmetic_expression(self):
        columns = self.get_dummy_columns(3)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.ArithmeticOperation),
            operator="+",
            columns=columns,
        )

        opseq = TkStr(qbtoken.Token.Operator, "+").to_seq()

        expected = (columns[0], opseq, columns[1], opseq, columns[2])

        assert expected == result

    def test_in_relation(self):
        columns = self.get_dummy_columns(3)
        relation = self.get_dummy_tkseq(value="relation")

        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.InRelation),
            operator="IN",
            columns=columns,
            relation=relation,
        )

        expected = (
            columns,
            TkStr(qbtoken.Operator, "IN").to_seq(),
            relation,
        )

        assert expected == result

    def test_comparison(self):
        columns = self.get_dummy_columns(2)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Comparison),
            operator=">",
            columns=columns,
        )

        opseq = TkStr(qbtoken.Token.Operator, ">").to_seq()

        expected = (columns[0], opseq, columns[1])

        assert expected == result

    def test_not(self):
        column = self.get_dummy_tkseq()
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Not),
            combinator="NOT",
            column=column,
        )

        opseq = TkStr(qbtoken.Token.Keyword, "NOT").to_seq()

        expected = (opseq, column)

        assert expected == result

    def test_boolean_combination(self):
        columns = self.get_dummy_columns(3)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.BooleanCombination),
            combinator="AND",
            columns=columns,
        )

        opseq = TkStr(qbtoken.Token.Keyword, "AND").to_seq()

        expected = (
            columns[0],
            opseq,
            columns[1],
            opseq,
            columns[2],
        )

        assert expected == result

    def test_tuple(self):
        columns = self.get_dummy_columns(3)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Tuple),
            columns=columns,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()

        expected = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (
                TkInd(),
                (columns[0] + sepseq, columns[1] + sepseq, columns[2]),
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        assert expected == result

    def test_pretuple(self):
        columns = self.get_dummy_columns(3)
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Pretuple),
            columns=columns,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()

        expected = (columns[0] + sepseq, columns[1] + sepseq, columns[2])

        assert expected == result

    @pytest.mark.parametrize(
        "typ, val",
        [
            (int, 12),
            (int, "12"),
            (bool, True),
            (str, "TEXT"),
            (str, 42),
        ],
    )
    def test_constant(self, typ, val):  # kinda bad test
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Constant),
            sqltype=typ,
            constant=val,
        )

        expected = self.tk.tokenize_constant(typ, val)

        assert expected == result

    def test_placeholder_with_key_provided(self):
        key = "42"
        result = self.tk(self.get_empty_instance(qb.atoms.columns.Placeholder), key=key)

        expected = TkStr(qbtoken.Token.Name.Variable, ":" + key).to_seq()

        assert expected == result

    def test_placeholder_with_no_key_provided(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.columns.Placeholder),
        )

        expected = TkStr(qbtoken.Token.Name.Variable, "?").to_seq()

        assert expected == result

    def test_values(self):
        orderby = self.get_dummy_tkseq(value="ORDER BY")
        offset = self.get_dummy_tkseq(value="OFFSET")
        limit = self.get_dummy_tkseq(value="LIMIT")
        values = self.get_dummy_columns(3)

        result = self.tk(
            self.get_empty_instance(qb.queries.dql.Values),
            orderby=orderby,
            offset=offset,
            limit=limit,
            values=values,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Token.Keyword, "VALUES"),
                    TkStr(qbtoken.Whitespace, " "),
                )
            ),
            (
                values[0] + sepseq,
                values[1] + sepseq,
                values[2],
            ),
            (
                orderby,
                (
                    offset,
                    limit,
                ),
            ),
        )

        assert expected == result

    def test_tokenize_default_values(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.DefaultValuesClause),
        )

        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "DEFAULT"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "VALUES"),
            )
        )

        assert expected == result

    def test_select(self):
        columns = self.get_dummy_columns(3)
        distinct = self.get_dummy_tkseq(value="DISTINCT")
        from_ = self.get_dummy_tkseq(value="FROM ...")
        where = self.get_dummy_tkseq(value="WHERE ...")
        groupby = self.get_dummy_tkseq(value="GROUP BY ...")
        having = self.get_dummy_tkseq(value="HAVING ...")
        orderby = self.get_dummy_tkseq(value="ORDER BY ...")
        offset = self.get_dummy_tkseq(value="OFFSET")
        limit = self.get_dummy_tkseq(value="LIMIT")

        result = self.tk(
            self.get_empty_instance(qb.queries.dql.Select),
            orderby=orderby,
            offset=offset,
            limit=limit,
            columns=columns,
            where=where,
            from_=from_,
            groupby=groupby,
            having=having,
            distinct=distinct,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()

        expected = (
            (
                (
                    TkStr(qbtoken.Token.Keyword, "SELECT").to_seq(),
                    TkInd(),
                    TkStr(qbtoken.Token.Keyword, "DISTINCT").to_seq(),
                ),
                TkInd(),
                (
                    TkInd(),
                    columns[0] + sepseq,
                    columns[1] + sepseq,
                    columns[2],
                ),
            ),
            TkInd(),
            from_,
            where,
            (
                groupby,
                TkInd(),
                having,
            ),
            orderby,
            (
                offset,
                limit,
            ),
        )

        assert expected == result

    def test_set_combination(self):
        subrelations = self.get_dummy_columns(2)
        combinator = self.get_dummy_tkstr(value="UNION")
        result = self.tk(
            self.get_empty_instance(qb.queries.dql.SetCombination),
            combinator=combinator,
            subrelations=subrelations,
        )

        expected = (subrelations[0], combinator, subrelations[1])

        assert expected == result

    def test_distinct_column_with_no_column(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.DistinctColumn),
            column=None,
        )

        expected = TkStr(qbtoken.Keyword, "DISTINCT").to_seq()

        assert expected == result

    def test_distinct_column_with_column(self):
        column = self.get_dummy_tkseq(value="column")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.DistinctColumn),
            column=column,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "DISTINCT"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ON"),
                )
            ),
            (
                TkStr(qbtoken.Punctuation, "(").to_seq(),
                (
                    TkInd(),
                    column,
                ),
                TkStr(qbtoken.Punctuation, ")").to_seq(),
            ),
        )

        assert expected == result

    def test_with_clause_with_False_materialized(self):
        name = "foo"
        query = self.get_dummy_tkseq(value="query")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.WithClause),
            name=name,
            query=query,
            materialized=False,
        )

        expected = (
            (
                TkStr(qbtoken.Name, name).to_seq(),
                TkSeq(
                    (
                        TkStr(qbtoken.Keyword, "AS"),
                        TkStr(qbtoken.Whitespace, " "),
                        TkStr(qbtoken.Keyword, "NOT"),
                        TkStr(qbtoken.Whitespace, " "),
                        TkStr(qbtoken.Keyword, "MATERIALIZED"),
                    )
                ),
            ),
            TkInd(),
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (
                TkInd(),
                query,
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        assert expected == result

    def test_with_clause_with_True_materialized(self):
        name = "foo"
        query = self.get_dummy_tkseq(value="query")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.WithClause),
            name=name,
            query=query,
            materialized=True,
        )

        expected = (
            (
                TkStr(qbtoken.Name, name).to_seq(),
                TkSeq(
                    (
                        TkStr(qbtoken.Keyword, "AS"),
                        TkStr(qbtoken.Whitespace, " "),
                        TkStr(qbtoken.Keyword, "MATERIALIZED"),
                    )
                ),
            ),
            TkInd(),
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (
                TkInd(),
                query,
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        assert expected == result

    def test_with_clause_with_None_materialized(self):
        name = "foo"
        query = self.get_dummy_tkseq(value="query")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.WithClause),
            name=name,
            query=query,
            materialized=None,
        )

        expected = (
            (
                TkStr(qbtoken.Name, name).to_seq(),
                TkSeq((TkStr(qbtoken.Keyword, "AS"),)),
            ),
            TkInd(),
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (
                TkInd(),
                query,
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        assert expected == result

    def test_with_closure_with_with_relations(self):
        with_relations = self.get_dummy_columns(3)
        query = self.get_dummy_tkseq(value="query")
        result = self.tk(
            self.get_empty_instance(qb.queries.dql.WithClosure),
            with_relations=with_relations,
            query=query,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            TkStr(qbtoken.Token.Keyword, "WITH").to_seq(),
            with_relations[0] + sepseq,
            with_relations[1] + sepseq,
            with_relations[2],
            query,
        )

        assert expected == result

    def test_with_closure_without_with_relations(self):
        query = self.get_dummy_tkseq(value="query")
        result = self.tk(
            self.get_empty_instance(qb.queries.dql.WithClosure),
            with_relations=(),
            query=query,
        )

        expected = (query,)

        assert expected == result

    def test_aliased_columns_with_alias(self):
        column = self.get_dummy_tkseq(value="column")
        alias = "foo"

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.AliasedColumn),
            column=column,
            alias=alias,
        )

        expected = (
            column,
            TkInd(),
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Whitespace, " "),
                        TkStr(qbtoken.Keyword, "AS"),
                        TkStr(qbtoken.Whitespace, " "),
                    )
                ),
                TkStr(qbtoken.Name, alias).to_seq(),
            ),
        )

        assert expected == result

    def test_aliased_columns_without_alias(self):
        column = self.get_dummy_tkseq(value="column")

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.AliasedColumn),
            column=column,
            alias=None,
        )

        expected = (column,)

        assert expected == result

    def test_relation_clause_wrapper(self):
        relation = self.get_dummy_tkseq(value="relation")

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.RelationClauseWrapper),
            relation=relation,
        )

        expected = (
            TkStr(qbtoken.Keyword, "FROM").to_seq(),
            TkInd(),
            relation,
        )

        assert expected == result

    def test_where_colum(self):
        column = self.get_dummy_tkseq(value="column")

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.WhereColumn),
            column=column,
        )

        expected = (
            TkStr(qbtoken.Keyword, "WHERE").to_seq(),
            TkInd(),
            column,
        )

        assert expected == result

    def test_group_column(self):
        column = self.get_dummy_tkseq(value="column")

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.GroupColumn),
            column=column,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "GROUP"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "BY"),
                )
            ),
            TkInd(),
            column,
        )

        assert expected == result

    def test_having_column(self):
        column = self.get_dummy_tkseq(value="column")

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.HavingColumn),
            column=column,
        )

        expected = (
            TkStr(qbtoken.Keyword, "HAVING").to_seq(),
            TkInd(),
            column,
        )

        assert expected == result

    def test_temporary_wrapper(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.TemporaryWrapper),
        )

        expected = TkStr(qbtoken.Keyword, "TEMPORARY").to_seq()

        assert expected == result

    def test_if_not_exists_wrapper(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.IfNotExistsWrapper),
        )

        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "IF"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "NOT"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "EXISTS"),
            )
        )

        assert expected == result

    def test_if_exists_wrapper(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.IfExistsWrapper),
        )

        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "IF"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "EXISTS"),
            )
        )

        assert expected == result

    def test_as_subquery_wrapper(self):
        subquery = self.get_dummy_tkseq(value="subquery")

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.AsQueryWrapper),
            subquery=subquery,
        )

        expected = (
            TkStr(qbtoken.Keyword, "AS").to_seq(),
            subquery,
        )

        assert expected == result

    def test_offset_clause(self):
        offset = 42

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.Offset),
            offset=offset,
        )

        expected = (
            TkStr(qbtoken.Keyword, "OFFSET").to_seq(),
            TkStr(qbtoken.Literal.Number, str(offset)).to_seq(),
        )

        assert expected == result

    def test_order_columns_with_true_how(self):
        column = self.get_dummy_tkseq(value="column")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.OrderColumn),
            column=column,
            how=True,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "ORDER"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "BY"),
                    TkStr(qbtoken.Whitespace, " "),
                )
            ),
            TkInd(),
            column,
            TkStr(qbtoken.Keyword, "ASC").to_seq(),
        )

        assert expected == result

    def test_order_columns_with_false_how(self):
        column = self.get_dummy_tkseq(value="column")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.OrderColumn),
            column=column,
            how=False,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "ORDER"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "BY"),
                    TkStr(qbtoken.Whitespace, " "),
                )
            ),
            TkInd(),
            column,
            TkStr(qbtoken.Keyword, "DESC").to_seq(),
        )

        assert expected == result

    def test_order_columns_with_none_how(self):
        column = self.get_dummy_tkseq(value="column")
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.OrderColumn),
            column=column,
            how=None,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "ORDER"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "BY"),
                    TkStr(qbtoken.Whitespace, " "),
                )
            ),
            TkInd(),
            column,
        )

        assert expected == result

    def test_limit_one_row_only(self):
        limit = 1
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.Limit),
            limit=limit,
            with_ties=False,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "FETCH"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "FIRST"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Literal.Number, str(limit)),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ROW"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ONLY"),
                )
            ),
        )

        assert expected == result

    def test_limit_some_rows_only(self):
        limit = 42
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.Limit),
            limit=limit,
            with_ties=False,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "FETCH"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "FIRST"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Literal.Number, str(limit)),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ROWS"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ONLY"),
                )
            ),
        )

        assert expected == result

    def test_limit_some_rows_with_ties(self):
        limit = 42
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.Limit),
            limit=limit,
            with_ties=True,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "FETCH"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "FIRST"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Literal.Number, str(limit)),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ROWS"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "WITH"),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "TIES"),
                )
            ),
        )

        assert expected == result

    def test_set_combinator_wrapper_with_True_all(self):
        combinator = qbconstants.SetCombinator.UNION
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.SetCombinatorWrapper),
            combinator=combinator,
            all=True,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, combinator.value),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "ALL"),
                )
            ),
        )

        assert expected == result

    def test_set_combinator_wrapper_with_False_all(self):
        combinator = qbconstants.SetCombinator.UNION
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.SetCombinatorWrapper),
            combinator=combinator,
            all=False,
        )

        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, combinator.value),
                    TkStr(qbtoken.Whitespace, " "),
                    TkStr(qbtoken.Keyword, "DISTINCT"),
                )
            ),
        )

        assert expected == result

    def test_set_combinator_wrapper_with_None_all(self):
        combinator = qbconstants.SetCombinator.UNION
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.SetCombinatorWrapper),
            combinator=combinator,
            all=None,
        )

        expected = (TkSeq((TkStr(qbtoken.Keyword, combinator.value),)),)

        assert expected == result

    def test_set_combinator_wrapper_without_all(self):
        combinator = qbconstants.SetCombinator.UNION
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.SetCombinatorWrapper),
            combinator=combinator,
        )

        expected = (TkSeq((TkStr(qbtoken.Keyword, combinator.value),)),)

        assert expected == result

    def test_set_columns(self):
        set_columns = self.get_dummy_columns(3)

        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.SetColumnsClause),
            set_columns=set_columns,
        )

        TkStr(qbtoken.Token.Operator, "=").to_seq()
        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            TkStr(qbtoken.Token.Keyword, "SET").to_seq(),
            TkInd(),
            (
                set_columns[0] + sepseq,
                set_columns[1] + sepseq,
                set_columns[2],
            ),
        )

        assert expected == result

    def test_tokenize_insert_with_columns(self):
        in_columns = self.get_dummy_columns(3)
        query = self.get_dummy_tkseq(value="query")
        relation = self.get_dummy_tkseq(value="relation")
        result = self.tk(
            self.get_empty_instance(qb.queries.dml.Insert),
            schema_relation=relation,
            in_columns=in_columns,
            query=query,
        )
        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            (
                (
                    TkSeq(
                        (
                            TkStr(qbtoken.Keyword, "INSERT"),
                            TkStr(qbtoken.Whitespace, " "),
                            TkStr(qbtoken.Keyword, "INTO"),
                        )
                    ),
                    TkInd(),
                    relation,
                ),
                TkInd(),
                TkStr(qbtoken.Punctuation, "(").to_seq(),
                (
                    TkInd(),
                    (
                        in_columns[0] + sepseq,
                        in_columns[1] + sepseq,
                        in_columns[2],
                    ),
                ),
                TkStr(qbtoken.Punctuation, ")").to_seq(),
            ),
            query,
        )

        assert expected == result

    def test_tokenize_insert_without_columns(self):
        in_columns = ()
        query = self.get_dummy_tkseq(value="query")
        relation = self.get_dummy_tkseq(value="relation")
        result = self.tk(
            self.get_empty_instance(qb.queries.dml.Insert),
            schema_relation=relation,
            in_columns=in_columns,
            query=query,
        )
        expected = (
            (
                (
                    TkSeq(
                        (
                            TkStr(qbtoken.Keyword, "INSERT"),
                            TkStr(qbtoken.Whitespace, " "),
                            TkStr(qbtoken.Keyword, "INTO"),
                        )
                    ),
                    TkInd(),
                    relation,
                ),
            ),
            query,
        )

        assert expected == result

    def test_delete_with_where(self):
        schema_relation = self.get_dummy_tkseq(value="rel")
        where = self.get_dummy_tkseq(value="WHERE ...")

        result = self.tk(
            self.get_empty_instance(qb.queries.dml.Delete),
            schema_relation=schema_relation,
            where=where,
        )

        expected = (
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Token.Keyword, "DELETE"),
                        TkStr(qbtoken.Whitespace, " "),
                        TkStr(qbtoken.Token.Keyword, "FROM"),
                    )
                ),
                TkInd(),
                schema_relation,
            ),
            where,
        )

        assert expected == result

    def test_delete_without_where(self):
        schema_relation = self.get_dummy_tkseq(value="rel")

        result = self.tk(
            self.get_empty_instance(qb.queries.dml.Delete),
            schema_relation=schema_relation,
            where=(),
        )

        expected = (
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Token.Keyword, "DELETE"),
                        TkStr(qbtoken.Whitespace, " "),
                        TkStr(qbtoken.Token.Keyword, "FROM"),
                    )
                ),
                TkInd(),
                schema_relation,
            ),
        )

        assert expected == result

    def test_update_with_where(self):
        schema_relation = self.get_dummy_tkseq(value="rel")
        set_columns = self.get_dummy_tkseq(value="SET ...")
        where = self.get_dummy_tkseq(value="WHERE ...")

        result = self.tk(
            self.get_empty_instance(qb.queries.dml.Update),
            schema_relation=schema_relation,
            set_columns=set_columns,
            where=where,
        )

        expected = (
            (
                TkStr(qbtoken.Token.Keyword, "UPDATE").to_seq(),
                schema_relation,
            ),
            set_columns,
            where,
        )

        assert expected == result

    def test_update_without_where(self):
        schema_relation = self.get_dummy_tkseq(value="rel")
        set_columns = self.get_dummy_tkseq(value="SET ...")

        result = self.tk(
            self.get_empty_instance(qb.queries.dml.Update),
            schema_relation=schema_relation,
            set_columns=set_columns,
        )

        expected = (
            (
                TkStr(qbtoken.Token.Keyword, "UPDATE").to_seq(),
                schema_relation,
            ),
            set_columns,
        )

        assert expected == result

    def test_cascade_clause_True(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.DropCascadeClause),
            cascade=True,
        )

        expected = (TkStr(qbtoken.Keyword, "CASCADE").to_seq(),)

        assert expected == result

    def test_cascade_clause_False(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.clauses.DropCascadeClause),
            cascade=False,
        )

        expected = (TkStr(qbtoken.Keyword, "RESTRICT").to_seq(),)

        assert expected == result

    def test_drop(self):
        name = self.get_dummy_tkseq(value="table")
        target_type = self.get_dummy_tkstr(value="TABLE")

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.Drop),
            name=name,
            target_type=target_type.to_seq(),
        )

        expected = (
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Token.Keyword, "DROP"),
                        TkStr(qbtoken.Whitespace, " "),
                        target_type,
                    )
                ),
                (),
            ),
            name,
            (),
        )

        assert expected == result

    def test_drop_with_cascade(self):
        name = self.get_dummy_tkseq(value="table")
        cascade = self.get_dummy_tkseq(value="cascade")
        target_type = self.get_dummy_tkstr(value="TABLE")

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.Drop),
            name=name,
            cascade=cascade,
            target_type=target_type.to_seq(),
        )

        expected = (
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Token.Keyword, "DROP"),
                        TkStr(qbtoken.Whitespace, " "),
                        target_type,
                    )
                ),
                (),
            ),
            name,
            cascade,
        )

        assert expected == result

    def test_drop_with_if_exists(self):
        name = self.get_dummy_tkseq(value="table")
        if_exists = self.get_dummy_tkseq(value="if exists")
        target_type = self.get_dummy_tkstr(value="TABLE")

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.Drop),
            name=name,
            if_exists=if_exists,
            target_type=target_type.to_seq(),
        )

        expected = (
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Token.Keyword, "DROP"),
                        TkStr(qbtoken.Whitespace, " "),
                        target_type,
                    )
                ),
                if_exists,
            ),
            name,
            (),
        )

        assert expected == result

    def test_drop_with_cascade_and_if_exists(self):
        name = self.get_dummy_tkseq(value="table")
        cascade = self.get_dummy_tkseq(value="cascade")
        if_exists = self.get_dummy_tkseq(value="if exists")
        target_type = self.get_dummy_tkstr(value="TABLE")

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.Drop),
            name=name,
            cascade=cascade,
            if_exists=if_exists,
            target_type=target_type.to_seq(),
        )

        expected = (
            (
                TkSeq(
                    (
                        TkStr(qbtoken.Token.Keyword, "DROP"),
                        TkStr(qbtoken.Whitespace, " "),
                        target_type,
                    )
                ),
                if_exists,
            ),
            name,
            cascade,
        )

        assert expected == result

    def test_create_view_without_columns(self):
        name = self.get_dummy_tkseq(value="view")
        as_query = self.get_dummy_tkseq(value="SELECT ...")
        temporary = self.get_dummy_tkseq(value="temporary")
        if_not_exists = self.get_dummy_tkseq(value="if not exists")

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.CreateView),
            name=name,
            as_query=as_query,
            if_not_exists=if_not_exists,
            temporary=temporary,
        )

        expected = (
            (
                (
                    (
                        TkStr(qbtoken.Token.Keyword, "CREATE").to_seq(),
                        temporary,
                        TkStr(qbtoken.Token.Keyword, "VIEW").to_seq(),
                    ),
                    if_not_exists,
                ),
                name,
            ),
            as_query,
        )

        assert expected == result

    def test_create_view_with_columns(self):
        name = self.get_dummy_tkseq(value="view")
        as_query = self.get_dummy_tkseq(value="SELECT ...")
        temporary = self.get_dummy_tkseq(value="temporary")
        if_not_exists = self.get_dummy_tkseq(value="if not exists")
        columns = self.get_dummy_columns(3)

        result = self.tk(
            self.get_empty_instance(qb.queries.ddl.CreateView),
            name=name,
            as_query=as_query,
            columns=columns,
            if_not_exists=if_not_exists,
            temporary=temporary,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            (
                (
                    (
                        (
                            TkStr(qbtoken.Token.Keyword, "CREATE").to_seq(),
                            temporary,
                            TkStr(qbtoken.Token.Keyword, "VIEW").to_seq(),
                        ),
                        if_not_exists,
                    ),
                    name,
                ),
                (
                    TkStr(qbtoken.Punctuation, "(").to_seq(),
                    (
                        TkInd(),
                        (
                            columns[0] + sepseq,
                            columns[1] + sepseq,
                            columns[2],
                        ),
                    ),
                    TkStr(qbtoken.Punctuation, ")").to_seq(),
                ),
            ),
            as_query,
        )

        assert expected == result

    def test_tokenize_star_without_relation(self):
        result = self.tk(
            self.get_empty_instance(qb.atoms.pseudo_columns.Star), relation=()
        )

        expected = TkStr(qbtoken.Operator, "*").to_seq()
        assert expected == result

    def test_tokenize_star_with_relation(self):
        relation = self.get_dummy_tkseq("relation")
        result = self.tk(
            self.get_empty_instance(qb.atoms.pseudo_columns.Star), relation=relation
        )

        expected = (
            relation
            + TkStr(qbtoken.Punctuation, ".").to_seq()
            + TkStr(qbtoken.Operator, "*").to_seq()
        )

        assert expected == result

    def test_aliased_relation_with_tkseq_without_column_aliases(self):
        name = self.get_dummy_tkseq(value="bar")
        subrelation = self.get_dummy_tkseq(value="foo")

        result = self.tk(
            self.get_empty_instance(qb.atoms.relations.Aliased),
            subrelation=subrelation,
            name=name,
            columns=(),
        )

        expected = (subrelation, TkStr(qbtoken.Token.Keyword, "AS").to_seq(), name)

        assert expected == result

    def test_aliased_relation_with_tkseq_and_column_aliases(self):
        name = self.get_dummy_tkseq(value="bar")
        subrelation = self.get_dummy_tkseq(value="foo")
        columns = self.get_dummy_columns(3)

        result = self.tk(
            self.get_empty_instance(qb.atoms.relations.Aliased),
            subrelation=subrelation,
            name=name,
            columns=columns,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            subrelation,
            TkStr(qbtoken.Token.Keyword, "AS").to_seq(),
            (
                name + TkStr(qbtoken.Punctuation, "(").to_seq(),
                (
                    columns[0] + sepseq,
                    columns[1] + sepseq,
                    columns[2],
                ),
                TkStr(qbtoken.Punctuation, ")").to_seq(),
            ),
        )

        assert expected == result

    def test_aliased_relation_with_tktree_without_column_aliases(self):
        name = self.get_dummy_tkseq(value="bar")
        subrelation_seq = self.get_dummy_tkseq(value="foo")

        subrelation = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (TkInd(), subrelation_seq),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        result = self.tk(
            self.get_empty_instance(qb.atoms.relations.Aliased),
            subrelation=subrelation,
            name=name,
            columns=(),
        )

        expected = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (TkInd(), subrelation_seq),
            (
                TkStr(qbtoken.Punctuation, ")").to_seq(),
                (TkStr(qbtoken.Token.Keyword, "AS").to_seq(), name),
            ),
        )

        assert expected == result

    def test_aliased_relation_with_tktree_and_column_aliases(self):
        name = self.get_dummy_tkseq(value="bar")
        subrelation_seq = self.get_dummy_tkseq(value="foo")
        columns = self.get_dummy_columns(3)

        subrelation = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (TkInd(), subrelation_seq),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        result = self.tk(
            self.get_empty_instance(qb.atoms.relations.Aliased),
            subrelation=subrelation,
            name=name,
            columns=columns,
        )

        sepseq = TkStr(qbtoken.Token.Punctuation, ",").to_seq()
        expected = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (TkInd(), subrelation_seq),
            (
                TkStr(qbtoken.Punctuation, ")").to_seq(),
                (
                    TkStr(qbtoken.Token.Keyword, "AS").to_seq(),
                    (
                        name + TkStr(qbtoken.Punctuation, "(").to_seq(),
                        (
                            columns[0] + sepseq,
                            columns[1] + sepseq,
                            columns[2],
                        ),
                        TkStr(qbtoken.Punctuation, ")").to_seq(),
                    ),
                ),
            ),
        )

        assert expected == result

    def test_transform_aliased_relation_without_column_aliases(self):
        column_names = ("x", "b", "z")
        columns = tuple(qbcolumns.Named(int, name) for name in column_names)

        subrel = qbrelations.Named("foo", columns=columns)
        rel = qbrelations.Aliased(subrel, "bar")

        assert rel == self.tk.transform(rel)

    def test_transform_aliased_relation_with_column_aliases(self):
        column_names = ("x", "b", "z")
        columns = tuple(qbcolumns.Named(int, name) for name in column_names)

        subrel = qbrelations.Named("foo", columns=columns)

        rel = qbrelations.Aliased(subrel, "bar", column_aliases={1: "col"})

        transrel = self.tk.transform(rel)

        assert rel == transrel
