import pytest
from uuid import UUID
import querybuilder as qb
from querybuilder.formatting import token as qbtoken
from querybuilder.formatting.tokentree import TkStr, TkSeq, TkInd
import querybuilder.utils.constants as qbconstants
from datetime import datetime, date, time, timedelta

raw_uuid = "74862f4f-0f68-4b3e-9a70-9207b1067d4b"
parsed_uuid = UUID(raw_uuid)


class TestTokenizeFuncs:
    tk = qb.drivers.sql.tokenizer.Tokenizer()

    ########
    # name #
    ########

    def test_tokenize_name_only_schema(self):
        expected = TkStr(qbtoken.Name, "foo").to_seq()
        result = self.tk.tokenize_name(schema_name="foo")

        assert expected == result

    def test_tokenize_name_only_table(self):
        expected = TkStr(qbtoken.Name, "foo").to_seq()
        result = self.tk.tokenize_name(relation_name="foo")

        assert expected == result

    def test_tokenize_name_only_column(self):
        expected = TkStr(qbtoken.Name, "foo").to_seq()
        result = self.tk.tokenize_name(name="foo")

        assert expected == result

    def test_tokenize_name__table_and_column(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Name, "bar"),
                TkStr(qbtoken.Punctuation, "."),
                TkStr(qbtoken.Name, "col"),
            )
        )
        result = self.tk.tokenize_name(relation_name="bar", name="col")

        assert expected == result

    def test_tokenize_name_schema_and_column(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Name, "foo"),
                TkStr(qbtoken.Punctuation, "."),
                TkStr(qbtoken.Name, "col"),
            )
        )
        result = self.tk.tokenize_name(schema_name="foo", name="col")

        assert expected == result

    def test_tokenize_name_schema_and_table(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Name, "foo"),
                TkStr(qbtoken.Punctuation, "."),
                TkStr(qbtoken.Name, "bar"),
            )
        )
        result = self.tk.tokenize_name(schema_name="foo", relation_name="bar")

        assert expected == result

    def test_tokenize_name_schema_and_table_and_column(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Name, "foo"),
                TkStr(qbtoken.Punctuation, "."),
                TkStr(qbtoken.Name, "bar"),
                TkStr(qbtoken.Punctuation, "."),
                TkStr(qbtoken.Name, "col"),
            )
        )
        result = self.tk.tokenize_name(
            schema_name="foo", relation_name="bar", name="col"
        )

        assert expected == result

    ###########
    # keyword #
    ###########

    def test_tokenize_keyword_one_word_no_spaces(self):
        expected = TkStr(qbtoken.Keyword, "FOO").to_seq()
        result = self.tk.tokenize_keyword("FOO")

        assert expected == result

    def test_tokenize_keyword_one_word_end_space(self):
        expected = (
            TkStr(qbtoken.Keyword, "FOO").to_seq()
            + TkStr(qbtoken.Whitespace, " ").to_seq()
        )
        result = self.tk.tokenize_keyword("FOO", finalspace=True)

        assert expected == result

    def test_tokenize_keyword_one_word_initial_space(self):
        expected = (
            TkStr(qbtoken.Whitespace, " ").to_seq()
            + TkStr(qbtoken.Keyword, "FOO").to_seq()
        )
        result = self.tk.tokenize_keyword("FOO", initialspace=True)

        assert expected == result

    def test_tokenize_keyword_one_word_both_spaces(self):
        expected = [
            (qbtoken.Whitespace, " "),
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Whitespace, " "),
        ]
        result = list(
            self.tk.tokenize_keyword("FOO", initialspace=True, finalspace=True)
        )

        assert expected == result

    def test_tokenize_keyword_two_words_no_spaces(self):
        expected = [
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Whitespace, " "),
            (qbtoken.Keyword, "BAR"),
        ]
        result = list(self.tk.tokenize_keyword("FOO BAR"))

        assert expected == result

    def test_tokenize_keyword_two_words_inital_space(self):
        expected = [
            (qbtoken.Whitespace, " "),
            (qbtoken.Keyword, "FOO"),
            (qbtoken.Whitespace, " "),
            (qbtoken.Keyword, "BAR"),
        ]
        result = list(self.tk.tokenize_keyword("FOO BAR", initialspace=True))

        assert expected == result

    def test_tokenize_keyword_two_words_end_space(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "FOO"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "BAR"),
                TkStr(qbtoken.Whitespace, " "),
            )
        )
        result = self.tk.tokenize_keyword("FOO BAR", finalspace=True)

        assert expected == result

    def test_tokenize_keyword_two_words_both_spaces(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "FOO"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "BAR"),
                TkStr(qbtoken.Whitespace, " "),
            )
        )

        result = self.tk.tokenize_keyword("FOO BAR", initialspace=True, finalspace=True)

        assert expected == result

    ############
    # jointype #
    ############

    # TODO: use hypothesis module?

    def test_tokenize_join(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "INNER"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "JOIN"),
            )
        )
        result = self.tk.tokenize_jointype(qbconstants.JoinType.INNER, False)

        assert expected == result

    def test_tokenize_natural_join(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Keyword, "NATURAL"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "FULL"),
                TkStr(qbtoken.Whitespace, " "),
                TkStr(qbtoken.Keyword, "JOIN"),
            )
        )
        result = self.tk.tokenize_jointype(qbconstants.JoinType.FULL, True)

        assert expected == result

    ###########
    # sqltype #
    ###########

    @pytest.mark.parametrize(
        "typ, expected_str",
        [
            (int, "INTEGER"),
            (float, "REAL"),
            (bool, "BOOLEAN"),
            (str, "TEXT"),
            (datetime, "TIMESTAMP"),
            (date, "DATE"),
            (time, "TIME"),
            (timedelta, "INTERVAL"),
        ],
    )
    def test_tokenize_sqltype(self, typ, expected_str):
        result = self.tk.tokenize_sqltype(typ)
        expected = TkStr(qbtoken.Token.Name.Builtin, expected_str).to_seq()

        assert expected == result

    def test_tokenize_sqltype_with_unregistered_type_with_sqltype_attr(self):
        sqltype = "Varchar(42)"

        class Varchar42:
            _sqltype = sqltype

        result = self.tk.tokenize_sqltype(Varchar42)
        expected = TkStr(qbtoken.Token.Name.Builtin, sqltype).to_seq()

        assert expected == result

    def test_tokenize_sqltype_with_unregistered_type_without_sqltype_attr(self):
        class DummyType:
            pass

        with pytest.raises(ValueError):
            self.tk.tokenize_sqltype(DummyType)

    #############
    # constants #
    #############

    def test_tokenize_constant_None(self):
        expected = TkStr(qbtoken.Token.Keyword, "NULL").to_seq()
        result = self.tk.tokenize_constant(type(None), None)

        assert expected == result

    @pytest.mark.parametrize(
        "val, typ, expected_str",
        [(42, int, "42"), (42, float, "42.0"), (3.14, int, "3"), (3.14, float, "3.14")],
    )
    def test_tokenize_constant_numbers(self, val, typ, expected_str):
        expected = TkStr(qbtoken.Token.Literal.Number, expected_str).to_seq()
        result = self.tk.tokenize_constant(typ, val)

        assert expected == result

    @pytest.mark.parametrize(
        "value",
        [
            parsed_uuid,
            raw_uuid,
            parsed_uuid.bytes,
            parsed_uuid.int,
            parsed_uuid.hex,
        ],
    )
    def test_tokenize_uuid(self, value):
        expected = TkStr(qbtoken.Token.Literal, repr(raw_uuid)).to_seq()
        result = self.tk.tokenize_constant(UUID, value)

        assert expected == result

    def test_tokenize_constant_string(self):
        expected = TkStr(qbtoken.Token.Literal.String, "'foo'").to_seq()
        result = self.tk.tokenize_constant(str, "foo")

        assert expected == result

    @pytest.mark.parametrize(
        "val, expected_str",
        [
            (True, "True"),
            (False, "False"),
        ],
    )
    def test_tokenize_constant_booleans(self, val, expected_str):
        expected = TkStr(qbtoken.Token.Keyword, expected_str).to_seq()
        result = self.tk.tokenize_constant(bool, val)

        assert expected == result

    @pytest.mark.parametrize(
        "val, expected_str",
        [
            (time(), "00:00:00"),
            (timedelta(), "0:00:00"),
            (date(1970, 1, 1), "1970-01-01"),
            (datetime(1970, 1, 1), "1970-01-01 00:00:00"),
        ],
    )
    def test_tokenize_constant_dates(self, val, expected_str):
        expected = TkStr(qbtoken.Token.Literal.Date, expected_str).to_seq()
        result = self.tk.tokenize_constant(type(val), val)

        assert expected == result

    def test_tokenize_constant_with_unregistered_type_with_hard_encode(self):
        class FancyString:
            @staticmethod
            def _hard_encode(value):
                return f"«{str(value)}»"

        TkStr(qbtoken.Token.Literal, "«42»").to_seq()
        self.tk.tokenize_constant(FancyString, 42)

    def test_tokenize_constant_with_uneregistered_type_without_hard_encode(self):
        class DummyType:
            pass

        with pytest.raises(TypeError):
            self.tk.tokenize_constant(DummyType, 42)

    ###############
    # placeholder #
    ###############

    def test_tokenize_placeholder_key_with_anon_index(self):
        expected = TkStr(qbtoken.Token.Name.Variable, ":auto42").to_seq()
        result = self.tk.tokenize_placeholder_key(None, anonymous_index=42)

        assert expected == result

    ###########
    # comment #
    ###########

    def test_tokenize_comment(self):
        comment = "FOO"
        expected = TkStr(qbtoken.Comment, "-- " + comment).to_seq()
        result = self.tk.tokenize_comment(comment)

        assert expected == result

    ############
    # value(s) #
    ############

    def test_tokenize_value_without_comment_marker(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Comment.Args.Arrow, "←"),
                TkStr(qbtoken.Comment.Args.Value, "42"),
            )
        )
        result = self.tk.tokenize_value(42, with_comment_marker=False)

        assert expected == result

    def test_tokenize_value_with_comment_marker(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Comment, "-- "),
                TkStr(qbtoken.Comment.Args.Arrow, "←"),
                TkStr(qbtoken.Comment.Args.Value, "42"),
            )
        )
        result = self.tk.tokenize_value(42, with_comment_marker=True)

        assert expected == result

    def test_tokenize_value_with_comment_marker_and_missing_value(self):
        expected = TkSeq(
            (
                TkStr(qbtoken.Comment, "-- "),
                TkStr(qbtoken.Comment.Args.Arrow, "←"),
                TkStr(qbtoken.Comment.Args.Value.MISSING, "ø"),
            )
        )
        result = self.tk.tokenize_value(qbconstants.MISSING, with_comment_marker=True)

        assert expected == result

    def test_scope_it_with_indent(self):
        dummy_tree = (TkStr(qbtoken.Keyword, "DUMMY").to_seq(),)
        expected = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            (
                TkInd(),
                dummy_tree,
            ),
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        result = self.tk.scope_it(dummy_tree)

        assert expected == result

    def test_scope_it_with_no_indent(self):
        dummy_tree = (TkStr(qbtoken.Keyword, "DUMMY").to_seq(),)
        expected = (
            TkStr(qbtoken.Punctuation, "(").to_seq(),
            dummy_tree,
            TkStr(qbtoken.Punctuation, ")").to_seq(),
        )

        result = self.tk.scope_it(dummy_tree, noindent=True)

        assert expected == result

    def test_tokenize_non_empty_list(self):
        lst = (TkStr(qbtoken.Keyword, f"DUMMY_{i}").to_seq() for i in range(3))
        sep = TkStr(qbtoken.Punctuation, ",").to_seq()
        expected = (
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "DUMMY_0"),
                    sep[0],
                )
            ),
            TkSeq(
                (
                    TkStr(qbtoken.Keyword, "DUMMY_1"),
                    sep[0],
                )
            ),
            TkSeq((TkStr(qbtoken.Keyword, "DUMMY_2"),)),
        )

        result = self.tk.tokenize_list(sep, lst, accolate=True)

        assert expected == result

    def test_tokenize_empty_list(self):
        lst = ()
        sep = TkStr(qbtoken.Punctuation, ",").to_seq()
        expected = ()

        result = self.tk.tokenize_list(sep, lst, accolate=True)

        assert expected == result


class TestTokenizeFuncsSqlite(TestTokenizeFuncs):
    tk = qb.drivers.sqlite.tokenizer.Tokenizer()
