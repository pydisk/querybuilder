from querybuilder.utils.decorators import TypeDispatch


class TestTypedSingledispatch:
    expected = ["number", "number", "object", "string", "object"]

    def test_typed_singledispatch_of_instance_methods(self):
        class A:
            @TypeDispatch
            def f(self, cls):
                assert isinstance(self, A)
                return "object"

            @f.register(str)
            def _(self, cls):
                assert isinstance(self, A)
                return "string"

            @f.register(int)
            @f.register(float)
            def _(self, cls):
                assert isinstance(self, A)
                return "number"

        assert [
            A().f(int),
            A().f(float),
            A().f(type(None)),
            A().f(str),
            A().f(bytes),
        ] == self.expected
