import pytest

from mock import Mock, create_autospec
import sqlite3

import querybuilder
from querybuilder.queries.dml import Insert
from querybuilder.atoms.relations import Named as NamedRel
from querybuilder.atoms.columns import (
    Placeholder,
    Constant,
    Value,
    Named as NamedCol,
)
from querybuilder.queries.dql import Values
from querybuilder.formatting.tokentree import TkStr
from querybuilder.formatting import token as qbtoken


@pytest.fixture
def raw_query():
    return "QUERY"


@pytest.fixture
def tkseq(raw_query):
    return TkStr(qbtoken.Keyword, raw_query).to_seq()


@pytest.fixture
def mock_cursor():
    cursor = Mock()

    execute = create_autospec(sqlite3.Cursor.execute)
    cursor.attach_mock(execute, "execute")

    executemany = create_autospec(sqlite3.Cursor.executemany)
    cursor.attach_mock(executemany, "executemany")

    def citer(self):
        yield None

    cursor.__iter__ = citer

    return cursor


@pytest.fixture
def mock_tokenizer(tkseq):
    tokenizer = Mock()

    tokenizer.return_value = tkseq

    tokenizer.attach_mock(lambda x: x, "_post")

    tokenizer.transform = lambda x: x

    return tokenizer


@pytest.fixture
def mock_connector(mock_cursor, mock_tokenizer):
    connector = Mock()

    connector.attach_mock(lambda: mock_cursor, "cursor")
    connector.attach_mock(mock_tokenizer, "tokenizer")
    connector.cast_values = lambda x: x
    connector.logger = None

    return connector


@pytest.fixture
def transaction_manager(mock_connector):
    return querybuilder.utils.transaction_manager.TransactionManager(mock_connector)


@pytest.fixture
def make_query():
    return lambda values: Insert(
        NamedRel("foo"), (NamedCol(int, "x"), NamedCol(str, "y")), values
    )


class TestTransactionManager:
    # TODO: properly account for cast_values

    def test_execute_with_placeholder(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Constant(int, 42), Placeholder(str, key="bar")),
            ]
        )

        parameters = dict(bar="some_value")

        transaction_manager.execute(make_query(values), parameters)

        expected_parameters = parameters
        mock_cursor.execute.assert_called_with("QUERY", expected_parameters)

    def test_execute_with_auto_values(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Value(int, 42), Value(str, "val")),
            ]
        )

        transaction_manager.execute(make_query(values))

        expected_parameters = dict(auto0=42, auto1="val")
        mock_cursor.execute.assert_called_with("QUERY", expected_parameters)

    def test_execute_with_keyed_values(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Value(int, 42, key="k"),),
            ]
        )

        transaction_manager.execute(make_query(values))

        expected_parameters = dict(k=42)

        mock_cursor.execute.assert_called_with("QUERY", expected_parameters)

    def test_execute_with_mixed_values(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Value(int, 0), Value(str, "val", key="k"), Value(int, 1)),
            ]
        )

        transaction_manager.execute(make_query(values))

        expected_parameters = dict(k="val", auto0=0, auto1=1)
        mock_cursor.execute.assert_called_with("QUERY", expected_parameters)

    def test_execute_with_mixed_values_and_placeholder(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (
                    Value(int, 0),
                    Value(str, "val", key="k"),
                    Value(int, 1),
                    Placeholder(int, "ph"),
                )
            ]
        )

        transaction_manager.execute(make_query(values), dict(ph=42))

        expected_parameters = dict(k="val", auto0=0, auto1=1, ph=42)
        mock_cursor.execute.assert_called_with("QUERY", expected_parameters)

    def test_execute_ignores_irrelevant_parameters(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (
                    Value(int, 0),
                    Value(str, "val", key="k"),
                    Placeholder(int, "ph"),
                )
            ]
        )

        transaction_manager.execute(make_query(values), dict(ph=42, foo=42, bar="1337"))

        expected_parameters = dict(k="val", auto0=0, ph=42)
        mock_cursor.execute.assert_called_with("QUERY", expected_parameters)

    def assert_called_executemany_with(
        self, mock_cursor, expected_query, expected_parameters
    ):
        mock_cursor.executemany.assert_called_once()
        actual_query, actual_parameters = mock_cursor.executemany.call_args.args
        assert expected_query == actual_query
        assert list(actual_parameters) == expected_parameters

    def test_executemany_with_placeholder(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Constant(int, 42), Placeholder(str, key="bar")),
            ]
        )

        parameters = [dict(bar=f"some_value_{i}") for i in range(10)]

        transaction_manager.executemany(make_query(values), parameters)

        expected_parameters = parameters
        self.assert_called_executemany_with(mock_cursor, "QUERY", expected_parameters)

    def test_executemany_with_auto_values(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Value(int, 42), Value(str, "val")),
            ]
        )

        transaction_manager.executemany(make_query(values), [dict() for i in range(4)])

        expected_parameters = [dict(auto0=42, auto1="val") for i in range(4)]
        self.assert_called_executemany_with(mock_cursor, "QUERY", expected_parameters)

    def test_executemany_with_keyed_values(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Value(int, 42, key="k"),),
            ]
        )

        transaction_manager.executemany(make_query(values), [dict() for i in range(4)])

        expected_parameters = [dict(k=42) for i in range(4)]

        self.assert_called_executemany_with(mock_cursor, "QUERY", expected_parameters)

    def test_executemany_with_mixed_values(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (Value(int, 0), Value(str, "val", key="k"), Value(int, 1)),
            ]
        )

        transaction_manager.executemany(make_query(values), [dict() for i in range(4)])

        expected_parameters = [dict(k="val", auto0=0, auto1=1) for i in range(4)]
        self.assert_called_executemany_with(mock_cursor, "QUERY", expected_parameters)

    def test_executemany_with_mixed_values_and_placeholder(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (
                    Value(int, 0),
                    Value(str, "val", key="k"),
                    Value(int, 1),
                    Placeholder(int, "ph"),
                )
            ]
        )

        transaction_manager.executemany(
            make_query(values), [dict(ph=i) for i in range(4)]
        )

        expected_parameters = [dict(k="val", auto0=0, auto1=1, ph=i) for i in range(4)]
        self.assert_called_executemany_with(mock_cursor, "QUERY", expected_parameters)

    def test_executemany_ignores_irrelevant_parameters(
        self, transaction_manager, raw_query, make_query, mock_cursor
    ):
        values = Values(
            [
                (
                    Value(int, 0),
                    Value(str, "val", key="k"),
                    Placeholder(int, "ph"),
                )
            ]
        )

        transaction_manager.executemany(
            make_query(values), [dict(ph=i, foo=2 * i, bar=f"foo{i}") for i in range(4)]
        )

        expected_parameters = [dict(k="val", auto0=0, ph=i) for i in range(4)]
        self.assert_called_executemany_with(mock_cursor, "QUERY", expected_parameters)
