import pytest
from querybuilder.utils.stores import (
    KeyedTuple,
    Frozenmap,
)


class TestKeyedTuple:
    @pytest.fixture
    def simple_keyed_store(self):
        # setup
        class KeyedStore(KeyedTuple[str]):
            _get_key = staticmethod("_".__add__)

        return KeyedStore

    def test_KeyedTuple_as_tuple(self, simple_keyed_store):
        tuple1 = ("a", "b", "c", "d")
        store1 = simple_keyed_store(tuple1)
        assert tuple1 == store1
        assert isinstance(store1, tuple)

        assert (
            store1[:]
            == store1
            == store1[0:]
            == store1[: len(store1)]
            == store1[0 : len(store1)]
        )
        assert store1[list(range(len(store1)))] == list(store1)
        assert store1[list(reversed(range(len(store1))))] == list(reversed(store1))

    @pytest.mark.parametrize(
        "slicing, expected",
        [
            (slice(1, None), "b--c--d"),
            (slice(1, 3), "b--c"),
            (slice(3, 1, -1), "d--c"),
        ],
    )
    def test_KeyedTuple_slicing(self, simple_keyed_store, slicing, expected):
        tuple1 = ("a", "b", "c", "d")
        store1 = simple_keyed_store(tuple1)
        assert "--".join(store1[slicing]) == expected

    def test_KeyedTuple_keys_map(self, simple_keyed_store):
        tuple1 = ("a", "b", "c", "d")
        store1 = simple_keyed_store(tuple1)

        assert store1._keys.keys() == {f"_{e}" for e in tuple1}
        for k, v in store1._keys.items():
            assert len(v) == 1
            v = v[0]
            assert hasattr(store1, k)
            assert getattr(store1, k) == v
            assert k == store1._get_key(v)
            assert k in store1._
            assert store1._[k] == v

    @pytest.mark.parametrize(
        "indexes",
        [
            [0, 1, 2, 3],
            [0, 1, 0, 2, 0, 3, 0],
            ["_a", "_b", "_c", "_d"],
            ["_a", "_b", "_a", "_c", "_a", "_d", "_a"],
        ],
    )
    def test_KeyedTuple_getitem_list(self, simple_keyed_store, indexes):
        tuple1 = ("a", "b", "c", "d")
        store1 = simple_keyed_store(tuple1)

        res = store1[indexes]
        assert isinstance(res, list)
        assert res == [store1.resolve(i) for i in indexes]


class TestFrozenmap:
    def _check_equality_to_dict(self, f, d):
        assert d == f == dict(f)
        assert bool(f) == bool(d)
        assert len(f) == len(d)
        for attr in ("keys", "values", "items"):
            assert tuple(getattr(f, attr)()) == tuple(getattr(d, attr)())

    def test_initialized_from_nothing(self):
        d = {}
        f = Frozenmap()
        self._check_equality_to_dict(f, d)

    def test_initialized_from_mapping(self):
        d = {0: "zero", "zero": 0, None: False, True: ("t", "r", "u", "e")}
        f = Frozenmap(d)
        self._check_equality_to_dict(f, d)

    def test_initialized_from_iterable(self):
        d = {0: "zero", "zero": 0, None: False, True: ("t", "r", "u", "e")}
        i = d.items()
        f = Frozenmap(i)
        self._check_equality_to_dict(f, d)

    def test_or_with_frozenmap(self):
        d0 = {0: "zero", "zero": 0, None: False}
        d1 = {0: "ZERO", True: ("t", "r", "u", "e")}
        d = d0 | d1
        f = Frozenmap(d0) | Frozenmap(d1)
        self._check_equality_to_dict(f, d)

    def test_or_with_dict(self):
        d0 = {0: "zero", "zero": 0, None: False}
        d1 = {0: "ZERO", True: ("t", "r", "u", "e")}
        d = d0 | d1
        f = Frozenmap(d0) | d1
        self._check_equality_to_dict(f, d)

    def test_hash(self):
        d0 = {0: "zero", "zero": 0, None: False, True: ("t", "r", "u", "e")}
        f0 = Frozenmap(d0)
        d1 = {None: False, True: tuple("true"), 0: "zero", "zero": 0}
        f1 = Frozenmap(d1)
        assert hash(f0) == hash(f1)
        assert f0 == f1
