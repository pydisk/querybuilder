import pytest
from mock import Mock
from querybuilder.helpers import make_column
from querybuilder.queries.ddl import (
    Drop,
    CreateIndex,
    CreateTable,
    CreateView,
    CreateSchema,
)
import querybuilder.atoms.relations as qbrelations
import querybuilder.queries.dql as qbdql
import querybuilder.atoms.schemas as qbschemas
from querybuilder.tests.utils import create_subtokenizable_mock


class TestDrop:
    def test_get_subtokenize_kwargs_with_if_exists_and_cascade(self, mocker):
        target_type = "TABLE"
        table = "table"
        if_exists = "if_exists"
        cascade = "cascade"
        create_subtokenizable_mock(mocker, "querybuilder.atoms.relations.Named", table)
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.IfExistsWrapper", if_exists
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.DropCascadeClause", cascade
        )
        create_subtokenizable_mock(
            mocker,
            "querybuilder.atoms.clauses.DBObjectNameWrapper",
            target_type,
        )

        drop = Drop(qbrelations.Named("rel"), if_exists=True, cascade=True)

        expected = {
            "name": table,
            "if_exists": if_exists,
            "cascade": cascade,
            "target_type": target_type,
        }

        result = drop._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs_with_if_exists(self, mocker):
        target_type = "TABLE"
        table = "table"
        if_exists = "if_exists"
        create_subtokenizable_mock(mocker, "querybuilder.atoms.relations.Named", table)
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.IfExistsWrapper", if_exists
        )
        create_subtokenizable_mock(
            mocker,
            "querybuilder.atoms.clauses.DBObjectNameWrapper",
            target_type,
        )

        drop = Drop(qbrelations.Named("rel"), if_exists=True)

        expected = {"name": table, "if_exists": if_exists, "target_type": target_type}

        result = drop._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs_with_cascade(self, mocker):
        target_type = "TABLE"
        table = "table"
        cascade = "cascade"
        create_subtokenizable_mock(mocker, "querybuilder.atoms.relations.Named", table)

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.DropCascadeClause", cascade
        )
        create_subtokenizable_mock(
            mocker,
            "querybuilder.atoms.clauses.DBObjectNameWrapper",
            target_type,
        )

        drop = Drop(qbrelations.Named("rel"), if_exists=False, cascade=True)

        expected = {"name": table, "cascade": cascade, "target_type": target_type}

        result = drop._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs(self, mocker):
        target_type = "TABLE"
        table = "table"
        create_subtokenizable_mock(mocker, "querybuilder.atoms.relations.Named", table)
        create_subtokenizable_mock(
            mocker,
            "querybuilder.atoms.clauses.DBObjectNameWrapper",
            target_type,
        )

        drop = Drop(qbrelations.Named("rel"), if_exists=False)

        expected = {"name": table, "target_type": target_type}

        result = drop._get_subtokenize_kwargs(None)

        assert expected == result

    def test_substitute(self):
        pre_schema = qbschemas.Schema("foo")
        post_schema = qbschemas.Schema("bar")

        pre_drop = Drop(pre_schema)

        subs = {pre_schema: post_schema}
        post_drop = pre_drop.substitute(subs)

        assert post_schema == post_drop.target


class TestCreateIndex:
    @pytest.fixture
    def mock_index_kwargs(self):
        return dict(arg0="foo", arg1="bar")

    @pytest.fixture
    def mock_index_subtoken(self):
        return "my_index"

    @pytest.fixture
    def mock_index(self, mocker, mock_index_subtoken, mock_index_kwargs):
        patcher = mocker.patch("querybuilder.atoms.schemas.Index")
        instance = patcher.return_value
        instance.subtokenize = Mock(return_value=mock_index_subtoken)
        instance._get_creation_kwargs = Mock(return_value=mock_index_kwargs)

    def test_get_subtokenize_kwargs_with_if__not_exists(
        self, mocker, mock_index, mock_index_subtoken, mock_index_kwargs
    ):
        if_not_exists = "if_not_exists"
        create_subtokenizable_mock(
            mocker,
            "querybuilder.atoms.clauses.IfNotExistsWrapper",
            if_not_exists,
        )

        create = CreateIndex(qbschemas.Index(), if_not_exists=True)

        expected = {
            "name": mock_index_subtoken,
            "if_not_exists": if_not_exists,
        }
        expected.update(mock_index_kwargs)

        result = create._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs(
        self, mocker, mock_index, mock_index_subtoken, mock_index_kwargs
    ):
        create = CreateIndex(qbschemas.Index())

        expected = {
            "name": mock_index_subtoken,
        }
        expected.update(mock_index_kwargs)

        result = create._get_subtokenize_kwargs(None)

        assert expected == result

    def test_substitute(self):
        rel = qbrelations.Named("foo")
        pre_index = qbschemas.Index("foo", rel, ())
        post_index = qbschemas.Index("bar", rel, ())

        pre_create = CreateIndex(pre_index)

        subs = {pre_index: post_index}
        post_create = pre_create.substitute(subs)

        assert post_index == post_create.target


class TestCreateTable:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_table = qbrelations.Table("foo", (pre_col,))
        post_table = qbrelations.Table("bar", (pre_col,))

        pre_query = qbdql.Select((pre_col, pre_col))
        post_query = qbdql.Select((pre_col, post_col))

        pre_create = CreateTable(pre_table, as_query=pre_query)

        subs = {pre_query: post_query, pre_table: post_table}
        post_create = pre_create.substitute(subs)

        assert post_query == post_create.as_query
        assert post_table == post_create.target

    def test_substitute_without_as_query(self):
        pre_col = make_column(1)
        make_column(2)

        pre_table = qbrelations.Table("foo", (pre_col,))
        post_table = qbrelations.Table("bar", (pre_col,))

        pre_create = CreateTable(pre_table)

        subs = {pre_table: post_table}
        post_create = pre_create.substitute(subs)

        assert post_table == post_create.target


class TestCreateView:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = qbdql.Select((pre_col, pre_col))
        post_query = qbdql.Select((pre_col, post_col))

        pre_view = qbrelations.View("foo", pre_query)
        post_view = qbrelations.View("bar", pre_query)

        pre_create = CreateView(pre_view)

        subs = {pre_query: post_query, pre_view: post_view}
        post_create = pre_create.substitute(subs)

        assert post_view == post_create.target


class TestCreateSchema:
    def test_substitute(self):
        pre_schema = qbschemas.Schema("foo")
        post_schema = qbschemas.Schema("bar")

        pre_create = CreateSchema(pre_schema)

        subs = {pre_schema: post_schema}
        post_create = pre_create.substitute(subs)

        assert post_schema == post_create.target
