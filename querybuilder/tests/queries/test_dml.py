from querybuilder.helpers import make_column
from querybuilder.queries.dml import Insert, Delete, Update
import querybuilder.atoms.relations as qbrelations
import querybuilder.atoms.columns as qbcolumns
import querybuilder.queries.dql as qbdql
from querybuilder.tests.utils import create_subtokenizable_mock
import querybuilder


class TestInsert:
    def test_get_subtokenize_kwargs_with_column_without_query(self, mocker):
        relation = "relation"
        column = "column"
        query = "default values"
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.DefaultValuesClause", query
        )

        insert = Insert(
            qbrelations.Named("rel"), in_columns=(qbcolumns.Named(int, "col"),)
        )

        expected = {
            "query": query,
            "schema_relation": relation,
            "in_columns": (column,),
        }

        result = insert._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs_with_columns_and_query(self, mocker):
        relation = "relation"
        column = "column"
        query = "select ..."
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.AliasedColumn", column
        )
        create_subtokenizable_mock(mocker, "querybuilder.queries.dql.Select", query)

        insert = Insert(
            qbrelations.Named("rel"),
            (qbcolumns.Named(int, "col"), qbcolumns.Named(int, "col")),
            qbdql.Select(),
        )

        expected = {
            "query": query,
            "schema_relation": relation,
            "in_columns": (column, column),
        }

        result = insert._get_subtokenize_kwargs(None)

        assert expected == result

    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = qbdql.Select((pre_col, pre_col))
        post_query = qbdql.Select((pre_col, post_col))

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_insert = Insert(pre_rel, (pre_col, post_col), pre_query)

        subs = {pre_query: post_query, pre_col: post_col, pre_rel: post_rel}
        post_insert = pre_insert.substitute(subs)

        assert post_query == post_insert.query
        assert (post_col, post_col) == post_insert.in_columns
        assert post_rel == post_insert.schema_relation

    def test_substitute_without_query(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_insert = Insert(pre_rel, (pre_col, post_col))

        subs = {pre_col: post_col, pre_rel: post_rel}
        post_insert = pre_insert.substitute(subs)

        assert (post_col, post_col) == post_insert.in_columns
        assert post_rel == post_insert.schema_relation


class TestDelete:
    def test_get_subtokenize_kwargs_with_where(self, mocker):
        relation = "relation"
        column = "column"
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.WhereColumn", column
        )

        delete = Delete(qbrelations.Named("rel"), qbcolumns.Named(int, "col").eq(3))

        expected = {
            "schema_relation": relation,
            "where": column,
        }

        result = delete._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs_without_where(self, mocker):
        relation = "relation"
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )

        delete = Delete(qbrelations.Named("rel"))

        expected = {
            "schema_relation": relation,
        }

        result = delete._get_subtokenize_kwargs(None)

        assert expected == result

    def test_substitute_deep_with_where(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_delete = Delete(pre_rel, where=pre_col)

        subs = {pre_col: post_col, pre_rel: post_rel}
        post_delete = pre_delete.substitute(subs)

        assert post_col == post_delete.where
        assert post_rel == post_delete.schema_relation

    def test_substitute_deep_whithout_where(self):
        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_delete = Delete(pre_rel)

        subs = {pre_rel: post_rel}
        post_delete = pre_delete.substitute(subs)

        assert post_rel == post_delete.schema_relation


class TestUpdate:
    def test_get_subtokenize_kwargs_with_where(self, mocker):
        relation = "relation"
        column = "column"
        set_columns = "SET ..."
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.WhereColumn", column
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.SetColumnsClause", set_columns
        )

        mapping = {qbcolumns.Named(int, "x"): qbcolumns.Constant(int, 3)}
        update = Update(
            qbrelations.Named("rel"), mapping, qbcolumns.Named(int, "col").eq(3)
        )

        expected = {
            "schema_relation": relation,
            "where": column,
            "set_columns": set_columns,
        }

        result = update._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_subtokenize_kwargs_without_where(self, mocker):
        relation = "relation"
        set_columns = "SET ..."
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.SetColumnsClause", set_columns
        )

        mapping = {qbcolumns.Named(int, "x"): qbcolumns.Constant(int, 3)}
        update = Update(qbrelations.Named("rel"), mapping)

        expected = {"schema_relation": relation, "set_columns": set_columns}

        result = update._get_subtokenize_kwargs(None)

        assert expected == result

    def test_delete_get_subtokenize_kwargs_with_where(self, mocker):
        relation = "relation"
        where = "where ..."
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.WhereColumn", where
        )

        delete = Delete(
            qbrelations.Named("rel"),
            qbcolumns.Named(int, "col").eq(qbcolumns.Named(int, "col")),
        )

        expected = {
            "schema_relation": relation,
            "where": where,
        }

        result = delete._get_subtokenize_kwargs(None)

        assert expected == result

    def test_delete_get_subtokenize_kwargs_without_where(self, mocker):
        relation = "relation"
        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.relations.Named", relation
        )

        delete = Delete(qbrelations.Named("rel"))

        expected = {
            "schema_relation": relation,
        }

        result = delete._get_subtokenize_kwargs(None)

        assert expected == result

    def test_get_placeholders_without_placeholders_in_query(self, person_table):
        query = Update(person_table, {person_table.c[0]: qbcolumns.Constant(int, 42)})

        phs = query.get_placeholders()

        assert isinstance(phs, querybuilder.visitors.stores.PlaceholderStore)
        assert 0 == len(phs)

    def test_get_placeholders_with_value_in_query(self, person_table):
        val_col = qbcolumns.Value(str, "foo")
        query = Update(person_table, {person_table.c[0]: val_col})

        phs = query.get_placeholders()
        expected_phs = [val_col]

        assert isinstance(phs, querybuilder.visitors.stores.PlaceholderStore)
        assert expected_phs == list(phs)

    def test_get_placeholders_with_value_in_where(self, person_table):
        val_col = qbcolumns.Value(str, "foo")
        query = Update(
            person_table,
            {person_table.c[1]: qbcolumns.Constant(int, 123)},
            where=person_table.c[0].eq(val_col),
        )

        phs = query.get_placeholders()
        expected_phs = [val_col]

        assert isinstance(phs, querybuilder.visitors.stores.PlaceholderStore)
        assert expected_phs == list(phs)

    def test_get_placeholders_with_value_in_where_and_set_column(self, person_table):
        val_col = qbcolumns.Value(int, 123)
        val_col2 = qbcolumns.Value(str, "foo")
        query = Update(
            person_table,
            {person_table.c[1]: val_col},
            where=person_table.c[0].eq(val_col2),
        )

        phs = query.get_placeholders()
        expected_phs = [val_col, val_col2]

        assert isinstance(phs, querybuilder.visitors.stores.PlaceholderStore)
        assert expected_phs == list(phs)

    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_map = {pre_col: post_col}

        pre_update = Update(pre_rel, pre_map, where=pre_col)

        subs = {pre_col: post_col, post_col: pre_col, pre_rel: post_rel}
        post_update = pre_update.substitute(subs)

        # columns are swapped in the map due to first 2 items in the substitutions
        expected_map = {post_col: pre_col}

        assert post_col == post_update.where
        assert post_rel == post_update.schema_relation
        assert expected_map == post_update.set_columns

    def test_substitute_without_where(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_map = {pre_col: post_col}

        pre_update = Update(pre_rel, pre_map)

        subs = {pre_col: post_col, post_col: pre_col, pre_rel: post_rel}
        post_update = pre_update.substitute(subs)

        expected_map = {post_col: pre_col}

        assert post_rel == post_update.schema_relation
        assert expected_map == post_update.set_columns
