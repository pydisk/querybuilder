import pytest
from mock import Mock
from querybuilder.tests.utils import create_subtokenizable_mock

from querybuilder.helpers import make_column
from querybuilder.utils.constants import SetCombinator
import querybuilder.atoms.pseudo_columns as qbpseudo_columns
import querybuilder.atoms.columns as qbcolumns
import querybuilder.atoms.relations as qbrelations
import querybuilder.queries.dql as dql
import querybuilder as qb


class TestDQL:
    def get_dql_queries(self):
        return ()

    @pytest.mark.parametrize("materialized", [True, False, None])
    def test_as_with_no_column_aliases(self, materialized):
        for q in self.get_dql_queries():
            wq = q.as_with("foo", materialized=materialized)
            assert wq.name == "foo"
            assert wq.subrelation == q
            assert wq.materialized == materialized
            # assert not q.aliases

    def test_with_query_selectable(self):
        for q in self.get_dql_queries():
            wq = q.as_with("foo")
            wq.select()
            assert wq.arity == q.arity


class TestSelect(TestDQL):
    def get_dql_queries(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        yield dql.Select(columns)

    def test_set_columns_unaliased_columns_with_list_of_integers(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = proj_indices

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_columns_unaliased_columns_with_list_of_strings(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = [f"c{i}" for i in proj_indices]

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_columns_unaliased_columns_with_list_of_columns(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_columns_unaliased_columns_with_list_of_mixed_specifications(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = [
            0,
            "c4",
            qbcolumns.Named(int, "c2"),
            columns[0],
            qbcolumns.Constant(int, 3),
        ]

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_columns.append(qbcolumns.Constant(int, 3))

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_columns_aliased_columns_with_list_of_integers(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_params = proj_indices

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_aliases = {0: "a0", 1: "a4", 3: "a0"}

        assert expected_columns == list(proj.selected_columns)
        assert expected_aliases == proj.aliases

    def test_set_columns_aliased_columns_with_list_of_strings(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_params = ["a0", "a4", "c2", "a0"]

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_aliases = {0: "a0", 1: "a4", 3: "a0"}

        assert expected_columns == list(proj.selected_columns)
        assert expected_aliases == proj.aliases

    def test_set_columns_aliased_columns_with_list_of_columns(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_names = ["a0", "a4", "c2", "a0"]
        proj_params = [qbcolumns.Named(int, n) for n in proj_names]

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_aliases = {0: "a0", 1: "a4", 3: "a0"}

        assert expected_columns == list(proj.selected_columns)
        assert expected_aliases == proj.aliases

    def test_set_columns_aliased_columns_with_list_of_mixed_specifications(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_params = [qbcolumns.Named(int, "a0"), 4, "c2", "a0"]
        proj_params.append(qbcolumns.Constant(int, 3))

        proj = sel.set_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_columns.append(qbcolumns.Constant(int, 3))
        expected_aliases = {0: "a0", 1: "a4", 3: "a0"}

        assert expected_columns == list(proj.selected_columns)
        assert expected_aliases == proj.aliases

    def test_set_selected_columns_unaliased_columns_with_list_of_integers(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = proj_indices

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_selected_columns_unaliased_columns_with_list_of_strings(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = [f"c{i}" for i in proj_indices]

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_selected_columns_unaliased_columns_with_list_of_columns(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_selected_columns_unaliased_columns_with_list_of_mixed_specifications(
        self,
    ):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns)
        proj_indices = [0, 4, 2, 0]
        proj_params = [
            0,
            "c4",
            qbcolumns.Named(int, "c2"),
            columns[0],
            qbcolumns.Constant(int, 3),
        ]

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_columns.append(qbcolumns.Constant(int, 3))

        assert expected_columns == list(proj.selected_columns)
        assert {} == proj.aliases

    def test_set_selected_columns_aliased_columns_with_list_of_integers(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_params = proj_indices

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert not proj.aliases

    def test_set_selected_columns_aliased_columns_with_list_of_strings(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_params = ["c0", "c4", "c2", "c0"]

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert not proj.aliases

    def test_set_selected_columns_aliased_columns_with_list_of_columns(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_names = ["c0", "c4", "c2", "c0"]
        proj_params = [qbcolumns.Named(int, n) for n in proj_names]

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]

        assert expected_columns == list(proj.selected_columns)
        assert not proj.aliases

    def test_set_selected_columns_aliased_columns_with_list_of_mixed_specifications(
        self,
    ):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 1: "a1", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)
        proj_indices = [0, 4, 2, 0]
        proj_params = [qbcolumns.Named(int, "c0"), 4, "c2", "c0"]
        proj_params.append(qbcolumns.Constant(int, 3))

        proj = sel.set_selected_columns(proj_params)

        expected_columns = [qbcolumns.Named(int, f"c{i}") for i in proj_indices]
        expected_columns.append(qbcolumns.Constant(int, 3))

        assert expected_columns == list(proj.selected_columns)
        assert not proj.aliases

    def test_column_property(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        aliases = {0: "a0", 3: "a3", 4: "a4"}
        sel = dql.Select(columns, aliases)

        expected_names = ["a0", "c1", "c2", "a3", "a4"]
        for i, c in enumerate(sel.columns):
            assert expected_names[i] == c.name

    def test_column_property_with_star(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(4)]
        aliases = {1: "a0"}

        rel_1 = qbrelations.Named("foo", columns=columns[:2])
        rel_2 = qbrelations.Named("bar", columns=columns[2:])

        sel = dql.Select(
            (qbpseudo_columns.Star(), columns[0]), aliases, from_=rel_1.product(rel_2)
        )

        expected_names = ["c0", "c1", "c2", "c3", "a0"]
        for i, c in enumerate(sel.columns):
            assert expected_names[i] == c.name

    def test_column_property_with_relation_star(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(4)]
        aliases = {1: "a0"}

        rel_1 = qbrelations.Named("foo", columns=columns[:2])
        rel_2 = qbrelations.Named("bar", columns=columns[2:])

        sel = dql.Select(
            (qbpseudo_columns.Star(relation=rel_2), columns[0]),
            aliases,
            from_=rel_1.product(rel_2),
        )

        expected_names = ["c2", "c3", "a0"]
        for i, c in enumerate(sel.columns):
            assert expected_names[i] == c.name

    def test_orderby(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns, orderby=columns[2])
        assert sel.orderby == columns[2]

    def test_set_order(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns).set_order(columns[2])
        assert sel.orderby == columns[2]

    def test_set_order_to_None(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns, orderby=columns[2]).set_order(None)
        assert sel.orderby is None

    def test_aliases_given_at_initialization(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns, aliases={0: "foo"})
        assert sel.columns[0].name == "foo"

    def test_aliases_ignored(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns, aliases={0: None, 5: "foo"})
        assert sel.columns[0].name == "c0"

    def test_set_aliases(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns, aliases={0: "foo"}).set_aliases({})
        assert sel.columns[0].name == "c0"
        sel = dql.Select(columns, aliases={0: "foo"}).set_aliases({0: "bar"})
        assert sel.columns[0].name == "bar"
        sel = dql.Select(columns, aliases={0: "foo"}).set_aliases({1: "bar"})
        assert sel.columns[0].name == "c0"
        assert sel.columns[1].name == "bar"

    def test_update_aliases(self):
        columns = [qbcolumns.Named(int, f"c{i}") for i in range(5)]
        sel = dql.Select(columns, aliases={0: "foo"}).update_aliases({})
        assert sel.columns[0].name == "foo"
        sel = dql.Select(columns, aliases={0: "foo"}).update_aliases({0: "bar"})
        assert sel.columns[0].name == "bar"
        sel = dql.Select(columns, aliases={0: "foo"}).update_aliases({1: "bar"})
        assert sel.columns[0].name == "foo"
        assert sel.columns[1].name == "bar"
        sel = dql.Select(columns, aliases={0: "foo"}).update_aliases(
            {0: "doo", 1: "bar"}
        )
        assert sel.columns[0].name == "doo"
        assert sel.columns[1].name == "bar"

    def test_substitute_in_columns(self):
        pre_col = make_column(1)
        post_col = make_column(2)
        other_col = make_column("foo", constant=True)

        pre_query = dql.Select((other_col, pre_col, other_col))

        post_query = pre_query.substitute({pre_col: post_col})
        expected_columns = [other_col, post_col, other_col]

        assert expected_columns == list(post_query.columns)

    def test_substitute_in_where(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((), where=pre_col)

        post_query = pre_query.substitute({pre_col: post_col})

        assert post_col == post_query.where

    def test_substitute_in_distinct(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((), distinct=pre_col)

        post_query = pre_query.substitute({pre_col: post_col})

        assert post_col == post_query.distinct

    def test_substitute_in_groupby(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((), groupby=pre_col)

        post_query = pre_query.substitute({pre_col: post_col})

        assert post_col == post_query.groupby

    def test_substitute_in_orderby(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((), orderby=pre_col)

        post_query = pre_query.substitute({pre_col: post_col})

        assert post_col == post_query.orderby

    def test_substitute_in_having(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((), having=pre_col)

        post_query = pre_query.substitute({pre_col: post_col})

        assert post_col == post_query.having

    def test_substitute_in_from(self):
        pre_rel = qbrelations.Named("foo")
        post_rel = qbrelations.Named("bar")

        pre_query = dql.Select((), from_=pre_rel)

        post_query = pre_query.substitute({pre_rel: post_rel})

        assert post_rel == post_query.from_

    def test_aliasing_star_raises_exception(self):
        with pytest.raises(KeyError):
            dql.Select(
                (
                    qbpseudo_columns.Star(),
                    qbcolumns.Named(int, "foo"),
                    qbpseudo_columns.Star(),
                ),
                aliases={2: "bar"},
            )

    def test_cannot_init_select_with_star_without_from(self):
        with pytest.raises(AssertionError):
            dql.Select(
                (
                    qbpseudo_columns.Star(),
                    qbcolumns.Named(int, "foo"),
                    qbpseudo_columns.Star(),
                ),
            )

    def test_cannot_set_selected_columns_with_star_without_from(self):
        select = dql.Select((make_column(1), make_column(2)))

        with pytest.raises(AssertionError):
            select.set_selected_columns((qbpseudo_columns.Star(),))

    def test_setting_alias_of_star_raises_exception(self):
        select = dql.Select(
            (
                qbpseudo_columns.Star(),
                qbcolumns.Named(int, "foo"),
                qbpseudo_columns.Star(),
            ),
            from_=qbrelations.Named("foo"),
        )

        with pytest.raises(KeyError):
            select.set_aliases({0: "foo"})

    def test_updating_alias_of_star_raises_exception(self):
        select = dql.Select(
            (
                qbpseudo_columns.Star(),
                qbcolumns.Named(int, "foo"),
                qbpseudo_columns.Star(),
            ),
            from_=qbrelations.Named("foo"),
        )
        with pytest.raises(KeyError):
            select.update_aliases({0: "foo"})

    @pytest.mark.parametrize("aliases", [{}, {0: "bar"}])
    def test_set_column_to_star_raises_exception(self, aliases):
        select = dql.Select(
            (qbcolumns.Named(int, "foo"),),
            aliases={0: "bar"},
            from_=qbrelations.Named("foo"),
        )

        with pytest.raises(AssertionError):
            select.set_columns((qbpseudo_columns.Star(),))

    def test_set_selected_column_to_star_with_alias(self):
        # checks that the alias is dropped before setting the column

        from_columns = (make_column(1), make_column(2))

        select = dql.Select(
            from_columns[0:],
            aliases={0: "bar"},
            from_=qbrelations.Named("foo", columns=from_columns),
        )

        select = select.set_selected_columns(
            (qbpseudo_columns.Star(),),
        )

        assert 2 == len(select.columns)
        assert from_columns == tuple(select.columns)


class TestValues(TestDQL):
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)
        other_col = make_column("foo", constant=True)

        pre_query = dql.Values([(other_col, pre_col, other_col)], orderby=pre_col)

        post_query = pre_query.substitute({pre_col: post_col})
        expected_columns = [(other_col, post_col, other_col)]

        assert expected_columns == list(post_query.values)
        assert post_col == post_query.orderby

    def test_substitute_without_orderby(self):
        pre_col = make_column(1)
        post_col = make_column(2)
        other_col = make_column("foo", constant=True)

        pre_query = dql.Values([(other_col, pre_col, other_col)])

        post_query = pre_query.substitute({pre_col: post_col})
        expected_columns = [(other_col, post_col, other_col)]

        assert expected_columns == list(post_query.values)


class TestSetCombination(TestDQL):
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((pre_col, pre_col))
        post_query = dql.Select((pre_col, post_col))

        combinator = qb.utils.constants.SetCombinator.UNION
        pre_comb = dql.SetCombination(combinator, pre_query, post_query)

        post_comb = pre_comb.substitute({pre_query: post_query})

        assert post_query == post_comb.left
        assert post_query == post_comb.right

    @pytest.mark.parametrize(
        "combinator, all", [(x, y) for x in SetCombinator for y in [None, True, False]]
    )
    def test_simple_set_combination(self, combinator, all):
        left = dql.Select([make_column(0), make_column(1)])
        right = dql.Select([make_column(1), make_column(2)])
        q = dql.SetCombination(combinator, left, right, all=all)
        assert q.combinator == combinator
        assert q.arity == left.arity == right.arity
        assert q.left == left
        assert q.right == right
        assert q.all == all

    @pytest.mark.parametrize(
        "l_cls, r_cls, l_scope, r_scope",
        [
            (qb.queries.dql.SetCombination, qb.queries.dql.SetCombination, True, True),
            (qb.queries.dql.SetCombination, qb.queries.dql.Select, True, False),
            (qb.queries.dql.Select, qb.queries.dql.SetCombination, False, True),
            (qb.queries.dql.Select, qb.queries.dql.Select, False, False),
        ],
    )
    def test_get_subtokenize_kwargs(self, mocker, l_cls, r_cls, l_scope, r_scope):
        left_tok = "left"
        right_tok = "right"
        comb_tok = "comb"

        comb = qb.utils.constants.SetCombinator.UNION
        all = True

        left = Mock()
        left.__class__ = l_cls
        left.subtokenize = Mock(return_value=left_tok)
        left.arity = 2
        left.columns = (Mock(), Mock())

        right = Mock()
        right.__class__ = r_cls
        right.subtokenize = Mock(return_value=right_tok)
        right.arity = 2
        right.columns = (Mock(), Mock())

        tokenizer = Mock()

        create_subtokenizable_mock(
            mocker, "querybuilder.atoms.clauses.SetCombinatorWrapper", comb_tok
        )

        rel = qb.queries.dql.SetCombination(comb, left, right, all=all)

        expected = {
            "subrelations": (left_tok, right_tok),
            "combinator": comb_tok,
        }

        result = rel._get_subtokenize_kwargs(tokenizer)

        assert expected == result

        left.subtokenize.assert_called_once_with(tokenizer, scoped=l_scope)
        right.subtokenize.assert_called_once_with(tokenizer, scoped=r_scope)

        SCW = qb.atoms.clauses.SetCombinatorWrapper
        SCW.assert_called_with(comb, all=all)


class TestWithClosure:
    def test_substitute(self):
        pre_col = make_column(1)
        post_col = make_column(2)

        pre_query = dql.Select((pre_col, pre_col))
        post_query = dql.Select((pre_col, post_col))

        pre_with = qbrelations.With(pre_query, "foo")
        post_with = qbrelations.With(pre_query, "bar")

        pre_closure = dql.WithClosure(pre_query, pre_with)

        subs = {pre_query: post_query, pre_with: post_with}
        post_closure = pre_closure.substitute(subs)

        assert post_query == post_closure.query
        assert (post_with,) == post_closure.with_relations
