import querybuilder.formatting.token as qbtoken
from querybuilder.formatting.tokentree import TkStr
import querybuilder.formatting.preformatter as pf


class TestPreformatter:
    def test_append_to_tree_concatenates_sequences(self):
        seqfoo = TkStr(qbtoken.Name, "FOO").to_seq()
        seqbar = TkStr(qbtoken.Name, "BAR").to_seq()
        result = pf.append_to_tree(seqfoo, seqbar)
        expected = seqfoo + seqbar

        assert expected == result

    def test_append_to_tree_concatenates_to_right_of_right_most_leaf(self):
        seqfoo = TkStr(qbtoken.Name, "FOO").to_seq()
        seqbar = TkStr(qbtoken.Name, "BAR").to_seq()
        seqno = TkStr(qbtoken.Name, "NO").to_seq()

        tree = ((seqbar, seqno, seqno), seqno)
        result = pf.append_to_tree(tree, seqfoo, to_left=True)
        expected = ((seqfoo + seqbar, seqno, seqno), seqno)

        assert expected == result

    def test_append_to_tree_concatenates_to_left_of_left_most_leaf(self):
        seqfoo = TkStr(qbtoken.Name, "FOO").to_seq()
        seqbar = TkStr(qbtoken.Name, "BAR").to_seq()
        seqno = TkStr(qbtoken.Name, "NO").to_seq()

        tree = ((seqno, seqno), seqfoo)
        result = pf.append_to_tree(tree, seqbar)
        expected = ((seqno, seqno), seqfoo + seqbar)

        assert expected == result
