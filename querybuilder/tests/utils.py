from pytest_mock import MockerFixture
from mock import Mock
from typing import Any


def create_subtokenizable_mock(
    mocker: MockerFixture, name: str, output_value: Any
) -> None:
    patcher = mocker.patch(name)
    instance = patcher.return_value
    instance.subtokenize = Mock(return_value=output_value)
