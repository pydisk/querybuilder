"""
>>> import querybuilder as qb
>>> qb.settings['pretty_formatter'] = qb.formatting.formatter.StandardFormatter()
>>> from uuid import UUID, uuid4 as uuid_gen
>>> from querybuilder.helpers import table
>>> @table
... class person:
...     id: UUID
...     name: str
...     birth_year: int
>>> q = person.select([person.c.birth_year, person.c.name]) # person.c is a shorthand for person.columns
>>> str(q)
'SELECT person.birth_year, person.name FROM person'
>>> q2 = q.add_where(q.c.birth_year.gt(1800)).set_limit(10).set_columns(["name"])
>>> str(q2)
'SELECT person.name FROM person WHERE person.birth_year > 1800 FETCH FIRST 10 ROWS ONLY'
>>> from querybuilder.drivers.sqlite import Connector
>>> db = Connector(":memory:") # The ":memory:" here is SQLite-specific for in-memory non-persistent database.
>>> str(person.create())
'CREATE TABLE person (id UUID, name TEXT, birth_year INTEGER)'
>>> _ = db.execute(person.create())
>>> q_insert = person.insert_values(in_columns=(person.c[["name", "birth_year"]]), values=[("Dijkstra", 1930), ("Euler", 1707), ("Steiner", 1796)])
>>> q_insert.pretty_print()
INSERT INTO person (name, birth_year) VALUES (?, 1930), (?, 1707), (?, 1796)
-- ↖{0: 'Dijkstra', 1: 'Euler', 2: 'Steiner'}
>>> _ = db.execute(q_insert)
>>> cursor = db.execute(person.select(person.c[1:], orderby=person.c.name))
>>> list(cursor.fetchall())
[('Dijkstra', 1930), ('Euler', 1707), ('Steiner', 1796)]
>>> q_inject = person.insert_values(in_columns=person.c[1:], values= [("\\";DROP TABLE person;", 0000)])
>>> _ = db.execute(q_inject)
>>> cursor = db.execute(person.select(person.c[1:], where=person.c.birth_year.eq(0)))
>>> list(cursor.fetchall())
[('";DROP TABLE person;', 0)]
>>> q_upd = person.update(dict(id=uuid_gen()), where=person.c.name.eq("Dijkstra"))
>>> q_upd.pretty_print() # doctest: +ELLIPSIS
UPDATE person SET id = ? WHERE person.name = ?
-- ↖{0: UUID('...'), 1: 'Dijkstra'}
>>> _ = db.execute(q_upd)
>>> q_del = person.delete(where=person.c.id.isnull())
>>> str(q_del)
'DELETE FROM person WHERE person.id IS NULL'
>>> _ = db.execute(q_del)
>>> from querybuilder.helpers import table, colspec
>>> @table
... class tag:
...     id: colspec(int, primary_key=True, generated_as_identity=True)
...     name: str
>>> str(tag.create())
'CREATE TABLE tag (id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, name TEXT)'
>>> db.stringify(tag.create())
'CREATE TABLE tag (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)'
>>> _ = db.execute(tag.create())
>>> q_tag = tag.insert_values(in_columns=(tag.c.name,), values=[("mathematician",)])
>>> q_tag.pretty_print()
INSERT INTO tag (name) VALUES (?) -- ↖{0: 'mathematician'}
>>> _ = db.execute(q_tag)
>>> q_tag_bulk = tag.insert_many(in_columns=(tag.c.name,))
>>> q_tag_bulk.pretty_print()
INSERT INTO tag (name) VALUES (:name) -- ↖{name: ø}
>>> _ = db.executemany(q_tag_bulk, [dict(name=f"value_{i}") for i in range(1000)])
>>> q_delete = tag.delete(where=tag.c.name.like("value_%"))
>>> _ = db.execute(q_delete)
>>> @table
... class tag_person:
...     person_id: person.c.id
...     tag_id: tag.c.id
>>> str(tag_person.create())
'CREATE TABLE tag_person (person_id UUID REFERENCES person(id), tag_id INTEGER REFERENCES tag(id))'
>>> db.stringify(tag_person.create())
'CREATE TABLE tag_person (person_id TEXT REFERENCES person(id), tag_id INTEGER REFERENCES tag(id))'
>>> _ = db.execute(tag_person.create())
>>> condition = person.c.name.eq("Dijkstra") & tag.c.name.eq("mathematician")
>>> str(condition)
'person.name = ? AND tag.name = ?'
>>> q_prod = person.product(tag).select(columns=(person.c.id, tag.c.id), where=condition)
>>> q_prod.pretty_print()
SELECT person.id, tag.id FROM person, tag WHERE person.name = ? AND tag.name = ?
-- ↖{0: 'Dijkstra', 1: 'mathematician'}
>>> _ = db.execute(tag_person.insert(query=q_prod))
>>> q_math = tag_person.inner_join(tag, on=tag.c.id.eq(tag_person.c.tag_id)).inner_join(person, on=person.c.id.eq(tag_person.c.person_id)).select([person.c.name], where=tag.c.name.eq("mathematician"))
>>> str(q_math)
'SELECT person.name FROM (tag_person INNER JOIN tag ON tag.id = tag_person.tag_id) INNER JOIN person ON person.id = tag_person.person_id WHERE tag.name = ?'
>>> cursor = db.execute(q_math)
>>> list(cursor.fetchall())
[('Dijkstra',)]
>>> from querybuilder.drivers.postgres import Connector
>>> db = Connector()
>>> @table
... class person:
...     id : colspec(UUID, primary_key=True)
...     name : str
...     birth_year : int
>>> @table
... class tag:
...     id: colspec(int, primary_key=True, generated_as_identity=True)
...     name: str
>>> @table
... class tag_person:
...     tag_id: tag.c.id
...     person_id: person.c.id
>>> _ = db.execute(person.create(temporary=True))
>>> _ = db.execute(tag.create(temporary=True))
>>> _ = db.execute(tag_person.create(temporary=True))
>>> str(person.create(temporary=True))
'CREATE TEMPORARY TABLE person (id UUID PRIMARY KEY, name TEXT, birth_year INTEGER)'
"""
